<!-- Our Footer -->
	<br><hr>
	<section class="footer_one " style="background-color: #fff; margin: 2em; padding: 0;">
		<div class="container">
			<div class="row">
				<div class="col-sm-5 col-md-3 col-md-2 col-lg-2">
					<div class="footer_contact_widget home3">
						<h4>HUBUNGI KAMI</h4>
						<p>Widoro 40/12, Sragen Wetan </p>
						<p>Sragen - Jawa Tengah.</p>
						<p>(62) 123 456 789</p>
						<p>info@smartlbm.com</p>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-md-3 col-lg-2">
					<div class="footer_company_widget home3">
						<h4>FITUR</h4>
						<ul class="list-unstyled">
							<li><a href="#">Mitra / Sekolah</a></li>
							<li><a href="#">Pengajar</a></li>
							<li><a href="#">Siswa</a></li>
							<li><a href="page-contact.html">Orang Tua</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-7 col-md-5 col-md-4 col-lg-3">
					<div class="footer_program_widget home3">
						<h4>PROGRAM BELAJAR</h4>
						<ul class="list-unstyled">
							<li><a href="#">Taman Kanak-kanak</a></li>
							<li><a href="#">Sekolah Dasar</a></li>
							<li><a href="#">Sekolah Menengah Pertama</a></li>
							<li><a href="#">Sekolah Menengah Atas</a></li>
							<li><a href="#">Pondok Pesantren</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-md-3 col-lg-2">
					<div class="footer_support_widget home3">
						<h4>SUPPORT</h4>
						<ul class="list-unstyled">
							<li><a href="#">Blog</a></li>
							<li><a href="#">Kegiatan</a></li>
							<li><a href="#">Testimoni</a></li>
							<li><a href="#">Dokumentasi</a></li>
							<li><a href="#">Forum</a></li>
							
						</ul>
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-md-3 col-lg-3">
					<div class="footer_apps_widget home3">
						<h4>APLIKASI </h4>
						<div class="app_grid">
							<button class="apple_btn btn-dark">
								<span class="icon">
									<span class="flaticon-apple"></span>
								</span>
								<span class="title">App Store</span>
								<span class="subtitle">Available now on the</span>
							</button>
							<button class="play_store_btn btn-dark">
								<span class="icon">
									<span class="flaticon-google-play"></span>
								</span>
								<span class="title">Google Play</span>
								<span class="subtitle">Get in on</span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	