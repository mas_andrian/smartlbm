<!-- Home Banner -->
	<section class="home-three home3-overlay home3_bgi6" style="background-image: url(<?= base_url(); ?>e-lbm/assets/frontend/images/background/banner-depan.jpg);">
		<div class="container">
			<div class="row posr">
				<div class="col-lg-12">
					<div class="home-text text-center">
						<h2 class="fz50">Selamat Datang di SMARTLBM</h2>
						<p class="color-white">Platform Pembelajaran Virtual Berkarakter Islami dan Sistem Informasi Manajemen Sekolah</p>
						<a class="btn home_btn" href="#startingdo">Mulai Belajar ?</a>
					</div>
				</div>
			</div>
			<div class="row_style">
				<svg class="waves" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 300" preserveAspectRatio="none"> <path d="M 1000 280 l 2 -253 c -155 -36 -310 135 -415 164 c -102.64 28.35 -149 -32 -235 -31 c -80 1 -142 53 -229 80 c -65.54 20.34 -101 15 -126 11.61 v 54.39 z"></path><path d="M 1000 261 l 2 -222 c -157 -43 -312 144 -405 178 c -101.11 33.38 -159 -47 -242 -46 c -80 1 -153.09 54.07 -229 87 c -65.21 25.59 -104.07 16.72 -126 16.61 v 22.39 z"></path><path d="M 1000 296 l 1 -230.29 c -217 -12.71 -300.47 129.15 -404 156.29 c -103 27 -174 -30 -257 -29 c -80 1 -130.09 37.07 -214 70 c -61.23 24 -108 15.61 -126 10.61 v 22.39 z"></path></svg>
			</div>
		</div>
	</section>

	<style type="text/css">
		.home3_wave:before {
		  background-image: url(<?= base_url(); ?>e-lbm/assets/frontend/images/home/wave2.png); 
		}
		.home3_wave:after {
		  background-image: url(<?= base_url(); ?>e-lbm/assets/frontend/images/home/wave3.png); 
		}
	</style>

	<!--  apa itu smartlbm -->
	<section class="home3_about home3_wave">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-xl-6">
					<div class="about_home3">
						<h3>Apa itu SMARTLBM ?</h3>
						
						<p>Sistem ini dikembangkan oleh Yayasan Lembaga Bakti Muslim Al Falah Sragen, bertujuan untuk membangun media pembelajaran online yang berstandar Sekolah Islam Terpadu dan mengintegrasikan artara sekolah, siswa, orang tua serta mitra pendidikan lainnya yang tergabung dalam platform SMARTLBM.</p>
						<br>
						<h5>Platform ini telah dipakai oleh <?= count($mitra_sekolah); ?> mitra yang terdiri dari (TK, SD, SMP, SMA, PonPes)</h5>
						<a href="#" class="btn about_btn_home3">Jadi Mitra Kami</a>
						<ul class="partners_thumb_list">
							<li class="list-inline-item"><a href="#"><img style="width: 60px; height: 70px;" class="img-fluid" src="<?= base_url(); ?>e-lbm/assets/frontend/images/tk.png" alt="tk.png"></a></li>
							<li class="list-inline-item"><a href="#"><img style="width: 60px; height: 70px;" class="img-fluid" src="<?= base_url(); ?>e-lbm/assets/frontend/images/sd.png" alt="sd.png"></a></li>
							<li class="list-inline-item"><a href="#"><img style="width: 60px; height: 70px;" class="img-fluid" src="<?= base_url(); ?>e-lbm/assets/frontend/images/smp.png" alt="smp.png"></a></li>
							<li class="list-inline-item"><a href="#"><img style="width: 60px; height: 70px;" class="img-fluid" src="<?= base_url(); ?>e-lbm/assets/frontend/images/sma.png" alt="sma.png"></a></li>
							
						</ul>
					</div>
				</div>
				<div class="col-lg-6 col-xl-6">
					<div class="row">
						<div class="col-sm-6 col-lg-6">
							<div class="home3_about_icon_box one">
								<span class="icon"><span class="flaticon-account"></span></span>
								<div class="details">
									<h4>Siswa</h4>
									<p>Siswa dapat melakukan akses materi pembelajaran secara digital dan mudah.</p>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-lg-6">
							<div class="home3_about_icon_box two">
								<span class="icon"><span class="flaticon-online"></span></span>
								<div class="details">
									<h4>Orang Tua / Wali Siswa</h4>
									<p>Orang tua dapat memantau proses pembelajaran siswa secara komprehensif.</p>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-lg-6">
							<div class="home3_about_icon_box three">
								<span class="icon"><span class="flaticon-student-1"></span></span>
								<div class="details">
									<h4>Mitra / Sekolah</h4>
									<p>Mitra / Sekolah dapat melakukan manjemen terkait proses pembelajaran yang akan ditawarkan.</p>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-lg-6">
							<div class="home3_about_icon_box four">
								<span class="icon"><span class="flaticon-book"></span></span>
								<div class="details">
									<h4>Materi Pembelajaran</h4>
									<p>Materi pembelajaran yang bervariatif sesuai dengan ketentuan kurikulum yang berlaku.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="about_home3_shape_container">
						<div class="about_home3_shape"><img src="<?= base_url(); ?>e-lbm/assets/frontend/images/about/shape1.png" alt="shape1.png"></div>
					</div>
				</div>
			</div>
		</div>

	</section>

<hr>

	<!-- Top materi unggulan -->
	<section class="home3_top_course pb0 pt0" id="startingdo">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="main-title text-center">
						<h3 class="mt0">Materi Unggulan</h3>
						<p>Menampilkan materi pembelajaran yang menjadi fokus utama kami.</p>
					</div>
				</div>
			</div>
			<div class="row">
			 	<div class="col-lg-12">
			 		<div id="options" class="alpha-pag full">
						<div class="option-isotop">
							<ul id="filter" class="option-set" data-option-key="filter">
								<li class="list-inline-item"><a href="#smw" data-option-value="*" class="selected">Semua Materi</a></li>
								<li class="list-inline-item"><a href="#mw" data-option-value=".muwaj">Muatan Wajib</a></li>
								<li class="list-inline-item"><a href="#ml" data-option-value=".mulok">Muatan Lokal</a></li>
								<li class="list-inline-item"><a href="#mk" data-option-value=".mukhu">Muatan Khusus</a></li>
								<li class="list-inline-item"><a href="#pd" data-option-value=".pedi">Pengembangan Diri</a></li>
								
							</ul>
						</div>
					</div><!-- FILTER BUTTONS -->
			 		<div class="emply-text-sec">
			 			<div class="row" id="masonry_abc">

			 				<?php
			 					foreach ($materi as $dm) {
			 					$rating = 0;
			 					if($dm['avg_rating'] != null){
			 						$rating = $dm['avg_rating'];
			 					} 
			 					$star_on = (int) $rating;
			 					$star_off = 5-$star_on;

			 					$xyz = $dm['kategori'];
			 					$kategori = "";
			 					if($xyz == "Muatan Wajib"){
			 						$kategori = "muwaj";
			 					} elseif ($xyz == "Muatan Lokal"){
			 						$kategori = "mulok";
			 					} elseif ($xyz == "Muatan Khusus"){
			 						$kategori = "mukhu";
			 					} else {
			 						$kategori = "pedi";
			 					}
			 				?>

			 				<div class="col-md-6 col-lg-4 col-xl-3 <?= $kategori; ?>">
								<div class="top_courses">
									<div class="thumb">
										<img class="img-whp" src="<?= base_url(); ?>e-lbm/<?= $dm['img_thumbnail'] ?>" alt="banner.jpg">
										<div class="overlay">
											<div class="tag">Jenjang</div>
											<div class="icon"><span class="flaticon-like"></span></div>
											<a class="tc_preview_course" href="#"><?= $dm['jenjang'] ?></a>
										</div>
									</div>
									<div class="details">
										<div class="tc_content">
											<p><small style="font-size: 10px;"><?= $dm['nama_mapel'];?></small></p>
											<h5><?= $dm['judul'];?></h5>
											<ul class="tc_review">
											<?php for($i = 1; $i <= $star_on; $i++){ ?>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											<?php } for($j = 1 ; $j <= $star_off; $j++){ ?>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star" style="color: silver;"></i></a></li>
											<?php } ?>
												<li class="list-inline-item"><a href="#"><strong>(<?= $rating; ?>)</strong></a></li>
												<small style="font-size: 10px;"><?=$xyz;?></small>
											</ul>
										</div>
										<div class="tc_footer">
											<ul class="tc_meta float-left">
												<li class="list-inline-item"><a href="#">Kelas:</a></li>
												<li class="list-inline-item"><a href="#"> <?= $dm['tingkatan']; ?></a></li>
											</ul>
										</div>
									</div>
								</div>
			 				</div>

			 			<?php } ?>

			 			</div>
			 		</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="about_home3_shape_container">
						<div class="about_home3_shape2"><img src="<?= base_url(); ?>e-lbm/assets/frontend/images/about/shape2.png" alt="shape2.png"></div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<!-- Jenjang dan Program Pendidikan -->
	<section class="school-category-courses pt30">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="main-title text-center">
						<h3 class="mt0">Jenjang Pendidikan dan Program Pembelajaran</h3>
						<p>Materi pembelajaran sesuai tingkatan setara (TK, SD, SMP, SMA) serta program terkait lainya seperti Pondok Pesantren, Webinar, Pelatihan dan Ujian Online.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-lg-3">
					<div class="img_hvr_box home3" style="background-image: url(<?= base_url(); ?>e-lbm/assets/frontend/images/courses/1.jpg);">
						<div class="overlay">
							<div class="details">
								<a href="<?= base_url(); ?>page/pendidikan/tk"><h5>Taman Kanak-kanak</h5>
								<p><?= $tk; ?> Pelajaran</p></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="img_hvr_box home3" style="background-image: url(<?= base_url(); ?>e-lbm/assets/frontend/images/courses/2.jpg);">
						<div class="overlay">
							<div class="details">
								<a href="<?= base_url(); ?>page/pendidikan/sd"><h5>Sekolah Dasar</h5>
								<p><?= $sd; ?> Pelajaran</p></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="img_hvr_box home3" style="background-image: url(<?= base_url(); ?>e-lbm/assets/frontend/images/courses/3.jpg);">
						<div class="overlay">
							<div class="details">
								<a href="<?= base_url(); ?>page/pendidikan/smp"><h5>Sekolah Menengah Pertama</h5>
								<p><?= $smp; ?> Pelajaran</p></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="img_hvr_box home3" style="background-image: url(<?= base_url(); ?>e-lbm/assets/frontend/images/courses/4.jpg);">
						<div class="overlay">
							<div class="details">
								<h5>Sekolah Menengah Atas</h5>
								<p><?= $sma; ?> Pelajaran</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="img_hvr_box home3" style="background-image: url(<?= base_url(); ?>e-lbm/assets/frontend/images/courses/5.jpg);">
						<div class="overlay">
							<div class="details">
								<h5>Pondok Pesantren</h5>
								<p>Cooming Soon</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="img_hvr_box home3" style="background-image: url(<?= base_url(); ?>e-lbm/assets/frontend/images/courses/6.jpg);">
						<div class="overlay">
							<div class="details">
								<h5>Webinar</h5>
								<p>Cooming Soon</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="img_hvr_box home3" style="background-image: url(<?= base_url(); ?>e-lbm/assets/frontend/images/courses/7.jpg);">
						<div class="overlay">
							<div class="details">
								<h5>Pelatihan</h5>
								<p>Cooming Soon</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="img_hvr_box home3" style="background-image: url(<?= base_url(); ?>e-lbm/assets/frontend/images/courses/8.jpg);">
						<div class="overlay">
							<div class="details">
								<h5>Ujian Online</h5>
								<p>Cooming Soon</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 offset-lg-3">
					<div class="courses_all_btn home3 text-center">
						<a class="btn btn-transparent" href="<?= base_url(); ?>page/muatan/wajib">Lihat Semua Pembelajaran</a>
					</div>
				</div>
			</div>
		</div>
	</section>


	<!-- Mitra / Sekolah yang Tergabung -->
	<section class="popular-courses pb0 pt0">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="main-title text-center">
						<h3 class="mt0">Mitra Sekolah yang Tergabung</h3>
						<p>Berikut mitra instansi pendidikan yang terganbung dalam program SMARTLBM.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="popular_course_slider_home3">

					<?php foreach ($mitra_sekolah as $ms) { ?>
						<div class="item">
							<div class="top_courses mb0">
								<div class="thumb">
									<img class="img-whp" src="<?= base_url(); ?>e-lbm/<?= $ms['logo_sekolah']; ?>" alt="<?= $ms['nama_sekolah']; ?>">
									<div class="overlay">
										<a class="tc_preview_course" href="#"><?= $ms['nama_sekolah']; ?></a>
									</div>
								</div>
								<div class="details">
									<div class="tc_content">
										<p><?= $ms['jenjang']; ?></p>
										<h5><?= $ms['nama_sekolah']; ?></h5>
										
									</div>
									<div class="tc_footer">
										<ul class="tc_meta float-left">
											<li class="list-inline-item"><a href="#"> &nbsp;<i class="flaticon-profile"></i></a></li>
											<li class="list-inline-item"><a href="#"><?= $ms['jml_guru']; ?> pengajar</a></li>
										</ul>
										<ul class="tc_meta float-right" >
											<li class="list-inline-item"><a href="#"><i class="flaticon-book"></i></a></li>
											<li class="list-inline-item"><a href="#"><?= $ms['jml_materi']; ?> materi</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>

					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Kegiatan dan last blog -->
	<!-- Our Blog -->
	<section class="our-blog">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="main-title text-center">
						<h3 class="mt0">Berita terbaru & Kegiatan terdekat</h3>
						<p>Informasi kegiatan dan berita dari smartlbm dan mitra.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-xl-6">
					<div class="blog_slider_home1">

				<?php foreach ($kegiatan as $k) { 
					$date = $k['tanggal'];
					$dateObj = DateTime::createFromFormat('Y-m-d', $date);
				?>
						<div class="item">
							<div class="blog_post one">
								<div class="thumb">
									<div class="post_title"><?= $k['judul']; ?></div>
									<img class="img-fluid w100" src="<?= base_url(); ?>e-lbm/<?= $k['gambar']; ?>" alt="gambar">
									<a class="post_date" href="#"><span><?= $dateObj->format('d'); ?><br> <?= $dateObj->format('M'); ?> </span></a>
								</div>
								<div class="details">
									<div class="post_meta">
										<ul>
											<li class="list-inline-item"><a href="#"><i class="flaticon-calendar"></i> <?= $k['jam']; ?></a></li>
											<li class="list-inline-item"><a href="#"><i class="flaticon-placeholder"></i> <?= $k['lokasi']; ?></a></li>
										</ul>
									</div>
									<a href="<?= base_url(); ?>kegiatan/detail/<?= $k['slug']; ?>">
									<h4><?= $k['judul']; ?></h4>
									</a>
								</div>
							</div>
						</div>		
				<?php } ?>

					</div>
				</div>

				<?php foreach ($blognew as $bn) { 
					$date = $bn['time_at'];
					$dateObj1 = DateTime::createFromFormat('Y-m-d H:i:s', $date);
				?>
				<div class="col-md-6 col-lg-3 col-xl-3">
					<div class="blog_post">
						<div class="thumb">
							<img class="img-fluid w100" src="<?= base_url(); ?>e-lbm<?= $bn['banner']; ?>" alt="banner">
							<a class="post_date" href="#"><?= $dateObj->format('d M Y'); ?></a>
						</div>
						<div class="details">
							<h5><?= $bn['kategori']; ?></h5>
							<a href="<?= base_url(); ?>blog/detail/<?= $bn['flag']; ?>"><h4><?= $bn['judul']; ?></h4></a>
						</div>
					</div>
				</div>
				<?php } ?>
				

			</div>
			<div class="row mt50">
				<div class="col-lg-12">
					<div class="read_more_home text-center"> 
						<h4>Suka dengan apa yang kamu lihat? <a href="<?= base_url(); ?>blog">Lihat postingan lainnya <span class="flaticon-right-arrow pl10"></span></a></h4>
					</div>
				</div>
			</div>
		</div>
	</section>



	<!-- Testimoni Pengguna -->
	<section class="home3_about2 pb10 pt30">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="main-title text-center">
						<h3 class="mt0">Testimoni Pengguna</h3>
						<p>Penilaian pengguna dan mitra tentang program smartlbm</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="instructor_slider_home3">

						<?php foreach ($testimoni as $t) { ?>
						<div class="item">
							<div class="instructor_col">
								<div class="thumb">
									<img class="img-fluid img-rounded-circle" src="<?= base_url(); ?>e-lbm<?= $t['gambar']; ?>" alt="image">
								</div>
								<div class="details">
									<ul>
										<?php for($i=1;$i<= $t['rating'];$i++){ ?>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
										<?php } ?>
										<li class="list-inline-item"><a href="#">(<?= $t['rating']; ?>)</a></li>
									</ul>
									<h4><?= $t['nama']; ?></h4>
									<p><?= $t['kategori']; ?></p>
								</div>
							</div>
						</div>
						<?php } ?>
						
					</div>
				</div>
			</div>
			<div class="row mt60">
				<div class="col-sm-6 col-lg-6 col-xl-6">
					<div class="becomea_instructor_home3 style1">
						<div class="bi_grid">
							<h3>Daftar sebagai Siswa ?</h3>
							<p>Dapatkan akses kemudahan belajar dengan materi terbaik dan dipandu <br class="dn-lg"> oleh tenaga pengajar yang profesional dibidangnya.</p>
							<a class="btn btn-white" target="_blank" href="https://api.whatsapp.com/send?phone=6285655305487&text=%20Selamat%20Datang%20di%20Unit%20Pelayanan%20SmartLBM%20-%20Ada%20yang%20bisa%20Kami%20Bantu">Daftar Sekarang <span class="flaticon-right-arrow-1"></span></a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-6 col-xl-6">
					<div class="becomea_instructor_home3 style2">
						<div class="bi_grid">
							<h3>Daftar Sebagai Mitra ?</h3>
							<p>Mitra sekolah dapat melakukan manajemen pembelajaran sesuai desain <br class="dn-lg"> yang telah dirancang disesuaikan dengan kurikulum yang sedang berjalan.</p>
							<a class="btn btn-white" target="_blank" href="https://api.whatsapp.com/send?phone=6285655305487&text=%20Selamat%20Datang%20di%20Unit%20Pelayanan%20SmartLBM%20-%20Ada%20yang%20bisa%20Kami%20Bantu">Daftar Sekarang <span class="flaticon-right-arrow-1"></span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>