<!-- Our Footer Middle Area -->
	<section class="footer_middle_area home3 p0">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-md-3 col-lg-3 col-xl-2 pb15 pt15">
					<div class="logo-widget home3">
						<img class="img-fluid" src="<?= base_url(); ?>e-lbm/assets/frontend/images/lg-botton.png" alt="smartlbm.png">
						<span></span>
					</div>
				</div>
				<div class="col-sm-8 col-md-5 col-lg-6 col-xl-6 pb25 pt25 brdr_left_right home3">
					<div class="footer_menu_widget home3">
						<ul>
							<li class="list-inline-item"><a href="<?= base_url(); ?>">Beranda</a></li>
							<li class="list-inline-item"><a href="<?= base_url(); ?>page/tentang_kami">Tentang Kami</a></li>
							
							<li class="list-inline-item"><a href="<?= base_url(); ?>page/faq">FAQ</a></li>
							<li class="list-inline-item"><a href="<?= base_url(); ?>page/hubungi_kami">Hubungi Kami</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-3 col-xl-4 pb15 pt15">
					<div class="footer_social_widget mt15">
						<ul>
							<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li class="list-inline-item"><a href="#"><i class="fa fa-google"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

<!-- Our Footer Bottom Area -->
	<section class="footer_bottom_area home3 pt30 pb30">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="copyright-widget text-center">
						<p>Copyright LBM Creative - Sragen © <?= date('Y') ?> All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
<a class="scrollToHome home3" href="#"><i class="flaticon-up-arrow-1"></i></a>
</div>

<style type="text/css">
.full-screen {
  position: fixed;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  background: rgba(0,0,0,1);
}

.flex-container-center {
  displaY: flex;
  flex-direction: column;
  justify-content: center;
  align-items:center;
}

.full-screen{
  color: white
}

.hidden {
    display: none;
}
</style>
 
<?php 
	
     if($this->session->userdata('feedback') != NULL){  ?>


<div class=" full-screen flex-container-center" id="alert">

      <?php if($this->session->userdata('role') != 'Siswa'){ $timer= 2000;?>
        <span class="icon"><span style="font-size: 80px;" class="flaticon-account"></span></span><br>
        <p style="margin-bottom: 0.5em; font-weight: bold;">[<?= $this->session->userdata('role'); ?>]</p>
       	<p style="margin-bottom: 1em;"><?= $this->session->userdata('feedback'); ?></p>
      <?php } else { $timer = 8000; ?>

        <img width="80%" src="https://1.bp.blogspot.com/-FB5OAr46qN0/Wb1EmM1D6CI/AAAAAAAAC9Y/hGxh2aU2UCk58WIYkayXVNXmTj093H5RwCLcBGAs/s1600/Doa%2Bsebelum%2Bbelajar.png" class="img-reponsive"> 
    <?php } ?>


</div>

    <script type="text/javascript">
        $(document).ready (function(){
          window.setTimeout(function() {
              $("#alert").fadeTo(500, 0).slideUp(500, function(){
                  $(this).remove(); 
              });
          }, <?=$timer;?>);
        });
    </script>
<?php 
    $this->session->set_userdata('feedback', null);
} ?>


<!-- Wrapper End -->
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/jquery-migrate-3.0.0.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/popper.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/jquery.mmenu.all.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/ace-responsive-menu.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/isotop.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/snackbar.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/simplebar.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/parallax.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/scrollto.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/jquery-scrolltofixed-min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/jquery.counterup.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/wow.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/progressbar.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/slider.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/timepicker.js"></script>
<!-- Custom script for all pages --> 
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/script.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/jquery-star-rating-master/src/rating.js"></script>
</body>
</html>

<script>
	$(function(){
		$('#star-rating').rating();
	});
	
	function lihat_nilai_siswa(id_siswa, id_bab){
		$.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('nilai/get_nilai_siswa/') ?>"+id_siswa+'/'+id_bab,
            "method": "GET",
        }).done(function (response) {
			var response = JSON.parse(response);
			var latihan = response.latihan;
			var html_latihan = "";
			if(latihan.length > 0){
				latihan.forEach(function(item, index){
					html_latihan += '<tr><td>Latihan ke-'+(index + 1)+'</td><td>'+item.nilai+'</td></tr>';
				});
			} else {
				html_latihan += '<tr><td colspan="2"2>Data Tidak Ada</td></tr>';
			}
			$("#list_latihan").html(html_latihan);

			var ujian = response.ujian;
			var html_ujian = "";
			if(ujian.length > 0){
				ujian.forEach(function(item, index){
					html_ujian += '<tr><td>Ujian</td><td>'+item.nilai+'</td></tr>';
				});
			} else {
				html_ujian += '<tr><td colspan="2"2>Data Tidak Ada</td></tr>';
			}
			

			$("#list_ujian").html(html_ujian);
			$("#modalNilai").modal("show");
		});
		
	}
</script>