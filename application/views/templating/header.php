<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta charset="utf-8"> 
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="Web official (Smartlbm) Learning Management System - YLBM Sragen">
<meta name="CreativeLayers" content="LC-Pro">

<!-- css file -->
<link rel="stylesheet" href="<?= base_url(); ?>e-lbm/assets/frontend/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>e-lbm/assets/frontend/css/style.css">

<!-- Responsive stylesheet -->
<link rel="stylesheet" href="<?= base_url(); ?>e-lbm/assets/frontend/css/responsive.css">
<link rel="stylesheet" href="<?= base_url(); ?>e-lbm/assets/jquery-star-rating-master/src/rating.css">

<!-- Title -->
<title><?= $title; ?> | SMARTLBM - Learning Management System</title>

<!-- Favicon -->
<link href="<?= base_url(); ?>e-lbm/assets/frontend/images/favicon-new.png" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="<?= base_url(); ?>e-lbm/assets/frontend/images/favicon-new.png" sizes="128x128" rel="shortcut icon" />
</head>
<body>
<div class="wrapper">
	<div class="preloader"></div>

	<!-- Main Header Nav -->
	<header class="header-nav menu_style_home_one navbar-scrolltofixed stricky main-menu">
		<div class="container-fluid">
		    <!-- Ace Responsive Menu -->
		    <nav>
		        <!-- Menu Toggle btn-->
		        <div class="menu-toggle">
		            <img class="nav_logo_img img-fluid" src="<?= base_url(); ?>e-lbm/assets/frontend/images/header-logo.png" alt="header-logo.png">
		            <button type="button" id="menu-btn">
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		            </button>
		        </div>
		        <a href="<?= base_url(); ?>" class="navbar_brand float-left dn-smd">
		            <img class="logo1 img-fluid" src="<?= base_url(); ?>e-lbm/assets/frontend/images/header-logo.png" alt="header-logo.png">
		            <img class="logo2 img-fluid" src="<?= base_url(); ?>e-lbm/assets/frontend/images/header-logo2.png" alt="header-logo2.png">
		            <span>SMARTLBM</span>
		        </a>
		        <!-- Responsive Menu Structure-->
		        <!--Note: declare the Menu style in the data-menu-style="horizontal" (options: horizontal, vertical, accordion) -->
		        <ul id="respMenu" class="ace-responsive-menu" data-menu-style="horizontal">
		            <li class="list_one">
		                <a href="<?= base_url(); ?>"><span class="title">Beranda</span></a>
		                <!-- Level Two-->
		            </li>
		            <li class="list_two">
		                <a href="#"><span class="title">Materi</span></a>
		                <!-- Level Two-->
	                	<ul>
                            <li><a href="<?= base_url(); ?>page/muatan/wajib">Muatan Wajib</a></li>
		                    <li><a href="<?= base_url(); ?>page/muatan/lokal">Muatan Lokal</a></li>
		                    <li><a href="<?= base_url(); ?>page/muatan/khusus">Muatan Khusus</a></li>
		                    <li><a href="<?= base_url(); ?>page/muatan/pengembangan">Pengembangan Diri</a></li>
	                	</ul>
		            </li>
		             <li class="list_four">
		                <a href="<?= base_url(); ?>blog"><span class="title">Blog</span></a>
		               
		            </li>
		             <li class="list_five">
		                <a href="<?= base_url(); ?>kegiatan"><span class="title">Kegiatan</span></a>
		                
		            </li>
		        </ul>
		        <ul class="sign_up_btn pull-right dn-smd mt20">
		        	<?php if($this->session->userdata('is_login') != NULL){ 
			        	$user_in = $this->session->userdata('detil_siswa');
			        ?>

     						<li class="list-inline-item list_s" title="Profil Siswa"><a href="<?= base_url(); ?>siswa" class="btn flaticon-user" > <span class="dn-lg"><?= $user_in['nama']; ?></span></a></li>

     						 <li class="list-inline-item list_s" title="Logout">
			                	<div class="cart_btn">
									<ul class="cart">
										<li>
											<a href="<?= base_url(); ?>login/do_logout" class="btn cart_btn flaticon-logout" ></a>
											
										</li>
									</ul>
								</div>
			                </li>
     						
     				<?php } else { ?>
		                	<li class="list-inline-item list_s"><a href="#" class="btn flaticon-user" data-toggle="modal" data-target="#exampleModalCenter"> <span class="dn-lg">Login Siswa</span></a></li>
     				<?php } ?>
	                

	                
	                <li class="list-inline-item list_s" title="Pencarian">
	                	<div class="search_overlay">
						 	<a id="search-button-listener" class="mk-search-trigger mk-fullscreen-trigger" href="#">
						    	<span id="search-button"><i class="flaticon-magnifying-glass"></i></span>
						 	</a>
						</div>
	                </li>

	            </ul><!-- Button trigger modal -->
		    </nav>
		</div>
	</header>


	<!-- Modal login siswa -->
	<div class="sign_up_modal modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
		      	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		      	</div>
	    		
				<div class="tab-content" id="myTabContent">
				  	<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						<div class="login_form">
							<form action="<?= base_url(); ?>login/do_login" method="post">
								<div class="heading">
									<h3 class="text-center">Login Siswa</h3>
									<center><img class="img-fluid" src="<?= base_url(); ?>e-lbm/assets/frontend/images/lg-botton.png" alt="smartlbm.png"></center>
									<p class="text-center">Learning Management System <br> <small>Lembaga Bakti Muslim</small></p>
								</div>
								 <div class="form-group">
							    	<input type="text" class="form-control" id="exampleInputEmail1" name="username" placeholder="Username" required="true">
								</div>
								<div class="form-group">
							    	<input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password" required="true">
								</div>
								<div class="form-group form-check">
									<input type="checkbox" class="form-check-input" name="is_ortu" value="is_ortu" id="exampleCheck1">
									<label class="form-check-label" for="exampleCheck1">Akses Orang Tua</label>
									<a class="tdu text-thm float-right" href="#">Lupa Password?</a>
								</div>
								<button type="submit" class="btn btn-log btn-block btn-thm2">Login</button>
								
							</form>
						</div>
				  	</div>
				  	
				</div>
	    	</div>
	  	</div>
	</div>


	<!-- Modal Search Button Bacground Overlay -->
    <div class="search_overlay dn-992">
		<div class="mk-fullscreen-search-overlay" id="mk-search-overlay">
		    <a href="#" class="mk-fullscreen-close" id="mk-fullscreen-close-button"><i class="fa fa-times"></i></a>
		    <div id="mk-fullscreen-search-wrapper">
		      <form method="get" id="mk-fullscreen-searchform" action="<?= base_url('page/search') ?>">
		        <input type="text" name="q" value="" placeholder="Cari materi pelajaran sesuai kebutuhan kamu ..." id="mk-fullscreen-search-input">
		        <i class="flaticon-magnifying-glass fullscreen-search-icon"><input value="" type="submit"></i>
		      </form>
		    </div>
		</div>
	</div>

	<!-- Main Header Nav For Mobile -->
	<div id="page" class="stylehome1 h0">
		<div class="mobile-menu">
			<div class="header stylehome1">
				<div class="main_logo_home2">
		            <img class="nav_logo_img img-fluid float-left mt20" src="<?= base_url(); ?>e-lbm/assets/frontend/images/header-logo.png" alt="header-logo.png">
		            <span>SMARTLBM</span>
				</div>
				<ul class="menu_bar_home2">
					<li class="list-inline-item">
	                	<div class="search_overlay">
						  	<a id="search-button-listener2" class="mk-search-trigger mk-fullscreen-trigger" href="#">
						   		<div id="search-button2"><i class="flaticon-magnifying-glass"></i></div>
						  	</a>
							<div class="mk-fullscreen-search-overlay" id="mk-search-overlay2">
							    <a href="#" class="mk-fullscreen-close" id="mk-fullscreen-close-button2"><i class="fa fa-times"></i></a>
							    <div id="mk-fullscreen-search-wrapper2">
							      	<form method="get" id="mk-fullscreen-searchform2">
							        	<input type="text" value="" placeholder="Cari materi pelajaran sesuai kebutuhan kamu ..." id="mk-fullscreen-search-input2">
							        	<i class="flaticon-magnifying-glass fullscreen-search-icon"><input value="" type="submit"></i>
							      	</form>
							    </div>
							</div>
						</div>
					</li>
					<li class="list-inline-item"><a href="#menu"><span></span></a></li>
				</ul>
			</div>
		</div><!-- /.mobile-menu -->
		<nav id="menu" class="stylehome1">
			<ul>
				<li><a href="<?= base_url(); ?>">Beranda</a></li>
				<li><span>Materi</span>
					<ul>
						<li><a href="<?= base_url(); ?>page/muatan/wajib">Muatan Wajib</a></li>
		                <li><a href="<?= base_url(); ?>page/muatan/lokal">Muatan Lokal</a></li>
		                <li><a href="<?= base_url(); ?>page/muatan/khusus">Muatan Khusus</a></li>
		                <li><a href="<?= base_url(); ?>page/muatan/pengembangan">Pengembangan Diri</a></li>
					</ul>
				</li>
				<li><a href="<?= base_url(); ?>blog">Blog</a>
					
				</li>
				<li><a href="<?= base_url(); ?>kegiatan">Kegiatan</a>
					
				<?php if($this->session->userdata('is_login') != NULL){ 
			        	$user_in = $this->session->userdata('detil_siswa');
			        	
			        ?>
     					<li ><a href="<?= base_url(); ?>siswa" class="btn btn-md"><i class="flaticon-user"></i><span style="" class="dn-md"> [<?= $user_in['nis']; ?>] <?= $user_in['nama']; ?></span></a></li>
     					<li ><a href="<?= base_url(); ?>login/do_logout" class="btn btn-md"><i class="flaticon-logout"></i><span style="" class="dn-md"> LOGOUT </span></a></li>
     						
     				<?php } else { ?>

     					</li>
								<li><a href="<?= base_url(); ?>e-lbm">Akses Sekolah</a>
						</li>
						<li><a href="#" data-toggle="modal" data-target="#exampleModalCenter">Login Siswa</a></li>
		                	
     				<?php } ?>
				
			</ul>
		</nav>
	</div>

	


