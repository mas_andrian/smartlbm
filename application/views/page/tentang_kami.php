<!-- Inner Page Breadcrumb -->
	<section class="inner_page_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-3 text-center">
					<div class="breadcrumb_content">
						
							<p class="color-white"><?= $title; ?> </p>
							<h4 class="breadcrumb_title">SMARTLBM</h4>
						   <p class="color-white">Yayasan Lembaga Bakti Muslim</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- About Text Content -->
	<section class="about-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="about_content">
						<h3>SMARTLBM</h3>
						<p class="color-black22 mt20">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis,et quasi architecto beatae vitae dicta sunt explicabo.</p>
						<p class="mt15">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis,et quasi architecto beatae vitae dicta sunt explicabo.</p>
						<p class="mt20">Nemo enim ipsam,voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia,consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.,Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, adipisci velit, sed quia non numquam eius modi tempora</p>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="about_thumb">
						<img class="img-fluid" src="<?= base_url(); ?>e-lbm/assets/frontend/images/about/banner-about-us.jpg" alt="smartlbm.jpg">
					</div>
				</div>
			</div>
			<div class="row mb60">
				<div class="col-lg-12 text-center mt60">
					<h3 class="fz26">Statistik Smartlbm</h3>
				</div>
				<div class="col-lg-12 text-center mt40">
					<ul class="funfact_two_details">
						<li class="list-inline-item">
							<div class="funfact_two text-left">
								<div class="details">
									<h5>MITRA SEKOLAH</h5>
									<div class="timer"><?= $mitra; ?></div>
								</div>
							</div>
						</li>
						<li class="list-inline-item">
							<div class="funfact_two text-left">
								<div class="details">
									<h5>PENGAJAR / GURU</h5>
									<div class="timer"><?= $guru; ?></div>
								</div>
							</div>
						</li>
						<li class="list-inline-item">
							<div class="funfact_two text-left">
								<div class="details">
									<h5>SISWA</h5>
									<div class="timer"><?= $siswa; ?></div>
								</div>
							</div>
						</li>
						<li class="list-inline-item">
							<div class="funfact_two text-left">
								<div class="details">
									<h5>MATERI PEMBELAJARAN</h5>
									<div class="timer"><?= $materi; ?></div>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="about_whoweare">
						<h4>Who We Are</h4>
						<p class="mt25">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis,et quasi architecto beatae vitae dicta sunt explicabo.</p>
						<p class="mt25">Nemo enim ipsam,voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia,consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.,Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, adipisci velit, sed quia non numquam eius modi tempora</p>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="about_whoweare">
						<h4>What We Do</h4>
						<p class="mt25">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis,et quasi architecto beatae vitae dicta sunt explicabo.</p>
						<p class="mt25">Nemo enim ipsam,voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia,consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.,Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, adipisci velit, sed quia non numquam eius modi tempora</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Divider -->
	<section class="divider parallax bg-img2" data-stellar-background-ratio="0.3">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 text-center">
					<div class="divider-one">
						<p class="color-white">MULAI BELAJAR ONLINE</p>
						<h1 class="color-white text-uppercase">Tingkatkan keterampilan Anda dengan Pembelajaran Online terbaik</h1>
						<a class="btn btn-transparent divider-btn" href="#">Mulai Sekarang</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	
	<!-- Our Testimonials -->
	<section id="our-testimonials" class="our-testimonials">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="main-title text-center">
						<h3 class="mt0">Apa Pendapat Mereka</h3>
						<p>Penilaian pengguna dan mitra tentang program smartlbm.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="testimonialsec">
						<ul class="tes-nav">

						<?php foreach ($testimoni as $dt_moni1) { ?>

							<li>
								<img class="img-fluid" style="width: 60px; height: 60px;" src="<?= base_url(); ?>e-lbm<?= $dt_moni1['gambar'] ?>" alt="img"/>
							</li>

						<?php } ?>

							
						</ul>
						<ul class="tes-for">

						<?php foreach ($testimoni as $dt_moni2) { ?>
							<li>
								<div class="testimonial_item">
									<div class="details">
										<h5><?= $dt_moni2['nama'] ?></h5>
										<span class="small text-thm"><?= $dt_moni2['kategori'] ?></span>
										<p><?php echo strip_tags(character_limiter($dt_moni2['kesan'], 200)); ?></p>
									</div>
								</div>
							</li>
						<?php } ?>

						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Our Partners -->
	<section id="our-partners" class="our-partners">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="main-title text-center">
						<h3 class="mt0">Mitra Instansi Pendidikan Kami</h3>
						<p>Platform ini telah dipakai oleh mitra yang terdiri dari (TK, SD, SMP, SMA, PonPes)</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8 offset-lg-2">
					<div class="row">
						<div class="col-sm-6 col-md-4 col-lg">
							<div class="our_partner">
								<img class="img-fluid" style="width: 80px; height: 80px;" src="<?= base_url(); ?>e-lbm/assets/frontend/images/tk.png" alt="tk">
							</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg">
							<div class="our_partner">
								<img class="img-fluid"style="width: 80px; height: 80px;" src="<?= base_url(); ?>e-lbm/assets/frontend/images/sd.png" alt="sd">
							</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg">
							<div class="our_partner">
								<img class="img-fluid" style="width: 80px; height: 80px;" src="<?= base_url(); ?>e-lbm/assets/frontend/images/smp.png" alt="smp">
							</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg">
							<div class="our_partner">
								<img class="img-fluid" style="width: 80px; height: 80px;" src="<?= base_url(); ?>e-lbm/assets/frontend/images/sma.png" alt="sma">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	