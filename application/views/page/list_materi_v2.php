<!-- Inner Page Breadcrumb -->
	<section class="inner_page_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-3 text-center">
					<div class="breadcrumb_content">
						<h4 class="breadcrumb_title"><?= $title; ?></h4>
						<ol class="breadcrumb">
						    <p class="color-white"><?= $subtitle; ?></p>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Top materi unggulan -->
	<section class="home3_top_course pb1 pt1">
		<div class="container">
			
			<div class="row ">
			 	
			 		<?php
			 			if(count($materi) == 0){ 
			 				echo "<center><p style='padding-bottom: 2em;'>Data tidak tersedia.</p></center>";
			 			}
			 		?>

			 		
			 			<div class="row courses_container" >

			 				<?php
			 				if(count($materi) == 0){ ?>
						
			 				<?php
			 				} else {
			 					foreach ($materi as $dm) {
			 					$rating = 0;
			 					if($dm['avg_rating'] != null){
			 						$rating = $dm['avg_rating'];
			 					} 
			 					$star_on = (int) $rating;
			 					$star_off = 5-$star_on;

			 					$xyz = $dm['kategori'];
			 					$kategori = "";
			 					if($xyz == "Muatan Wajib"){
			 						$kategori = "muwaj";
			 					} elseif ($xyz == "Muatan Lokal"){
			 						$kategori = "mulok";
			 					} elseif ($xyz == "Muatan Khusus"){
			 						$kategori = "mukhu";
			 					} else {
			 						$kategori = "pedi";
			 					}
			 				?>

			 				<div class="col-lg-6 p0">
							<div class="courses_list_content">
								<div class="top_courses list">
									<div class="thumb">
										<img class="img-whp" src="<?= base_url(); ?>e-lbm/<?= $dm['img_thumbnail'] ?>" alt="t1.jpg">
										<div class="overlay">
											<div class="icon"><span class="flaticon-like"></span></div>
											<a class="tc_preview_course" href="#"><?= $dm['jenjang'] ?></a>
										</div>
									</div>
									<div class="details">
										<div class="tc_content">
											<p><small style="font-size: 10px;"><?= $dm['nama_mapel'];?></small></p>
											<h5><?= $dm['judul'];?></h5>
											<p style="text-align: justify;"><?php echo strip_tags(character_limiter($dm['deskripsi'], 150)); ?></p>
										</div>
										<div class="tc_footer">
											<ul class="tc_meta float-left fn-414">
												<li class="list-inline-item"><a href="#">Kelas:</a></li>
												<li class="list-inline-item"><a href="#"> <?= $dm['tingkatan']; ?></a></li>
											</ul>
											
											<ul class="tc_review float-right fn-414">
												<?php for($i = 1; $i <= $star_on; $i++){ ?>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											<?php } for($j = 1 ; $j <= $star_off; $j++){ ?>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star" style="color: silver;"></i></a></li>
											<?php } ?>
												<li class="list-inline-item"><a href="#"><strong>(<?= $rating; ?>)</strong></a></li>
												<small style="font-size: 10px;"><?=$xyz;?></small>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>

			 			<?php } } ?>

			 			</div>
			 		
				
			</div>
			
		</div>
	</section>
