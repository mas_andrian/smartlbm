<!-- Inner Page Breadcrumb -->
	<section class="inner_page_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-3 text-center">
					<div class="breadcrumb_content">
						<h4 class="breadcrumb_title"><?= $title; ?></h4>
						<ol class="breadcrumb">
						    <p class="color-white"><?= $subtitle; ?></p>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Top materi unggulan -->
	<section class="home3_top_course pb0 pt1">
		<div class="container">
			
			<div class="row">
			 	<div class="col-lg-12">
			 		<?php
			 			if(count($materi) == 0){ 
			 				echo "<center><p style='padding-bottom: 2em;'>Data tidak tersedia.</p></center>";
			 			}
			 		?>

			 		<div class="emply-text-sec">
			 			<div class="row" id="masonry_abc">

			 				<?php
			 				if(count($materi) == 0){ ?>
						
			 				<?php
			 				} else {
			 					foreach ($materi as $dm) {
			 					$rating = 0;
			 					if($dm['avg_rating'] != null){
			 						$rating = $dm['avg_rating'];
			 					} 
			 					$star_on = (int) $rating;
			 					$star_off = 5-$star_on;

			 					$xyz = $dm['kategori'];
			 					$kategori = "";
			 					if($xyz == "Muatan Wajib"){
			 						$kategori = "muwaj";
			 					} elseif ($xyz == "Muatan Lokal"){
			 						$kategori = "mulok";
			 					} elseif ($xyz == "Muatan Khusus"){
			 						$kategori = "mukhu";
			 					} else {
			 						$kategori = "pedi";
			 					}
			 				?>

			 				<div class="col-md-6 col-lg-4 col-xl-3 <?= $kategori; ?>">
								<div class="top_courses">
									<div class="thumb">
										<img class="img-whp" src="<?= base_url(); ?>e-lbm/<?= $dm['img_thumbnail'] ?>" alt="banner.jpg">
										<div class="overlay">
											<div class="tag">Jenjang</div>
											<div class="icon"><span class="flaticon-like"></span></div>
											<a class="tc_preview_course" href="#"><?= $dm['jenjang'] ?></a>
										</div>
									</div>
									<div class="details">
										<div class="tc_content">
											<p><small style="font-size: 10px;"><?= $dm['nama_mapel'];?></small></p>
											<h5><?= $dm['judul'];?></h5>
											<ul class="tc_review">
											<?php for($i = 1; $i <= $star_on; $i++){ ?>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											<?php } for($j = 1 ; $j <= $star_off; $j++){ ?>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star" style="color: silver;"></i></a></li>
											<?php } ?>
												<li class="list-inline-item"><a href="#"><strong>(<?= $rating; ?>)</strong></a></li>
												<small style="font-size: 10px;"><?=$xyz;?></small>
											</ul>
										</div>
										<div class="tc_footer">
											<ul class="tc_meta float-left">
												<li class="list-inline-item"><a href="#">Kelas:</a></li>
												<li class="list-inline-item"><a href="#"> <?= $dm['tingkatan']; ?></a></li>
											</ul>
										</div>
									</div>
								</div>
			 				</div>

			 			<?php } } ?>

			 			</div>
			 		</div>
				</div>
			</div>
			
		</div>
	</section>
