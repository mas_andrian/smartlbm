<!-- Inner Page Breadcrumb -->
	<section class="inner_page_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-xl-12 offset-xl-12 text-center">
					<div class="breadcrumb_content">
						<h4 class="breadcrumb_title"><?= $title; ?></h4>
						<ol class="breadcrumb">
						   
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Our FAQ -->
	<section class="our-faq">
		<div class="container">
			<div class="row">
				
				<div class="col-lg-12 col-xl-12">
					
					<div class="faq_according">
					  	<div id="accordion" class="panel-group">
					  		<?php $a=0; foreach ($faq as $fa) { ?>
						    <div class="panel">
						      	<div class="panel-heading">
							      	<h4 class="panel-title">
							        	<a href="#panelBody<?= $fa['id_faq'];?>" class="accordion-toggle link fz20 mb15" data-toggle="collapse" data-parent="#accordion"><?= $fa['pertanyaan'];?></a>
							        </h4>
						      	</div>
							    <div id="panelBody<?= $fa['id_faq'];?>" class="panel-collapse collapse <?php if($a == 0){ echo 'show'; } ?>">
							        <div class="panel-body">
								    	
								    	<p class="mb25"><?= $fa['jawaban'];?></p>
							        </div>
							    </div>
						    </div>
						<?php $a++; } ?>

						    
						   
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>