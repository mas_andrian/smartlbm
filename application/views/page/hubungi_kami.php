<!-- Inner Page Breadcrumb -->
	<section class="inner_page_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-3 text-center">
					<div class="breadcrumb_content">
						<h4 class="breadcrumb_title"><?= $title; ?></h4>
						<ol class="breadcrumb">
						    <p class="color-white">Yayasan Lembaga Bakti Muslim</p>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- How It's Work -->
	<section class="our-contact">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-lg-4">
					<div class="contact_localtion text-center">
						<div class="icon"><span class="flaticon-placeholder-1"></span></div>
						<h4>Alamat</h4>
						<p>Widoro 40/12, Sragen Wetan Sragen - Jawa Tengah.</p>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4">
					<div class="contact_localtion text-center">
						<div class="icon"><span class="flaticon-phone-call"></span></div>
						<h4>Telepon</h4>
						<p class="mb0">Mobile: (+096) 468 235 <br> Fax: (+096) 468 235</p>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4">
					<div class="contact_localtion text-center">
						<div class="icon"><span class="flaticon-email"></span></div>
						<h4>Write Some Words</h4>
						<p><a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="e2ab8c848da28786978f9bcc818d8f">info@smartlbm.com</a></p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div >
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15825.717055367695!2d111.0309675!3d-7.4176524!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x33ca6e2ee05748d3!2sSMP%20IT%20Az%20Zahra%20Sragen!5e0!3m2!1sid!2sid!4v1605704412245!5m2!1sid!2sid" width="100%" height="630" frameborder="1" style="border:0;" allowfullscreen="true" aria-hidden="false" tabindex="0"></iframe>
					</div>
				</div>
				<div class="col-lg-6 form_grid">
					<h4 class="mb5">Kirim Pesan</h4>
					<p>Ada yang bisa kami bantu, silakan kirim pesan untuk bertanya.</p>
		            <form class="contact_form" id="contact_form" name="contact_form" action="#" method="post" novalidate="novalidate">
						<div class="row">
			                <div class="col-sm-12">
			                    <div class="form-group">
			                    	<label for="exampleInputName">Nama Lengkap</label>
									<input id="form_name" name="form_name" class="form-control" required="required" type="text">
								</div>
			                </div>
			                <div class="col-sm-12">
			                    <div class="form-group">
			                    	<label for="exampleInputEmail">Email</label>
			                    	<input id="form_email" name="form_email" class="form-control required email" required="required" type="email">
			                    </div>
			                </div>
			                <div class="col-sm-12">
			                    <div class="form-group">
			                    	<label for="exampleInputSubject">Perihal</label>
				                    <input id="form_subject" name="form_subject" class="form-control required" required="required" type="text">
								</div>
			                </div>
			                <div class="col-sm-12">
	                            <div class="form-group">
	                            	<label for="exampleInputEmail1">Pesan</label>
	                                <textarea id="form_message" name="form_message" class="form-control required" rows="5" required="required"></textarea>
	                            </div>
			                    <div class="form-group ui_kit_button mb0">
				                    <button type="button" class="btn dbxshad btn-lg btn-thm circle white">Kirim</button>
			                    </div>
			                </div>
		                </div>
		            </form>
				</div>
			</div>
		</div>
	</section>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAz77U5XQuEME6TpftaMdX0bBelQxXRlM&callback=initMap"type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/googlemaps1.js"></script>