<!-- Inner Page Breadcrumb -->
	<section class="inner_page_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-3 text-center">
					<div class="breadcrumb_content">
						<h4 class="breadcrumb_title"><?= $title; ?></h4>
						<ol class="breadcrumb">
						   
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

<!-- Main Blog Post Content -->
	<section class="blog_post_container">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-xl-9">
					<div class="main_blog_post_content">
						<div class="mbp_thumb_post">
							<div class="details pt0">
								<h3 class="mb25">UX/UI Design Conference</h3>
							</div>
							<div class="thumb">
								<img class="img-fluid" src="<?= base_url(); ?>e-lbm/assets/frontend/images/blog/12.jpg" alt="12.jpg">
								<div class="post_date"><h2>28</h2> <span>DECEMBER</span></div>
							</div>
							<div class="event_counter_plugin_container">
								<div class="event_counter_plugin_content">
									<ul>
										<li>days<span id="days"></span></li>
										<li>Hours<span id="hours"></span></li>
										<li>Minutes<span id="minutes"></span></li>
										<li>Seconds<span id="seconds"></span></li>
									</ul>
								</div>
							</div>
							<div class="details">
								<h4>Event Description</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
								<p class="mb25">It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
								<h4 class="mb0">Event Content</h4>
								<ul class="bs_content_list">
									<li><p>You will need a copy of Adobe XD 2019 or above. A free trial can be downloaded from Adobe.</p></li>
									<li><p>No previous design experience is needed.</p></li>
									<li><p>No previous Adobe XD skills are needed.</p></li>
								</ul>
							</div>
							<ul class="blog_post_share mb0">
								<li><p>Share</p></li>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-google"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google"></i></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-xl-3 pl10 pr10">
					<div class="main_blog_post_widget_list">
						<div class="event_details_widget">
							<h4 class="title">Event Details</h4>
							<ul>
								<li><span class="flaticon-appointment"></span> Date: 06.09.2019 - 06.09.2020</li>
								<li><span class="flaticon-clock"></span> Time: 8:00 am - 5:00 pm</li>
								<li><span class="flaticon-placeholder"></span> Address: Cambridge, MA 02138, USA</li>
							</ul>
						</div>
						<div class="event_details_widget">
							<h4 class="title">Event Details</h4>
							<div class="h200 mb30 bdrs5" id="map-canvas"></div>
							<ul>
								<li><span class="flaticon-phone-call"></span> 1-896-567-23497</li>
								<li><span class="flaticon-email"></span> <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="fc998a999288bc9998899185d29f9391">[email&#160;protected]</a></li>
								<li><span class="flaticon-www"></span> http://www.edumy.com</li>
							</ul>
						</div>
						<div class="blog_tag_widget">
							<h4 class="title">Tags</h4>
							<ul class="tag_list">
								<li class="list-inline-item"><a href="#">Photoshop</a></li>
								<li class="list-inline-item"><a href="#">Sketch</a></li>
								<li class="list-inline-item"><a href="#">Beginner</a></li>
								<li class="list-inline-item"><a href="#">UX/UI</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/single-counter.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAz77U5XQuEME6TpftaMdX0bBelQxXRlM&callback=initMap"type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/googlemaps1.js"></script>