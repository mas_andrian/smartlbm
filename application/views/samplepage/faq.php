<!-- Inner Page Breadcrumb -->
	<section class="inner_page_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-3 text-center">
					<div class="breadcrumb_content">
						<h4 class="breadcrumb_title"><?= $title; ?></h4>
						<ol class="breadcrumb">
						   
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Our FAQ -->
	<section class="our-faq">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-xl-4">
					<div class="faq_question_widget">
						<h4 class="title">Faq</h4>
						<div class="widget_list">
							<ul class="list_details">
								<li><a href="#">Payments</a></li>
								<li><a href="#">Suggestions</a></li>
								<li><a href="#">Company Policies</a></li>
								<li><a href="#">Terms&conditons</a></li>
								<li><a href="#">Payment Options</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-8 col-xl-8">
					<h4 class="fz20 mb30">Payments</h4>
					<div class="faq_according">
					  	<div id="accordion" class="panel-group">
						    <div class="panel">
						      	<div class="panel-heading">
							      	<h4 class="panel-title">
							        	<a href="#panelBodyOne" class="accordion-toggle link fz20 mb15" data-toggle="collapse" data-parent="#accordion">Why won't my payment go through?</a>
							        </h4>
						      	</div>
							    <div id="panelBodyOne" class="panel-collapse collapse show">
							        <div class="panel-body">
								    	<h4>Course Description</h4>
								    	<p class="mb25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
								        <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
							        </div>
							    </div>
						    </div>
						    <div class="panel">
						      	<div class="panel-heading">
							      	<h4 class="panel-title">
							        	<a href="#panelBodyTwo" class="accordion-toggle link fz20 mb15" data-toggle="collapse" data-parent="#accordion">How do I get a refund?</a>
							        </h4>
						      	</div>
							    <div id="panelBodyTwo" class="panel-collapse collapse">
							        <div class="panel-body">
								    	<h4>Course Description</h4>
								    	<p class="mb25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
								        <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
							        </div>
							    </div>
						    </div>
						    <div class="panel">
						      	<div class="panel-heading">
							      	<h4 class="panel-title">
							        	<a href="#panelBodyThree" class="accordion-toggle link fz20 mb15" data-toggle="collapse" data-parent="#accordion">How do I redeem a coupon?</a>
							        </h4>
						      	</div>
							    <div id="panelBodyThree" class="panel-collapse collapse">
							        <div class="panel-body">
								    	<h4>Course Description</h4>
								    	<p class="mb25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
								        <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
							        </div>
							    </div>
						    </div>
						    <div class="panel">
						      	<div class="panel-heading">
							      	<h4 class="panel-title">
							        	<a href="#panelBodyFour" class="accordion-toggle link fz20 mb15" data-toggle="collapse" data-parent="#accordion">Changing account name</a>
							        </h4>
						      	</div>
							    <div id="panelBodyFour" class="panel-collapse collapse">
							        <div class="panel-body">
								    	<h4>Course Description</h4>
								    	<p class="mb25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
								        <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
							        </div>
							    </div>
						    </div>
						</div>
					</div>
					<h4 class="fz20 mb30 mt25">Suggestions</h4>
					<div class="faq_according">
					  	<div id="accordion" class="panel-group">
						    <div class="panel">
						      	<div class="panel-heading">
							      	<h4 class="panel-title">
							        	<a href="#panelBodyFive" class="accordion-toggle link fz20 mb15" data-toggle="collapse" data-parent="#accordion">Why won't my payment go through?</a>
							        </h4>
						      	</div>
							    <div id="panelBodyFive" class="panel-collapse collapse show">
							        <div class="panel-body">
								    	<h4>Course Description</h4>
								    	<p class="mb25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
								        <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
							        </div>
							    </div>
						    </div>
						    <div class="panel">
						      	<div class="panel-heading">
							      	<h4 class="panel-title">
							        	<a href="#panelBodySix" class="accordion-toggle link fz20 mb15" data-toggle="collapse" data-parent="#accordion">How do I get a refund?</a>
							        </h4>
						      	</div>
							    <div id="panelBodySix" class="panel-collapse collapse">
							        <div class="panel-body">
								    	<h4>Course Description</h4>
								    	<p class="mb25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
								        <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
							        </div>
							    </div>
						    </div>
						    <div class="panel">
						      	<div class="panel-heading">
							      	<h4 class="panel-title">
							        	<a href="#panelBodySeven" class="accordion-toggle link fz20 mb15" data-toggle="collapse" data-parent="#accordion">How do I redeem a coupon?</a>
							        </h4>
						      	</div>
							    <div id="panelBodySeven" class="panel-collapse collapse">
							        <div class="panel-body">
								    	<h4>Course Description</h4>
								    	<p class="mb25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
								        <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
							        </div>
							    </div>
						    </div>
						    <div class="panel">
						      	<div class="panel-heading">
							      	<h4 class="panel-title">
							        	<a href="#panelBodyEight" class="accordion-toggle link fz20 mb15" data-toggle="collapse" data-parent="#accordion">Changing account name</a>
							        </h4>
						      	</div>
							    <div id="panelBodyEight" class="panel-collapse collapse">
							        <div class="panel-body">
								    	<h4>Course Description</h4>
								    	<p class="mb25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
								        <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
							        </div>
							    </div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>