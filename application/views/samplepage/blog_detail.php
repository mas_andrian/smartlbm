<!-- Inner Page Breadcrumb -->
	<section class="inner_page_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-3 text-center">
					<div class="breadcrumb_content">
						<h4 class="breadcrumb_title"><?= $title; ?></h4>
						<ol class="breadcrumb">
						   
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Main Blog Post Content -->
	<section class="blog_post_container">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-xl-9">
					<div class="main_blog_post_content">
						<div class="mbp_thumb_post">
							<div class="thumb">
								<img class="img-fluid" src="<?= base_url(); ?>e-lbm/assets/frontend/images/blog/12.jpg" alt="12.jpg">
								<div class="tag">Marketing</div>
								<div class="post_date"><h2>28</h2> <span>DECEMBER</span></div>
							</div>
							<div class="details">
								<h3>Learning, Friendship and Fun</h3>
								<ul class="post_meta">
									<li><a href="#"><span class="flaticon-profile"></span></a></li>
									<li><a href="#"><span>Ali Tufan</span></a></li>
									<li><a href="#"><span class="flaticon-comment"></span></a></li>
									<li><a href="#"><span>7 comments</span></a></li>
								</ul>
								<h4>Description</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
								<p class="mb25">It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
								<h4 class="mb0">Content</h4>
								<ul class="bs_content_list">
									<li><p>You will need a copy of Adobe XD 2019 or above. A free trial can be downloaded from Adobe.</p></li>
									<li><p>No previous design experience is needed.</p></li>
									<li><p>No previous Adobe XD skills are needed.</p></li>
								</ul>
								<div class="mbp_blockquote">
									<div class="blockquote">
										<span class="font-italic"><i class="fa fa-quote-left"></i></span><em class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</em>
									</div>
								</div>
								<p class="mb25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
								<p class="mb25">It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
							</div>
							<ul class="blog_post_share">
								<li><p>Share</p></li>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-google"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google"></i></a></li>
							</ul>
						</div>
						<div class="mbp_pagination_tab">
							<div class="row">
								<div class="col-sm-6 col-lg-6">
									<div class="pag_prev">
										<a href="#"><span class="flaticon-left-arrow"></span>Previous Post</a> <br> <p>The bedding was hardly able</p>
									</div>
								</div>
								<div class="col-sm-6 col-lg-6">
									<div class="pag_next text-right">
										<a href="#">Next Post <span class="flaticon-right-arrow-1"></span></a> <br> <p>11 Tips to Help You Get New</p>
									</div>
								</div>
							</div>
							<ul>
								<li class="list-inline-item float-left"></li>
								<li class="list-inline-item float-right"></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3 pl10 pr10">
					<div class="main_blog_post_widget_list">
						<div class="blog_search_widget">
							<div class="input-group mb-3">
								<input type="text" class="form-control" placeholder="Search Here" aria-label="Recipient's username" aria-describedby="button-addon2">
								<div class="input-group-append">
							    	<button class="btn btn-outline-secondary" type="button" id="button-addon2"><span class="flaticon-magnifying-glass"></span></button>
								</div>
							</div>
						</div>
						<div class="blog_category_widget">
							<ul class="list-group">
								<h4 class="title">Category</h4>
								<li class="list-group-item d-flex justify-content-between align-items-center">
							    	Admissions <span class="float-right">6</span>
								</li>
								<li class="list-group-item d-flex justify-content-between align-items-center">
							    	News <span class="float-right">1</span>
								</li>
								<li class="list-group-item d-flex justify-content-between align-items-center">
							    	Event <span class="float-right">6</span>
								</li>
								<li class="list-group-item d-flex justify-content-between align-items-center">
							    	Focus in the lab <span class="float-right">16</span>
								</li>
							</ul>
						</div>
						<div class="blog_recent_post_widget media_widget">
							<h4 class="title">Recent Posts</h4>
							<div class="media">
								<img class="align-self-start mr-3" src="<?= base_url(); ?>e-lbm/assets/frontend/images/blog/s1.jpg" alt="s1.jpg">
								<div class="media-body">
							    	<h5 class="mt-0 post_title">Half of What We Know About Coffee</h5>
							    	<a href="#">October 25, 2019.</a>
								</div>
							</div>
							<div class="media">
								<img class="align-self-start mr-3" src="<?= base_url(); ?>e-lbm/assets/frontend/images/blog/s2.jpg" alt="s2.jpg">
								<div class="media-body">
							    	<h5 class="mt-0 post_title">The Best Places to Start Your Travel</h5>
							    	<a href="#">October 25, 2019.</a>
								</div>
							</div>
							<div class="media">
								<img class="align-self-start mr-3" src="<?= base_url(); ?>e-lbm/assets/frontend/images/blog/s3.jpg" alt="s3.jpg">
								<div class="media-body">
							    	<h5 class="mt-0 post_title">The Top 25 London</h5>
							    	<a href="#">October 25, 2019.</a>
								</div>
							</div>
						</div>
						<div class="blog_tag_widget">
							<h4 class="title">Tags</h4>
							<ul class="tag_list">
								<li class="list-inline-item"><a href="#">Photoshop</a></li>
								<li class="list-inline-item"><a href="#">Sketch</a></li>
								<li class="list-inline-item"><a href="#">Beginner</a></li>
								<li class="list-inline-item"><a href="#">UX/UI</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>