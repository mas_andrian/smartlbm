<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$url = "http://192.168.100.19/sim-apps/";
?>

<!--

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>
<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
}

h1 {
	color: #444;
	background-color: orange;
	border-bottom: 1px solid #D0D0D0;
	font-size: 19px;
	font-weight: bold;
	margin: 0 0 14px 0;
	padding: 14px 15px 10px 15px;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
	margin: 10px;
	border: 1px solid #D0D0D0;
	box-shadow: 0 0 8px #D0D0D0;
}

p {
	margin: 12px 15px 12px 15px;
}
</style>
</head>
<body>
	<div id="container">
		<h1><?php echo $heading; ?></h1>
		<?php echo $message; ?>
	</div>
</body>
</html>
-->

<!DOCTYPE html>
<html lang="en">
<head>
<title>404 : Halaman Tidak Tersedia</title>
<!-- for-mobile-apps -->

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?= $url; ?>assets/front_end/images/logo/sim-apps.png">
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
	
	<!-- css files -->
	<link href="<?= $url; ?>assets/feed_back/css/css_slider.css" type="text/css" rel="stylesheet" media="all"><!-- slider css -->
    <link href="<?= $url; ?>assets/feed_back/css/bootstrap.css" rel='stylesheet' type='text/css' /><!-- bootstrap css -->
    <link href="<?= $url; ?>assets/feed_back/css/style.css" rel='stylesheet' type='text/css' /><!-- custom css -->
    <link href="<?= $url; ?>assets/feed_back/css/font-awesome.min.css" rel="stylesheet"><!-- fontawesome css -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
	<!-- //css files -->
	
	<!-- google fonts -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800&amp;subset=latin-ext" rel="stylesheet">
	<!-- //google fonts -->
	
</head>
<body>




<!-- banner -->
<div class="banner" id="home">
	<div class="layer">
		<div class="container">
			<div class="banner-text-agile p-5">
				<div class="row">
					<div class="col-lg-6 p-0">
						<!-- banner slider-->
					<div class="csslider infinity" id="slider1">
						<input type="radio" name="slides" checked="checked" id="slides_1" />
						<input type="radio" name="slides" id="slides_2" />
						<input type="radio" name="slides" id="slides_3" />
						<ul class="banner_slide_bg">
							<li>
								<div class="container-fluid">
									<div class="w3ls_banner_txt">
										<h3 class="b-w3ltxt text-capitalize mt-md-4 animated bounceInDown" style="padding-top: 0; margin-top: 0;"><span class="" style="color: red;">Error code </span> 404<?php echo ' '//$heading; ?> Heart Not Found</h3>
										<p class="w3ls_pvt-title my-3 animated bounceInLeft delay-1s"> The heart you request was not found
<?php echo ' '//$message; ?>.</p>

										<a href="<?php echo $url;?>" class="btn btn-banner my-sm-3 mb-3 animated bounceInUp delay-3s" style="color: #fff;">Back to => <span style="font-weight: bold;">Ex-lover</span></a>
										
									</div>
								</div>
							</li>
							
						</ul>
						<div class="navigation">
							<div>
								<label for="slides_1"></label>
								<label for="slides_2"></label>
								<label for="slides_3"></label>
							</div>
						</div>
					</div>
					<!-- //banner slider-->

					</div>
					<div class="col-lg-6 col-md-8">
						<img src="<?= $url; ?>assets/feed_back/images/heart_.jpg" alt="" class="img-fluid animated bounceInDown delay-2s" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- //banner -->

</body>
</html>