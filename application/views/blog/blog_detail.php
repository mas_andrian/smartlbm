<!-- Inner Page Breadcrumb -->
	<section class="inner_page_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-3 text-center">
					<div class="breadcrumb_content">
						<h4 class="breadcrumb_title"><?= $title; ?></h4>
						<ol class="breadcrumb">
						   <p class="color-white"><?= $blog['judul']; ?></p>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Main Blog Post Content -->
	<section class="blog_post_container">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-xl-9">
					<div class="main_blog_post_content">
						<div class="mbp_thumb_post">
							<?php
								$date = $blog['time_at'];
								$dateObj = DateTime::createFromFormat('Y-m-d H:i:s', $date);
							?>

							<div class="thumb">
								<a href="<?= base_url(); ?>blog/detail/<?= $blog['flag']; ?>"><img class="img-fluid" src="<?= base_url(); ?>e-lbm<?= $blog['banner']; ?>" alt="banner"></a>
								<div class="tag"><?= $blog['kategori']; ?></div>
								<div class="post_date"><h2><?= $dateObj->format('d'); ?></h2> <span><?= $dateObj->format('F'); ?></span></div>
							</div>
							<div class="details">
								<a href="<?= base_url(); ?>blog/detail/<?= $blog['flag']; ?>"><h3><?= $blog['judul']; ?></h3></a>
								<ul class="post_meta">
									<li><a href="#"><span class="flaticon-profile"></span></a></li>
									<li><a href="#"><span>Admin</span></a></li>
									
								</ul>
								<hr>
								<div style="text-align: justify;">
									<?php echo $blog['deskripsi']; ?>
								</div>
								<hr>
							</div>

							<ul class="blog_post_share">
								<li><p>Share</p></li>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-google"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google"></i></a></li>
							</ul>
						</div>
						
					</div>
				</div>
				<div class="col-lg-4 col-xl-3 pl10 pr10">
					<div class="main_blog_post_widget_list">
						
						
						<div class="blog_category_widget">
							<ul class="list-group">
								<h4 class="title">Kategori</h4>
								<hr>
								<?php foreach ($datablog as $db) { ?>
								<li class="list-group-item d-flex justify-content-between align-items-center">
							    	<?= $db['kategori'] ?> <span class="float-right"><?= $db['jumlah'] ?></span>
								</li>
								<?php } ?>
							</ul>
						</div>
						<div class="blog_recent_post_widget media_widget">
							<h4 class="title">Postingan Terbaru</h4>
						<?php foreach ($terbaru as $dt_new) { ?>
							<div class="media">
								<a href="<?= base_url(); ?>blog/detail/<?= $dt_new['flag']; ?>"><img class="align-self-start mr-3" src="<?= base_url(); ?>e-lbm<?= $dt_new['banner'] ?>" alt="images"></a>
								<div class="media-body">
							    	<a href="<?= base_url(); ?>blog/detail/<?= $dt_new['flag']; ?>"><h5 class="mt-0 post_title"><?= $dt_new['judul'] ?></h5></a>
							    	<a href="#"><?= $dt_new['time_at'] ?></a>
								</div>
							</div>
						<?php } ?>

						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>