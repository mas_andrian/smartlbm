 <!-- Inner Page Breadcrumb --> 
	<section class="inner_page_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-3 text-center">
					<div class="breadcrumb_content">
						<h4 class="breadcrumb_title"><?= $title; ?></h4>
						<ol class="breadcrumb">
						   
						</ol>
					</div>
				</div>
			</div> 
		</div>
	</section>

	<!-- Main Blog Post Content -->
	<section class="blog_post_container bgc-fa">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="main-title text-center">
						<h3 class="mt0 mb0">Pemberitahuan</h3>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="feature_post_slider">
					<?php foreach ($pemberitahuan as $dt_alert) { ?>
						<div class="item">
							<div class="blog_post">
								<div class="thumb">
									<img class="img-fluid w100" src="<?= base_url(); ?>e-lbm<?= $dt_alert['banner']; ?>" alt="gambar">
									<a class="post_date" href="#"><?= $dt_alert['time_at']; ?></a>
								</div>
								<div class="details">
									<h5><?= $dt_alert['kategori']; ?></h5>
									<a href="<?= base_url(); ?>blog/detail/<?= $dt_alert['flag']; ?>"><h4><?= $dt_alert['judul']; ?></h4></a>
								</div>
							</div>
						</div>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Main Blog Post Content -->
	<section class="blog_post_container">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-xl-9">
					<div class="main_blog_post_content">

					<?php foreach ($result as $dt_blog) { 
						$date = $dt_blog['time_at'];
						$dateObj = DateTime::createFromFormat('Y-m-d H:i:s', $date);
					?>
						<div class="mbp_thumb_post mb35">
							<div class="thumb">
								<a href="<?= base_url(); ?>blog/detail/<?= $dt_blog['flag']; ?>"><img class="img-fluid" src="<?= base_url(); ?>e-lbm<?= $dt_blog['banner']; ?>" alt="banner"></a>
								<div class="tag"><?= $dt_blog['kategori']; ?></div>
								<div class="post_date"><h2><?= $dateObj->format('d'); ?></h2> <span><?= $dateObj->format('F'); ?></span></div>
							</div>
							<div class="details">
								<a href="<?= base_url(); ?>blog/detail/<?= $dt_blog['flag']; ?>"><h3><?= $dt_blog['judul']; ?></h3></a>
								<ul class="post_meta">
									<li><a href="#"><span class="flaticon-profile"></span></a></li>
									<li><a href="#"><span>Admin</span></a></li>
									
								</ul>
								<p><?php echo strip_tags(character_limiter($dt_blog['deskripsi'], 300)); ?></p>
							</div>
						</div>

					<?php } ?>
						

						<div class="row">
							<div class="col-lg-12">
								<div class="mbp_pagination mt20">
									<?= $pagination; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3 pl10 pr10">
					<div class="main_blog_post_widget_list">
						<form action="<?= base_url(); ?>blog/list" method="post" >
						<div class="blog_search_widget">
							<div class="input-group mb-3">
							
								<input type="text"  name="search"  value="<?= $search; ?>" class="form-control" placeholder="Telusuri blog disini ..." >
								<div class="input-group-append">
							    	<button class="btn btn-outline-secondary" type="submit" name="submit"><span class="flaticon-magnifying-glass"></span></button>
								</div>
							
							</div>
						</div>
						</form>
						<div class="blog_category_widget">
							<ul class="list-group">
								<h4 class="title">Kategori</h4>
								<hr>
								<?php foreach ($datablog as $db) { ?>
								<li class="list-group-item d-flex justify-content-between align-items-center">
							    	<?= $db['kategori'] ?> <span class="float-right"><?= $db['jumlah'] ?></span>
								</li>
								<?php } ?>
							</ul>
						</div>
						<div class="blog_recent_post_widget media_widget">
							<h4 class="title">Postingan Terbaru</h4>
						<?php foreach ($terbaru as $dt_new) { ?>
							<div class="media">
								<a href="<?= base_url(); ?>blog/detail/<?= $dt_new['flag']; ?>"><img class="align-self-start mr-3" src="<?= base_url(); ?>e-lbm<?= $dt_new['banner'] ?>" alt="images"></a>
								<div class="media-body">
							    	<a href="<?= base_url(); ?>blog/detail/<?= $dt_new['flag']; ?>"><h5 class="mt-0 post_title"><?= $dt_new['judul'] ?></h5></a>
							    	<a href="#"><?= $dt_new['time_at'] ?></a>
								</div>
							</div>
						<?php } ?>

						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>