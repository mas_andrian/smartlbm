<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style type="text/css">
	
.emp-profile{
    padding: 3%;
    margin-top: 3%;
    margin-bottom: 3%;
    border-radius: 0.5rem;
    background: #fff;
}
.profile-img{
    text-align: center;
}
.profile-img img{
    width: 70%;
    height: 100%;
}
.profile-img .file {
    position: relative;
    overflow: hidden;
    margin-top: -20%;
    width: 70%;
    border: none;
    border-radius: 0;
    font-size: 15px;
    background: #212529b8;
}
.profile-img .file input {
    position: absolute;
    opacity: 0;
    right: 0;
    top: 0;
}
.profile-head h5{
    color: #333;
}
.profile-head h6{
    color: #0062cc;
}
.profile-edit-btn{
    border: none;
    border-radius: 1.5rem;
    width: 70%;
    padding: 2%;
    font-weight: 600;
    color: #6c757d;
    cursor: pointer;
}
.proile-rating{
    font-size: 12px;
    color: #818182;
    margin-top: 5%;
}
.proile-rating span{
    color: #495057;
    font-size: 15px;
    font-weight: 600;
}
.profile-head .nav-tabs{
    margin-bottom:5%;
}
.profile-head .nav-tabs .nav-link{
    font-weight:600;
    border: none;
}
.profile-head .nav-tabs .nav-link.active{
    border: none;
    border-bottom:2px solid #0062cc;
}
.profile-work{
    padding: 14%;
    margin-top: -15%;
}
.profile-work p{
    font-size: 12px;
    color: #818182;
    font-weight: 600;
    margin-top: 10%;
}
.profile-work a{
    text-decoration: none;
    color: #495057;
    font-weight: 600;
    font-size: 14px;
}
.profile-work ul{
    list-style: none;
}
.profile-tab label{
    font-weight: 600;
}
.profile-tab p{
    font-weight: 600;
    color: #0062cc;
}
</style>

<!-- Inner Page Breadcrumb -->
	<section class="inner_page_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-xl-12 offset-xl-12 text-center">
					<div class="breadcrumb_content">
						<p class="color-white" style="padding-bottom: 0; margin-bottom: 0;"><?= $siswa['nis']; ?></p>
						<h4 class="breadcrumb_title"><?= $siswa['nama']; ?></h4>
						<p class="color-white"><?= $siswa['tingkatan']; ?> - <?= $siswa['kelas']; ?> | <?= $siswa['nama_sekolah']; ?></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Our Team Members -->
	<section class="our-team">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="instructor_personal_infor">
						<div class="instructor_thumb text-center">
							<img style="width: 150px; height: 150px;" class="img-fluid" src="<?= base_url(); ?>e-lbm<?= $siswa['foto_diri']; ?>" alt="<?= $siswa['nama']; ?>">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-lg-12 col-xl-12">
					<div class="row">
						<div class="col-lg-12">
							<div class="cs_rwo_tabs csv2 ">
								
									<ul class="nav nav-tabs" id="myTab" role="tablist">
										
										<li class="nav-item">
										    <a class="nav-link active" id="Overview-tab" data-toggle="tab" href="#profil" role="tab" aria-controls="profil" aria-selected="true">Profil </a>
										</li>
										<li class="nav-item" <?php if($this->session->userdata('role') == 'Orang Tua / Wali Siswa'){ echo 'style="display: none;"';} ?>>
										    <a class="nav-link" id="course-tab" data-toggle="tab" href="#course" role="tab" aria-controls="course" aria-selected="false">Pembelajaran</a>
										</li>
										<li class="nav-item">
										    <a class="nav-link" id="ujian-tab" data-toggle="tab" href="#ujian" role="tab" aria-controls="ujian" aria-selected="false">Nilai </a>
										</li>
										<li class="nav-item">
										    <a class="nav-link" id="instructor-tab" data-toggle="tab" href="#presensi" role="tab" aria-controls="presensi" aria-selected="false">Presensi</a>
										</li>
										<li class="nav-item">
										    <a class="nav-link" id="review-tab" data-toggle="tab" href="#raport" role="tab" aria-controls="raport" aria-selected="false">Raport</a>
										</li>
										<li class="nav-item">
										    <a class="nav-link" id="review-tab" data-toggle="tab" href="#log" role="tab" aria-controls="log" aria-selected="false">Log Aktivitas</a>
										</li>
									</ul>

									<div class="tab-content" id="myTabContent">
										<div class="tab-pane fade show active" id="profil" role="tabpanel" aria-labelledby="profil-tab">
											<div class="cs_row_two csv2">
												<div class="cs_overview">
													<h4 class="title">Profil Siswa</h4>
													
													<div class="container emp-profile">
										                <div class="row">
										                    <div class="col-md-4">
										                        <div class="profile-img">
										                            <img src="<?= base_url(); ?>e-lbm<?= $siswa['foto_diri']; ?>" alt=""/>
										                            <a <?php if($this->session->userdata('role') == 'Orang Tua / Wali Siswa'){ echo 'style="display: none;"';} ?> data-toggle="modal" data-target="#myModalfoto">
										                            	<div class="file btn btn-lg btn-primary">
										                                Ubah Foto Diri
										                            	</div>
										                            </a>

										                             <!-- Modal -->
  <div class="modal fade" id="myModalfoto" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
      <form method="post" action="<?= base_url(); ?>siswa/do_edit_foto_profil/<?= $siswa['id_siswa']; ?>" enctype="multipart/form-data">
        <div class="modal-header">
         
          <h5 class="modal-title">Ubah Foto Profil</h5>
        </div>
        <div class="modal-body">
          <label>Pilih Foto</label>
          <input type="file" name="foto" accept="image/*" >
        </div>
        <div class="modal-footer">
        	<button type="submit" class="btn btn-primary">Simpan</button>
        	<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
      
    </div>
  </div>

										                        </div>
										                    </div>
										                    <div class="col-md-8">
										                        <div class="profile-head">
										                                    <h5>
										                                        <?= $siswa['nama']; ?>
										                                    </h5>
										                                    <h6>
										                                        <?= $siswa['tingkatan']; ?> - <?= $siswa['kelas']; ?>
										                                    </h6>
										                                    <p class="proile-rating">SEKOLAH : <span><?= $siswa['nama_sekolah']; ?></span></p>
										                                    <br>

										                            <ul class="nav nav-tabs" id="myTab" role="tablist">
										                                
										                                <li class="nav-item">
																			<a class="nav-link active" id="siswa-tab" data-toggle="tab" href="#siswa" role="tab" aria-controls="siswa" aria-selected="true">Siswa</a>
																		</li>
																		<li class="nav-item">
																			<a class="nav-link" id="ortu-tab" data-toggle="tab" href="#ortu" role="tab" aria-controls="ortu" aria-selected="false">Orang Tua</a>
																		</li>

																		<li class="nav-item">
																			<a class="nav-link" id="aksesku-tab" data-toggle="tab" href="#aksesku" role="tab" aria-controls="aksesku" aria-selected="false">Akses Akun</a>
																		</li>
										                               
										                            </ul>
										                            <hr>
										                        </div>
										                    </div>
										                    
										                </div>
										                <div class="row">
										                    <div class="col-md-4">
										                        <div class="profile-work">
										                            <p>FITUR SISWA</p>
										                             <i class="fa fa-check"></i> <span>Profil Siswa</span><br/>
										                            <i class="fa fa-check"></i> <span>Pembelajaran Online</span><br/>
										                            <i class="fa fa-check"></i> <span>Presensi Kehadiran</span><br/>
										                            <i class="fa fa-check"></i> <span>Raport Hasil Belajar</span><br>
										                            <i class="fa fa-check"></i> <span>Pengaturan Akses Akun</span>
										                        </div>
										                    </div>

										                    <div class="col-md-8">
										                        <div class="tab-content profile-tab" id="myTabContent">
										                            <div class="tab-pane fade show active" id="siswa" role="tabpanel" aria-labelledby="siswa-tab">
										                                        <div class="row">
										                                            <div class="col-md-4">
										                                                <label>NISN</label>
										                                            </div>
										                                            <div class="col-md-8">
										                                                <p><?= $siswa['nis']; ?></p>
										                                            </div>
										                                        </div>
										                                        <div class="row">
										                                            <div class="col-md-4">
										                                                <label>Nama Lengkap</label>
										                                            </div>
										                                            <div class="col-md-8">
										                                                <p><?= $siswa['nama']; ?></p>
										                                            </div>
										                                        </div>
										                                        <div class="row">
										                                            <div class="col-md-4">
										                                                <label>Kelas</label>
										                                            </div>
										                                            <div class="col-md-8">
										                                                <p><?= $siswa['tingkatan']; ?> - <?= $siswa['kelas']; ?></p>
										                                            </div>
										                                        </div>
										                                        <div class="row">
										                                            <div class="col-md-4">
										                                                <label>Jenis Kelamin</label>
										                                            </div>
										                                            <div class="col-md-8">
										                                                <p><?= $siswa['jk']; ?></p>
										                                            </div>
										                                        </div>
										                                        <div class="row">
										                                            <div class="col-md-4">
										                                                <label>Tempat / Tanggal Lahir</label>
										                                            </div>
										                                            <div class="col-md-8">
										                                            	<?php
																							$date = $siswa['tgl_lahir'];
																							$dateObj = DateTime::createFromFormat('Y-m-d', $date);
																						?>
																										
										                                                <p><?= $siswa['tmp_lahir']; ?>, <?= $dateObj->format('d F Y'); ?></p>
										                                            </div>
										                                        </div>
										                                        <div class="row">
										                                            <div class="col-md-4">
										                                                <label>Usia</label>
										                                            </div>
										                                            <div class="col-md-8">
										                                            	<?php
																							$date = new DateTime($siswa['tgl_lahir']);
																							$now = new DateTime();
																							$usia = $now->diff($date)->y;
																						?>
										                                                <p><?= $usia; ?> tahun</p>
										                                            </div>
										                                        </div>
										                                        <div class="row">
										                                            <div class="col-md-4">
										                                                <label>Alamat</label>
										                                            </div>
										                                            <div class="col-md-8">
										                                                <p><?= $siswa['alamat']; ?></p>
										                                            </div>
										                                        </div>
										                                       
										                            </div>

										                             <div class="tab-pane fade" id="ortu" role="tabpanel" aria-labelledby="ortu-tab">
										                             	 		<div class="row">
										                                            <div class="col-md-4">
										                                                <label>Nama</label>
										                                            </div>
										                                            <div class="col-md-8">
										                                                <p><?= $siswa['nama_wali']; ?></p>
										                                            </div>
										                                        </div>
										                                        <div class="row">
										                                            <div class="col-md-4">
										                                                <label>Nomor Telepon</label>
										                                            </div>
										                                            <div class="col-md-8">
										                                                <p><?= $siswa['telp_wali']; ?></p>
										                                            </div>
										                                        </div>
										                             </div>

										                            <div class="tab-pane fade" id="aksesku" role="tabpanel" aria-labelledby="aksesku-tab">
										                             	<div class="row">
										                                            <div class="col-md-4">
										                                                <label>Username</label>
										                                            </div>
										                                            <div class="col-md-8">
										                                                <p><?= $siswa['username']; ?></p>
										                                            </div>
										                                </div>
										                                <div class="row">
										                                            <div class="col-md-4">
										                                                <label>Email</label>
										                                            </div>
										                                            <div class="col-md-8">
										                                                <p><?= $siswa['email']; ?></p>
										                                            </div>
										                                </div>
										                            </div>
										                        </div>
										                    </div>
										                </div>        
										        </div>
												</div>
											</div>
										</div>

										<div class="tab-pane fade" id="course" role="tabpanel" aria-labelledby="course-tab">
											<div class="cs_row_three csv2">
												<div class="course_content">
													<div class="cc_headers">
														<h4 class="title">Materi Pembelajaran Online</h4>
														<ul class="course_schdule float-right">
															<li class="list-inline-item"><a href="#">[<?= count($materi); ?>] Mata Pelajaran</a></li>
															
														</ul>
													</div>
													<br>

												<?php  $a = 0; foreach ($materi as $dt_materi) { ?>

													<div class="details">
													  	<div id="accordion" class="panel-group cc_tab">
														    <div class="panel">
														      	<div class="panel-heading">
															      	<h4 class="panel-title">
															        	<a href="#panelBodyCourse<?= $a; ?>" class="accordion-toggle link" data-toggle="collapse" data-parent="#accordion"> <b><?= $dt_materi['materi']['nama_mapel']; ?></b></a>
															        </h4>
														      	</div>
															    <div id="panelBodyCourse<?= $a; ?>" class="panel-collapse collapse <?php if($a == 0){ echo 'show'; } ?>">
															        <div class="panel-body">
															        	<ul class="cs_list mb0">

															        	<?php foreach ($dt_materi['list_bab'] as $dt_bab) { ?>
															        		<li>
															        			<span class="flaticon-play-button-1 icon"></span> BAB.<?= $dt_bab['urutan']; ?> <?= $dt_bab['judul']; ?> 

															        		<?php if($dt_bab['is_aktif'] == "Y") { ?>
															        			<span style="float: right;"  class="cs_time">
															        				<a href="<?= base_url('materi/bab/').$dt_bab['id'].'/'.$dt_bab['slug'] ?>"> Open <i class="fa fa-paper-plane-o"></i></a>
															        			</span>
															        		<?php } else { ?>
															        			<span style="float: right;"  class="cs_time">
															        				Close <i class="fa fa-window-close-o"></i>
															        			</span>
															        		<?php } ?>

															        			<span style="float: right;" class="cs_preiew">&nbsp; : &nbsp;</span>
															        			<span style="float: right;"  class="cs_preiew">Status</span>
															        		</li>
															        	<?php } ?>

															        	</ul>
															        </div>
															    </div>
														    </div>
														</div>
													</div>

												<?php $a++; } ?>

													
													
												</div>
											</div>
										</div>

										<div class="tab-pane fade" id="ujian" role="tabpanel" aria-labelledby="ujian-tab">
											<div class="cs_row_three csv2">
												<div class="course_content">
													<div class="cc_headers">
														<h4 class="title">Hasil Latihan & Ujian Pembelajaran Online</h4>
														<ul class="course_schdule float-right">
															<li class="list-inline-item"><a href="#">[<?= count($materi); ?>] Mata Pelajaran</a></li>
															
														</ul>
													</div>
													<br>

													<?php  $a = 0; foreach ($materi as $dt_materi) { ?>

													<div class="details">
														<div id="accordion" class="panel-group cc_tab">
															<div class="panel">
																<div class="panel-heading">
																	<h4 class="panel-title">
																		<a href="#panelBodyCourse<?= $a; ?>" class="accordion-toggle link" data-toggle="collapse" data-parent="#accordion"> <b><?= $dt_materi['materi']['nama_mapel']; ?></b></a>
																	</h4>
																</div>
																<div id="panelBodyCourse<?= $a; ?>" class="panel-collapse collapse <?php if($a == 0){ echo 'show'; } ?>">
																	<div class="panel-body">
																		<ul class="cs_list mb0">

																		<?php foreach ($dt_materi['list_bab'] as $dt_bab) { ?>
																			<li>
																				<span class="flaticon-play-button-1 icon"></span> BAB.<?= $dt_bab['urutan']; ?> <?= $dt_bab['judul']; ?> 
																				<span style="float: right;"  class="cs_time">
																					<a href="#" onClick="lihat_nilai_siswa(<?= $this->session->userdata('detil_siswa')['id_siswa']; ?>,<?= $dt_bab['id'] ?>)" > Lihat Nilai</a>
																				</span>
																			</li>
																		<?php } ?>

																		</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<?php $a++; } ?>
													
													
													
												</div>
											</div>
										</div>

										<div class="tab-pane fade" id="presensi" role="tabpanel" aria-labelledby="presensi-tab">
											<div class="cs_row_four csv2">
												<div class="about_ins_container">
													<h4 class="aii_title">List Data Presensi Siswa</h4>
													<div class="row">
														<div class="table-responsive">
														<table class="table table-striped table-bordered" style="width:100%">
													        <thead>
													            <tr>
													                <th>No.</th>
													                <th>Kelas</th>
													                <th>Tahun Ajaran</th>
													                <th>Rekap Presensi</th>
													                
													            </tr>
													        </thead>
													        <tbody>
													        <?php 
													        	if(count($presensi) == 0){
													        		echo "<tr><td colspan='4'> Data tidak tersedia.</td> </tr>";
													        	}

													        	$a = 1; 
													        	foreach ($presensi as $dt_presensi) { 
													        	$explod = explode('/',$dt_presensi['file']);
													        ?>
													            <tr>
													                <td><?= $a++; ?></td>
													                <td><?= $dt_presensi['tingkatan']; ?> - <?= $dt_presensi['kelas']; ?></td>
													                <td><?= $dt_presensi['semester']; ?> <?= $dt_presensi['tahun_ajaran']; ?></td>
													                <td><a href="<?= base_url(); ?>e-lbm<?= $dt_presensi['file']; ?>" target="_blank"><?= $explod[3]; ?></a></td>
													                
													            </tr>
													        <?php } ?>
													            

													        </tbody>
													    </table>
													   </div>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="raport" role="tabpanel" aria-labelledby="raport-tab">
											<div class="cs_row_five csv2">
												<div class="student_feedback_container">
													<h4 class="aii_title">List Data Raport Siswa</h4>
													<div class="row">
														<div class="table-responsive">
														<table class="table table-striped table-bordered" style="width:100%">
													        <thead>
													            <tr>
													                <th>No.</th>
													                <th>Kelas</th>
													                <th>Tahun Ajaran</th>
													                <th>Raport Siswa</th>
													                
													            </tr>
													        </thead>
													        <tbody>
													        <?php 
													        	if(count($raport) == 0){
													        		echo "<tr><td colspan='4'> Data tidak tersedia.</td> </tr>";
													        	}

													        	$b = 1; 
													        	foreach ($raport as $dt_raport) { 
													        	$explod = explode('/',$dt_raport['file']);
													        ?>
													            <tr>
													                <td><?= $b++; ?></td>
													                <td><?= $dt_raport['tingkatan']; ?> - <?= $dt_raport['kelas']; ?></td>
													                <td><?= $dt_raport['semester']; ?> <?= $dt_raport['tahun_ajaran']; ?></td>
													                <td><a href="<?= base_url(); ?>e-lbm<?= $dt_raport['file']; ?>" target="_blank"><?= $explod[3]; ?></a></td>
													                
													            </tr>
													        <?php } ?>
													            

													        </tbody>
													    </table>
													   </div>
													</div>
												</div>
											</div>
										</div>

										<div class="tab-pane fade" id="log" role="tabpanel" aria-labelledby="log-tab">
											<div class="cs_row_five csv2">
												<div class="student_feedback_container">
													<h4 class="aii_title">List Histori Akses Materi Siswa</h4>
													<div class="row">
														<div class="table-responsive">
														<table class="table table-striped table-bordered" style="width:100%">
													        <thead>
													            <tr>
													                <th>No.</th>
													                <th>Sub Bab</th>
													                <th>Bab</th>
													                <th>Mata Pelajaran</th>
													                <th>Kelas</th>
													                <th>Waktu Akses</th>
													            </tr>
													        </thead>
													        <tbody>
													       	 <?php 
													        	if(count($log) == 0){
													        		echo "<tr><td colspan='6'> Data tidak tersedia.</td> </tr>";
													        	}

													        	$c = 1; 
													        	foreach ($log as $dt_log) { 
													        	
																						
																 
													        ?>
													            <tr>
													                <td><?= $c++; ?></td>
													                <td><?= $dt_log['sub']; ?></td>
													                <td><?= $dt_log['bab']; ?></td>
													                <td><?= $dt_log['nama_mapel']; ?></td>
													                <td><?= $dt_log['tingkatan']; ?></td>
													                <td><?= $dt_log['akses']; ?></td>
													            </tr>
													        <?php } ?>
													            

													        </tbody>
													    </table>
													   </div>
													</div>
												</div>
											</div>

										</div>

									</div>
								</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<div class="modal fade" id="modalNilai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xs" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Nilai Siswa</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Latihan</th>
									<th>Nilai</th>
								</tr>
							</thead>
							<tbody id="list_latihan"></tbody>
						</table>

						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Ujian</th>
									<th>Nilai</th>
								</tr>
							</thead>
							<tbody id="list_ujian"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>