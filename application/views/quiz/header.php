<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta charset="utf-8"> 
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="Web official (Smartlbm) Learning Management System - YLBM Sragen">
<meta name="CreativeLayers" content="LC-Pro">

<!-- css file -->
<link rel="stylesheet" href="<?= base_url(); ?>e-lbm/assets/frontend/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>e-lbm/assets/frontend/css/style.css">

<!-- Responsive stylesheet -->
<link rel="stylesheet" href="<?= base_url(); ?>e-lbm/assets/frontend/css/responsive.css">
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/jquery-3.3.1.js"></script>

<!-- Title -->
<title><?= $title; ?> | SMARTLBM - Learning Management System</title>

<!-- Favicon -->
<link href="<?= base_url(); ?>e-lbm/assets/frontend/images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="<?= base_url(); ?>e-lbm/assets/frontend/images/favicon.ico" sizes="128x128" rel="shortcut icon" />
</head>
<body>
<div class="wrapper">
	<div class="preloader"></div>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary"">
	<a class="navbar-brand" href="#">
    <img src="<?= base_url('e-lbm/assets/frontend/images/header-logo.png') ?>" width="30" height="30" class="d-inline-block align-top" alt="">
    SMARTLBM
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
	  <li class="nav-item active">
        <a class="nav-link" href="#"><?= $this->session->userdata('detil_siswa')['nama']; ?> <span class="sr-only">(current)</span></a>
      </li>
    </ul>
  </div>
</nav>

	


