<section>
    <div class="container">
        <div class="jumbotron">
            <p class="lead">
                <center>
                    <img src="<?= base_url('e-lbm/assets/images/icon-centang.png') ?>" width="100px" /><br/>
                    <h2>Terima Kasih</h2>
                </center>
            </p>
            <hr class="my-4">
            <p style="text-align:center;">
                <b>Nama : </b><?= $this->session->userdata('detil_siswa')['nama']; ?><br/>
                <b>Nilai : </b><?= $quiz[0]['nilai'] ?><br/>
                <?php 
                    $start_date = new DateTime($quiz[0]['waktu_mulai']);
                    $since_start = $start_date->diff(new DateTime($quiz[0]['waktu_selesai']));
                ?>
                <b>Waktu Ujian : </b><?= $since_start->i ?> Menit<br/>
            </p>
            <p>
                <center>Terima kasih sudah mengikuti latihan soal ini, silahkan tekan tombol KELUAR untuk mengakhiri latihan soal ini</center>
            </p>
            <p class="lead">
                <center>
                    <a class="btn btn-primary btn-lg" href="<?= base_url('siswa') ?>" role="button">KELUAR</a>
                </center>
            </p>
        </div>
    </div>
</section>