<section>
    <div class="container">
        <div class="jumbotron">
            <h2><?= $this->uri->segment(2) == 'latihan' ? 'Latihan Soal' : 'Ujian' ?></h2>
            <p class="lead"><?= $materi[0]['nama_mapel']." | Bab.".$materi[0]['urutan']." ".$materi[0]['judul'] ?></p>
            <hr class="my-4">
            <p>
                Aturan :
                <ol start="1">
                    <li>1. Terdapat 10 soal pilihan ganda</li>
                    <li>2. Kerjakan semua</li>
                </ol>
            </p>
            <p class="lead">
                <a class="btn btn-info btn-lg" href="<?= base_url('materi/bab/').$this->uri->segment('3').'/'.$this->uri->segment('4') ?>" role="button">Kembali</a>
                <a class="btn btn-primary btn-lg" href="<?= base_url('quiz/start/').$this->uri->segment(2).'/'.$this->session->userdata('detil_siswa')['id_siswa'].'/'.$this->uri->segment('3').'/'.$this->uri->segment('4') ?>" role="button">Mulai <?= $this->uri->segment(2) == 'latihan' ? 'Latihan Soal' : 'Ujian' ?></a>
            </p>
        </div>
    </div>
</section>