
<!-- Our Footer Bottom Area -->
	<section class="footer_bottom_area home3 pt30 pb30">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="copyright-widget text-center">
						<p>Copyright Yayasan Lembaga Bakti Muslim (YLBM) - Sragen © <?= date('Y') ?>. All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
<a class="scrollToHome home3" href="#"><i class="flaticon-up-arrow-1"></i></a>
</div>

<?php 
     if($this->session->userdata('feedback') != NULL){ ?>
	<div class="alert alert-success " role="alert" style=" background-color: #F3F5F6; color: #000000; border: 2px solid #aaa; position: fixed; width: 300px; left: 38.5%; top: 40%; box-sizing: border-box; text-align: center;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="icon"><span style="font-size: 80px;" class="flaticon-account"></span></span><br>
        <p style="margin-bottom: 0.5em; font-weight: bold;">[<?= $this->session->userdata('role'); ?>]</p>
        <p style="margin-bottom: 1em;"><?= $this->session->userdata('feedback'); ?></p>
        <br>
     </div>
    <script type="text/javascript">
        $(document).ready (function(){
          window.setTimeout(function() {
              $(".alert").fadeTo(500, 0).slideUp(500, function(){
                  $(this).remove(); 
              });
          }, 5000);
        });
    </script>
<?php 
    $this->session->set_userdata('feedback', null);
} ?>


<!-- Wrapper End -->

<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/jquery-migrate-3.0.0.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/popper.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/jquery.mmenu.all.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/ace-responsive-menu.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/isotop.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/snackbar.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/simplebar.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/parallax.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/scrollto.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/jquery-scrolltofixed-min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/jquery.counterup.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/wow.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/progressbar.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/slider.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/timepicker.js"></script>
<!-- Custom script for all pages --> 
<script type="text/javascript" src="<?= base_url(); ?>e-lbm/assets/frontend/js/script.js"></script>
</body>
</html>

<script type="text/javascript">
	

</script>