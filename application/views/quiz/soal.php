<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="card" style="border:1px solid rgba(0,0,0,.125) !important;">
                    
                        <div class="card-header">
                        <form id="quiz_jawaban" action="<?= base_url('quiz/save') ?>" method="POST">
                            <button type="button" class="btn btn-primary">
                                Soal No. <span class="badge badge-light"><?= $detail_soal[0]['urutan'] ?></span>
                            </button>
                            <div style="float:right">
                                <p id="demo" style="font-weight:bold;"></p>
                                <button class="btn btn-danger" name="submit" value="stop">SELESAI <?= strtoupper($title) ?></button>
                            </div>
                            

                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><?= $detail_soal[0]['pertanyaan'] ?></h5>
                            <hr/>
                            
                                <input type="hidden" name="id_quiz" value="<?= $this->uri->segment('3')?>" />
                                <input type="hidden" name="id" value="<?= $this->uri->segment('4')?>" />
                                <?php
                                    foreach($jawaban as $j){
                                ?>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="jawaban_terpilih" id="jawaban" value="<?= $j ?>" <?= ($detail_soal[0]['jawaban_terpilih'] == $j ? 'checked' : '' ) ?>>
                                    <?= strtoupper($j).". " ?>
                                    <label class="form-check-label" for="jawaban">
                                        <?= $detail_soal[0]['jawaban_'.$detail_soal[0]['jawaban_'.$j]] ?>
                                    </label>
                                </div>
                                
                                    <?php } ?>
                                <hr/>
                                <div>
                                    <?php if($detail_soal[0]['urutan'] != 1){ ?>
                                    <button type="submit" class="btn btn-primary" name="submit" value="back"><i class="fa fa-arrow-left"></i> Soal Sebelumnya</button>
                                    <?php } ?>
                                    <?php if($detail_soal[0]['urutan'] != $total_soal[0]['total']){ ?>
                                    <button style="float:right;" class="btn btn-primary" name="submit" value="next">Soal Selanjutnya <i class="fa fa-arrow-right"></i></button>
                                    <?php } ?>
                                </div>
                                </form>
                        </div>
                   
                </div>
            </div>
            <div class="col-lg-4">
                
                <div class="card" style="border:1px solid rgba(0,0,0,.125) !important;">
                    <div class="card-header">
                        <b>Nomor Soal</b>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <?php foreach($soal as $s){ ?>
                            <div class="col-sm-3 col-lg-3" style="margin-bottom:5px;"><a href="<?= base_url('quiz/kerjakan/').$this->uri->segment('3').'/'.$s['id'] ?>" class="btn btn-block btn-sm <?= ($s['jawaban_terpilih'] == '0' ? 'btn-secondary' : 'btn-success') ?>"><?= $s['urutan'] ?></a></div>
                            <?php } ?>
                        </div>
                        <hr/>
                        <span class="badge badge-success">Hijau</span> = Sudah dijawab
                        <span class="badge badge-secondary">Abu-abu</span> = Belum dijawab
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var countDownDate = new Date("<?= $quiz[0]['tmp_waktu_kerjakan'] ?>").getTime();
    var x = setInterval(function() {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        document.getElementById("demo").innerHTML = "Sisa Waktu ( "+minutes + ":" + seconds+" )";
        if (distance < 0) {
            var form_data = new FormData($("#quiz_jawaban")[0]);
            form_data.append("submit", "stop");
            $.ajax({
                "async": true,
                "crossDomain": true,
                "processData": false,
                "contentType": false,
                "url": "<?= base_url('quiz/save') ?>",
                "method": "POST",
                "data": form_data,
            }).done(function (response) {
                window.location = "<?= base_url() ?>quiz/berhenti/<?= $this->uri->segment('3') ?>";
            });
            clearInterval(x);
            window.location = "<?= base_url() ?>quiz/berhenti/<?= $this->uri->segment('3') ?>";
        }
    }, 1000);
</script>