<!-- Inner Page Breadcrumb -->
	<section class="inner_page_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-3 text-center">
					<div class="breadcrumb_content">
						<h4 class="breadcrumb_title"><?= $title; ?></h4>
						<ol class="breadcrumb">
						   
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

<!-- Main Blog Post Content -->
	<section class="blog_post_container">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="main_blog_post_content">

					<?php foreach ($result as $dt_k) {
						$date = $dt_k['tanggal'];
						$dateObj = DateTime::createFromFormat('Y-m-d', $date);
					?>

						<div class="row event_lists p0">
							<div class="col-xl-5 pr15-xl pr0">
								<div class="blog_grid_post event_lists mb35">
									<div class="thumb">
										<img class="img-fluid w100" src="<?= base_url(); ?>e-lbm/<?= $dt_k['gambar']; ?>" alt="banner.jpg">
										<div class="post_date"><h2><?= $dateObj->format('d'); ?></h2> <span><?= $dateObj->format('F'); ?></span></div>
									</div>
								</div>
							</div>
							<div class="col-xl-7 pl15-xl pl0">
								<div class="blog_grid_post style2 event_lists mb35">
									<div class="details">
										<a href="<?= base_url(); ?>kegiatan/detail/<?= $dt_k['slug']; ?>"><h3><?= $dt_k['judul']; ?></h3></a>
										<p><?php echo strip_tags(character_limiter($dt_k['deskripsi'], 300)); ?></p>
										<ul class="mb0">
											<li><a href="#"><span class="flaticon-appointment"></span><?= $dateObj->format('d F Y'); ?></a></li>
											<li><a href="#"><span class="flaticon-clock"></span><?= $dt_k['jam']; ?></a></li>
											<li><a href="#"><span class="flaticon-placeholder"></span><?= $dt_k['lokasi']; ?></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>

					<?php } ?>

					<div class="row">
							<div class="col-lg-12">
								<div class="mbp_pagination mt20">
									<?= $pagination; ?>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>