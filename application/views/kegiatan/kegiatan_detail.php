<!-- Inner Page Breadcrumb -->
	<section class="inner_page_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-3 text-center">
					<div class="breadcrumb_content">
						<h4 class="breadcrumb_title"><?= $title; ?></h4>
						<ol class="breadcrumb">
						   <p class="color-white"><?= $kegiatan['judul']; ?></p>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

<!-- Main Blog Post Content -->
	<section class="blog_post_container">
		<div class="container">
			<div class="row">
				<center>
					<?php
						$date = $kegiatan['tanggal'];
						$dateObj = DateTime::createFromFormat('Y-m-d', $date);
					?>
				<div class="col-lg-8 col-xl-9">
					<div class="main_blog_post_content">
						<div class="mbp_thumb_post" style="text-align: justify;">
							<div class="details pt0">
								<h3 class="mb25"><?= $kegiatan['judul']; ?></h3>
							</div>
							<div class="thumb">
								<img class="img-fluid" src="<?= base_url(); ?>e-lbm/<?= $kegiatan['gambar']; ?>" alt="banner.jpg">
								<div class="post_date"><h2><?= $dateObj->format('d'); ?></h2> <span><?= $dateObj->format('F'); ?></span></div>
							</div>
							<div class="event_counter_plugin_container">
								<div class="event_counter_plugin_content">
									<ul>
										<li>Hari<span id="days"></span></li>
										<li>Jam<span id="hours"></span></li>
										<li>Menit<span id="minutes"></span></li>
										<li>Detik<span id="seconds"></span></li>
									</ul>
								</div>
							</div>
							<div class="details">
								
							<h4 class="title">Detail Kegiatan</h4>
							<ul class="mb0">
								<li><a href="#"><span class="flaticon-appointment"></span> <?= $dateObj->format('d F Y'); ?></a></li><br>
								<li><a href="#"><span class="flaticon-clock"></span> <?= $kegiatan['jam']; ?></a></li><br>
								<li><a href="#"><span class="flaticon-placeholder"></span> <?= $kegiatan['lokasi']; ?></a></li>
							</ul>
							<hr>
								<h4>Deskripsi Kegiatan</h4>
								<?= $kegiatan['deskripsi']; ?>
								
							</div>
							<hr>
							<ul class="blog_post_share mb0">
								<li><p>Share</p></li>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-google"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				</center>
				
			</div>
		</div>
	</section>

<script type="text/javascript">
	/*Evemt Single Page Counter JS Code*/
	const second = 1000,
	      minute = second * 60,
	      hour = minute * 60,
	      day = hour * 24;
	let countDown = new Date('<?= $dateObj->format('M') ?> <?= $dateObj->format('d'); ?>, <?= $dateObj->format('Y'); ?> 01:00:00').getTime(),
	    x = setInterval(function() {
			let now = new Date().getTime(),
				distance = countDown - now;
			document.getElementById('days').innerText = Math.floor(distance / (day)),
			document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
			document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
			document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);
	}, second)
</script>
