<!-- Inner Page Breadcrumb -->
<section class="inner_page_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-3 text-center">
					<div class="breadcrumb_content">
						<h4 class="breadcrumb_title"><?= $title; ?></h4>
						<ol class="breadcrumb">
						   
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

<!-- Our Team Members -->
	<section class="our-team pb40">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-8 col-xl-8">
					<div class="row">
						<div class="col-lg-12">
							<div class="courses_single_container">
								<div class="cs_row_one">
									<div class="cs_ins_container">
										<div class="cs_instructor">
											<ul class="cs_instrct_list float-left mb0">
												<li class="list-inline-item"><a href="#"><?= "Bab ".$detail_materi[0]['bab_urutan'].". ".$detail_materi[0]['bab_judul'] ?></a></li>
											</ul>
										</div>
										<h3 class="cs_title"><?= "Materi ".$detail_materi[0]['urutan'].". ".$detail_materi[0]['judul']; ?></h3>
									</div>
								</div>
								<div class="cs_row_two">
									<div class="cs_overview">
										<h4 class="title">Deskripsi</h4>
										<p class="mb30"><?= $detail_materi[0]['deskripsi'] ?></p>
                                        <div class="courses_big_thumb">
											<div class="thumb">
                                                <?php if($detail_materi[0]['file_video'] != ""){ ?>
                                                    <iframe class="iframe_video" width="450" height="550" src="<?= $detail_materi[0]['file_video'] ?>" frameborder="0" allowfullscreen></iframe>
                                                <?php } ?>
                                                <?php if($detail_materi[0]['file_pdf'] != ""){ ?>
                                                    <iframe style="width: 100%; height: 500px" id="pdf" class="pdf" src="<?= base_url() ?>assets/pdfviewer/web/viewer.html?file=<?= base_url('e-lbm/').$detail_materi[0]['file_pdf'] ?>" scrolling="no"></iframe>
                                                <?php } ?>
                                            </div>
										</div>
									</div>
										
								</div>

								
							</div>
						</div>
					</div>

				</div>
				<div class="col-lg-4 col-xl-4">
					<div class="cs_row_three">
						<div class="course_content">
							<div class="cc_headers">
								<h4 class="title">Daftar Materi</h4>
							</div>
							<br>
							<?php  $a = 0; foreach ($bab as $dt_bab) { ?>
							<div class="details">
								<div id="accordion" class="panel-group cc_tab">
									<div class="panel">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a href="#panelBodyCourse<?= $a; ?>" class="accordion-toggle link" data-toggle="collapse" data-parent="#accordion"><b><?= "Bab ".$dt_bab['bab']['urutan'].". ".$dt_bab['bab']['judul']; ?></b></a>
											</h4>
										</div>
										<div id="panelBodyCourse<?= $a; ?>" class="panel-collapse collapse <?php if($dt_bab['bab']['id'] == $detail_materi[0]['id_bab']){ echo 'show'; } ?>">
											<div class="panel-body">
												<ul class="cs_list mb0">
												<?php if($dt_bab['list_sub_bab']){ foreach ($dt_bab['list_sub_bab'] as $dt_sub_bab) { ?>
													<li>
														<?php if($dt_sub_bab['is_aktif'] == "Y") { ?>
															<a href="<?= base_url('materi/sub_bab/').$dt_sub_bab['id'].'/'.$dt_sub_bab['slug'] ?>"><span class="flaticon-play-button-1 icon"></span> Materi.<?= $dt_sub_bab['urutan']; ?> <?= $dt_sub_bab['judul']; ?> </a>
														<?php } else { ?>
															<span class="flaticon-play-button-1 icon"></span> Materi.<?= $dt_sub_bab['urutan']; ?> <?= $dt_sub_bab['judul']; ?>
														<?php } ?>
														
													</li>
												<?php } } ?>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php $a++; } ?>
						</div>
					</div>
					<a href="<?= base_url('materi/bab/').$detail_materi[0]['id_bab'].'/'.$detail_materi[0]['slug_bab'] ?>" class="btn btn-block btn-info">Kembali Ke Materi</a>
				</div>
			</div>
		</div>
	</section>
