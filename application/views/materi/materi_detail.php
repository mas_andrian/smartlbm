<!-- Inner Page Breadcrumb -->
<section class="inner_page_breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-3 text-center">
					<div class="breadcrumb_content">
						<h4 class="breadcrumb_title"><?= $title; ?></h4>
						<ol class="breadcrumb">
						   
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

<!-- Our Team Members -->
	<section class="our-team pb40">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-8 col-xl-9">
					<div class="row">
						<div class="col-lg-12">
							<div class="courses_single_container">
								<div class="cs_row_one">
									<div class="cs_ins_container">
										<div class="cs_instructor">
											<ul class="cs_instrct_list float-left mb0">
												<li class="list-inline-item">
													<a href="#">
														<?php foreach($guru_pic as $gr){ echo $gr['nama'].","; } ?>
													</a>
												</li>
											</ul>
										</div>
										<h3 class="cs_title"><?= "BAB ".$detail_materi[0]['urutan'].". ".$detail_materi[0]['judul']; ?></h3>
										<ul class="cs_review_enroll">
											<li class="list-inline-item"><a href="#"><span class="flaticon-comment"></span> <?= $rating['total_review'] ?> Review</a></li>
										</ul>
										<div class="courses_big_thumb">
											<div class="thumb">
                                                <img class="img-thumbnail" src="<?= base_url('e-lbm').substr($detail_materi[0]['img_thumbnail'], 1) ?>" />
											</div>
										</div>
									</div>
								</div>
								<div class="cs_row_two">
									<div class="cs_overview">
										<h4 class="title">Deskripsi</h4>
										<p class="mb30"><?= $detail_materi[0]['deskripsi'] ?></p>
									</div>
								</div>
								<div class="cs_row_three">
									<div class="course_content">
										<div class="cc_headers">
											<h4 class="title">Daftar Materi</h4>
											<ul class="course_schdule float-right">
												<li class="list-inline-item"><a href="#"><?= count($detail_sub_bab) ?> Materi</a></li>
											</ul>
										</div>
										<br>
										<div class="details">
										  	<div id="accordion" class="panel-group cc_tab">
											    <div class="panel">
											      	<div class="panel-heading">
												      	<h4 class="panel-title">
												        	<a href="#panelBodyCourseStart" class="accordion-toggle link" data-toggle="collapse" data-parent="#accordion">Sub Bab</a>
												        </h4>
											      	</div>
												    <div id="panelBodyCourseStart" class="panel-collapse collapse show">
												        <div class="panel-body">
												        	<ul class="cs_list mb0">
                                                                <?php foreach($detail_sub_bab as $dt_sbab){ ?>
                                                                    <li>
																		<span class="flaticon-play-button-1 icon"></span>
																		<?php if($dt_sbab['id_log'] != ""){ ?>
																		 	Materi <?= $dt_sbab['urutan'].". ".$dt_sbab['judul'] ?> 
																		<?php } else { ?>
																			<b>Materi <?= $dt_sbab['urutan'].". ".$dt_sbab['judul'] ?> </b>
																		<?php } ?>
																		<?php if($dt_sbab['is_aktif'] == 'Y'){ ?>
																			<span style="float: right;"  class="cs_time">
																				<a href="<?= base_url('materi/sub_bab/').$dt_sbab['id'].'/'.$dt_sbab['slug'] ?>">Status : Open <i class="fa fa fa-paper-plane-o"></i></a>
															        		</span>
																				
																		<?php } else { ?>
																			<span style="float: right;"  class="cs_time">
															        			Status : Close <i class="fa fa-window-close-o"></i>
															        		</span>
																		<?php } ?>
																	</li>
                                                                <?php } ?>
												        	</ul>
												        </div>
												    </div>
											    </div>
											</div>
										</div>
										<div class="details">
										  	<div id="accordion" class="panel-group cc_tab">
											    <div class="panel">
											      	<div class="panel-heading">
												      	<h4 class="panel-title">
												        	<a href="#panelBodyCourseBrief" class="accordion-toggle link" data-toggle="collapse" data-parent="#accordion">Latihan & Ujian</a>
												        </h4>
											      	</div>
												    <div id="panelBodyCourseBrief" class="panel-collapse collapse">
												        <div class="panel-body">
												        	<ul class="cs_list mb0">
												        		<li>
																	<span class="flaticon-play-button-1 icon"></span> Latihan Soal
																	<?php if($is_aktif_latihan == 1){ //JIKA LATIHAN AKTIF ?>
																		<span style="float: right;"  class="cs_time">
																			<a href="<?= base_url('quiz/latihan/').$this->uri->segment('3').'/'.$this->uri->segment('4') ?>">Status : Open <i class="fa fa fa-paper-plane-o"></i></a>
															        	</span>
																	<?php } else { ?>
																		<span style="float: right;"  class="cs_time">
															        		Status : Close <i class="fa fa-window-close-o"></i>
															        	</span>
																	<?php } ?>
																</li>
												        		<li><span class="flaticon-play-button-1 icon"></span> Ujian
																<?php if($is_aktif_ujian == 1){ ?>
																	<span style="float: right;"  class="cs_time">
																		<a href="#" data-toggle="modal" data-target="#exampleModal">Status : Open <i class="fa fa fa-paper-plane-o"></i></a>
															        </span>
																<?php } else if($is_aktif_ujian == 2){ ?>
																	<span style="float: right;"  class="cs_time">
																		<a href="<?= base_url('quiz/ujian/').$this->uri->segment('3').'/'.$this->uri->segment('4') ?>">Status : Open <i class="fa fa fa-paper-plane-o"></i></a>
																	</span>
																<?php } else { ?>
																	<span style="float: right;"  class="cs_time">
															        	Status : Close <i class="fa fa-window-close-o"></i>
															        </span>
																<?php } ?>
																</li>
												        	</ul>
												        </div>
												    </div>
											    </div>
											</div>
										</div>
									</div>
								</div>

								<div class="cs_row_five">
									<div class="student_feedback_container">
										<h4 class="aii_title">Penilaian Siswa</h4>
										<div class="s_feeback_content">
									        <ul class="skills">
									        	<li class="list-inline-item">
													<ul class="aii_rive_list mb0">
														<li class="list-inline-item"><i class="fa fa-star"></i></li>
														<li class="list-inline-item"><i class="fa fa-star"></i></li>
														<li class="list-inline-item"><i class="fa fa-star"></i></li>
														<li class="list-inline-item"><i class="fa fa-star"></i></li>
														<li class="list-inline-item"><i class="fa fa-star"></i></li>
													</ul>
												</li>
									            <li class="list-inline-item progressbar1" data-width="<?= $rating['detail'][4]['total'] ?>" data-target="100"><?= $rating['detail'][4]['total'] ?>%</li>
									        </ul>
									        <ul class="skills">
									        	<li class="list-inline-item">
													<ul class="aii_rive_list mb0">
														<li class="list-inline-item"><i class="fa fa-star"></i></li>
														<li class="list-inline-item"><i class="fa fa-star"></i></li>
														<li class="list-inline-item"><i class="fa fa-star"></i></li>
														<li class="list-inline-item"><i class="fa fa-star"></i></li>
													</ul>
												</li>
									            <li class="list-inline-item progressbar2" data-width="<?= $rating['detail'][3]['total'] ?>" data-target="100"><?= $rating['detail'][3]['total'] ?>%</li>
									        </ul>
									        <ul class="skills">
									        	<li class="list-inline-item">
													<ul class="aii_rive_list mb0">
														<li class="list-inline-item"><i class="fa fa-star"></i></li>
														<li class="list-inline-item"><i class="fa fa-star"></i></li>
														<li class="list-inline-item"><i class="fa fa-star"></i></li>
													</ul>
												</li>
									            <li class="list-inline-item progressbar3" data-width="<?= $rating['detail'][2]['total'] ?>" data-target="100"><?= $rating['detail'][2]['total'] ?>%</li>
									        </ul>
									        <ul class="skills">
									        	<li class="list-inline-item">
													<ul class="aii_rive_list mb0">
														<li class="list-inline-item"><i class="fa fa-star"></i></li>
														<li class="list-inline-item"><i class="fa fa-star"></i></li>
													</ul>
												</li>
									            <li class="list-inline-item progressbar4" data-width="<?= $rating['detail'][1]['total'] ?>" data-target="100"><?= $rating['detail'][1]['total'] ?>%</li>
									        </ul>
									        <ul class="skills">
									        	<li class="list-inline-item">
													<ul class="aii_rive_list mb0">
														<li class="list-inline-item"><i class="fa fa-star"></i></li>
													</ul>
												</li>
									            <li class="list-inline-item progressbar5" data-width="<?= $rating['detail'][0]['total'] ?>" data-target="100"><?= $rating['detail'][0]['total'] ?>%</li>
									        </ul>
										</div>
										<div class="aii_average_review text-center">
											<div class="av_content">
												<h2><?= round($rating['rata_rata'][0]['rerata'], 1) ?></h2>
												<ul class="aii_rive_list mb0">
													<li class="list-inline-item"><i class="fa fa-star"></i></li>
													<li class="list-inline-item"><i class="fa fa-star"></i></li>
													<li class="list-inline-item"><i class="fa fa-star"></i></li>
													<li class="list-inline-item"><i class="fa fa-star"></i></li>
													<li class="list-inline-item"><i class="fa fa-star"></i></li>
												</ul>
												<p>Rating Materi</p>
											</div>
										</div>
									</div>
								</div>

								<div class="cs_row_two">
									<div class="cs_overview">
										<h4 class="title">Profil Guru</h4>
										<div class="row">
										<?php foreach($guru_pic as $gr){?>
											<div class="col-sm-4 col-md-2" style="text-align:center;">
												<img src="<?= base_url().'e-lbm'.$gr['foto_profil']  ?>" width="100px" alt="Profile" class="img-thumbnail rounded-circle" /><br/>
												<b><?= $gr['nama'] ?></b>
											</div>
										<?php } ?>
											
										</div>
									</div>
								</div>

								
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xs" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Penilaian Materi Oleh Siswa</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form method="post" action="<?= base_url(); ?>rating/do_tambah/<?= $this->uri->segment(3).'/'.$this->uri->segment(4) ?>" >

						<div class="form-group">
							<div id="star-rating" width="100%">
								<input type="radio" name="rating_value" class="rating" value="1" />
								<input type="radio" name="rating_value" class="rating" value="2" />
								<input type="radio" name="rating_value" class="rating" value="3" />
								<input type="radio" name="rating_value" class="rating" value="4" />
								<input type="radio" name="rating_value" class="rating" value="5" />
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-xs-12 col-sm-12">
								<div class="clearfix">
									<textarea class="form-control" name="catatan" id="catatan" placeholder="Catatan Siswa" required></textarea>
								</div>
							</div>
						</div>

						<div class="form-group">
							<button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-sm btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>