<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quiz extends CI_Controller {

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
        $this->load->model("Materi_model");
        $this->load->model("Quiz_model");

        if($this->session->userdata('is_login') != true) {
            redirect(site_url('beranda'));
        }
    }

    public function latihan($id, $slug){
        $data['title'] = "Latihan Soal";
        $data['materi'] = $this->Materi_model->get_detail_materi($id, $slug);
        $this->load->view('quiz/header', $data);
        $this->load->view('quiz/index');
        $this->load->view('quiz/footer');
    }

    public function ujian($id, $slug){
        $data['title'] = "Ujian";
        $data['materi'] = $this->Materi_model->get_detail_materi($id, $slug);
        $this->load->view('quiz/header', $data);
        $this->load->view('quiz/index');
        $this->load->view('quiz/footer');
    }

    public function start($type, $id_siswa, $id_bab, $slug ){
        if($id_siswa != $this->session->userdata('detil_siswa')['id_siswa']){
            redirect(site_url('quiz/latihan/'.$id_bab.'/'.$slug));
        }

        $get_kelas_siswa = $this->Quiz_model->get_kelas_siswa($this->session->userdata('detil_siswa')['id_siswa']);

        $quiz = array(
            "jenis_quiz" => $type,
            "id_siswa" => $id_siswa,
            "id_bab" => $id_bab,
            "waktu_mulai" => date("Y-m-d H:i:s")
        );

        if($type == "ujian"){
            $quiz["tmp_waktu_kerjakan"] = date("Y-m-d H:i:s", strtotime("+20 minutes"));
        }
        
        $add_quiz = $this->Quiz_model->add_data_quiz($quiz);
        if($add_quiz){
            $get_soal = $this->Quiz_model->get_all_soal($type, $id_bab);

            $arr_id_soal = array();
            foreach ($get_soal as $s_key => $s_value) {
                array_push($arr_id_soal, $s_value['id']);
            }

            if(count($arr_id_soal) >= 10){
                $limit = 10;
            } else {
                $limit = count($arr_id_soal);
            }

            for($i=0;$i<$limit;$i++){
                $random_soal = array_rand($arr_id_soal,1);
                $soal['id_quiz'] = $add_quiz;
                $soal['id_soal'] = $arr_id_soal[$random_soal];
                $soal['urutan'] = ($i + 1);

                if($get_kelas_siswa){
                    $tingkatan = trim(strtoupper($get_kelas_siswa->tingkatan));            
        
                    if($tingkatan == "III/3 (TIGA)" || $tingkatan == "IV/4 (EMPAT)" || $tingkatan == "V/5 (LIMA)" || $tingkatan == "VI/6 (ENAM)"){
                        $jawaban = array(1,2,3,4);
                        $jawaban_string = array('a','b','c','d');
                    } else if($tingkatan == "I/1 (SATU)" || $tingkatan == "II/2 (DUA)"){
                        $jawaban = array(1,2,3);
                        $jawaban_string = array('a','b','c');
                    } else {
                        $jawaban = array(1,2,3,4,5);
                        $jawaban_string = array('a','b','c','d','e');
                    }
                }
                
                $tmp_jawaban = array();               
                for($j=0;$j<count($jawaban_string);$j++){
                    $random_jawaban = array_rand($jawaban,1);
                    $soal['jawaban_'.$jawaban_string[$j] ] = $jawaban[$random_jawaban];
                    array_push($tmp_jawaban, array("jawaban" => $jawaban_string[$j], "id" => $jawaban[$random_jawaban]));
                    unset($jawaban[$random_jawaban]);
                }

                $soal['jawaban_benar'] = $jawaban_string[$this->searchForId($get_soal[$random_soal]['jawaban_benar'], $tmp_jawaban)];
                $soal['jawaban_terpilih'] = 0;
                unset($arr_id_soal[$random_soal]);

                $data_soal[] = $soal;
            }

            $add_detail_quiz = $this->Quiz_model->add_data_detail_quiz($data_soal);
            if($add_detail_quiz){
                $get_soal_pertama = $this->Quiz_model->get_data_soal_pertama($add_quiz);
                redirect(site_url('quiz/kerjakan/'.$add_quiz.'/'.$get_soal_pertama[0]['id']));
            } else {
                redirect(site_url('quiz/latihan/'.$id_bab.'/'.$slug));
            }
        } else {
            redirect(site_url('quiz/latihan/'.$id_bab.'/'.$slug));
        }
       
    }

    public function kerjakan($id_quiz, $id_soal){
        $data["total_soal"] = $this->Quiz_model->get_total_soal($id_quiz);

        $get_kelas_siswa = $this->Quiz_model->get_kelas_siswa($this->session->userdata('detil_siswa')['id_siswa']);

        if($get_kelas_siswa){
            $tingkatan = trim(strtoupper($get_kelas_siswa->tingkatan));            

            if($tingkatan == "III/3 (TIGA)" || $tingkatan == "IV/4 (EMPAT)" || $tingkatan == "V/5 (LIMA)" || $tingkatan == "VI/6 (ENAM)"){
                $data['jawaban'] = array('a','b','c','d');
            } else if($tingkatan == "I/1 (SATU)" || $tingkatan == "II/2 (DUA)"){
                $data['jawaban'] = array('a','b','c');
            } else {
                $data['jawaban'] = array('a','b','c','d','e');
            }
        }

        $detail = $this->Quiz_model->get_data_quiz($id_quiz);

        if($detail[0]['jenis_quiz'] == "latihan"){
            $title = "Latihan Soal";
        } else {
            $title = "Ujian";
        }

        $data['title'] = $title;
        $data['quiz'] = $this->Quiz_model->get_data_quiz($id_quiz);
        $data['soal'] = $this->Quiz_model->get_soal_by_quiz($id_quiz);
        $data['detail_soal'] = $this->Quiz_model->get_detail_soal($id_quiz, $id_soal);

        $this->load->view('quiz/header', $data);
        $this->load->view('quiz/soal');
        $this->load->view('quiz/footer');
    }

    public function save(){
        $id = $this->input->post('id');
        $id_quiz = $this->input->post('id_quiz');
        $jawaban = $this->input->post('jawaban_terpilih');
        $submit = $this->input->post('submit');

        $detail = $this->Quiz_model->get_detail_soal($id_quiz, $id);

        $data = array("jawaban_terpilih" => $jawaban);
        $update = $this->Quiz_model->update_data_detail_quiz($data, $id_quiz, $id);

        if($submit == "stop"){
            $quiz = $this->Quiz_model->get_data_quiz($id_quiz);

            if($quiz[0]['waktu_selesai'] == "0000-00-00 00:00:00.000000"){ //JIKA QUIZ BELUM SELESAI MAKA SIMPAN WAKTU SELESAI
                $nilai_benar = $this->Quiz_model->get_nilai_quiz($id_quiz);
                $total_soal = $this->Quiz_model->get_total_soal($id_quiz);
    
                $nilai = (100 / $total_soal[0]['total']) * $nilai_benar[0]['total'];
                $data_update = array("waktu_selesai" => date("Y-m-d H:i:s"), "nilai" => $nilai);
                $update = $this->Quiz_model->update_data_quiz($data_update, $id_quiz);
            }

            redirect(site_url('quiz/berhenti/'.$id_quiz));
        } else {
            if($submit == "next"){
                $urutan = $detail[0]['urutan'] + 1;
            } else {
                $urutan = $detail[0]['urutan'] - 1;
            }

            $get_next_soal = $this->Quiz_model->get_soal_by_quiz($id_quiz, $urutan);
            if($update){
                redirect(site_url('quiz/kerjakan/'.$id_quiz.'/'.$get_next_soal[0]['id']));
            } else {
                redirect(site_url('quiz/kerjakan/'.$id_quiz.'/'.$id));
            }
        }
        
    }

    public function berhenti($id_quiz){
        $data['quiz'] = $this->Quiz_model->get_data_quiz($id_quiz);
        if($data['quiz'][0]['jenis_quiz'] == "latihan"){
            $title = "Latihan Soal";
        } else {
            $title = "Ujian";
        }

        $data['title'] = $title;

        $this->load->view('quiz/header', $data);
        $this->load->view('quiz/selesai');
        $this->load->view('quiz/footer');
    }

    function searchForId($id, $array) {
        foreach ($array as $key => $val) {
            if ($val['id'] == $id) {
                return $key;
            }
        }
        return null;
    }

}