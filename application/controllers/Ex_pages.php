<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ex_pages extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
	}

	public function index(){
		$data['title'] = "Page Other Example";

		$this->load->view('templating/header',$data);
		$this->load->view('samplepage/page_ex');
		$this->load->view('templating/footer');
	}

	// desain sample tempalting halaman depan

	public function list_materi(){
		$data['title'] = "Daftar Materi";

		$this->load->view('templating/header',$data);
		$this->load->view('samplepage/materi_list');
		$this->load->view('templating/footer');
	}

	public function detail_materi(){
		$data['title'] = "Detail Materi";

		$this->load->view('templating/header',$data);
		$this->load->view('samplepage/materi_detail');
		$this->load->view('templating/footer');
	}

	public function pengajar(){
		$data['title'] = "Daftar Pengajar";

		$this->load->view('templating/header',$data);
		$this->load->view('samplepage/pengajar_list');
		$this->load->view('templating/footer');
	}

	public function list_kegiatan(){
		$data['title'] = "Daftar Kegiatan";

		$this->load->view('templating/header',$data);
		$this->load->view('samplepage/kegiatan_list');
		$this->load->view('templating/footer');
	}

	public function detail_kegiatan(){
		$data['title'] = "Detail Kegiatan";

		$this->load->view('templating/header',$data);
		$this->load->view('samplepage/kegiatan_detail');
		$this->load->view('templating/footer');
	}

	public function list_blog(){
		$data['title'] = "Daftar Blog";

		$this->load->view('templating/header',$data);
		$this->load->view('samplepage/blog_list');
		$this->load->view('templating/footer');
	}

	public function detail_blog(){
		$data['title'] = "Detail Blog";

		$this->load->view('templating/header',$data);
		$this->load->view('samplepage/blog_detail');
		$this->load->view('templating/footer');
	}

	public function tentang_kami(){
		$data['title'] = "Tentang Kami";

		$this->load->view('templating/header',$data);
		$this->load->view('samplepage/tentang_kami');
		$this->load->view('templating/footer');
	}

	public function hubungi_kami(){
		$data['title'] = "Hubungi Kami";

		$this->load->view('templating/header',$data);
		$this->load->view('samplepage/hubungi_kami');
		$this->load->view('templating/footer');
	}

	public function faq(){
		$data['title'] = "FAQ (Frequently Asked Questions)";

		$this->load->view('templating/header',$data);
		$this->load->view('samplepage/faq');
		$this->load->view('templating/footer');
	}

	public function pages_other(){
		
	}

}

?>