<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rating extends CI_Controller {

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
        $this->load->model("Rating_model");

        if($this->session->userdata('is_login') != true) {
            redirect(site_url('beranda'));
        }
    }

    public function do_tambah($id_bab, $slug){
        $id_siswa = $this->session->userdata('detil_siswa')['id_siswa'];
        $rating_value = $this->input->post('rating_value');
        $catatan = $this->input->post('catatan');

        $data = array(
            "id_siswa" => $id_siswa,
            "id_bab" => $id_bab,
            "rating_value" => $rating_value,
            "catatan" => $catatan
        );

        $add = $this->Rating_model->add_data_rating($data);
        if($add){
            redirect(site_url('quiz/ujian/'.$id_bab.'/'.$slug));
        } else {
            redirect(site_url('materi/bab/'.$id_bab.'/'.$slug));
        }
    }
}