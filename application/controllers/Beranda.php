<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('beranda_model');
		$this->load->model('blog_model','blogpost_model');
	}

	public function index(){
		$data['title'] = "Beranda";

		$data['materi'] = $this->beranda_model->get_materi_depan(8);

		$data['mitra_sekolah'] = $this->beranda_model->get_mitra_sekolah();
		$data['blognew'] = $this->blogpost_model->get_blog_terbaru(2);
		$data['kegiatan'] = $this->beranda_model->get_kegiatan();
		$data['testimoni'] = $this->beranda_model->get_testimoni();

		$data['tk'] = sizeof($this->beranda_model->get_jml_mapel("Taman Kanak-kanak"));
		$data['sd'] = count($this->beranda_model->get_jml_mapel("Sekolah Dasar"));
		$data['smp'] = count($this->beranda_model->get_jml_mapel("Sekolah Menengah Pertama"));
		$data['sma'] = count($this->beranda_model->get_jml_mapel("Sekolah Menengah Atas"));

		$this->load->view('beranda/header_home',$data);
		$this->load->view('beranda/beranda_index');
		$this->load->view('beranda/footer-home');
		$this->load->view('templating/footer');
	}

	

}

?>