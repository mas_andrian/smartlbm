<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Kegiatan extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');

        // Load Pagination library
		$this->load->library('pagination');
		$this->load->model('kegiatan_model','k_model');
	}

	public function index()
	{
		redirect('kegiatan/list');
	}

	public function list($rowno=0)
	{
		// Search text
		//print_r($this->input->post('search'));
		//exit();

		$search_text = "";
		if($this->input->post('search') != NULL ){
			$search_text = $this->input->post('search');
			$this->session->set_userdata(array("search"=>$search_text));

		}else{
			if($this->session->userdata('search') != NULL){
				$search_text = $this->session->userdata('search');
			}
		}

		// Row per page
		$rowperpage = 10;

		// Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
      	
      	// All records count
      	$allcount = $this->k_model->getrecordCount($search_text);

      	// Get  records
      	$users_record = $this->k_model->getData($rowno,$rowperpage,$search_text);
      	
      	// Pagination Configuration
      	$config['base_url'] = base_url().'/kegiatan/list';
      	$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $allcount;
		$config['per_page'] = $rowperpage;

		
		$config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
 
        $this->pagination->initialize($config); 
		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();
		$data['result'] = $users_record;
		$data['row'] = $rowno;
		$data['search'] = $search_text;

		$data['title'] = "Kegiatan"; 

		$this->load->view('templating/header',$data);
		$this->load->view('kegiatan/kegiatan_index');
		$this->load->view('templating/footer');
	}

	public function detail($slug)
	{
		$data['title'] = "Detail Kegiatan";

		$data['kegiatan'] = $this->k_model->get_kegiatan_detail($slug);
		

		$this->load->view('templating/header',$data);
		$this->load->view('kegiatan/kegiatan_detail');
		$this->load->view('templating/footer');
	}


}

?>