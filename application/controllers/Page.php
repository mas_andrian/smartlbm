<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('page_model');
	}

	public function tentang_kami(){
		$data['title'] = "Tentang Kami";

		$data['mitra'] = count($this->page_model->get_mitra_sekolah());
		$data['guru'] = count($this->page_model->get_pengajar());
		$data['siswa'] = count($this->page_model->get_siswa());
		$data['materi'] = count($this->page_model->get_materi());
		$data['testimoni'] = $this->page_model->get_testimoni();

		$this->load->view('templating/header',$data);
		$this->load->view('page/tentang_kami');
		$this->load->view('templating/footer');
	}

	public function hubungi_kami(){
		$data['title'] = "Hubungi Kami";

		$this->load->view('templating/header',$data);
		$this->load->view('page/hubungi_kami');
		$this->load->view('templating/footer');
	}

	public function faq(){
		$data['title'] = "FAQ (Frequently Asked Questions)";

		$data['faq'] = $this->page_model->get_faq();

		$this->load->view('templating/header',$data);
		$this->load->view('page/faq');
		$this->load->view('templating/footer');
	}

	public function pendidikan($jenjang){
		$data['title'] = "Materi Jenjang Pendidikan";

		if($jenjang == "sd"){
			$jenjang_ = "Sekolah Dasar";
		} else if($jenjang == "smp"){
			$jenjang_ = "Sekolah Menengah Pertama";
		} else if($jenjang == "sma"){
			$jenjang_ = "Sekolah Menengah Atas";
		} else if($jenjang == "tk"){
			$jenjang_ = "Taman Kanak-kanak";
		} else if($jenjang == "ponpes"){
			$jenjang_ = "Pondok Pesantren";
		} 
		$data['subtitle'] = $jenjang_;
		$data['materi'] = $this->page_model->get_materi_list('jenjang', $jenjang_, 40);

		$this->load->view('templating/header',$data);
		$this->load->view('page/list_materi');
		$this->load->view('templating/footer');
	}

	public function muatan($tipe){
		$data['title'] = "Materi Muatan Pembelajaran";

		if($tipe == "wajib"){
			$tipe_ = "Muatan Wajib";
		} else if($tipe == "lokal"){
			$tipe_ = "Muatan Lokal";
		} else if($tipe == "khusus"){
			$tipe_ = "Muatan Khusus";
		} else if($tipe == "pengembangan"){
			$tipe_ = "Pengembangan Diri";
		} 
		$data['subtitle'] = $tipe_;
		$data['materi'] = $this->page_model->get_materi_list('muatan', $tipe_, 20);

		$this->load->view('templating/header',$data);
		$this->load->view('page/list_materi_v2');
		$this->load->view('templating/footer');
	}

	public function search(){
		$q = $this->input->get("q");
		$data["title"] = "Pencarian";
		$data["subtitle"] = $q;
		$data["materi"] = $this->page_model->get_materi_list('search', $q, 20);
		// echo $this->db->last_query();exit();
		$this->load->view('templating/header',$data);
		$this->load->view('page/list_materi_v2');
		$this->load->view('templating/footer');
	}

}

?>