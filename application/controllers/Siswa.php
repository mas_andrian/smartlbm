<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		if($this->session->userdata('is_login') != true) {
			redirect(site_url('beranda'));
		}

		$this->load->model('siswa_model');

	} 

	public function index(){
		$data['title'] = "Profil Siswa";

		$data['siswa'] = $this->session->userdata('detil_siswa');
		$id = $data['siswa']['id_siswa'];

		$data['presensi'] = $this->siswa_model->get_dokumen('presensi', $id);
		$data['raport'] = $this->siswa_model->get_dokumen('raport', $id);

		// pengambilan data materi pembelajaran
		$list [] = array();
		$data['materi'] = $this->siswa_model->get_kelas_mapel($data['siswa']['id_sekolah'], $data['siswa']['tingkatan']);
		$a = 0;
		foreach ($data['materi'] as $key) {
			$data['bab'] = $this->siswa_model->get_bab_mapel($key['id']);
			$list [$a]['materi'] = $key;
			$list [$a]['list_bab'] = $data['bab'];
			$a++;
		}
		$data['materi'] = $list;
		
		$data['log'] = $this->siswa_model->get_log_akses_materi($id);

		$this->load->view('templating/header',$data);
		$this->load->view('siswa/siswa_profil');
		$this->load->view('templating/footer');
	}

	public function do_edit_foto_profil($id){
		$this->load->library('form_validation');
		$this->load->helper('url');
		
		$data['user'] = $this->siswa_model->get_siswa_by_id($id);
		
		unlink(".".$data['user']['foto_diri']);

		$folder = "./e-lbm/assets/foto_siswa/";
		$path = $_FILES['foto']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);

		$file_path = $folder.$data['user']['nis'].".".$ext;
		move_uploaded_file($_FILES['foto']['tmp_name'], $file_path);
		$path = "/assets/foto_siswa/".$data['user']['nis'].".".$ext;

		$this->siswa_model->set_single_data_siswa('foto_diri', $path, $id);
		$this->session->set_userdata('status_foto_siswa', 'sukses');
		 $this->session->set_userdata('detil_siswa', $data['user'] );

		$url = "siswa";
		redirect(site_url($url));
	}
	

}

?>