<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materi extends CI_Controller {

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
        $this->load->model("Materi_model");
        $this->load->model("Log_model");
        $this->load->model("Rating_model");
        $this->load->model("Quiz_model");

        if($this->session->userdata('is_login') != true) {
            redirect(site_url('beranda'));
        }
    }

    public function bab($id, $slug){
        $log = $this->save_log($this->session->userdata("id_siswa"), "bab", $id);
        $data['title'] = "Detail Materi";
        $data['detail_materi'] = $this->Materi_model->get_detail_materi($id, $slug);
        $data['detail_sub_bab'] = $this->Materi_model->get_list_sub_bab_materi($id);
        $data['guru_pic'] = $this->Materi_model->get_guru_pic($data['detail_materi'][0]['guru_penanggung_jawab']);

        // LOG + LATIHAN
        $temp_id_sub_bab = array();
        if($data['detail_sub_bab']){
            foreach($data['detail_sub_bab'] as $dt_sb){
                array_push($temp_id_sub_bab, $dt_sb['id']);
            }
        }
        

        $is_aktif_latihan = 0;
        $get_log_akses = $this->Log_model->get_log_sub_bab_by_siswa($this->session->userdata("id_siswa"), $temp_id_sub_bab);
        if(count($get_log_akses) >= count($temp_id_sub_bab)){ //JIKA LOG TERSIMPAN == TOTAL SUB BAB MAKA DINYATAKAN TELAH MEMBUKA SEMUA SUB BAB
            $is_aktif_latihan = 1;
        }

        $is_aktif_ujian = 0;
        $get_latihan_siswa = $this->Quiz_model->get_latihan_bab_siswa($this->session->userdata("id_siswa") ,$id);
        if($get_latihan_siswa){
            
            $get_rating_siswa = $this->Rating_model->get_review_siswa_for_bab($this->session->userdata("id_siswa"), $id);
            if($get_rating_siswa){
                $get_ujian_bab_by_siswa = $this->Quiz_model->get_ujian_bab_siswa($this->session->userdata("id_siswa"), $id);
                if($get_ujian_bab_by_siswa){
                    $is_aktif_latihan = 0;
                    $is_aktif_ujian = 0; //TELAH SELESAI UJIAN
                } else {
                    $is_aktif_ujian = 2; // SIAP MENGIKUTI UJIAN
                }
            } else {
                $is_aktif_ujian = 1; // BELUM ISI REVIEW
            }
        } else {
            $is_aktif_ujian = 0;
        }
            
        $data["is_aktif_latihan"] = $is_aktif_latihan;
        $data["is_aktif_ujian"] = $is_aktif_ujian;
        // END LOG + LATIHAN

        // RATING
        $get_rating = $this->Rating_model->get_rating_by_bab($id);
        $get_rating_star = $this->Rating_model->get_rating_by_star($id);
        $get_rating_rerata = $this->Rating_model->get_rating_by_bab_rerata($id);


        for($s=1;$s<=5;$s++){
            $rating["rating"] = $s;
            if($get_rating_star){
                foreach($get_rating_star as $rt_star){
                    $s_val = 0;
                    if($s == $rt_star['rating_value']){
                        $s_val = round(($rt_star['total'] / count($get_rating)) * 100);
                    }
                }
            } else {
                $s_val = 0;
            }
            
            $rating["total"] = $s_val;

            $data_rating[] = $rating;
        }
        $data["rating"] = array("total_review" => count($get_rating), "rata_rata" => $get_rating_rerata, "detail" => $data_rating);
        // END RATING

		$this->load->view('templating/header', $data);
		$this->load->view('materi/materi_detail');
		$this->load->view('templating/footer');
    }
    
    public function sub_bab($id, $slug){
        $log = $this->save_log($this->session->userdata("id_siswa"), "sub_bab", $id);
        $data['title'] = "Preview Materi";
        $data['detail_materi'] = $this->Materi_model->get_detail_sub_materi($id, $slug);
        $data['bab'] = $this->Materi_model->get_list_bab_materi($data['detail_materi'][0]['id_materi']);
        $a = 0;
        $list [] = array();
        foreach($data["bab"] as $key){
            $data['sub_bab'] = $this->Materi_model->get_list_sub_bab_materi($key['id']);
            $list [$a]['bab'] = $key;
			$list [$a]['list_sub_bab'] = $data['sub_bab'];
			$a++;
        }
        $data['bab'] = $list;

		$this->load->view('templating/header', $data);
		$this->load->view('materi/materi_preview');
		$this->load->view('templating/footer');
    }
    
    public function save_log($id_siswa, $kategori, $id_materi){
        if($this->session->userdata("is_login") != 1){
            return false;
        }

        $get_exist = $this->Log_model->get_log_materi($id_siswa, $kategori, $id_materi);
        if($get_exist){
            $data = array(
                "tgl_akses" => date("Y-m-d H:i:s")
            );
            $update_log = $this->Log_model->update_log_materi($data, $get_exist[0]["id"]);
        } else {
            $data = array(
                "id_siswa" => $id_siswa,
                "kategori" => $kategori,
                "id_materi" => $id_materi,
                "tgl_akses" => date("Y-m-d H:i:s")
            );

            $add_log = $this->Log_model->add_log_materi($data);
        }
    }
}