<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller {

	public function __construct() {
		parent::__construct();
        $this->load->library('session');
        $this->load->model("Nilai_model");

        if($this->session->userdata('is_login') != true) {
            redirect(site_url('beranda'));
        }
    }

    public function get_nilai_siswa($id_siswa, $id_bab){
        $latihan = $this->Nilai_model->get_nilai_siswa($id_siswa, $id_bab, 'latihan');
        $ujian = $this->Nilai_model->get_nilai_siswa($id_siswa, $id_bab, 'ujian');

        $data_latihan = array();
        if($latihan){
            $data_latihan = $latihan;
        }

        $data_ujian = array();
        if($ujian){
            $data_ujian = $ujian;
        }

        $data = array("latihan" => $data_latihan, "ujian" => $data_ujian);
        echo json_encode($data);
    }
}