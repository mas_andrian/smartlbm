<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('login_model');
	}

	public function do_login(){
		if($this->session->userdata('is_login') == true) {
	        redirect(site_url('beranda'));
	    }

	    $this->load->library('form_validation');
	    $this->load->helper('url');

	    $data['user'] = $this->login_model->get_siswa($this->input->post('username'));
	    //password_verify(md5($this->input->post('password')), $data['user']['password']) and
	    if( $data['user']['is_aktif'] == "Y") {
	    	$this->session->set_userdata('is_login', true);

	    	if(empty($this->input->post('is_ortu'))){
	    		$this->session->set_userdata('role', 'Siswa');
	    	} else {
	    		$this->session->set_userdata('role', 'Orang Tua / Wali Siswa');
	    	}

	        $this->session->set_userdata('id_siswa', $data['user']['id_siswa'] );
	        $this->session->set_userdata('detil_siswa', $data['user'] );

	        $this->session->set_userdata('feedback', '<b>Login Berhasil</b>. Selamat datang kembali.');
	        redirect(site_url('siswa'));
	    } else {
	    	if ($data['user']['is_aktif'] == "N") {
	    		$this->session->set_userdata('feedback', '<b>Login Ditolak</b>. Akun anda telah dinonaktifkan!');
	    	} else {
	        	$this->session->set_userdata('feedback', '<b>Login Gagal</b>. Silakan coba lagi, pastikan username dan password anda sesuai!');
	    	}
	    	redirect(site_url('beranda'));
	    }

	}

	public function do_logout(){
		$this->session->sess_destroy();
		$this->session->set_userdata('feedback', '<b>Logout Berhasil</b>. Terima Kasih.');
	    redirect(site_url('beranda'));
	}

}

?>