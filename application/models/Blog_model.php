<?php
date_default_timezone_set('Asia/Jakarta'); 
class Blog_model extends CI_Model {
 
    public function __construct()
    {
        $this->load->database();
    }

    public function get_blog_terbaru($limit) {
        $this->db->select('*');
        $this->db->from('blogpost');
        $this->db->limit($limit, 0);  
        $this->db->order_by("time_at", "desc");
        $query = $this->db->get();

        return $query->result_array();
    }

    public function get_blog_kategori($kategori) {
        $this->db->order_by("time_at", "desc");
        $query = $this->db->get_where('blogpost', array('kategori' => $kategori));
        return $query->result_array();
    }

    public function get_blog_detail($slug) {
        $query = $this->db->get_where('blogpost', array('flag' => $slug));
        return $query->row_array();
    }

    public function getData($rowno,$rowperpage,$search="") {
            
        $this->db->select('*');
        $this->db->from('blogpost');

        if($search != ''){
           $this->db->like('judul', $search);
           $this->db->or_like('deskripsi', $search);
           $this->db->or_like('kategori', $search);
        }
        $this->db->where("kategori != ", "pemberitahuan");
        $this->db->limit($rowperpage, $rowno);  
        $this->db->order_by("time_at", "desc");
        $query = $this->db->get();
        
        return $query->result_array();
    }

    // Select total records
    public function getrecordCount($search = '') {

        $this->db->select('count(*) as allcount');
        $this->db->from('blogpost');
      
        if($search != ''){
            $this->db->like('judul', $search);
            $this->db->or_like('deskripsi', $search);
            $this->db->or_like('kategori', $search);
        }
         $this->db->where("kategori != ", "pemberitahuan");
        $query = $this->db->get();
        $result = $query->result_array();
      
        return $result[0]['allcount'];
    }

    public function get_datablog(){
        $query = $this->db->query('SELECT kategori, count(id_blog) as jumlah from blogpost group by kategori');
        return $query->result_array();
    }

}

?>