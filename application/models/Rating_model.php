<?php
date_default_timezone_set('Asia/Jakarta');

class Rating_model extends CI_Model {
 
    public function __construct()
    {
        $this->load->database();
    }

    public function add_data_rating($data){
        $this->db->insert("rating", $data);
        if($this->db->affected_rows() > 0){
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function get_rating_by_bab($id_bab){
        $this->db->select("*");
        $this->db->from("rating");
        $this->db->where("id_bab", $id_bab);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function get_rating_by_star($id_bab){
        $this->db->select("id_bab, rating_value, count(rating_value) as total");
        $this->db->from("rating");
        $this->db->where("id_bab", $id_bab);
        $this->db->group_by("rating_value");
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_rating_by_bab_rerata($id_bab){
        $this->db->select("id_bab, AVG(rating_value) as rerata");
        $this->db->from("rating");
        $this->db->where("id_bab", $id_bab);
        $this->db->group_by("id_bab");
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return array(array("id_bab" => $id_bab, "rerata" => 0));
        }
    }

    public function get_review_siswa_for_bab($id_siswa, $id_bab){
        $this->db->select("id");
        $this->db->from("rating");
        $this->db->where("id_bab", $id_bab);
        $this->db->where("id_siswa", $id_siswa);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }
}