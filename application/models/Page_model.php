<?php
date_default_timezone_set('Asia/Jakarta');

class Page_model extends CI_Model {
 
    public function __construct()
    {
        $this->load->database();
    }

   public function get_testimoni(){
   		$query = $this->db->query('select * from testimoni order by id_testimoni ASC');
        return $query->result_array();
    }

    public function get_mitra_sekolah(){
   		$query = $this->db->query('select * from profil_sekolah where id > 1 order by jenjang ASC');
        return $query->result_array();
    }

    public function get_pengajar(){
   		$query = $this->db->query('select * from pengguna where progli_ampu != "0"');
        return $query->result_array();
    }

    public function get_siswa(){
   		$query = $this->db->query('select * from siswa where is_aktif = "Y"');
        return $query->result_array();
    }

     public function get_materi(){
   		$query = $this->db->query('select * from materi');
        return $query->result_array();
   }

   public function get_faq(){
   		$query = $this->db->query('select * from faq');
        return $query->result_array();
   }

  public function get_materi_list($aksi, $request, $limit){
      if($aksi == 'jenjang'){
        $whereing = " where mata_pelajaran.jenjang = '$request' ";
      } else if($aksi == 'search'){
        $ex_q = explode(" ", $request);
        $whereing = " where ( ";
        $counter = 1;
        foreach($ex_q as $q){
          $whereing .= "materi_bab.judul like '%".$q."%' OR materi_bab.deskripsi like '%".$q."%'";
          if($counter != count($ex_q)){
            $whereing .= " OR ";
          }
          $counter++;
        }
        $whereing .= ")";
      } else {
        $whereing = " where mata_pelajaran.kategori = '$request' ";
      }

      $query = $this->db->query("SELECT materi.tingkatan, materi_bab.*, mata_pelajaran.nama_mapel, mata_pelajaran.jenjang, mata_pelajaran.kategori, round(rating.avg_rating, 1) as avg_rating FROM materi_bab JOIN materi ON materi_bab.id_materi = materi.id JOIN mata_pelajaran ON materi.id_mata_pelajaran = mata_pelajaran.id LEFT JOIN ( SELECT id_bab, avg( rating_value ) AS avg_rating FROM rating GROUP BY id_bab ) rating ON materi_bab.id = rating.id_bab $whereing ORDER BY avg_rating DESC LIMIT ". $limit);
      return $query->result_array();
   }

}

?>