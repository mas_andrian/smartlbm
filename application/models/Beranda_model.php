<?php 
date_default_timezone_set('Asia/Jakarta');

class Beranda_model extends CI_Model {
 
    public function __construct()
    {
        $this->load->database();
    }

   public function get_mitra_sekolah(){
   		$query = $this->db->query('select *,(select count(*) from pengguna where pengguna.id_sekolah = profil_sekolah.id) as jml_guru, (select count(*) from materi where materi.id_sekolah = profil_sekolah.id) as jml_materi from profil_sekolah where id > 1 order by jenjang ASC');
        return $query->result_array();
   }

   public function get_kegiatan(){
   		$query = $this->db->query('select * from kegiatan order by tanggal desc LIMIT 3');
        return $query->result_array();
   }

   public function get_testimoni(){
   		$query = $this->db->query('select * from testimoni order by id_testimoni desc LIMIT 15');
        return $query->result_array();
   }

   public function get_jml_mapel($jenjang){
   		$query = $this->db->query("select * from mata_pelajaran where jenjang = '$jenjang'");
        return $query->result_array();
   }

   public function get_materi_depan($limit){
      $query = $this->db->query('SELECT materi.tingkatan, materi_bab.*, mata_pelajaran.nama_mapel, mata_pelajaran.jenjang, mata_pelajaran.kategori, round(rating.avg_rating, 1) as avg_rating FROM materi_bab JOIN materi ON materi_bab.id_materi = materi.id JOIN mata_pelajaran ON materi.id_mata_pelajaran = mata_pelajaran.id LEFT JOIN ( SELECT id_bab, avg( rating_value ) AS avg_rating FROM rating GROUP BY id_bab ) rating ON materi_bab.id = rating.id_bab ORDER BY avg_rating DESC LIMIT '. $limit);
      return $query->result_array();
   }


}

?>