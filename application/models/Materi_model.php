<?php
date_default_timezone_set('Asia/Jakarta');

class Materi_model extends CI_Model {
 
    public function __construct()
    {
        $this->load->database();
    }

    public function get_detail_materi($id, $slug){
        $this->db->select("materi_bab.*, materi.guru_penanggung_jawab, mata_pelajaran.nama_mapel");
        $this->db->from("materi_bab");
        $this->db->join("materi", "materi_bab.id_materi = materi.id");
        $this->db->join("mata_pelajaran", "materi.id_mata_pelajaran = mata_pelajaran.id");
        $this->db->where(array("materi_bab.id" => $id, "materi_bab.slug" => $slug));
        $this->db->where("materi_bab.is_aktif", "Y");

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_detail_sub_materi($id, $slug){
        $this->db->select("materi_bab.id as id_bab, materi_bab.slug as slug_bab, materi_sub_bab.*, materi_bab.urutan as bab_urutan, materi_bab.judul as bab_judul, materi_bab.id_materi");
        $this->db->from("materi_sub_bab");
        $this->db->join("materi_bab", "materi_sub_bab.id_bab = materi_bab.id");
        $this->db->where(array("materi_sub_bab.id" => $id, "materi_sub_bab.slug" => $slug));
        $this->db->where("materi_sub_bab.is_aktif", "Y");

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_list_bab_materi($id_materi){
        $this->db->select("materi_bab.*");
        $this->db->from("materi_bab");
        $this->db->where(array("id_materi" => $id_materi));
        $this->db->where("is_aktif", "Y");
        $this->db->order_by("urutan", "asc");

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_list_sub_bab_materi($id_bab){
        $this->db->select("materi_sub_bab.*, log_aktifitas_materi.id as id_log");
        $this->db->from("materi_sub_bab");
        $this->db->join("log_aktifitas_materi", "materi_sub_bab.id = log_aktifitas_materi.id_materi and log_aktifitas_materi.kategori = 'sub_bab' and log_aktifitas_materi.id_siswa = '".$this->session->userdata("id_siswa")."'", "left");
        $this->db->where(array("materi_sub_bab.id_bab" => $id_bab));
        $this->db->order_by("materi_sub_bab.urutan", "asc");

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }
    

    public function get_guru_pic($guru){
        $guru = explode(",", $guru);
        unset($guru[count($guru) - 1]);
        unset($guru[0]);

        $this->db->select("nama, foto_profil");
        $this->db->from("pengguna");
        $this->db->where_in("id", $guru);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }
}