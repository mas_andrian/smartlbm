<?php
date_default_timezone_set('Asia/Jakarta'); 
class Kegiatan_model extends CI_Model {
 
    public function __construct()
    {
        $this->load->database();
    }

     public function get_kegiatan_detail($slug) {
        $query = $this->db->get_where('kegiatan', array('slug' => $slug));
        return $query->row_array();
    }

    public function getData($rowno,$rowperpage,$search="") {
            
        $this->db->select('*');
        $this->db->from('kegiatan');

        if($search != ''){
           $this->db->like('judul', $search);
           $this->db->or_like('deskripsi', $search);
           $this->db->or_like('lokasi', $search);
        }
        
        $this->db->limit($rowperpage, $rowno);  
        $this->db->order_by("tanggal", "desc");
        $query = $this->db->get();
        
        return $query->result_array();
    }

    // Select total records
    public function getrecordCount($search = '') {

        $this->db->select('count(*) as allcount');
        $this->db->from('kegiatan');
      
        if($search != ''){
            $this->db->like('judul', $search);
            $this->db->or_like('deskripsi', $search);
            $this->db->or_like('lokasi', $search);
        }
        
        $query = $this->db->get();
        $result = $query->result_array();
      
        return $result[0]['allcount'];
    }


}

?>