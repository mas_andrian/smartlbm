<?php
date_default_timezone_set('Asia/Jakarta');

class Quiz_model extends CI_Model {
 
    public function __construct()
    {
        $this->load->database();
    }

    public function get_all_soal($type, $id_bab){
        $this->db->select("*");
        $this->db->from("bank_soal");
        $this->db->where("jenis_soal", $type);
        $this->db->where("id_bab", $id_bab);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function add_data_quiz($data){
        $this->db->insert("quiz", $data);
        if($this->db->affected_rows() > 0){
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function update_data_quiz($data, $id){
        $this->db->where('id', $id)->update("quiz", $data);
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function get_data_quiz($id = null){
        if(isset($id)){
            $this->db->where("id", $id);
        }
        $this->db->select("*");
        $this->db->from("quiz");

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function update_data_detail_quiz($data, $id_quiz, $id){
        $this->db->where("id", $id)->where("id_quiz", $id_quiz)->update("quiz_detail", $data);
        return true;
    }

    public function add_data_detail_quiz($data){
        $this->db->insert_batch("quiz_detail", $data);
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function get_data_soal_pertama($id_quiz){
        $this->db->select("*");
        $this->db->from("quiz_detail");
        $this->db->where("id_quiz", $id_quiz);
        $this->db->where("urutan", 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_soal_by_quiz($id_quiz, $urutan = null){
        if(isset($urutan)){
            $this->db->where("urutan", $urutan);
        }
        $this->db->select("id, id_quiz, urutan, jawaban_terpilih");
        $this->db->from("quiz_detail");
        $this->db->where("id_quiz", $id_quiz);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_detail_soal($id_quiz, $id_dtl_quiz){
        $this->db->select("quiz_detail.id, quiz_detail.urutan,
        quiz_detail.jawaban_a, quiz_detail.jawaban_b, quiz_detail.jawaban_c, quiz_detail.jawaban_d, quiz_detail.jawaban_e, quiz_detail.jawaban_terpilih,
        bank_soal.pertanyaan, bank_soal.jawaban_1, bank_soal.jawaban_2, bank_soal.jawaban_3, bank_soal.jawaban_4, bank_soal.jawaban_5");
        $this->db->from("quiz_detail");
        $this->db->join("bank_soal", "quiz_detail.id_soal = bank_soal.id");
        $this->db->where("quiz_detail.id", $id_dtl_quiz);
        $this->db->where("quiz_detail.id_quiz", $id_quiz);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_nilai_quiz($id_quiz){
        $this->db->select("count(id) as total");
        $this->db->from("quiz_detail");
        $this->db->where("id_quiz", $id_quiz);
        $this->db->where("jawaban_benar = jawaban_terpilih");
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_total_soal($id_quiz){
        $this->db->select("count(id) as total");
        $this->db->from("quiz_detail");
        $this->db->where("id_quiz", $id_quiz);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_latihan_bab_siswa($id_siswa, $id_bab){
        $this->db->select("id");
        $this->db->from("quiz");
        $this->db->where("id_siswa", $id_siswa);
        $this->db->where("id_bab", $id_bab);
        $this->db->where("jenis_quiz", "latihan");
        $this->db->where("waktu_selesai != ","0000-00-00 00:00:00.000000");
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_ujian_bab_siswa($id_siswa, $id_bab){
        $this->db->select("id");
        $this->db->from("quiz");
        $this->db->where("id_siswa", $id_siswa);
        $this->db->where("id_bab", $id_bab);
        $this->db->where("jenis_quiz", "ujian");
        $this->db->where("waktu_selesai != ","0000-00-00 00:00:00.000000");
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_kelas_siswa($id_siswa){
        $this->db->select("tingkatan");
        $this->db->from("siswa");
        $this->db->where("id_siswa", $id_siswa);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        } else {
            return false;
        }
    }
}