<?php
date_default_timezone_set('Asia/Jakarta');

class Siswa_model extends CI_Model {
 
    public function __construct()
    {
        $this->load->database();
    }

    public function get_dokumen($tipe, $id_siswa){
        $query = $this->db->query("select * from dokumen_siswa where id_siswa = ".$id_siswa." and tipe = '".$tipe."' order by tahun_ajaran DESC");
        return $query->result_array();
    }

    public function get_kelas_mapel($id_sekolah, $tingkatan){
    	$query = $this->db->query('select a.*, b.nama_sekolah, c.nama_mapel from materi a join profil_sekolah b on a.id_sekolah = b.id join mata_pelajaran c on a.id_mata_pelajaran = c.id where a.id_sekolah = '.$id_sekolah.' and tingkatan = "'.$tingkatan.'"');
        return $query->result_array();
    }

    public function get_bab_mapel($id_materi){
    	$query = $this->db->query('SELECT materi_bab.*, mata_pelajaran.nama_mapel FROM materi_bab JOIN materi ON materi_bab.id_materi = materi.id JOIN mata_pelajaran ON materi.id_mata_pelajaran = mata_pelajaran.id where materi_bab.id_materi = '.$id_materi.'  ORDER BY urutan ASC');
    	return $query->result_array();
    }

    public function get_siswa_by_id($id){
        $query = $this->db->query('select s.*, ps.nama_sekolah from siswa s join profil_sekolah ps on s.id_sekolah = ps.id where s.id_siswa = '.$id);
        return $query->row_array();
    }

    public function set_single_data_siswa($field, $data, $id){
        $this->load->helper('url');
        $data = array(
            $field => $data,
        );
        $this->db->where('id_siswa', $id);
        return $this->db->update('siswa', $data);
    }

    public function get_log_akses_materi($id_siswa){
        $query =  $this->db->query("
            SELECT msb.judul as sub, mb.judul as bab, mp.nama_mapel, m.tingkatan, DATE_FORMAT(lam.tgl_akses , '%H:%i:%s %d/%m/%Y') AS 'akses'
            FROM log_aktifitas_materi lam 
                left join materi_sub_bab msb on msb.id = lam.id_materi
                left join materi_bab mb on mb.id = msb.id_bab
                left join materi m on m.id = mb.id_materi
                left join mata_pelajaran mp on mp.id = m.id_mata_pelajaran
            WHERE lam.kategori = 'sub_bab' and lam.id_siswa = $id_siswa
            ORDER BY lam.tgl_akses desc
        ");
        return $query->result_array();
    }

}

?>