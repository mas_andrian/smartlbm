<?php
date_default_timezone_set('Asia/Jakarta');

class Log_model extends CI_Model {
 
    public function __construct()
    {
        $this->load->database();
    }

    public function get_log_materi($id_siswa, $kategori, $id_materi){
        $this->db->select("*");
        $this->db->from("log_aktifitas_materi");
        $this->db->where("id_siswa", $id_siswa);
        $this->db->where("kategori", $kategori);
        $this->db->where("id_materi", $id_materi);

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function add_log_materi($data){
        $this->db->insert("log_aktifitas_materi", $data);
    }

    public function update_log_materi($data, $id){
        $this->db->where("id", $id)->update("log_aktifitas_materi", $data);
    }

    public function get_log_sub_bab_by_siswa($id_siswa, $id_materi){
        $this->db->select("*");
        $this->db->from("log_aktifitas_materi");
        $this->db->where("id_siswa", $id_siswa);
        $this->db->where("kategori", "sub_bab");
        $this->db->where_in("id_materi", $id_materi);

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return array();
        }
    }
}