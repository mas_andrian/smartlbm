<?php
date_default_timezone_set('Asia/Jakarta');

class Nilai_model extends CI_Model {
 
    public function __construct()
    {
        $this->load->database();
    }

    public function get_nilai_siswa($id_siswa, $id_bab, $type){
        $this->db->select("*");
        $this->db->from("quiz");
        $this->db->where("jenis_quiz", $type);
        $this->db->where("id_bab", $id_bab);
        $this->db->where("id_siswa", $id_siswa);
        $this->db->where("waktu_selesai != ","0000-00-00 00:00:00.000000");
        $this->db->order_by("id", "asc");
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }
}