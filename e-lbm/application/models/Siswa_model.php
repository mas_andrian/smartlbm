<?php
class Siswa_Model extends CI_Model {

    public $data;

    public function __construct() {
        $this->load->database();
        $this->load->helper('url');
    }

     public function cek_data_sudah_ada($tabel, $field, $data){
        $query = $this->db->get_where($tabel, array($field => $data));
        return $query->row_array();
    }

    private function enkripsi($password){
        $enskrip_md5 = md5($password);
        $enkrip_bcrytp = password_hash($enskrip_md5, PASSWORD_BCRYPT);
        return $enkrip_bcrytp;
    }

    public function insert_siswa($path, $password){
        $this->load->helper('url');
        $data = array(
            'nis' => $this->input->post('nis'),
            'nama' => $this->input->post('nama'),
            'flag' =>  url_title($this->input->post('nama'), 'dash', TRUE),
            'tingkatan' => $this->input->post('tingkatan'),
            'kelas' => $this->input->post('kelas'),
            'jk' => $this->input->post('j_kelamin'),
            'tmp_lahir' => $this->input->post('tmp_lahir'),
            'tgl_lahir' => $this->input->post('tgl_lahir'),
            'nama_wali' => $this->input->post('nm_wali'),
            'telp_wali' => $this->input->post('telp_wali'),
            'alamat' => $this->input->post('alamat'),
            'foto_diri' => $path,
            'email' => $this->input->post('email'),
            'username' => $this->input->post('email'),
            'password' => $this->enkripsi($password),
            'is_aktif' => 'Y',
            'id_sekolah' => $this->session->userdata('id_sekolah'),
        );
        return $this->db->insert('siswa', $data);
    }

    public function data_siswa($id_sekolah = false){
    	if($id_sekolah == 0){
    		$query = $this->db->query('select s.*, ps.nama_sekolah from siswa s join profil_sekolah ps on s.id_sekolah = ps.id order by nama');
    	} else{
    		$query = $this->db->query('select s.*, ps.nama_sekolah from siswa s join profil_sekolah ps on s.id_sekolah = ps.id where s.id_sekolah = '.$id_sekolah.' order by nama');
    	}
    	return $query->result_array();
    }

    public function get_siswa_by_id($id){
    	$query = $this->db->query('select s.*, ps.nama_sekolah from siswa s join profil_sekolah ps on s.id_sekolah = ps.id where s.id_siswa = '.$id);
    	return $query->row_array();
    }

     public function set_single_data_siswa($field, $data, $id){
        $this->load->helper('url');
        $data = array(
            $field => $data,
        );
        $this->db->where('id_siswa', $id);
        return $this->db->update('siswa', $data);
    }

    public function edit_profil_siswa($id){
        $this->load->helper('url');
        $data = array(
            'nama' => $this->input->post('nama'),
            'flag' =>  url_title($this->input->post('nama'), 'dash', TRUE),
            'tingkatan' => $this->input->post('tingkatan'),
            'kelas' => $this->input->post('kelas'),
            'jk' => $this->input->post('j_kelamin'),
            'tmp_lahir' => $this->input->post('tmp_lahir'),
            'tgl_lahir' => $this->input->post('tgl_lahir'),
            'nama_wali' => $this->input->post('nm_wali'),
            'telp_wali' => $this->input->post('telp_wali'),
            'alamat' => $this->input->post('alamat'),
        );
        $this->db->where('id_siswa', $id);
        return $this->db->update('siswa', $data);
    }

    public function edit_password_siswa($id_siswa){
        $this->load->helper('url');
        $data = array(
             'password' => $this->enkripsi($this->input->post('password')),
        );
        $this->db->where('id_siswa', $id_siswa);
        return $this->db->update('siswa', $data);
    }

    public function insert_dokumen($tipe, $path){
        $this->load->helper('url');
        $data = array(
            'tipe' => $tipe,
            'id_siswa' => $this->input->post('id_siswa'),
            'semester' => $this->input->post('semester'),
            'tahun_ajaran' => $this->input->post('thn_ajar'),
            'tingkatan' => $this->input->post('tingkatan'),
            'kelas' => $this->input->post('kelas'),
            'file' => $path,
        );
        return $this->db->insert('dokumen_siswa', $data);
    }

    public function get_dokumen($tipe, $id_siswa){
        $query = $this->db->query("select * from dokumen_siswa where id_siswa = ".$id_siswa." and tipe = '".$tipe."' order by tahun_ajaran DESC");
        return $query->result_array();
    }

    public function get_dokumen_by_id($id_dok){
        $query = $this->db->query("select * from dokumen_siswa where id_dok_siswa = ".$id_dok);
        return $query->row_array();
    }

    public function hapus_dokumen($id){
        $this->load->helper('url');
        $this->db->where('id_dok_siswa', $id);
        return $this->db->delete('dokumen_siswa');
    }

}

?>