<?php
class Kalender_Model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_kalender($slug = false)
    {
       if($slug == false) {
            $query = $this->db->query('select * from kalender_pendidikan order by tahun_ajaran desc');
            return $query->result_array();
        } else {
            $query = $this->db->get_where('kalender_pendidikan', array('slug' => $slug));
            return $query->row_array();
        }
    }

    public function set_kalender($path, $size, $slug)
    {
        $this->load->helper('url');
        $data = array(
            'jenjang' =>  $this->input->post('jenjang'),
            'tahun_ajaran' => $this->input->post('thn_ajar'),
            'path' => $path,
            'ukuran' => $size,
            'slug' => $slug,
        );
        return $this->db->insert('kalender_pendidikan', $data);  
    }

    public function hapus_kalender($slug) 
    {
        $this->load->helper('url');
        $this->db->where('slug', $slug);
        return $this->db->delete('kalender_pendidikan');
    }
}
