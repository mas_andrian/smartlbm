<?php
class Profil_sekolah_Model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_profil($id = false){
        if ($id != null) {
            $query = $this->db->get_where('profil_sekolah', array('id' => $id));
            return $query->row_array();
        } else {
            $query = $this->db->get_where('profil_sekolah', array('id' => 1));
            return $query->row_array();
        }
    }

    public function get_sekolah($id){
        $query = $this->db->get_where('profil_sekolah', array('id' => $id));
        return $query->row_array();
    }

    public function edit_profil_sekolah($id){
        $this->load->helper('url');
        $data = array(
            'nama_sekolah' => $this->input->post('nama_sekolah'),
            'alamat' => $this->input->post('alamat'),
            'kecamatan' => $this->input->post('kecamatan'),
            'kab_kota' => $this->input->post('kab_kota'),
            'kode_pos' =>  $this->input->post('kode_pos'),
            'telp' =>  $this->input->post('telp'),
            'fax' =>  $this->input->post('fax'),
            'email' =>  $this->input->post('email'),
            'website' =>  $this->input->post('website'),
            'nss' =>  $this->input->post('nss'),
            'npsn' =>  $this->input->post('npsn'),
            'nip_kepsek' =>  $this->input->post('nip_kepsek'),
            'nama_kepsek' =>  $this->input->post('nama_kepsek'),
            'nip_wakakur' =>  $this->input->post('nip_wakakur'),
            'nama_wakakur' =>  $this->input->post('nama_wakakur'),
        );
        $this->db->where('id', $id);
        return $this->db->update('profil_sekolah', $data);
    }

    public function edit_logo_sekolah($id,$path){
        $this->load->helper('url');
        if($id == 1){
            $data1 = array(
                'foto_profil' =>  $path,
            );
            $this->db->where('id', $id);
            $this->db->update('pengguna', $data1);
        }

        $data = array(
            'logo_sekolah' =>  $path,
        );
        $this->db->where('id', $id);
        return $this->db->update('profil_sekolah', $data);
    }
 
}
