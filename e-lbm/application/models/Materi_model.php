<?php
class Materi_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function _query($where = ""){
        $query = $this->db->query('select a.*, b.nama_sekolah, c.nama_mapel from materi a join profil_sekolah b on a.id_sekolah = b.id join mata_pelajaran c on a.id_mata_pelajaran = c.id '.$where.' order by id desc');
        return $query;
    }

    public function get_materi($id_sekolah = false)
    {
       if($id_sekolah == false) {
            $query = $this->_query();
            return $query->result_array();
        } else {
            $where = "where id_sekolah = ".$id_sekolah;
            $query = $this->_query($where);
            return $query->result_array();
        }
    }

    public function get_materi_by_id($id_materi, $id_sekolah)
    {
        if($id_sekolah == false) {
            $where = "where a.id = ".$id_materi;
            $query = $this->_query($where);
            return $query->row();
        } else {
            $where = 'where id_sekolah = '.$id_sekolah.' and a.id = '.$id_materi;
            $query = $this->_query($where);
            return $query->row();
        }
    }

    public function get_materi_by_guru($id_guru, $id_sekolah)
    {
        $where = "where guru_penanggung_jawab like '%,".$id_guru.",%' AND id_sekolah = '".$id_sekolah."'";
        $query = $this->_query($where);
        return $query->result_array();
    }

    public function set_materi($id = false)
    {
        $this->load->helper('url');

        $data = array(
            'id_sekolah' => $this->session->userdata('id_sekolah'),
            'id_mata_pelajaran' => $this->input->post('id_mata_pelajaran'),
            'tingkatan' => $this->input->post('tingkatan'),
            'guru_penanggung_jawab' => ",".implode(",", $this->input->post('guru_penanggung_jawab')).",",
        );

        if ($id == false) {
            $data['is_aktif'] = 'Y';
            return $this->db->insert('materi', $data);
        } else {
            $this->db->where('id', $id);
            return $this->db->update('materi', $data);
        }     
    }

    public function set_single_data_materi($field, $data, $id){
        $this->load->helper('url');
        $data = array(
            $field => $data,
        );
        $this->db->where('id', $id);
        return $this->db->update('materi', $data);
    }

    public function cek_materi_exist($id_sekolah, $id_mapel, $tingkatan){
        $query = $this->db->get_where('materi', array('id_sekolah' => $id_sekolah, 'id_mata_pelajaran' => $id_mapel, 'tingkatan' => $tingkatan));
        return $query->result_array();
    }
}