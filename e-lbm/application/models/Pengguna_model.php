<?php
class Pengguna_Model extends CI_Model {

    public $data;

    public function __construct() {
        $this->load->database();
        $this->load->helper('url');
    }

    public function get_tenaga_pengajar($id = false){
        if ($id == 0) {
            $query = $this->db->query('select p.*, o.id_pengguna, o.email, o.role_id, o.username, ps.nama_sekolah from pengguna p join otorisasi o on p.id = o.id_pengguna join profil_sekolah ps on p.id_sekolah = ps.id where role_id = 2 order by p.nama');
            return $query->result_array();
        } else {
            $query = $this->db->query('select p.*, o.id_pengguna, o.email, o.role_id, o.username, ps.nama_sekolah from pengguna p join otorisasi o on p.id = o.id_pengguna join profil_sekolah ps on p.id_sekolah = ps.id where role_id = 2 and p.id = '.$id);
            return $query->row_array();
        }
    }

    public function get_tenaga_pengajar_sekolah($id_sekolah){
       // $query = $this->db->query('select * from pengguna p join otorisasi o on p.id = o.id_pengguna where role_id = 2 and id_sekolah = '.$id_sekolah.' order by nama');
        $query = $this->db->query('select p.*, o.id_pengguna, o.email, o.role_id, o.username, ps.nama_sekolah from pengguna p join otorisasi o on p.id = o.id_pengguna join profil_sekolah ps on p.id_sekolah = ps.id where role_id = 2 and p.id_sekolah = '.$id_sekolah.' order by p.nama');
        return $query->result_array();
    }

    public function get_tenaga_pengajar_aktif(){
        $query = $this->db->query('select * from pengguna p join otorisasi o on p.id = o.id_pengguna where role_id = 2 and status = 1 order by p.nama');
        return $query->result_array();
    }

    public function get_pengguna_lain($id_pengguna = false){
         if ($id_pengguna == 0) {
            $query = $this->db->query('select * from pengguna p join otorisasi o on p.id = o.id_pengguna');
            return $query->result_array();
        } else {
           $query = $this->db->get_where('pengguna', array('id' => $id_pengguna));
            return $query->row_array();
        }
    }

    public function get_id_pengguna($id)
    {
        $query = $this->db->get_where('pengguna', array('id' => $id));
        return $query->row_array();
    }

    public function get_by_nip_pengguna($nip_nik)
    {
        $query = $this->db->get_where('pengguna', array('nip_nik' => $nip_nik));
        return $query->row_array();
    }

     public function insert_tenaga_pengajar($path){
        $this->load->helper('url');
        $data = array(
            'nip_nik' => $this->input->post('nip_nik'),
            'nama' => $this->input->post('nama'),
            'pangkat' => $this->input->post('pangkat'),
            'progli_ampu' => $this->input->post('pang_jab'),
            'j_kelamin' => $this->input->post('j_kelamin'),
            'tmp_lahir' => $this->input->post('tmp_lahir'),
            'tgl_lahir' => $this->input->post('tgl_lahir'),
            'alamat' => $this->input->post('alamat'),
            'telp_hp' => $this->input->post('telp_hp'),
            'foto_profil' => $path,
            'status' => 1,
            'nuptk' => $this->input->post('nuptk'),
            'tmt_guru' => $this->input->post('tmt_guru'),
            'spesialisasi' => $this->input->post('spesialisasi'),
            'id_sekolah' => $this->session->userdata('id_sekolah'),
        );
        return $this->db->insert('pengguna', $data);
    }

    private function enkripsi($password){
        $enskrip_md5 = md5($password);
        $enkrip_bcrytp = password_hash($enskrip_md5, PASSWORD_BCRYPT);
        return $enkrip_bcrytp;
    }

    public function set_otorisasi_pengguna($id_pengguna,$role,$aksi,$pass){
        $this->load->helper('url');
        $data = array(
            'id_pengguna' => $id_pengguna,
            'username' => $this->input->post('email'),
            'password' => $this->enkripsi($pass),
            'email' => $this->input->post('email'),
            'role_id' => $role,
        );
        if ($aksi == 0) {
            return $this->db->insert('otorisasi', $data);
        } else {
            $this->db->where('id_pengguna', $id_pengguna);
            return $this->db->update('otorisasi', $data);
        }
    }

     public function edit_foto_profil($id,$path){
        $this->load->helper('url');
        $data = array(
            'foto_profil' =>  $path,
        );
        $this->db->where('id', $id);
        return $this->db->update('pengguna', $data);
    }

    public function edit_profil_tenaga_pengajar($id){
        $this->load->helper('url');
        $data = array(
            'nip_nik' => $this->input->post('nip_nik'),
            'nama' => $this->input->post('nama'),
            'pangkat' => $this->input->post('pangkat'),
            //'guru_mapel' => $this->input->post('guru_mapel'),
            'j_kelamin' => $this->input->post('j_kelamin'),
            'tmp_lahir' => $this->input->post('tmp_lahir'),
            'tgl_lahir' => $this->input->post('tgl_lahir'),
            'alamat' => $this->input->post('alamat'),
            'nuptk' => $this->input->post('nuptk'),
            'tmt_guru' => $this->input->post('tmt_guru'),
            'progli_ampu' => $this->input->post('pang_jab'),
            'spesialisasi' => $this->input->post('spesialisasi'),
            'telp_hp' => $this->input->post('telp_hp'), 
        );
        $this->db->where('id', $id);
        return $this->db->update('pengguna', $data);
    }

    public function set_single_data_pengguna($field, $data, $nip_nik){
        $this->load->helper('url');
        $data = array(
            $field => $data,
        );
        $this->db->where('nip_nik', $nip_nik);
        return $this->db->update('pengguna', $data);
    }

    public function edit_otorisasi_pengguna($id_pengguna){
        $this->load->helper('url');
        $data = array(
            'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
        );
        $this->db->where('id_pengguna', $id_pengguna);
        return $this->db->update('otorisasi', $data);
    }

    public function set_single_data_otorisasi($field, $data, $id_pengguna){
        $this->load->helper('url');
        $data = array(
            $field => $data,
        );
        $this->db->where('id_pengguna', $id_pengguna);
        return $this->db->update('otorisasi', $data);
    }

    public function cek_data_sudah_ada($tabel, $field, $data){
        $query = $this->db->get_where($tabel, array($field => $data));
        return $query->row_array();
    }

     public function edit_password_pengguna($id_pengguna){
        $this->load->helper('url');
        $data = array(
             'password' => $this->enkripsi($this->input->post('password')),
        );
        $this->db->where('id_pengguna', $id_pengguna);
        return $this->db->update('otorisasi', $data);
    }

    public function edit_status($status, $id){
        $this->load->helper('url');
        $data = array(
            'status' =>  $status,
        );
        $this->db->where('id', $id);
        return $this->db->update('pengguna', $data);
    }

    public function set_pemulihan($email, $kode){
        $this->load->helper('url');
        $data = array(
            'email' => $email,
            'kode' => $kode,
            'is_pakai' => 0,
        );
        return $this->db->insert('pemulihan_akun', $data);
    }

    public function hapus_kode_pemulihan($email){
        $this->load->helper('url');
        $this->db->where('email', $email);
        $this->db->where('is_pakai', 0);
        return $this->db->delete('pemulihan_akun');
    }

    public function cek_kode_pemulihan($email, $kode){
        $query = $this->db->query('select * from pemulihan_akun where email = "'.$email.'" and kode = '.$kode.' and is_pakai = 0');
        return $query->row_array();
    }

    public function update_is_pakai_kode($id){
        $this->load->helper('url');
        $data = array(
            'is_pakai' =>  1,
        );
        $this->db->where('id', $id);
        return $this->db->update('pemulihan_akun', $data);
    }
}