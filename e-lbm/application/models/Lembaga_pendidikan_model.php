<?php
class Lembaga_pendidikan_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_lp_byid($id)
    {
         $query = $this->db->get_where('mata_pelajaran', array('id' => $id));
        return $query->row_array();
    }

    public function get_lembaga_pendidikan($slug = false)
    {
       if($slug == 0) {
            $query = $this->db->query('select * from profil_sekolah where id > 1 order by jenjang ASC');
            return $query->result_array();
        } else {
            $query = $this->db->get_where('profil_sekolah', array('slug' => $slug));
            return $query->row_array();
        }
    }

    public function set_lembaga_pendidikan($slug = false)
    {
        $this->load->helper('url');

        $data = array(
            'nama_sekolah' => $this->input->post('nama_sekolah'),
            'alamat' => $this->input->post('alamat'),
            'kecamatan' => $this->input->post('kecamatan'),
            'kab_kota' => $this->input->post('kab_kota'),
            'kode_pos' =>  $this->input->post('kode_pos'),
            'telp' =>  $this->input->post('telp'),
            'fax' =>  $this->input->post('fax'),
            'email' =>  $this->input->post('email'),
            'website' =>  $this->input->post('website'),
            'nss' =>  $this->input->post('nss'),
            'npsn' =>  $this->input->post('npsn'),
            'nip_kepsek' =>  $this->input->post('nip_kepsek'),
            'nama_kepsek' =>  $this->input->post('nama_kepsek'),
            'jenjang' =>  $this->input->post('jenjang'),
            'flag' => url_title($this->input->post('nama_sekolah'), 'dash', TRUE),
            'akses' => $this->input->post('akses'),
        );

        if ($slug == false) {
            return $this->db->insert('profil_sekolah', $data);
        } else {
            $this->db->where('id', $slug);
            return $this->db->update('profil_sekolah', $data);
        }     
    }

    public function hapus_lembaga_pendidikan($slug) 
    {
        $this->load->helper('url');
        $this->db->where('id', $slug);
        return $this->db->delete('profil_sekolah');
    }
}
