<?php
class Dashboard_Model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_id_pengguna($username){
    	$query = $this->db->get_where('otorisasi', array('username' => $username));
        return $query->row_array();
    }

    public function get_identitas($id_pengguna){
    	$query = $this->db->get_where('pengguna', array('id' => $id_pengguna));
        return $query->row_array();
    }

    public function get_identitas_sekolah($username){
        $query = $this->db->get_where('profil_sekolah', array('flag' => $username));
        return $query->row_array();
    }

}
