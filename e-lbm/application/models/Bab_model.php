<?php
class Bab_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function _query($where = ""){
        $query = $this->db->query('SELECT materi_bab.*, mata_pelajaran.nama_mapel, round(rating.avg_rating, 1) as avg_rating  FROM materi_bab JOIN materi ON materi_bab.id_materi = materi.id JOIN mata_pelajaran ON materi.id_mata_pelajaran = mata_pelajaran.id LEFT JOIN ( SELECT id_bab, avg( rating_value ) AS avg_rating FROM rating GROUP BY id_bab ) rating ON materi_bab.id = rating.id_bab '.$where.' ORDER BY urutan ASC');
        return $query;
    }

    public function get_bab_by_id($id, $slug){
    	$where = "where materi_bab.id = ".$id." and materi_bab.slug = '".$slug."'";
    	$query = $this->_query($where);
    	return $query->row();
    }

     public function get_bab_by_materi($id_materi = false)
     {
     	$where = "where id_materi = ".$id_materi;
     	$query = $this->_query($where);
     	return $query->result_array();
    }

    public function set_bab($id = false, $id_materi = false, $slug = false, $path = false)
    {
        $this->load->helper('url');
        $data = array(
            'urutan' => $this->input->post('urutan'),
            'judul' => $this->input->post('judul'),
            'deskripsi' => $this->input->post('deskripsi'),
            'slug' => $slug,
        );

        if($id_materi != false){
        	$data['id_materi'] = $id_materi;
        }

        if($path != false){
            $data['img_thumbnail'] = $path;
        }

        if ($id == false) {
            $data['is_aktif'] = 'Y';
            return $this->db->insert('materi_bab', $data);
        } else {
            $this->db->where('id', $id);
            return $this->db->update('materi_bab', $data);
        }     
    }

    public function set_single_data_bab($field, $data, $id){
        $this->load->helper('url');
        $data = array(
            $field => $data,
        );
        $this->db->where('id', $id);
        return $this->db->update('materi_bab', $data);
    }

    public function get_kelas_by_materi($id_materi){
        $this->db->select("*");
        $this->db->from("materi");
        $this->db->where("id", $id_materi);
        $query = $this->db->get();
        return $query->row();
    }
}