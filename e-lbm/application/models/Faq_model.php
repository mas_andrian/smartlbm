<?php
 
class Faq_Model extends CI_Model { 
    public function __construct() {
        $this->load->database();
    }

    public function getData($rowno,$rowperpage,$search="") {
            
        $this->db->select('*');
        $this->db->from('faq');

        if($search != ''){
           $this->db->like('pertanyaan', $search);
           $this->db->or_like('jawaban', $search);
        }
        $this->db->limit($rowperpage, $rowno);  
        $this->db->order_by("id_faq", "asc");
        $query = $this->db->get();
        
        return $query->result_array();
    }

    // Select total records
    public function getrecordCount($search = '') {

        $this->db->select('count(*) as allcount');
        $this->db->from('faq');
      
        if($search != ''){
            $this->db->like('pertanyaan', $search);
           $this->db->or_like('jawaban', $search);
        }
        $query = $this->db->get();
        $result = $query->result_array();
      
        return $result[0]['allcount'];
    }

     public function set_faq(){
        $this->load->helper('url');
        $data = array(
            'pertanyaan' => $this->input->post('pertanyaan'),
            'jawaban' => $this->input->post('jawaban'),
        );
        return $this->db->insert('faq', $data);
    }

    public function update_faq($id){
         $this->load->helper('url');
        $data = array(
            'pertanyaan' => $this->input->post('pertanyaan'),
            'jawaban' => $this->input->post('jawaban'),
        );
         
        $this->db->where('id_faq', $id);
        return $this->db->update('faq', $data);
    }

    public function hapus_faq($id){
        $this->load->helper('url');
        $this->db->where('id_faq', $id);
        return $this->db->delete('faq');
    }

}

?>