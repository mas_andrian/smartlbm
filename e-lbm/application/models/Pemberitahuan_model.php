<?php
class Pemberitahuan_Model extends CI_Model {

    public function __construct() {
        $this->load->database(); 
    }

    public function get_pemberitahuan_aktif(){
        $query = $this->db->query('select * from pemberitahuan where batas >= current_date');
        return $query->result_array();
    }

    public function get_pemberitahuan($slug = false)
    {
       if($slug == false) {
            $query = $this->db->query('select * from pemberitahuan order by batas asc');
            return $query->result_array();
        } else {
            $query = $this->db->get_where('pemberitahuan', array('slug' => $slug));
            return $query->row_array();
        }
    }

    public function set_pemberitahuan($slug = false)
    {
        $this->load->helper('url');
        $data = array(
            'perihal' => $this->input->post('perihal'),
            'isi' => $this->input->post('isi'),
            'batas' => $this->input->post('batas'),
            'slug' => date('YmdHis-').url_title($this->input->post('perihal')),
        );
         
        if ($slug == false) {
            return $this->db->insert('pemberitahuan', $data);
        } else {
            $this->db->where('slug', $slug);
            return $this->db->update('pemberitahuan', $data);
        }     
    }

     public function getData($rowno,$rowperpage,$search="") {
            
        $this->db->select('*');
        $this->db->from('pemberitahuan');

        if($search != ''){
            $this->db->like('perihal', $search);
            $this->db->or_like('isi', $search);
        }
        $this->db->limit($rowperpage, $rowno);  
        $this->db->order_by("batas", "desc");
        $query = $this->db->get();
        
        return $query->result_array();
    }

    // Select total records
    public function getrecordCount($search = '') {

        $this->db->select('count(*) as allcount');
        $this->db->from('pemberitahuan');
      
        if($search != ''){
            $this->db->like('perihal', $search);
            $this->db->or_like('isi', $search);
        }
        $query = $this->db->get();
        $result = $query->result_array();
      
        return $result[0]['allcount'];
    }

    public function hapus_pemberitahuan($slug) 
    {
        $this->load->helper('url');
        $this->db->where('slug', $slug);
        return $this->db->delete('pemberitahuan');
    }
}
