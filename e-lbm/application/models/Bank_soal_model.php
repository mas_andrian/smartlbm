<?php
class Bank_soal_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function _query($where = ""){
        $query = $this->db->query("SELECT * from bank_soal ".$where." ORDER BY id");
        return $query;
    }

    public function get_soal_by_id($id){
    	$where = "where bank_soal.id = ".$id."";
    	$query = $this->_query($where);
    	return $query->row();
    }

     public function get_soal_by_bab($id_bab, $type)
     {
     	$where = "where bank_soal.id_bab = ".$id_bab." and bank_soal.jenis_soal = '$type'";
     	$query = $this->_query($where);
     	return $query->result_array();
    }

    public function set_bab($id = false, $id_bab = false, $type = false)
    {
        $this->load->helper('url');
        $data = array(
            'jenis_soal' => $type,
            'pertanyaan' => $this->input->post('pertanyaan'),
            'jawaban_1' => $this->input->post('jawaban_1'),
            'jawaban_2' => $this->input->post('jawaban_2'),
            'jawaban_3' => $this->input->post('jawaban_3'),
            'jawaban_4' => $this->input->post('jawaban_4'),
            'jawaban_5' => $this->input->post('jawaban_5'),
            'jawaban_benar' => $this->input->post('jawaban_benar'),
        );

        if($id_bab != false){
        	$data['id_bab'] = $id_bab;
        }

        if ($id == false) {
            return $this->db->insert('bank_soal', $data);
        } else {
            $this->db->where('id', $id);
            return $this->db->update('bank_soal', $data);
        }     
    }
}