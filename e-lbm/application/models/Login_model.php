<?php
class Login_Model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_username($username) {
        $query = $this->db->get_where('otorisasi', array('username' => $username));
        return $query->row_array();
    }

    public function get_email($email) {
        $query = $this->db->get_where('otorisasi', array('email' => $email));
        return $query->row_array();
    }

    public function akses_sekolah($username, $password){
        $query = $this->db->get_where('profil_sekolah', array('flag' => $username, 'akses' => $password));
        return $query->row_array();
    }

}
