<?php 
 
class Blogpost_Model extends CI_Model { 
    public function __construct() {
        $this->load->database();
    }

    public function get_post_detail($slug) {
        $query = $this->db->get_where('blogpost', array('flag' => $slug));
        return $query->row_array();
    }

    public function getData($rowno,$rowperpage,$search="") {
            
        $this->db->select('*');
        $this->db->from('blogpost');

        if($search != ''){
           $this->db->like('judul', $search);
           $this->db->or_like('deskripsi', $search);
        }
        $this->db->limit($rowperpage, $rowno);  
        $this->db->order_by("time_at", "desc");
        $query = $this->db->get();
        
        return $query->result_array();
    }

    // Select total records
    public function getrecordCount($search = '') {

        $this->db->select('count(*) as allcount');
        $this->db->from('blogpost');
      
        if($search != ''){
            $this->db->like('judul', $search);
            $this->db->or_like('deskripsi', $search);
        }
        $query = $this->db->get();
        $result = $query->result_array();
      
        return $result[0]['allcount'];
    }


    public function set_post($path_banner){
        $this->load->helper('url');
        $data = array(
            'kategori' => $this->input->post('kategori'),
            'judul' => $this->input->post('judul'),
            'deskripsi' => $this->input->post('isi'),
            'banner' => $path_banner,
            'flag' => date('siHdmY-').url_title($this->input->post('judul')),
        );
        return $this->db->insert('blogpost', $data);
    }

    public function update_post($slug){
         $this->load->helper('url');
        $data = array(
            'kategori' => $this->input->post('kategori'),
            'judul' => $this->input->post('judul'),
            'deskripsi' => $this->input->post('isi'),
            'flag' => date('YmdHis-').url_title($this->input->post('judul')),
        );
         
        $this->db->where('flag', $slug);
        return $this->db->update('blogpost', $data);
    }

    public function update_img($path, $slug){
        $this->load->helper('url');
        $data = array(
            'banner' => $path,
        );
         
        $this->db->where('flag', $slug);
        return $this->db->update('blogpost', $data);
    }

    public function hapus_post($slug){
        $this->load->helper('url');
        $this->db->where('flag', $slug);
        return $this->db->delete('blogpost');
    }

}

?>