<?php
class Stakeholder_Model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_stakeholder($id_pengguna = false){
         if ($id_pengguna == 0) {
            $query = $this->db->query('select * from pengguna p join otorisasi o on p.id = o.id_pengguna join role_aktor ra on ra.id = o.role_id where role_id != 2 and status = 1 order by o.id_pengguna');
            return $query->result_array();
        } else {
            $query = $this->db->query('select * from pengguna p join otorisasi o on p.id = o.id_pengguna where role_id != 2 and p.id_pengguna = '.$id_pengguna);
            return $query->row_array();
        }
    }

    public function update_stakeholder($id)
    {
        $this->load->helper('url');
        $data = array(
            'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
        );
        $this->db->where('id_pengguna', $id);
        return $this->db->update('otorisasi', $data);
    }

    private function enkripsi($password){
        $enskrip_md5 = md5($password);
        $enkrip_bcrytp = password_hash($enskrip_md5, PASSWORD_BCRYPT);
        return $enkrip_bcrytp;
    }

    public function update_pass($id)
    {
        $this->load->helper('url');
        $data = array(
            'password' => $this->enkripsi($this->input->post('password1')),
        );
        $this->db->where('id_pengguna', $id);
        return $this->db->update('otorisasi', $data);
    }
}
