<?php
 
class Kegiatan_Model extends CI_Model { 
    public function __construct() {
        $this->load->database();
    }

    public function get_kegiatan_detail($slug) {
        $query = $this->db->get_where('kegiatan', array('slug' => $slug));
        return $query->row_array();
    }

    public function getData($rowno,$rowperpage,$search="") {
            
        $this->db->select('*');
        $this->db->from('kegiatan');

        if($search != ''){
           $this->db->like('judul', $search);
           $this->db->or_like('deskripsi', $search);
           $this->db->or_like('lokasi', $search);
        $this->db->or_like('tanggal', $search);
        }
        $this->db->limit($rowperpage, $rowno);  
        $this->db->order_by("tanggal", "desc");
        $query = $this->db->get();
        
        return $query->result_array();
    }

    // Select total records
    public function getrecordCount($search = '') {

        $this->db->select('count(*) as allcount');
        $this->db->from('kegiatan');
      
        if($search != ''){
            $this->db->like('judul', $search);
            $this->db->or_like('deskripsi', $search);
            $this->db->or_like('lokasi', $search);
            $this->db->or_like('tanggal', $search);
        }
        $query = $this->db->get();
        $result = $query->result_array();
      
        return $result[0]['allcount'];
    }

     public function set_kegiatan($path_banner){
        $this->load->helper('url');
        $data = array(
            'judul' => $this->input->post('judul'),
            'deskripsi' => $this->input->post('isi'),
            'jam' => $this->input->post('jam'),
            'tanggal' => $this->input->post('tanggal'),
            'lokasi' => $this->input->post('lokasi'),
            'gambar' => $path_banner,
            'slug' => date('HisdmY-').url_title($this->input->post('judul')),
        );
        return $this->db->insert('kegiatan', $data);
    }

    public function update_kegiatan($slug){
         $this->load->helper('url');
        $data = array(
            'judul' => $this->input->post('judul'),
            'deskripsi' => $this->input->post('isi'),
            'jam' => $this->input->post('jam'),
            'tanggal' => $this->input->post('tanggal'),
            'lokasi' => $this->input->post('lokasi'),
            'slug' => date('HisdmY-').url_title($this->input->post('judul')),
        );
         
        $this->db->where('slug', $slug);
        return $this->db->update('kegiatan', $data);
    }

    public function update_img($path, $slug){
        $this->load->helper('url');
        $data = array(
            'gambar' => $path,
        );
         
        $this->db->where('slug', $slug);
        return $this->db->update('kegiatan', $data);
    }

    public function hapus_kegiatan($slug){
        $this->load->helper('url');
        $this->db->where('slug', $slug);
        return $this->db->delete('kegiatan');
    }

}

?>