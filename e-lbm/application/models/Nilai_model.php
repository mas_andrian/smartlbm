<?php
class Nilai_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_nilai_mapel_siswa($id_sekolah, $id_siswa, $kelas){
        $sql = "SELECT
        mata_pelajaran.id AS id_mapel,
        mata_pelajaran.nama_mapel,
        materi_bab.id as id_bab,
        materi_bab.judul AS bab,
        materi.tingkatan,
        latihan.nilai AS nilai_latihan,
        quiz.nilai AS nilai_ujian 
    FROM
        mata_pelajaran
        JOIN materi ON mata_pelajaran.id = materi.id_mata_pelajaran
        JOIN materi_bab ON materi.id = materi_bab.id_materi
        LEFT JOIN (
    SELECT
        id_bab,
        avg( nilai ) AS nilai 
    FROM
        quiz 
    WHERE
        jenis_quiz = 'latihan' 
        AND id_siswa = '".$id_siswa."' 
        AND waktu_selesai != '0000-00-00 00:00:00.000000' 
    GROUP BY
        id_siswa 
        ) latihan ON materi_bab.id = latihan.id_bab
        LEFT JOIN quiz ON materi_bab.id = quiz.id_bab 
        AND id_siswa = '".$id_siswa."' 
        AND jenis_quiz = 'ujian' 
    WHERE
        materi.tingkatan = '".$kelas."' 
        AND materi.id_sekolah = '".$id_sekolah."'";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_nilai_siswa($id_sekolah, $kelas, $bab){
        $sql = "SELECT
        siswa.id_siswa,
        siswa.nama,
        siswa.tingkatan,
        siswa.kelas,
        round(latihan.nilai) AS nilai_latihan,
        quiz.nilai AS nilai_ujian,
        quiz.waktu_mulai,
        quiz.waktu_selesai 
    FROM
        siswa
        LEFT JOIN (
    SELECT
        id_siswa,
        avg( nilai ) AS nilai 
    FROM
        quiz 
    WHERE
        jenis_quiz = 'latihan' 
        AND id_bab = '".$bab."' 
        AND waktu_selesai != '0000-00-00 00:00:00.000000' 
    GROUP BY
        id_siswa 
        ) latihan ON siswa.id_siswa = latihan.id_siswa
        LEFT JOIN quiz ON siswa.id_siswa = quiz.id_siswa 
        AND jenis_quiz = 'ujian' 
        AND id_bab = '".$bab."' 
        AND waktu_selesai != '0000-00-00 00:00:00.000000' 
    WHERE
        siswa.tingkatan = '".$kelas."' 
        AND siswa.id_sekolah = '".$id_sekolah."'";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_nilai_latihan_siswa($id_siswa, $id_bab){
        $this->db->select("*");
        $this->db->from("quiz");
        $this->db->where("jenis_quiz", "latihan");
        $this->db->where("id_bab", $id_bab);
        $this->db->where("id_siswa", $id_siswa);
        $this->db->where("waktu_selesai != ","0000-00-00 00:00:00.000000");
        $this->db->order_by("id", "asc");
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }
}