<?php
 
class Testimoni_Model extends CI_Model { 
    public function __construct() {
        $this->load->database();
    }

    public function get_testimoni_detail($id) {
        $query = $this->db->get_where('testimoni', array('id_testimoni' => $id));
        return $query->row_array();
    }

    public function getData($rowno,$rowperpage,$search="") {
            
        $this->db->select('*');
        $this->db->from('testimoni');

        if($search != ''){
           $this->db->like('nama', $search);
           $this->db->or_like('kategori', $search);
           $this->db->or_like('kesan', $search);
           $this->db->or_like('saran', $search);
           $this->db->or_like('phone', $search);
        }
        $this->db->limit($rowperpage, $rowno);  
        $this->db->order_by("time_at", "desc");
        $query = $this->db->get();
        
        return $query->result_array();
    }

    // Select total records
    public function getrecordCount($search = '') {

        $this->db->select('count(*) as allcount');
        $this->db->from('testimoni');
      
        if($search != ''){
           $this->db->like('nama', $search);
           $this->db->or_like('kategori', $search);
           $this->db->or_like('kesan', $search);
           $this->db->or_like('saran', $search);
           $this->db->or_like('phone', $search);
        }
        $query = $this->db->get();
        $result = $query->result_array();
      
        return $result[0]['allcount'];
    }

     public function set_testimoni($path_banner){
        $this->load->helper('url');
        $data = array(
            'nama' => $this->input->post('nama'),
            'phone' => $this->input->post('phone'),
            'kategori' => $this->input->post('kategori'),
            'kesan' => $this->input->post('kesan'),
            'saran' => $this->input->post('saran'),
            'rating' => $this->input->post('rating'),
            'gambar' => $path_banner,
        );
        return $this->db->insert('testimoni', $data);
    }

    public function update_testimoni($id){
         $this->load->helper('url');
        $data = array(
            'nama' => $this->input->post('nama'),
            'phone' => $this->input->post('phone'),
            'kategori' => $this->input->post('kategori'),
            'kesan' => $this->input->post('kesan'),
            'saran' => $this->input->post('saran'),
            'rating' => $this->input->post('rating'),
        );
         
        $this->db->where('id_testimoni', $id);
        return $this->db->update('testimoni', $data);
    }

    public function update_img($path, $id){
        $this->load->helper('url');
        $data = array(
            'gambar' => $path,
        );
         
        $this->db->where('id_testimoni', $id);
        return $this->db->update('testimoni', $data);
    }

    public function hapus_testimoni($id){
        $this->load->helper('url');
        $this->db->where('id_testimoni', $id);
        return $this->db->delete('testimoni');
    }

}

?>