<?php
class Mata_pelajaran_Model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_mp_byid($id)
    {
         $query = $this->db->get_where('mata_pelajaran', array('id' => $id));
        return $query->row_array();
    }

    public function get_mata_pelajaran($slug = false)
    {
       if($slug == 0) {
            $query = $this->db->query('select * from mata_pelajaran order by jenjang DESC');
            return $query->result_array();
        } else {
            $query = $this->db->get_where('mata_pelajaran', array('id' => $slug));
            return $query->row_array();
        }
    }

    public function get_mapel_by_jenjang($jenjang){
        $query = $this->db->query('select * from mata_pelajaran where jenjang = "'.$jenjang.'" order by jenjang DESC');
        return $query->result_array();
    }

    public function set_mata_pelajaran($slug = false)
    {
        $this->load->helper('url');

        $data = array(
            'nama_mapel' => $this->input->post('mapel'),
            'kategori' => $this->input->post('kat'),
            'jenjang' => $this->input->post('jenjang'),
            'slug' => url_title($this->input->post('mapel'), 'dash', TRUE).'_'.url_title($this->input->post('jenjang'), 'dash', TRUE),
        );

        if ($slug == false) {
            return $this->db->insert('mata_pelajaran', $data);
        } else {
            $this->db->where('id', $slug);
            return $this->db->update('mata_pelajaran', $data);
        }     
    }

    public function hapus_mata_pelajaran($slug) 
    {
        $this->load->helper('url');
        $this->db->where('id', $slug);
        return $this->db->delete('mata_pelajaran');
    }
}
