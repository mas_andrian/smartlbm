<?php
class Sub_bab_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function _query($where = ""){
        $query = $this->db->query('SELECT materi_sub_bab.*, materi_bab.slug as slug_bab from materi_sub_bab JOIN materi_bab ON materi_sub_bab.id_bab = materi_bab.id '.$where.' ORDER BY materi_sub_bab.urutan ASC');
        return $query;
    }

    public function get_sub_bab_by_id($id = false)
    {
     	$where = "where materi_sub_bab.id = ".$id;
     	$query = $this->_query($where);
     	return $query->row();
    }

    public function get_sub_bab_by_bab($id_bab = false)
    {
        $where = "where materi_sub_bab.id_bab = ".$id_bab;
        $query = $this->_query($where);
        return $query->result_array();
    }

    public function set_bab($id = false, $id_bab = false, $slug = false, $path = false)
    {
        $this->load->helper('url');
        $data = array(
            'urutan' => $this->input->post('urutan'),
            'judul' => $this->input->post('judul'),
            'deskripsi' => $this->input->post('deskripsi'),
            'file_video' => $this->input->post('file_video'),
            'slug' => $slug,
        );

        if($id_bab != false){
            $data['id_bab'] = $id_bab;
        }

        if($path != false){
            $data['file_pdf'] = $path;
        }

        if ($id == false) {
            $data['is_aktif'] = 'Y';
            return $this->db->insert('materi_sub_bab', $data);
        } else {
            $this->db->where('id', $id);
            return $this->db->update('materi_sub_bab', $data);
        }     
    }

    public function set_single_data_sub_bab($field, $data, $id){
        $this->load->helper('url');
        $data = array(
            $field => $data,
        );
        $this->db->where('id', $id);
        return $this->db->update('materi_sub_bab', $data);
    }
}