<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_bab extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		if($this->session->userdata('username') == NULL) {
			redirect(site_url('login'));
		}
		$this->load->model('dashboard_model');
		$this->load->model('profil_sekolah_model');
		$this->load->model('pemberitahuan_model');
		$this->load->model('pengguna_model');
		$this->load->model('bab_model');
		$this->load->model('sub_bab_model');
	}

	public function tambah($id, $slug)
	{
		$data['title'] = "Tambah Sub BAB Materi";

		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$this->load->view('templates/header',$data);
		if ($this->session->userdata('role') != 'guru') {
			$this->load->view('pages/error_500');
		} else {
			$this->load->view('sub_bab/sub_bab_tambah');
		}
		$this->load->view('templates/footer');
	}

	public function edit($id, $slug, $id_bab, $slug_bab)
	{
		$data['title'] = "Ubah Sub BAB Materi";

		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$data['sub_bab'] = $this->sub_bab_model->get_sub_bab_by_id($id, $slug);
		$this->load->view('templates/header',$data);
		if ($this->session->userdata('role') != 'guru') {
			$this->load->view('pages/error_500');
		} else {
			$this->load->view('sub_bab/sub_bab_edit');
		}
		$this->load->view('templates/footer');
	}

	public function detail($id, $slug)
	{
		$data['title'] = "Detail Sub BAB Materi";

		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$data['sub_bab'] = $this->sub_bab_model->get_sub_bab_by_id($id, $slug);
		$this->load->view('templates/header',$data);
		if ($this->session->userdata('role') != 'guru' && $this->session->userdata('role') != 'sekolah' && $this->session->userdata('role') != 'superadmin') {
			$this->load->view('pages/error_500');
		} else {
			$this->load->view('sub_bab/sub_bab_detail');
		}
		$this->load->view('templates/footer');
	}

	public function do_tambah($id, $bab)
	{
		$this->load->library('form_validation');
		$this->load->helper('url');

		$config['upload_path']= './assets/file_materi/';
		$config['allowed_types'] = 'pdf';
		$config['max_size'] = 30720;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if(isset($_FILES['file_pdf'])){
			if ($this->upload->do_upload('file_pdf')){
				$file_pdf = $this->upload->data();
				$path ='./assets/file_materi/'.$file_pdf['file_name'];
			}
		}

		$slug = "sub_bab_".url_title(strtolower($this->input->post('judul')));

		$this->sub_bab_model->set_bab(false, $id, $slug, $path);
		$this->session->set_userdata('status_tambah_sub_bab', '1');
		redirect(site_url('bab/detail/'.$id.'/'.$bab));
	}

	public function do_edit($id, $slug, $id_bab, $slug_bab)
	{
		$this->load->library('form_validation');
		$this->load->helper('url');

		$config['upload_path']= './assets/file_materi/';
		$config['allowed_types'] = 'pdf';
		$config['max_size'] = 30720;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if(isset($_FILES['file_pdf'])){
			if ($this->upload->do_upload('file_pdf')){
				$file_pdf = $this->upload->data();
				$path ='./assets/file_materi/'.$file_pdf['file_name'];
			}
		}

		$slug = "sub_bab_".url_title(strtolower($this->input->post('judul')));

		$this->sub_bab_model->set_bab($id, false, $slug, $path);
		$this->session->set_userdata('status_ubah_sub_bab', '1');
		redirect(site_url('bab/detail/'.$id_bab.'/'.$slug_bab));
	}

	public function do_edit_status()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');
		$status = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$slug = $this->uri->segment(5);

		$this->sub_bab_model->set_single_data_sub_bab('is_aktif', $status, $id);
		$this->session->set_userdata('status_aktif_siswa', '1');
		redirect(site_url('sub_bab/detail/'.$id.'/'.$slug));
	}
}