<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_beranda extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		
	}

	public function index()
	{
		$data['title'] = "Beranda";

		$this->load->view('frontend/beranda/header_home',$data);
		$this->load->view('frontend/beranda/beranda_index');
		$this->load->view('frontend/beranda/footer-home');
		$this->load->view('frontend/templating/footer');
	}


	public function pages(){
		$data['title'] = "Page Example";

		$this->load->view('frontend/templating/header',$data);
		$this->load->view('frontend/samplepage/page_index');
		$this->load->view('frontend/templating/footer');
	}
}

?>