<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recover extends CI_Controller {
	public function __construct() {
		parent::__construct();
    	$this->load->library('session');
    	
		$this->load->model('login_model');
		$this->load->model('pengguna_model');
		$this->load->model('profil_sekolah_model');
	}

	public function index(){
		$email = $this->input->post('email');

		$data['user'] = $this->login_model->get_email($email);

		if ($data['user'] == 0) {
	    	$this->session->set_userdata('lupa_pass', '<b>Mohon maaf</b>, email anda tidak terdaftar!');
	    	redirect(site_url('login'));
	    } else {
	    	$this->session->set_userdata('email', $email);
	        $this->session->set_userdata('lupa_pass_to', 'Silakan periksa email Anda, apakah ada pesan yang berisi kode Anda! <br>');
	        $data['pengguna'] = $this->pengguna_model->get_pengguna_lain($data['user']['id_pengguna']);
	        $nama = $data['pengguna']['nama'];
	        $this->kirim_kode($email,$nama);

	        redirect(site_url('recover/konfirmasi'));
	    }
	} 

	public function konfirmasi()
	{
		if($this->session->userdata('email') != NULL) {
        	$this->load->view('pages/lupa_password');
      	} else {
      		redirect(site_url('login'));
      	}
	}

	public function batal(){
		$this->pengguna_model->hapus_kode_pemulihan($this->session->userdata('email'));
		redirect(site_url('login/do_logout'));
	}

	public function do_konfirmasi(){
		$email = $this->input->post('email');
        $kode = $this->input->post('kode');

		$data['akun'] = $this->pengguna_model->cek_kode_pemulihan($email, $kode);

		if (sizeof($data['akun']) == 0) {

			$this->session->set_userdata('cek_kode', 'Mohon maaf, kode tidak sesuai!');
			redirect(site_url('recover/konfirmasi'));
			
		} else {

			$this->pengguna_model->update_is_pakai_kode($data['akun']['id']);
			$this->session->set_userdata('new_password', 'aktif');
			$data['pengguna'] = $this->login_model->get_email($email);
			$this->session->set_userdata('id_pengguna', $data['pengguna']['id_pengguna']);
			$this->session->set_userdata('user_name', $data['pengguna']['username']);
			redirect(site_url('recover/ubah_password'));
		}
	}

	public function ubah_password(){
		if($this->session->userdata('new_password') == 'aktif') {
        	$this->load->view('pages/ubah_password');
      	} else {
      		redirect(site_url('login'));
      	}
	}

	public function do_ubah_password(){
		$this->load->library('form_validation');
		$this->load->helper('url');
		$id_pengguna = $this->input->post('id_pengguna');
	
		$this->pengguna_model->edit_password_pengguna($id_pengguna);
		//$this->session->sess_destroy();

		$this->session->set_userdata('pemulihan', 'Pemulihan akun Anda berhasil, silakan Login dengan passsword baru Anda.');
		redirect(site_url('login'));
	}

	public function kirim_ulang(){
		$email = $this->input->get('email');

		$data['user'] = $this->login_model->get_email($email);
		$data['pengguna'] = $this->pengguna_model->get_pengguna_lain($data['user']['id_pengguna']);
		$nama = $data['pengguna']['nama'];

		$this->session->set_userdata('email', $email);
		$this->session->set_userdata('kirim_ulang', 'Silakan periksa kembali email Anda, kode pemulihan anda telah dikirimkan ulang!');
		$this->kirim_kode($email,$nama);
	    redirect(site_url('recover/konfirmasi'));
	}

	private function get_kode(){
		$length = 6;
	    $token = "";
	    $codeAlphabet = "1234567890";
	     
	    $max = strlen($codeAlphabet); // edited

	    for ($i=0; $i < $length; $i++) {
	        $token .= $codeAlphabet[random_int(0, $max-1)];
	    }
	    return $token;
	}

	private function kirim_kode($email,$nama){
		$data['sekolah'] = $this->profil_sekolah_model->get_profil(); //profil sekolah

		$to = $email;
		$kode_baru = $this->get_kode();
		$subject = $kode_baru." merupakan kode pemulihan akun SMART-LBM Anda";
		    
		$headers = "From: SMART-LBM Notification <info.noreply@smartlbm.com>" . "\r\n";
		$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
		$htmlContent = '
			<html>
			<head>
			  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			  <meta name="viewport" content="initial-scale=1.0" />
			  <meta name="format-detection" content="telephone=no" />
			  <title></title>
			  <style type="text/css">
			 	body {
					width: 100%;
					margin: 0;
					padding: 0;
					-webkit-font-smoothing: antialiased;
				}
				@media only screen and (max-width: 600px) {
					table[class="table-row"] {
						float: none !important;
						width: 98% !important;
						padding-left: 20px !important;
						padding-right: 20px !important;
					}
					table[class="table-row-fixed"] {
						float: none !important;
						width: 98% !important;
					}
					table[class="table-col"], table[class="table-col-border"] {
						float: none !important;
						width: 100% !important;
						padding-left: 0 !important;
						padding-right: 0 !important;
						table-layout: fixed;
					}
					td[class="table-col-td"] {
						width: 100% !important;
					}
					table[class="table-col-border"] + table[class="table-col-border"] {
						padding-top: 12px;
						margin-top: 12px;
						border-top: 1px solid #E8E8E8;
					}
					table[class="table-col"] + table[class="table-col"] {
						margin-top: 15px;
					}
					td[class="table-row-td"] {
						padding-left: 0 !important;
						padding-right: 0 !important;
					}
					table[class="navbar-row"] , td[class="navbar-row-td"] {
						width: 100% !important;
					}
					img {
						max-width: 100% !important;
						display: inline !important;
					}
					img[class="pull-right"] {
						float: right;
						margin-left: 11px;
			            max-width: 125px !important;
						padding-bottom: 0 !important;
					}
					img[class="pull-left"] {
						float: left;
						margin-right: 11px;
						max-width: 125px !important;
						padding-bottom: 0 !important;
					}
					table[class="table-space"], table[class="header-row"] {
						float: none !important;
						width: 98% !important;
					}
					td[class="header-row-td"] {
						width: 100% !important;
					}
				}
				@media only screen and (max-width: 480px) {
					table[class="table-row"] {
						padding-left: 16px !important;
						padding-right: 16px !important;
					}
				}
				@media only screen and (max-width: 320px) {
					table[class="table-row"] {
						padding-left: 12px !important;
						padding-right: 12px !important;
					}
				}
				@media only screen and (max-width: 458px) {
					td[class="table-td-wrap"] {
						width: 100% !important;
					}
				}
			  </style>
			 </head>
			 <body style="font-family: Arial, sans-serif; font-size:13px; color: #444444; min-height: 200px;" bgcolor="#f4f6f9" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
			 <table width="100%" height="100%" bgcolor="#f4f6f9" cellspacing="0" cellpadding="0" border="0">
			 <tr><td width="100%" align="center" valign="top" bgcolor="#f4f6f9" style="background-color:#f4f6f9; min-height: 200px;">
			<table><tr><td class="table-td-wrap" align="center" width="458"><table class="table-space" height="18" style="height: 18px; font-size: 0px; line-height: 0; width: 450px; background-color: #f4f6f9;" width="450" bgcolor="#f4f6f9" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="18" style="height: 18px; width: 450px; background-color: #f4f6f9;" width="450" bgcolor="#f4f6f9" align="left">&nbsp;</td></tr></tbody></table>
			<table class="table-space" height="8" style="height: 8px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="8" style="height: 8px; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>

			<table class="table-row" width="450" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
			  <table class="table-col" align="left" width="378" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="table-col-td" width="378" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; width: 378px;" valign="top" align="left">
			    <table class="header-row" width="378" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="header-row-td" width="378" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: #478fca; margin: 0px; font-size: 18px; padding-bottom: 10px; padding-top: 15px;" valign="top" align="left">

			    Yth. '.$nama.',

			    </td></tr></tbody></table>
			    <div style="font-family: Arial, sans-serif; line-height: 20px; color: #444444; font-size: 13px;">
			      

			      Kami menerima permintaan untuk mengatur ulang kata sandi SMART-LBM.
			      Anda dapat memasukkan kode pengaturan ulang kata sandi berikut:

			    </div>
			  </td></tr></tbody></table>
			</td></tr></tbody></table>
			    
			<table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>

			<table class="table-row" width="450" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
			  <table class="table-col" align="left" width="378" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="table-col-td" width="378" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; width: 378px;" valign="top" align="left">
			    <div style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; text-align: center;">
			      <a style="color: #ffffff; text-decoration: none; margin: 0px; text-align: center; vertical-align: baseline; border: 4px solid #6fb3e0; padding: 4px 9px; font-size: 15px; line-height: 21px; background-color: #6fb3e0;">

			      &nbsp; '.$kode_baru.' &nbsp;

			      </a>
			    </div>
			   <table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 378px; background-color: #ffffff;" width="378" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 378px; background-color: #ffffff;" width="378" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>
  </td></tr></tbody></table>
</td></tr></tbody></table>

<table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>
			<table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 450px; padding-left: 16px; padding-right: 16px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" align="center">&nbsp;<table bgcolor="#E8E8E8" height="0" width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td bgcolor="#E8E8E8" height="1" width="100%" style="height: 1px; font-size:0;" valign="top" align="left">&nbsp;</td></tr></tbody></table></td></tr></tbody></table>


<table class="table-row" width="450" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
			  <table class="table-col" align="left" width="378" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="table-col-td" width="378" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; width: 378px;" valign="top" align="left">
			    
			    <div style="font-family: Arial, sans-serif; line-height: 20px; color: #444444; font-size: 10px;">
			      

			      Pesan ini dikirim ke '.$email.' atas permintaan Anda.
			      Untuk membantu menjaga akun Anda tetap aman, jangan teruskan email ini.

			      <br>
			      <p>Terima kasih,</p>
			      <br>
			      <br>
			      <p>Admin SMART-LBM</p>

			    </div>
			  </td></tr></tbody></table>
			</td></tr></tbody></table>

<table class="table-space" height="6" style="height: 6px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="6" style="height: 6px; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>

<table class="table-row-fixed" width="450" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-row-fixed-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 1px; padding-right: 1px;" valign="top" align="left">
  <table class="table-col" align="left" width="448" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="table-col-td" width="448" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
    <table width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td width="100%" align="center" bgcolor="#f5f5f5" style="font-family: Arial, sans-serif; line-height: 24px; color: #bbbbbb; font-size: 13px; font-weight: normal; text-align: center; padding: 9px; border-width: 1px 0px 0px; border-style: solid; border-color: #e3e3e3; background-color: #f5f5f5;" valign="top">
      <a href="#" style="color: #428bca; text-decoration: none; background-color: transparent;">

      SMART-LBM &copy; '.date('Y').'

      </a>
      <br>
      <a style="color: #5b7a91; text-decoration: none; background-color: transparent;">SMART-LBM (Learning Management System) by Lembaga Bakti Muslim</a>
     
    </td></tr></tbody></table>
  </td></tr></tbody></table>
</td></tr></tbody></table>
<table class="table-space" height="1" style="height: 1px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="1" style="height: 1px; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>
<table class="table-space" height="36" style="height: 36px; font-size: 0px; line-height: 0; width: 450px; background-color: #f4f6f9;" width="450" bgcolor="#f4f6f9" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="36" style="height: 36px; width: 450px; background-color: #f4f6f9;" width="450" bgcolor="#f4f6f9" align="left">&nbsp;</td></tr></tbody></table></td></tr></table>
</td></tr>
 </table>


			 </body>
			 </html>';


		mail($to, $subject, $htmlContent, $headers);
		$this->pengguna_model->set_pemulihan($email,$kode_baru);
	}

}