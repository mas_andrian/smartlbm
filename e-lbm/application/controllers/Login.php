<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct() {
		parent::__construct();
    	$this->load->library('session');
    	
		$this->load->model('login_model');
		$this->load->model('pengguna_model');
	}
 
	public function index()
	{
		if($this->session->userdata('username') != NULL) {
        	redirect(site_url('dashboard'));
      	}

		$this->load->view('pages/login');
	}

	public function do_login() {
	    if($this->session->userdata('username') != NULL) {
	        redirect(site_url('dashboard'));
	    }

	    $this->load->library('form_validation');
	    $this->load->helper('url');

	    $data['user'] = $this->login_model->get_username($this->input->post('username'));
	    $data['pengguna'] = $this->pengguna_model->get_pengguna_lain($data['user']['id_pengguna']);

	    $data['sekolah'] = $this->login_model->akses_sekolah($this->input->post('username'),$this->input->post('password'));

	    if(password_verify(md5($this->input->post('password')), $data['user']['password']) and $data['pengguna']['status'] == 1) {
	        $this->session->set_userdata('username', $this->input->post('username'));
	        if($data['user']['role_id'] == 2){
	        	$this->session->set_userdata('role', 'guru');
	        	$this->session->set_userdata('id_sekolah', $data['pengguna']['id_sekolah']);
	        } else {
	        	$this->session->set_userdata('role', 'superadmin');
	        }
	        redirect(site_url('dashboard'));
	    } else if(count($data['sekolah']) > 0) {
	    	$this->session->set_userdata('username', $this->input->post('username'));
	    	$this->session->set_userdata('role', 'sekolah');
	    	$this->session->set_userdata('id_sekolah', $data['sekolah']['id']);
	        redirect(site_url('dashboard'));
		} else {
	    	if ($data['pengguna']['status'] == 0 and count($data['user']) != 0) {
	    		$this->session->set_userdata('error', '<b>Login Ditolak</b>. Akun anda telah dinonaktifkan!');
	    	} else {
	        $this->session->set_userdata('error', '<b>Login Gagal</b>. Silakan coba lagi, pastikan username dan password anda sesuai!');
	    	}
	    	redirect(site_url('login'));
	    }
	}

	//private function deskripsi($pass_in, $enkrip_bcrytp){
	//	return password_verify(md5($pass_in), $enkrip_bcrytp);
	//}

	public function do_logout() {
	    $this->session->sess_destroy();
	    redirect(site_url('login'));
	}

}
