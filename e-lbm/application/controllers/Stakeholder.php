<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stakeholder extends CI_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->library('session');

		if($this->session->userdata('username') == NULL) {
			redirect(site_url('login'));
		}

		$this->load->model('dashboard_model');
		$this->load->model('profil_sekolah_model');
		$this->load->model('pemberitahuan_model');
		
		$this->load->model('stakeholder_model');
	}

	public function index()
	{
		$data['title'] = "Stakeholder";

		//get data model
		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));

		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah	
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();

		$data['stakeholder'] = $this->stakeholder_model->get_stakeholder();

		if ($data['pengguna']['role_id'] == 1) {
			$this->load->view('templates/header',$data);
			$this->load->view('stakeholder/stakeholder_index');
			$this->load->view('templates/footer');
		} else {
			redirect(site_url('dashboard'));
		}
	}

	public function do_edit()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');
		$id =  $this->uri->segment(3);

		$this->stakeholder_model->update_stakeholder($id);
		redirect(site_url('stakeholder'));
	}

	public function do_edit_pass()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');
		$id =  $this->uri->segment(3);

		$this->stakeholder_model->update_pass($id);
		redirect(site_url('stakeholder'));
	}
}
