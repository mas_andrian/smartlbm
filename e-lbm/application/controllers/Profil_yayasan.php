<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil_yayasan extends CI_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->library('session');

		if($this->session->userdata('username') == NULL) {
			redirect(site_url('login'));
		}

		$this->load->model('dashboard_model');
		$this->load->model('profil_sekolah_model');

		
		$this->load->model('pengguna_model');
		$this->load->model('mata_pelajaran_model');
		$this->load->model('pemberitahuan_model');
		$this->load->model('siswa_model');
	}

	public function index()
	{
		$data['title'] = "Profil Yayasan";

		//get data model
		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		//$data['pengajuan_pkg'] = $this->PKG_model->get_pengajuan_supervisi();
		//$data['tugas_asesor'] = $this->PKG_model->get_tugas_asesor($data['pengguna']['id_pengguna']);

		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));

		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}

		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		//$data['tot_lp'] = count($this->lembaga_pendidikan_model->get_lembaga_pendidikan());
		if($this->session->userdata('role') == 'sekolah'){
			$data['tot_pengajar'] = count($this->pengguna_model->get_tenaga_pengajar_sekolah($this->session->userdata('id_sekolah')));
			$data['tot_siswa'] = count($this->siswa_model->data_siswa($this->session->userdata('id_sekolah')));
		} else if($this->session->userdata('role') == 'superadmin'){
			$data['tot_pengajar'] = count($this->pengguna_model->get_tenaga_pengajar());
			$data['tot_siswa'] = count($this->siswa_model->data_siswa());
		}
		
		$data['tot_info'] = sizeof($this->pemberitahuan_model->get_pemberitahuan());
		$this->load->view('templates/header', $data);
		$this->load->view('profil_yayasan/profil_yayasan_index');
		$this->load->view('templates/footer');
	}

	public function edit()
	{
		$data['title'] = "Edit Profil Yayasan";

		//get data model
		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}

		$this->load->view('templates/header', $data);
		if ($data['pengguna']['role_id'] == 1 or $this->session->userdata('role') == 'sekolah') {
			$this->load->view('profil_yayasan/profil_yayasan_edit');
		} else {
			$this->load->view('profil_yayasan/profil_yayasan_index');
		}
		$this->load->view('templates/footer');
	}

	public function do_edit_profil(){
		$this->load->library('form_validation');
		$this->load->helper('url');
		$id = $this->input->post('id_sekolah');
		
		$this->profil_sekolah_model->edit_profil_sekolah($id);
		$this->session->set_userdata('status_edit_sekolah', '1');
		$url = "profil_yayasan";
		redirect(site_url($url));
	}

	public function do_edit_logo(){
		$this->load->library('form_validation');
		$this->load->helper('url');
		$id = $this->input->post('id_sekolah');

		$data['path'] = $this->profil_sekolah_model->get_profil($id);
		
		$slug = url_title($data['path']['nama_sekolah'], 'dash', TRUE);
		unlink(".".$data['path']['logo_sekolah']);

		$folder = "./assets/logo/";
		$exp  = explode('.',$_FILES['foto']['name']);
		$nama = $exp[0];
		$ext  = $exp[1];

		$file_path = $folder.$slug.".".$ext;
		move_uploaded_file($_FILES['foto']['tmp_name'], $file_path);
		$path = "/assets/logo/".$slug.".".$ext;

		$this->profil_sekolah_model->edit_logo_sekolah($id,$path);
		$this->session->set_userdata('status_edit_sekolah', '1');
		$url = "profil_yayasan";
		redirect(site_url($url));
	}
}

