<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kalender_pendidikan extends CI_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->library('session');

		if($this->session->userdata('username') == NULL) {
			redirect(site_url('login'));
		}

		$this->load->model('dashboard_model');
		$this->load->model('profil_sekolah_model');
		$this->load->model('pemberitahuan_model');

		$this->load->model('kalender_model');

	}

	public function index()
	{
		$data['title'] = "Kalender Pendidikan";

		//get data model
		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));

		$data['kalender'] = $this->kalender_model->get_kalender();

		$this->load->view('templates/header',$data);
		$this->load->view('kalender/kalender_index');
		$this->load->view('templates/footer');
	}

	public function view()
	{
		$data['title'] = "Lihat Kalender Pendidikan";

		$slug = $this->uri->segment(3);
		//get data model
		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil sekolah
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));

		$data['kalender'] = $this->kalender_model->get_kalender($slug);

		$this->load->view('templates/header',$data);
		$this->load->view('kalender/kalender_view');
		$this->load->view('templates/footer');

	}

	public function do_tambah()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');

		$slug = "kalender-tahun-ajaran_".url_title($this->input->post('jenjang'))."_".url_title($this->input->post('thn_ajar'));

		$folder = "./assets/kalender/";
		$exp  = explode('.',$_FILES['file']['name']);
		$nama = $exp[0];
		$ext  = $exp[1];

		$file_path = $folder.$slug.".".$ext;
		move_uploaded_file($_FILES['file']['tmp_name'], $file_path);
		$path = "/assets/kalender/".$slug.".".$ext;
		$size = $_FILES['file']['size'];

		$this->kalender_model->set_kalender($path, $size, $slug);
		$this->session->set_userdata('status_tambah_kalender', '1');
		redirect(site_url('kalender_pendidikan'));
	}

	public function do_hapus()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');
		$slug = $this->uri->segment(3);

		$data['file'] = $this->kalender_model->get_kalender($slug);
		unlink(".".$data['file']['path']);

		$this->kalender_model->hapus_kalender($slug);
		 $this->session->set_userdata('status_hapus_kalender', '1');
		redirect(site_url('kalender_pendidikan'));
	}
}
