<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materi extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		if($this->session->userdata('username') == NULL) {
			redirect(site_url('login'));
		}
		$this->load->model('dashboard_model');
		$this->load->model('materi_model');
		$this->load->model('bab_model');
		$this->load->model('profil_sekolah_model');
		$this->load->model('pemberitahuan_model');
		$this->load->model('Mata_pelajaran_model');
		$this->load->model('pengguna_model');
	}

	public function index()
	{
		$data['title'] = "Materi Kelas";
		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));
		
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
			if($this->session->userdata('role') == 'sekolah'){
				$data['tenaga_pengajar'] = $this->pengguna_model->get_tenaga_pengajar_sekolah($this->session->userdata('id_sekolah'));
				$data['materi'] = $this->materi_model->get_materi($this->session->userdata('id_sekolah'));
			} else {
				$data['tenaga_pengajar'] = $this->pengguna_model->get_tenaga_pengajar_sekolah($this->session->userdata('id_sekolah'));
				$data['materi'] = $this->materi_model->get_materi_by_guru($data['pengguna']['id_pengguna'], $this->session->userdata('id_sekolah'));
			}
			
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
			$data['materi'] = $this->materi_model->get_materi();
			$data['tenaga_pengajar'] = $this->pengguna_model->get_tenaga_pengajar();

		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$this->load->view('templates/header',$data);
		$this->load->view('materi/materi_index');
		$this->load->view('templates/footer');
	}

	public function tambah()
	{
		$data['title'] = "Tambah Materi Kelas";

		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['mapel'] = $this->Mata_pelajaran_model->get_mapel_by_jenjang($data['sekolah']['jenjang']);
		$data['tenaga_pengajar'] = $this->pengguna_model->get_tenaga_pengajar_sekolah($this->session->userdata('id_sekolah'));
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$this->load->view('templates/header',$data);
		if ($this->session->userdata('role') != 'sekolah') {
			$this->load->view('pages/error_500');
		} else {
			$this->load->view('materi/materi_tambah');
		}
		$this->load->view('templates/footer');
	}

	public function edit($id)
	{
		$data['title'] = "Ubah Materi Kelas";

		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['mapel'] = $this->Mata_pelajaran_model->get_mapel_by_jenjang($data['sekolah']['jenjang']);
		$data['tenaga_pengajar'] = $this->pengguna_model->get_tenaga_pengajar_sekolah($this->session->userdata('id_sekolah'));

		$id = $this->uri->segment(3);
		$data['materi'] = $this->materi_model->get_materi_by_id($id, $this->session->userdata('id_sekolah'));
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$this->load->view('templates/header',$data);
		if ($this->session->userdata('role') != 'sekolah') {
			$this->load->view('pages/error_500');
		} else {
			$this->load->view('materi/materi_edit');
		}
		$this->load->view('templates/footer');
	}

	public function detail($id, $mapel)
	{
		$data['title'] = "Detail Materi Kelas";

		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));
		
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['tenaga_pengajar'] = $this->pengguna_model->get_tenaga_pengajar_sekolah($this->session->userdata('id_sekolah'));
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['tenaga_pengajar'] = $this->pengguna_model->get_tenaga_pengajar();
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}

		$id = $this->uri->segment(3);
		$data['materi'] = $this->materi_model->get_materi_by_id($id, $this->session->userdata('id_sekolah'));
		$data['bab'] = $this->bab_model->get_bab_by_materi($id);

		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$this->load->view('templates/header',$data);
		$this->load->view('materi/materi_detail');
		$this->load->view('templates/footer');
	}

	public function do_tambah() 
	{
		$this->load->library('form_validation');
		$this->load->helper('url');

		$id_sekolah = $this->session->userdata('id_sekolah');
        $id_mapel 	= $this->input->post('id_mata_pelajaran');
        $tingkatan 	= $this->input->post('tingkatan');
		$cek_in = $this->materi_model->cek_materi_exist($id_sekolah, $id_mapel, $tingkatan);

		// print_r(sizeof($cek_in));
		// exit();

		if(sizeof($cek_in) > 0){
			$this->session->set_userdata('status_tambah_materi', 'exist');
			redirect(site_url('materi'));
		} else {
			$this->materi_model->set_materi();
			$this->session->set_userdata('status_tambah_materi', '1');
			redirect(site_url('materi'));
		}
	}

	public function do_edit($id)
	{
		$this->load->library('form_validation');
		$this->load->helper('url');

		$this->materi_model->set_materi($id);
		$this->session->set_userdata('status_edit_materi', '1');
		redirect(site_url('materi'));
	}

	public function do_edit_status()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');
		$status = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$slug = $this->uri->segment(5);

		$this->materi_model->set_single_data_materi('is_aktif', $status, $id);
		$this->session->set_userdata('status_aktif_materi', '1');
		redirect(site_url('materi/detail/'.$id.'/'.$slug));
	}
}