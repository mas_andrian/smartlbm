<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->library('session');

		if($this->session->userdata('username') == NULL) {
			redirect(site_url('login'));
		}

		$this->load->model('dashboard_model');
		$this->load->model('profil_sekolah_model');
		$this->load->model('pemberitahuan_model');
		$this->load->model('pengguna_model');

		$this->load->model('siswa_model');
		$this->load->model('nilai_model');
	}

	public function index(){
		$data['title'] = "Siswa";

		//get data model
		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));

		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
			$data['siswa'] = $this->siswa_model->data_siswa($this->session->userdata('id_sekolah'));
			//print_r($data['siswa']);
			//exit();
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
			$data['siswa'] = $this->siswa_model->data_siswa();
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();

		$this->load->view('templates/header',$data);
		$this->load->view('siswa/siswa_index');
		$this->load->view('templates/footer');
	}


	public function tambah()
	{
		$data['title'] = "Tambah Siswa";

		//get data model
		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));

		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();


		$this->load->view('templates/header',$data);
		if ($this->session->userdata('role') != 'sekolah') {
			$this->load->view('pages/error_500');
		} else {
			$this->load->view('siswa/siswa_tambah');
		}
		$this->load->view('templates/footer');
	}

	public function do_tambah()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');

		$nis = $this->input->post('nis');
		$email = $this->input->post('email');

		$is_cek['nis'] = $this->siswa_model->cek_data_sudah_ada("siswa", "nis", $nis);
		$is_cek['email'] = $this->siswa_model->cek_data_sudah_ada("siswa", "email", $email);

		$is_cek_nis = sizeof($is_cek['nis']);
		$is_cek_email = sizeof($is_cek['email']);

		$a = 0; $b = 0; 
		
		if ($is_cek_nis != 0) {
			$this->session->set_userdata('cek_nis', 'ada');
			$a=1;
		}
		if ($is_cek_email != 0) {
			$this->session->set_userdata('cek_email', 'ada');
			$d=1;
		}

		$tot_cek = $a+$b;

		if ($tot_cek != 0) {
			$url_link = "siswa/tambah";
		} else {
			$aksi = 0; //insert
			$slug = $this->input->post('nis');

			$folder = "./assets/foto_siswa/";
			$exp  = explode('.',$_FILES['foto']['name']);
			$nama = $exp[0];
			$ext  = $exp[1];

			$file_path = $folder.$slug.".".$ext;
			move_uploaded_file($_FILES['foto']['tmp_name'], $file_path);
			$path = "/assets/foto_siswa/".$slug.".".$ext;

			$get_password = $this->generate_password();
			$this->siswa_model->insert_siswa($path, $get_password);
			
			$this->kirim_info_akun($email,$this->input->post('nama'),$get_password);

			$this->session->set_userdata('status_tambah_siswa', '1');
			$url_link = "siswa";
		}
		redirect(site_url($url_link));
	}

	private function generate_password(){
		$length = 8;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function get_nilai_latihan_siswa(){
		$id_siswa = $this->input->post("id_siswa");
		$id_bab = $this->input->post("id_bab");

		$get = $this->nilai_model->get_nilai_latihan_siswa($id_siswa, $id_bab);
		if($get){
			echo json_encode(array("status" => "success", "data" => $get));
		} else {
			echo json_encode(array("status" => "error", "data" => array()));
		}
	}

	public function profil()
	{
		//get data model
		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));

		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah	
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();

		$data['title'] =  "Profil Siswa";
		//print_r($this->session->userdata('role'));
		//exit();

		$id = $this->uri->segment(3);
		$data['profil_siswa'] = $this->siswa_model->get_siswa_by_id($id);
		$data['presensi_siswa'] = $this->siswa_model->get_dokumen('presensi', $id);
		$data['raport_siswa'] = $this->siswa_model->get_dokumen('raport', $id);
		$data['nilai'] = $this->nilai_model->get_nilai_mapel_siswa($this->session->userdata('id_sekolah'), $id, $data['profil_siswa']['tingkatan']);

		$this->load->view('templates/header',$data);

		if ($this->session->userdata('id_sekolah') == $data['profil_siswa']['id_sekolah'] or $this->session->userdata('role')=='superadmin' or $this->session->userdata('role')=='guru') {
			$this->load->view('siswa/siswa_profil');
		} else {
			$this->load->view('pages/error_500');
		}
		$this->load->view('templates/footer');
	}

	public function do_edit_foto_profil(){
		$this->load->library('form_validation');
		$this->load->helper('url');
		$id = $this->uri->segment(3);

		$data['path'] = $this->siswa_model->get_siswa_by_id($id);
		
		unlink(".".$data['path']['foto_diri']);

		$folder = "./assets/foto_siswa/";
		$exp  = explode('.',$_FILES['foto']['name']);
		$nama = $exp[0];
		$ext  = $exp[1];

		$file_path = $folder.$id.".".$ext;
		move_uploaded_file($_FILES['foto']['tmp_name'], $file_path);
		$path = "/assets/foto_siswa/".$id.".".$ext;

		$this->siswa_model->set_single_data_siswa('foto_diri', $path, $id);
		$this->session->set_userdata('status_foto_siswa', 'sukses');
		
		$url = "siswa/profil/".$id."/".$this->generate_password();
		redirect(site_url($url));
	}

	public function do_edit_status()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');
		$status = $this->uri->segment(3);
		$id = $this->uri->segment(4);

		$this->siswa_model->set_single_data_siswa('is_aktif', $status, $id);
		$url = "siswa/profil/".$id."/".$this->generate_password();
		$this->session->set_userdata('status_aktif_siswa', '1');
		redirect(site_url($url));
	}

	public function edit()
	{
		$data['title'] = "Edit Siswa";

		//get data model
		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));

		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();

		$id = $this->uri->segment(3);
		$data['profil_siswa'] = $this->siswa_model->get_siswa_by_id($id);

		$this->load->view('templates/header',$data);
		if ($this->session->userdata('id_sekolah') == $data['profil_siswa']['id_sekolah'] and $this->session->userdata('role')=='sekolah') {
			$this->load->view('siswa/siswa_edit'); 
		} else {
			$this->load->view('pages/error_500');
		}
		$this->load->view('templates/footer');
	}

	public function do_edit_profil()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');
		$id = $this->uri->segment(3);
		$flag = $this->uri->segment(4);

		$nis = $this->input->post('nis');

		$is_cek['nis'] = $this->siswa_model->cek_data_sudah_ada("siswa", "nis", $nis);
		
		$is_cek_nis = sizeof($is_cek['nis']);
		
		$this->siswa_model->edit_profil_siswa($id);
		$a = 0; 
		
			if ($is_cek_nis > 0 and $is_cek['nis']['nis'] == $nis) {
				$this->session->set_userdata('cek_nis', 'ada');
				$a = 1;
			} else {
				$this->siswa_model->set_single_data_siswa("nis", $nis, $id);
				$this->session->set_userdata('cek_nis', 'sukses');
			}

		if ($a != 0) {
			$this->session->set_userdata('kolom_lain', 'sukses');
			$url = "siswa/edit/".$id."/".$flag."/".$this->generate_password();

		} else {
			$this->session->set_userdata('status_edit_siswa', 'sukses');
			$url = "siswa/profil/".$id."/".$flag."/".$this->generate_password();
		}
		
		redirect(site_url($url));
	}

	public function do_edit_otorisasi(){
		$this->load->library('form_validation');
		$this->load->helper('url');
		$id_siswa = $this->uri->segment(3);
		$flag = $this->uri->segment(4);

		$username = $this->input->post('username');
		$email = $this->input->post('email');

		$is_cek['username'] = $this->siswa_model->cek_data_sudah_ada("siswa", "username", $username);
		$is_cek['email'] = $this->siswa_model->cek_data_sudah_ada("siswa", "email", $email);

		$is_cek_username = sizeof($is_cek['username']);
		$is_cek_email = sizeof($is_cek['email']);

			if ($is_cek_username > 0 and $is_cek['username']['username'] == $username) {
				$this->session->set_userdata('cek_username', 'ada');
				$this->session->set_userdata('info_username', 'Perhatian!');
				$this->session->set_userdata('pesan_username', 'Username sudah digunakan atau username masih sama dengan sebelumnya, perubahan username ditangguhkan.');
				$this->session->set_userdata('class_username', 'alert-warning');
			} else {
				$this->siswa_model->set_single_data_siswa("username", $username, $id_siswa);
				$this->session->set_userdata('cek_username', 'sukses');
				$this->session->set_userdata('info_username', 'Berhasil');
				$this->session->set_userdata('pesan_username', 'Username berhasil diubah.');
				$this->session->set_userdata('class_username', 'alert-success');
			}

			if ($is_cek_email > 0 and $is_cek['email']['email'] == $email) {
				$this->session->set_userdata('cek_email', 'ada');
				$this->session->set_userdata('info_email', 'Perhatian!');
				$this->session->set_userdata('pesan_email', 'Email sudah digunakan atau email masih sama dengan sebelumnya, perubahan email ditangguhkan.');
				$this->session->set_userdata('class_email', 'alert-warning');
			} else {
				$this->siswa_model->set_single_data_siswa("email", $email, $id_siswa);
				$this->session->set_userdata('cek_email', 'sukses');
				$this->session->set_userdata('info_email', 'Berhasil');
				$this->session->set_userdata('pesan_email', 'Email berhasil diubah.');
				$this->session->set_userdata('class_email', 'alert-success');
			}

		$url = "siswa/edit/".$id_siswa."/".$flag."/".$this->generate_password();
		redirect(site_url($url));
	}

	public function do_edit_password()
	{
		$this->load->library('form_validation');
		$this->load->helper('url'); 
		$id_siswa = $this->uri->segment(3);
		$flag = $this->uri->segment(4);
		
		$this->siswa_model->edit_password_siswa($id_siswa);
		$this->session->set_userdata('status_edit_siswa', '1');
		$url = "siswa/profil/".$id_siswa."/".$flag."/".$this->generate_password();
		redirect(site_url($url));
	}

	public function do_tambah_dokumen($tipe){
		$this->load->library('form_validation');
		$this->load->helper('url');

		$slug = $tipe."_".url_title($this->input->post('id_siswa'))."_".url_title($this->input->post('nama_siswa'))."_".url_title($this->input->post('tingkatan'))."_".url_title($this->input->post('kelas'))."_".url_title($this->input->post('semester'))."_".url_title($this->input->post('thn_ajar'))."_".url_title($this->input->post('sekolah'));

		$folder = "./assets/".$tipe."/";
		$path = $_FILES['file']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);

		$file_path = $folder.$slug.".".$ext;
		move_uploaded_file($_FILES['file']['tmp_name'], $file_path);
		$path = "/assets/".$tipe."/".$slug.".".$ext;

		$this->siswa_model->insert_dokumen($tipe, $path);
		$this->session->set_userdata('status_tambah_dokumen', $tipe);
		$url = "siswa/profil/".$this->input->post('id_siswa')."/".url_title($this->input->post('nama_siswa'))."/".$this->generate_password();
		redirect(site_url($url));
	}

	public function do_delete_dokumen($id, $id_siswa, $nama_siswa, $tipe){
		$this->load->library('form_validation');
		$this->load->helper('url');

		$data['file'] = $this->siswa_model->get_dokumen_by_id($id);
		unlink(".".$data['file']['file']);

		$this->siswa_model->hapus_dokumen($id);
		$this->session->set_userdata('status_hapus_dokumen', $tipe);
		$url = "siswa/profil/".$id_siswa."/".$nama_siswa."/".$this->generate_password();
		redirect(site_url($url));
	}

	private function kirim_info_akun($email,$nama,$password){
		//$data['sekolah'] = $this->profil_sekolah_model->get_profil(); //profil sekolah

		$to = $email;
		$kode_baru = $password;
		$subject = "Informasi Akun SMART-LBM Siswa";
		    
		$headers = "From: SMART-LBM Notification <info.noreply@smartlbm.com>" . "\r\n"; 
		$headers .= "Content-type:text/html" . "\r\n";
		$htmlContent = '
			<html>
			<head>
			  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			  <meta name="viewport" content="initial-scale=1.0" />
			  <meta name="format-detection" content="telephone=no" />
			  <title></title>
			  <style type="text/css">
			 	body {
					width: 100%;
					margin: 0;
					padding: 0;
					-webkit-font-smoothing: antialiased;
				}
				@media only screen and (max-width: 600px) {
					table[class="table-row"] {
						float: none !important;
						width: 98% !important;
						padding-left: 20px !important;
						padding-right: 20px !important;
					}
					table[class="table-row-fixed"] {
						float: none !important;
						width: 98% !important;
					}
					table[class="table-col"], table[class="table-col-border"] {
						float: none !important;
						width: 100% !important;
						padding-left: 0 !important;
						padding-right: 0 !important;
						table-layout: fixed;
					}
					td[class="table-col-td"] {
						width: 100% !important;
					}
					table[class="table-col-border"] + table[class="table-col-border"] {
						padding-top: 12px;
						margin-top: 12px;
						border-top: 1px solid #E8E8E8;
					}
					table[class="table-col"] + table[class="table-col"] {
						margin-top: 15px;
					}
					td[class="table-row-td"] {
						padding-left: 0 !important;
						padding-right: 0 !important;
					}
					table[class="navbar-row"] , td[class="navbar-row-td"] {
						width: 100% !important;
					}
					img {
						max-width: 100% !important;
						display: inline !important;
					}
					img[class="pull-right"] {
						float: right;
						margin-left: 11px;
			            max-width: 125px !important;
						padding-bottom: 0 !important;
					}
					img[class="pull-left"] {
						float: left;
						margin-right: 11px;
						max-width: 125px !important;
						padding-bottom: 0 !important;
					}
					table[class="table-space"], table[class="header-row"] {
						float: none !important;
						width: 98% !important;
					}
					td[class="header-row-td"] {
						width: 100% !important;
					}
				}
				@media only screen and (max-width: 480px) {
					table[class="table-row"] {
						padding-left: 16px !important;
						padding-right: 16px !important;
					}
				}
				@media only screen and (max-width: 320px) {
					table[class="table-row"] {
						padding-left: 12px !important;
						padding-right: 12px !important;
					}
				}
				@media only screen and (max-width: 458px) {
					td[class="table-td-wrap"] {
						width: 100% !important;
					}
				}
			  </style>
			 </head>
			 <body style="font-family: Arial, sans-serif; font-size:13px; color: #444444; min-height: 200px;" bgcolor="#f4f6f9" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
			 <table width="100%" height="100%" bgcolor="#f4f6f9" cellspacing="0" cellpadding="0" border="0">
			 <tr><td width="100%" align="center" valign="top" bgcolor="#f4f6f9" style="background-color:#f4f6f9; min-height: 200px;">
			<table><tr><td class="table-td-wrap" align="center" width="458"><table class="table-space" height="18" style="height: 18px; font-size: 0px; line-height: 0; width: 450px; background-color: #f4f6f9;" width="450" bgcolor="#f4f6f9" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="18" style="height: 18px; width: 450px; background-color: #f4f6f9;" width="450" bgcolor="#f4f6f9" align="left">&nbsp;</td></tr></tbody></table>
			<table class="table-space" height="8" style="height: 8px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="8" style="height: 8px; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>

			<table class="table-row" width="450" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
			  <table class="table-col" align="left" width="378" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="table-col-td" width="378" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; width: 378px;" valign="top" align="left">
			    <table class="header-row" width="378" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="header-row-td" width="378" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: #478fca; margin: 0px; font-size: 18px; padding-bottom: 10px; padding-top: 15px;" valign="top" align="left">

			    Kepada siswa. '.$nama.',

			    </td></tr></tbody></table>
			    <div style="font-family: Arial, sans-serif; line-height: 20px; color: #444444; font-size: 13px;">
			      
			      Bersama e-mail ini kami beritahukan informasi tentang akun SMART-LBM Anda yang sudah dibuat:

			    </div>
			  </td></tr></tbody></table>
			</td></tr></tbody></table>
			    
			<table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>

			<table class="table-row" width="450" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
			  <table class="table-col" align="left" width="378" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="table-col-td" width="378" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; width: 378px;" valign="top" align="left">
			    <div style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; text-align: left;">
			      
			    <p>Username : '.$email.'</p>
			    <p>Password : '.$kode_baru.'</p> 

			     <p><center><a href="smartlbm.com" target="_blank">Klik di sini untuk Login.</a></center></p>

			    </div>
			   <table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 378px; background-color: #ffffff;" width="378" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 378px; background-color: #ffffff;" width="378" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>
  </td></tr></tbody></table>
</td></tr></tbody></table>

<table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>
			<table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 450px; padding-left: 16px; padding-right: 16px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" align="center">&nbsp;<table bgcolor="#E8E8E8" height="0" width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td bgcolor="#E8E8E8" height="1" width="100%" style="height: 1px; font-size:0;" valign="top" align="left">&nbsp;</td></tr></tbody></table></td></tr></tbody></table>


<table class="table-row" width="450" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
			  <table class="table-col" align="left" width="378" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="table-col-td" width="378" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; width: 378px;" valign="top" align="left">
			    
			    <div style="font-family: Arial, sans-serif; line-height: 20px; color: #444444; font-size: 10px;">
			      

			      Untuk membantu menjaga akun Anda tetap aman, jangan teruskan email ini.

			    </div>
			  </td></tr></tbody></table>
			</td></tr></tbody></table>

<table class="table-space" height="6" style="height: 6px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="6" style="height: 6px; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>

<table class="table-row-fixed" width="450" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-row-fixed-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 1px; padding-right: 1px;" valign="top" align="left">
  <table class="table-col" align="left" width="448" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="table-col-td" width="448" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
    <table width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td width="100%" align="center" bgcolor="#f5f5f5" style="font-family: Arial, sans-serif; line-height: 24px; color: #bbbbbb; font-size: 13px; font-weight: normal; text-align: center; padding: 9px; border-width: 1px 0px 0px; border-style: solid; border-color: #e3e3e3; background-color: #f5f5f5;" valign="top">
      <a href="#" style="color: #428bca; text-decoration: none; background-color: transparent;">

      SMART-LBM &copy; '.date('Y').'

      </a>
      <br>
      <a style="color: #5b7a91; text-decoration: none; background-color: transparent;">SMART-LBM (Learning Management System) by Lembaga Bakti Muslim</a>
     
    </td></tr></tbody></table>
  </td></tr></tbody></table>
</td></tr></tbody></table>
<table class="table-space" height="1" style="height: 1px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="1" style="height: 1px; width: 450px; background-color: #ffffff;" width="450" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>
<table class="table-space" height="36" style="height: 36px; font-size: 0px; line-height: 0; width: 450px; background-color: #f4f6f9;" width="450" bgcolor="#f4f6f9" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="36" style="height: 36px; width: 450px; background-color: #f4f6f9;" width="450" bgcolor="#f4f6f9" align="left">&nbsp;</td></tr></tbody></table></td></tr></table>
</td></tr>
 </table>
			 </body>
			 </html>';


		mail($to, $subject, $htmlContent, $headers);
	}

}

?>