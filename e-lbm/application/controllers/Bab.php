<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bab extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		if($this->session->userdata('username') == NULL) {
			redirect(site_url('login'));
		}
		$this->load->model('dashboard_model');
		$this->load->model('profil_sekolah_model');
		$this->load->model('pemberitahuan_model');
		$this->load->model('pengguna_model');
		$this->load->model('bab_model');
		$this->load->model('sub_bab_model');
		$this->load->model('bank_soal_model');
		$this->load->model('nilai_model');
	}

	public function tambah()
	{
		$data['title'] = "Tambah BAB Materi";

		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$this->load->view('templates/header',$data);
		if ($this->session->userdata('role') != 'guru' && $this->session->userdata('role') != 'sekolah') {
			$this->load->view('pages/error_500');
		} else {
			$this->load->view('bab/bab_tambah');
		}
		$this->load->view('templates/footer');
	}

	public function edit($id, $slug)
	{
		$data['title'] = "Ubah BAB Materi";

		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$data['bab'] = $this->bab_model->get_bab_by_id($id, $slug);
		$this->load->view('templates/header',$data);
		if ($this->session->userdata('role') != 'guru') {
			$this->load->view('pages/error_500');
		} else {
			$this->load->view('bab/bab_edit');
		}
		$this->load->view('templates/footer');
	}

	public function detail($id, $slug)
	{
		$data['title'] = "Detail BAB Materi";

		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$data['bab'] = $this->bab_model->get_bab_by_id($id, $slug);
		$data['sub_bab'] = $this->sub_bab_model->get_sub_bab_by_bab($id, $slug);
		$data['latihan'] = $this->bank_soal_model->get_soal_by_bab($id, 'latihan');
		$data['ujian'] = $this->bank_soal_model->get_soal_by_bab($id, 'ujian');
		$get_kelas_bab = $this->bab_model->get_kelas_by_materi($data['bab']->id_materi);
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
			$data['nilai'] = $this->nilai_model->get_nilai_siswa($this->session->userdata('id_sekolah'), $get_kelas_bab->tingkatan, $id);
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		
		$this->load->view('templates/header',$data);
		if ($this->session->userdata('role') != 'guru' && $this->session->userdata('role') != 'sekolah' && $this->session->userdata('role') != 'superadmin') {
			$this->load->view('pages/error_500');
		} else {
			$this->load->view('bab/bab_detail');
		}
		$this->load->view('templates/footer');
	}

	public function do_tambah($id, $mapel)
	{
		$this->load->library('form_validation');
		$this->load->helper('url');

		$config['upload_path']= './assets/bab_thumbnail/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['max_size'] = 30720;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if(isset($_FILES['thumbnail'])){
			if ($this->upload->do_upload('thumbnail')){
				$thumbnail = $this->upload->data();
				$path ='./assets/bab_thumbnail/'.$thumbnail['file_name'];
			}
		}

		$slug = "bab_".url_title(strtolower($this->input->post('judul')));

		$this->bab_model->set_bab(false, $id, $slug, $path);
		$this->session->set_userdata('status_tambah_bab', '1');
		redirect(site_url('materi/detail/'.$id.'/'.$mapel));
	}

	public function do_edit($id, $slug, $id_materi, $mapel)
	{
		$this->load->library('form_validation');
		$this->load->helper('url');

		$config['upload_path']= './assets/bab_thumbnail/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['max_size'] = 30720;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if(isset($_FILES['thumbnail'])){
			if ($this->upload->do_upload('thumbnail')){
				$thumbnail = $this->upload->data();
				$path ='./assets/bab_thumbnail/'.$thumbnail['file_name'];
			}
		}

		$slug = "bab_".url_title(strtolower($this->input->post('judul')));

		$this->bab_model->set_bab($id, false, $slug, $path);
		$this->session->set_userdata('status_tambah_bab', '1');
		redirect(site_url('materi/detail/'.$id_materi.'/'.$mapel));
	}

	public function do_edit_status()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');
		$status = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$slug = $this->uri->segment(5);

		$this->bab_model->set_single_data_bab('is_aktif', $status, $id);
		$this->session->set_userdata('status_aktif_siswa', '1');
		redirect(site_url('bab/detail/'.$id.'/'.$slug));
	}
}