<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lembaga_pendidikan extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('session');

		if($this->session->userdata('username') == NULL) {
			redirect(site_url('login'));
		}

		$this->load->model('dashboard_model');
		$this->load->model('profil_sekolah_model');
		$this->load->model('pengguna_model');
		$this->load->model('pemberitahuan_model');

		$this->load->model('lembaga_pendidikan_model');

	}

	public function index()
	{
		$data['title'] = "Lembaga Pendidikan";

		//get data model
		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil sekolah
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		
		$data['sekolah'] = $this->lembaga_pendidikan_model->get_lembaga_pendidikan();

		if($data['pengguna']['role_id'] == 1){
			$this->load->view('templates/header',$data);
			$this->load->view('lembaga_pendidikan/lembaga_pendidikan_index');
			$this->load->view('templates/footer');
		} else {
			redirect(site_url('dashboard'));
		}
		
	}

	public function do_tambah_sekolah()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');

		$this->lembaga_pendidikan_model->set_lembaga_pendidikan();
		$this->session->set_userdata('status_tambah_lembaga_pendidikan', '1');
		redirect(site_url('lembaga_pendidikan'));
	}

	public function do_edit_sekolah()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');
		$slug =  $this->uri->segment(3);

		$this->lembaga_pendidikan_model->set_lembaga_pendidikan($slug);
		 $this->session->set_userdata('status_edit_lembaga_pendidikan', '1');
		redirect(site_url('lembaga_pendidikan'));
	}

	public function do_hapus_sekolah()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');
		$slug = $this->uri->segment(3);

		$this->lembaga_pendidikan_model->hapus_lembaga_pendidikan($slug);
		$this->session->set_userdata('status_hapus_lembaga_pendidikan', '1');
		redirect(site_url('lembaga_pendidikan'));
	}
}

