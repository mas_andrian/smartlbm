<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mata_pelajaran extends CI_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->library('session');

		if($this->session->userdata('username') == NULL) {
			redirect(site_url('login'));
		}

		$this->load->model('dashboard_model');
		$this->load->model('profil_sekolah_model');
		$this->load->model('pengguna_model');
		$this->load->model('pemberitahuan_model');

		$this->load->model('mata_pelajaran_model');

	}

	public function index()
	{
		$data['title'] = "Mata Pelajaran";

		//get data model
		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));

		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
			$data['mata_pelajaran'] = $this->mata_pelajaran_model->get_mapel_by_jenjang($data['profil']['jenjang']);
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
			$data['mata_pelajaran'] = $this->mata_pelajaran_model->get_mata_pelajaran();
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();

		$this->load->view('templates/header',$data);
		$this->load->view('mata_pelajaran/mata_pelajaran_index');
		$this->load->view('templates/footer');
	}

	public function do_tambah()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');

		$this->mata_pelajaran_model->set_mata_pelajaran();
		$this->session->set_userdata('status_tambah_mapel', '1');
		redirect(site_url('mata_pelajaran'));
	}

	public function do_edit()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');
		$slug =  $this->uri->segment(3);

		$this->mata_pelajaran_model->set_mata_pelajaran($slug);
		 $this->session->set_userdata('status_edit_mapel', '1');
		redirect(site_url('mata_pelajaran'));
	}

	public function do_hapus()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');
		$slug = $this->uri->segment(3);

		$this->mata_pelajaran_model->hapus_mata_pelajaran($slug);
		$this->session->set_userdata('status_hapus_mapel', '1');
		redirect(site_url('mata_pelajaran'));
	}

}
