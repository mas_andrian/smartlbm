<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
  
class Blogpost extends CI_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->library('session');

        // Load Pagination library
		$this->load->library('pagination');

		if($this->session->userdata('username') == NULL) {
			redirect(site_url('login'));
		}
		$this->session->set_userdata('file_manager',true);
		$this->load->model('dashboard_model');
		$this->load->model('profil_sekolah_model');
		$this->load->model('pengguna_model');
		$this->load->model('pemberitahuan_model');

		$this->load->model('blogpost_model');

	}

	public function index()
	{
		redirect('blogpost/list');
	}

	public function list($rowno=0)
	{
		// Search text
		$search_text = "";
		if($this->input->post('submit') != NULL ){
			$search_text = $this->input->post('search');
			$this->session->set_userdata(array("search"=>$search_text));
		}else{
			if($this->session->userdata('search') != NULL){
				$search_text = $this->session->userdata('search');
			}
		}

		// Row per page
		$rowperpage = 10;

		// Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
      	
      	// All records count
      	$allcount = $this->blogpost_model->getrecordCount($search_text);

      	// Get  records
      	$users_record = $this->blogpost_model->getData($rowno,$rowperpage,$search_text);
      	
      	// Pagination Configuration
      	$config['base_url'] = base_url().'/blogpost/list';
      	$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $allcount;
		$config['per_page'] = $rowperpage;

		
		$config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
 
        $this->pagination->initialize($config); 
		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();
		$data['result'] = $users_record;
		$data['row'] = $rowno;
		$data['search'] = $search_text;

		$data['title'] = "Blogpost";

		//get data model
		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));

		$this->load->view('templates/header',$data);
		$this->load->view('blogpost/blogpost_index');
		$this->load->view('templates/footer');
	}

	public function do_tambah()
	{
		$this->load->library('form_validation');
		$this->load->helper('url');

		$slug = "banner-img_".date('YmdHis-').url_title($this->input->post('judul'));

		$folder = "./assets/blogpost/";
		$path = $_FILES['file']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);

		$file_path = $folder.$slug.".".$ext;
		move_uploaded_file($_FILES['file']['tmp_name'], $file_path);
		$path = "/assets/blogpost/".$slug.".".$ext;

		$this->blogpost_model->set_post($path);

		redirect(site_url('blogpost'));
	}

	public function view($slug)
	{
		$data['title'] = "Detail Post";

		//get data model
		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));

		$data['blogpost'] = $this->blogpost_model->get_post_detail($slug);

		$this->load->view('templates/header',$data);
		$this->load->view('blogpost/blogpost_view');
		$this->load->view('templates/footer');
	}

	public function do_edit_post($slug){
		$this->load->library('form_validation');
		$this->load->helper('url');

		$this->blogpost_model->update_post($slug);

		redirect(site_url('blogpost'));
	}

	public function do_edit_img($flag){
		$this->load->library('form_validation');
		$this->load->helper('url');

		unlink(".".$this->input->post('path'));

		$slug = $this->input->post('flagname');

		$folder = "./assets/blogpost/";
		$path = $_FILES['file']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);

		$file_path = $folder.$slug.".".$ext;
		move_uploaded_file($_FILES['file']['tmp_name'], $file_path);
		$path = "/assets/blogpost/".$slug.".".$ext;

		$this->blogpost_model->update_img($path, $flag);

		redirect(site_url('blogpost/view/'.$flag));
	}

	public function do_hapus($flag){

		$this->load->library('form_validation');
		$this->load->helper('url');

		$data['file'] = $this->blogpost_model->get_post_detail($flag);
		unlink(".".$data['file']['banner']);

		$this->blogpost_model->hapus_post($flag);
		redirect(site_url('blogpost'));
	}

}

?>