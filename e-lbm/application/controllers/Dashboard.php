<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->library('session');

		if($this->session->userdata('username') == NULL) {
			redirect(site_url('login'));
		}

		$this->load->model('profil_sekolah_model');
		$this->load->model('pemberitahuan_model');
		$this->load->model('pengguna_model');
		$this->load->model('lembaga_pendidikan_model');

		$this->load->model('dashboard_model');
		$this->load->model('siswa_model');
	}

	public function index()
	{
		$data['title'] = "Dashboard";

		//get data model
		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));

		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));

		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}

		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();

		
		$data['tot_lp'] = count($this->lembaga_pendidikan_model->get_lembaga_pendidikan());
		if($this->session->userdata('role') == 'sekolah'){
			$data['tot_pengajar'] = count($this->pengguna_model->get_tenaga_pengajar_sekolah($this->session->userdata('id_sekolah')));
			$data['tot_siswa'] = count($this->siswa_model->data_siswa($this->session->userdata('id_sekolah')));
		} else if($this->session->userdata('role') == 'superadmin'){
			$data['tot_pengajar'] = count($this->pengguna_model->get_tenaga_pengajar());
			$data['tot_siswa'] = count($this->siswa_model->data_siswa());
		}
		
		$data['tot_info'] = sizeof($this->pemberitahuan_model->get_pemberitahuan());
		

		$this->load->view('templates/header',$data);
		$this->load->view('dashboard/dashboard_index');
		$this->load->view('templates/footer');

	}
}
