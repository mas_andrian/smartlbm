<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank_soal extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		if($this->session->userdata('username') == NULL) {
			redirect(site_url('login'));
		}
		$this->load->model('dashboard_model');
		$this->load->model('profil_sekolah_model');
		$this->load->model('pemberitahuan_model');
		$this->load->model('pengguna_model');
		$this->load->model('bank_soal_model');
	}

	public function tambah($id, $slug, $type)
	{
		$data['title'] = "Tambah Bank Soal ".ucfirst($type);

		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$this->load->view('templates/header',$data);
		if ($this->session->userdata('role') != 'guru') {
			$this->load->view('pages/error_500');
		} else {
			$this->load->view('bank_soal/bank_soal_tambah');
		}
		$this->load->view('templates/footer');
	}

	public function edit($id, $id_bab, $slug, $type)
	{
		$data['title'] = "Ubah Bank Soal ".ucfirst($type);

		$data['pengguna'] = $this->dashboard_model->get_id_pengguna($this->session->userdata('username'));
		$data['identitas'] = $this->dashboard_model->get_identitas($data['pengguna']['id_pengguna']);
		$data['sekolah'] = $this->dashboard_model->get_identitas_sekolah($this->session->userdata('username'));
		if($this->session->userdata('role') == 'sekolah' || $this->session->userdata('role') == 'guru'){
			$data['profil'] = $this->profil_sekolah_model->get_sekolah($this->session->userdata('id_sekolah')); //profil sekolah
		} else { 
			$data['profil'] = $this->profil_sekolah_model->get_profil(); //profil yayasan
		}
		$data['info_aktif'] = $this->pemberitahuan_model->get_pemberitahuan_aktif();
		$data['soal'] = $this->bank_soal_model->get_soal_by_id($id);
		$this->load->view('templates/header',$data);
		if ($this->session->userdata('role') != 'guru') {
			$this->load->view('pages/error_500');
		} else {
			$this->load->view('bank_soal/bank_soal_edit');
		}
		$this->load->view('templates/footer');
	}

	public function do_tambah($id, $slug, $type)
	{
		$this->load->library('form_validation');
		$this->load->helper('url');

		$this->bank_soal_model->set_bab(false, $id, $type);
		$this->session->set_userdata('status_tambah_bank_soal', '1');
		redirect(site_url('bab/detail/'.$id.'/'.$slug));
	}

	public function do_edit($id, $id_bab, $slug, $type)
	{
		$this->load->library('form_validation');
		$this->load->helper('url');

		$this->bank_soal_model->set_bab($id, false, $type);
		$this->session->set_userdata('status_tambah_bank_soal', '1');
		redirect(site_url('bab/detail/'.$id_bab.'/'.$slug));
	}

}