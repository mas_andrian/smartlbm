 
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Siswa</a>
				</li>
				
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Profil Siswa
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<b>[<?= $profil_siswa['nis'];?>] <?= $profil_siswa['nama'];?></b>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $profil['nama_sekolah'];?>
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
					
					<div>
						<div id="user-profile-2" class="user-profile">
							<div class="tabbable">
								<ul class="nav nav-tabs padding-18">
									<li class="active">
										<a data-toggle="tab" href="#home">
											<i class="green ace-icon fa fa-user bigger-120"></i>
											Profil Siswa
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#presensi">
											<i class="blue ace-icon fa fa-check-square-o bigger-120"></i>
											Presensi Siswa
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#nilai">
											<i class="orange ace-icon fa fa-pencil-square-o bigger-120"></i>
											Rekap Nilai Pembelajaran Online
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#raport">
											<i class="blue ace-icon fa fa-book bigger-120"></i>
											Raport Siswa
										</a>
									</li>
								</ul>

								<div class="tab-content no-border padding-24">
									<div id="home" class="tab-pane in active">
										<div class="row">
											<div class="col-xs-12 col-sm-3 center">
												<span class="profile-picture">
													<img class="editable img-responsive" alt="<?= $profil_siswa['nama'];?>" id="avatar2" src="<?= base_url(); ?><?= $profil_siswa['foto_diri'];?>" />
												</span>

												<div class="space space-4"></div>
												<?php 
												if ($this->session->userdata('role') == 'sekolah') {
													?>
													<a href="#modal-table" role="button" data-toggle="modal" class="btn btn-sm btn-block btn-success">
														<i class="ace-icon fa fa-camera-retro bigger-110"></i>
														<span class="bigger-110">Ubah Foto Profil</span>
													</a>
												
													<a href="<?php echo site_url('siswa/edit');?>/<?= $profil_siswa['id_siswa']; ?>/<?= url_title($profil_siswa['nama'], 'dash', TRUE); ?>" class="btn btn-sm btn-block btn-primary">
														<i class="ace-icon fa fa-edit bigger-110"></i>
														<span class="bigger-110">Edit Profil</span>
													</a>
												<?php } ?>
												

											</div><!-- /.col -->

											<?php 
											if ($this->session->userdata('role') == 'sekolah') {
												?>
												<div id="modal-table" class="modal fade" tabindex="-1">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header no-padding">
																<div class="table-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		<span class="white">&times;</span>
																	</button>
																	Ubah Foto Profil
																</div>
															</div>

															<div class="modal-body">
																<form role="form" action="<?= base_url(); ?>siswa/do_edit_foto_profil/<?= $profil_siswa['id_siswa'];?>" method="POST" enctype="multipart/form-data">	
																	<input type="file" id="id-input-file-2"  accept="image/png, image/jpeg" class="form-control" name="foto" required/>
																	
																	
																</div>

																<div class="modal-footer no-margin-top">

																	<button class="btn btn-sm btn-primary pull-right" type="submit">
																		<i class="ace-icon fa fa-save"></i>
																		Simpan
																	</button>
																</form>
																<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
																	<i class="ace-icon fa fa-times"></i>
																	Tutup
																</button>

																
															</div>
														</div><!-- /.modal-content -->
													</div><!-- /.modal-dialog -->
												</div>
											<?php } ?>

											<div class="col-xs-12 col-sm-9">
												
												<h4 class="blue">
													<span class="middle" style="font-weight: bold;">Profil Siswa</span>


													<div class="btn-group pull-right">

														<?php if ($profil_siswa['is_aktif'] == 'N') { ?>
															<button class="btn btn-xs btn-info btn-inverse">
																<i class="ace-icon fa fa-toggle-off smaller-80 align-middle"></i> Non-aktif
															</button>
														<?php } else { ?>
															<button class="btn btn-xs btn-info btn-primary">
																<i class="ace-icon fa fa-toggle-on smaller-80 align-middle"></i> Aktif
															</button>
														<?php } ?>

														<?php if ($this->session->userdata('role') == 'sekolah') { ?>
															<button data-toggle="dropdown" class="btn btn-xs btn-info btn-white dropdown-toggle">
																<span class="ace-icon fa fa-caret-down icon-only"></span>
															</button>

															<ul class="dropdown-menu dropdown-info dropdown-menu-right ">
																<li>
																	<a href="<?= base_url(); ?>siswa/do_edit_status/Y/<?= $profil_siswa['id_siswa'];?>"><i class="ace-icon fa fa-toggle-on"></i> Aktifkan</a>
																</li>

																<li>
																	<a href="<?= base_url(); ?>siswa/do_edit_status/N/<?= $profil_siswa['id_siswa'];?>"><i class="ace-icon fa fa-toggle-off"></i> Nonaktifkan</a>
																</li>
															</ul>
														<?php } ?>

													</div><!-- /.btn-group -->
												</h4>

												<div class="hr hr-18 dotted hr-double"></div>
												<div class="profile-user-info">
													<div class="profile-info-row">
														<div class="profile-info-name"> Sekolah</div>

														<div class="profile-info-value">
															<span><?= $profil_siswa['nama_sekolah'];?></span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> NISN</div>

														<div class="profile-info-value">
															<span><?= $profil_siswa['nis'];?></span>
														</div>
													</div>
													
													<div class="profile-info-row">
														<div class="profile-info-name"> Nama Lengkap</div>

														<div class="profile-info-value">
															<span><?= $profil_siswa['nama'];?></span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> Kelas</div>

														<div class="profile-info-value">
															<span><?= $profil_siswa['tingkatan'];?> | <?= $profil_siswa['kelas'];?></span>
														</div>
													</div>
													
													<div class="profile-info-row">
														<div class="profile-info-name"> Jenis Kelamin</div>

														<div class="profile-info-value">
															<span><?= $profil_siswa['jk'];?></span>
														</div>
													</div>

													<div class="profile-info-row">
														<div class="profile-info-name"> Tempat Lahir </div>

														<div class="profile-info-value">

															<span><?= $profil_siswa['tmp_lahir']; ?></span>

														</div>
													</div>


													<div class="profile-info-row">
														<div class="profile-info-name"> Tanggal Lahir </div>

														<div class="profile-info-value">
															<span>
																<?php
																$date = $profil_siswa['tgl_lahir'];
																$dateObj = DateTime::createFromFormat('Y-m-d', $date);
																?>
																<?= $dateObj->format('d F Y'); ?>
															</span>
														</div>
													</div>

													<div class="profile-info-row">
														<div class="profile-info-name"> Usia </div>

														<div class="profile-info-value">
															<?php
															$date = new DateTime($profil_siswa['tgl_lahir']);
															$now = new DateTime();
															$usia = $now->diff($date)->y;
															?>
															<span><?= $usia; ?> tahun</span>
														</div>
													</div>

													<div class="profile-info-row">
														<div class="profile-info-name"> Nama Wali </div>
														<div class="profile-info-value">
															<span><?= $profil_siswa['nama_wali'];?></span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> Telp Wali </div>

														<div class="profile-info-value">
															<i class="fa fa-phone orange bigger-110"></i>
															<span><?= $profil_siswa['telp_wali'];?></span>

														</div>
													</div>

													<div class="profile-info-row">
														<div class="profile-info-name"> Alamat </div>

														<div class="profile-info-value">
															<i class="fa fa-map-marker orange bigger-110"></i>
															<span><?= $profil_siswa['alamat'];?></span>

														</div>
													</div>

													
													<div class="profile-info-row">
														<div class="profile-info-name"> Email siswa</div>

														<div class="profile-info-value">
															<i class="fa fa-envelope light-orange bigger-110"></i>
															<span><?= $profil_siswa['email'];?></span>
														</div>
													</div>

												</div>															
											</div><!-- /.col -->
										</div><!-- /.row -->


									</div><!-- /#home -->

									<div id="presensi" class="tab-pane">
										<div class="profile-feed row">
											<div class="row">
												<div class="col-xs-12">
													<!-- div.table-responsive -->
													
													<h4 class="blue">
														<span class="middle" style="font-weight: bold;">Presensi Siswa</span>
													<?php if ($this->session->userdata('role') == 'sekolah') { ?>
														<a href="#modal-presensi" role="button" data-toggle="modal"><button  class="btn tn-xs btn-info btn-xs pull-right"><b>+</b> Tambah Presensi</button></a>
													<?php } ?>
													</h4>
													<div class="hr hr-18 dotted hr-double"></div>

							<div id="modal-presensi" class="modal fade" tabindex="-1">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header no-padding">
													<div class="table-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
															<span class="white">&times;</span>
														</button>
														Tambah Presensi Siswa
													</div>
												</div>
												<form role="form" action="<?= base_url(); ?>siswa/do_tambah_dokumen/presensi" method="POST" enctype="multipart/form-data">
												<div class="modal-body">
														<input type="hidden" name="id_siswa" value="<?= $profil_siswa['id_siswa']; ?>">
														<input type="hidden" name="nama_siswa" value="<?= $profil_siswa['nama']; ?>">
														<input type="hidden" name="tingkatan" value="<?= $profil_siswa['tingkatan']; ?>">
														<input type="hidden" name="kelas" value="<?= $profil_siswa['kelas']; ?>">
														<input type="hidden" name="sekolah" value="<?= $profil_siswa['nama_sekolah']; ?>">
														<label>Semester</label>
														<select class="form-control" id="form-field-select-1" name="semester" required>
															<option selected="true" disabled="true"> -- Pilih semester -- </option>
															<option value="Ganjil">Ganjil</option>
															<option value="Genap">Genap</option>
														</select>		
														<br>
														<label>Tahun Ajaran</label>
														<select class="form-control" id="form-field-select-1" name="thn_ajar" required>
															<option selected="true" disabled="true"> -- Pilih tahun ajaran -- </option>
															<option value="<?php echo (date('Y')-1).'/'.date('Y'); ?>"><?php echo(date('Y')-1)."/".date('Y'); ?></option>
															<option value="<?php echo date('Y').'/'.(date('Y')+1); ?>"><?php echo date('Y')."/".(date('Y')+1); ?></option>
															<option value="<?php echo (date('Y')+1).'/'.(date('Y')+2); ?>"><?php echo (date('Y')+1)."/".(date('Y')+2); ?></option>
														</select>		
														<br>
														<label>Pilih File Dokumen(.pdf)</label>
														<input type="file" id="form-field-1-1" name="file" accept="application/pdf" class="form-control" required/>		
													</div>

													<div class="modal-footer no-margin-top">

														<button class="btn btn-sm btn-primary pull-right" type="submit">
															<i class="ace-icon fa fa-save"></i>
															Simpan
														</button>
													
													<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
														<i class="ace-icon fa fa-times"></i>
														Tutup
													</button>
												</div>
												</form>
											</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
									</div>

													<!-- div.dataTables_borderWrap -->
							<div class="table-responsive">
								<table id="dynamic-table-presensi" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th class="center">No.</th>
											<th>Kelas</th>
											<th>Tahun Ajaran</th>
											<th>File dokumen presensi</th>
											<th></th>
										</tr>
									</thead>

									<tbody>
										<?php 
										$count = 1;
										foreach ($presensi_siswa as $dt_presensi) { 
											$explod = explode('/',$dt_presensi['file']);

											?>
											<tr>
												<td class="center"><?= $count++; ?></td>

												<td><?= $dt_presensi['tingkatan']; ?> <?= $dt_presensi['kelas']; ?></td>
												<td><?= $dt_presensi['semester']; ?> <?= $dt_presensi['tahun_ajaran']; ?></td>
												<td><a href="<?= base_url(); ?><?= $dt_presensi['file']; ?>" target="_blank"><?= $explod[3]; ?></a></td>

												<td class="center">
													<div class="hidden-sm hidden-xs action-buttons">
														<?php 
														if ($this->session->userdata('role') == 'sekolah') {
															?>
															<a class="green" href="<?= base_url(); ?>siswa/do_delete_dokumen/<?= $dt_presensi['id_dok_siswa']; ?>/<?= $profil_siswa['id_siswa']; ?>/<?= url_title($profil_siswa['nama']); ?>/presensi" onClick="return doconfirm();">
																<i class="ace-icon fa fa-trash bigger-130" title="Hapus"></i>
															</a>
														<?php } ?>

													</div>

													<div class="hidden-md hidden-lg">
														<?php 
														if ($this->session->userdata('role') == 'sekolah') {
															?>
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-110"></i>
																</button>

																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																	
																<li>
																	<a href="<?= base_url(); ?>siswa/do_delete_dokumen/<?= $dt_presensi['id_dok_siswa']; ?>/<?= $profil_siswa['id_siswa']; ?>/<?= url_title($profil_siswa['nama']); ?>/presensi" class="tooltip-success" data-rel="tooltip" title="hapus" onClick="return doconfirm();">
																		<span class="green">
																			<i class="ace-icon fa fa-trash bigger-120"></i>
																		</span>
																	</a>
																</li>
																
																
															</ul>
														</div>
													<?php } ?>
												</div>

											</td>
										</tr>
										<?php } ?>

								</tbody>
							</table>
						</div>

												</div>
											</div>
										</div>
									</div>

									<div id="nilai" class="tab-pane">
										<div class="profile-feed row">
											<div class="row">
												<div class="col-xs-12">
													<!-- div.table-responsive -->
													<h4 class="blue">
														<span class="middle" style="font-weight: bold;">Rekap Nilai Pembelajaran Online</span>
													</h4>
													<div class="hr hr-18 dotted hr-double"></div>
													<div class="table-responsive">
														<table id="dynamic-table-nilai" class="table table-striped table-bordered table-hover">
															<thead>
																<tr>
																	<th class="center">No.</th>
																	<th>Mata Pelajaran</th>
																	<th>Judul Bab</th>
																	<th>Kelas</th>
																	<th>Rata-Rata Latihan</th>
																	<th>Ujian</th>
																</tr>
															</thead>
															<tbody>
																<?php 
																$count = 1;
																foreach ($nilai as $dt_nilai) { 
																	?>
																	<tr>
																		<td class="center"><?= $count++; ?></td>

																		<td><?= $dt_nilai['nama_mapel']; ?></td>
																		<td><?= $dt_nilai['bab']; ?></td>
																		<td><?= $dt_nilai['tingkatan']; ?></td>
																		<td>
																			<span style="cursor:pointer" onClick="detail_nilai_latihan(
																				<?= $this->uri->segment('3') ?>, 
																				<?= $dt_nilai['id_mapel'] ?>, 
																				<?= $dt_nilai['id_bab'] ?>,
																				'/<?= $dt_nilai['tingkatan'] ?>/',
																			)"><?= round($dt_nilai['nilai_latihan']); ?></span>
																		</td>
																		<td><?= $dt_nilai['nilai_ujian']; ?></td>
																	</tr>
																<?php } ?>

															</tbody>
														</table>
													</div>


												</div>
											</div>
										</div>
									</div>

									<div id="raport" class="tab-pane">
										<div class="profile-feed row">
											<div class="row">
												<div class="col-xs-12">
													<!-- div.table-responsive -->
													<h4 class="blue">
														<span class="middle" style="font-weight: bold;">Raport Siswa</span>
													<?php if ($this->session->userdata('role') == 'sekolah') { ?>
														<a href="#modal-raport" role="button" data-toggle="modal"><button  class="btn tn-xs btn-info btn-xs pull-right"><b>+</b> Tambah Raport</button></a>
													<?php } ?>
													</h4>
													<div class="hr hr-18 dotted hr-double"></div>

							<div id="modal-raport" class="modal fade" tabindex="-1">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header no-padding">
													<div class="table-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
															<span class="white">&times;</span>
														</button>
														Tambah Raport Siswa
													</div>
												</div>
												<form role="form" action="<?= base_url(); ?>siswa/do_tambah_dokumen/raport" method="POST" enctype="multipart/form-data">
												<div class="modal-body">
														<input type="hidden" name="id_siswa" value="<?= $profil_siswa['id_siswa']; ?>">
														<input type="hidden" name="nama_siswa" value="<?= $profil_siswa['nama']; ?>">
														<input type="hidden" name="tingkatan" value="<?= $profil_siswa['tingkatan']; ?>">
														<input type="hidden" name="kelas" value="<?= $profil_siswa['kelas']; ?>">
														<input type="hidden" name="sekolah" value="<?= $profil_siswa['nama_sekolah']; ?>">
														<label>Semester</label>
														<select class="form-control" id="form-field-select-1" name="semester" required>
															<option selected="true" disabled="true"> -- Pilih semester -- </option>
															<option value="Ganjil">Ganjil</option>
															<option value="Genap">Genap</option>
														</select>		
														<br>
														<label>Tahun Ajaran</label>
														<select class="form-control" id="form-field-select-1" name="thn_ajar" required>
															<option selected="true" disabled="true"> -- Pilih tahun ajaran -- </option>
															<option value="<?php echo (date('Y')-1).'/'.date('Y'); ?>"><?php echo(date('Y')-1)."/".date('Y'); ?></option>
															<option value="<?php echo date('Y').'/'.(date('Y')+1); ?>"><?php echo date('Y')."/".(date('Y')+1); ?></option>
															<option value="<?php echo (date('Y')+1).'/'.(date('Y')+2); ?>"><?php echo (date('Y')+1)."/".(date('Y')+2); ?></option>
														</select>		
														<br>
														<label>Pilih File Dokumen(.pdf)</label>
														<input type="file" id="form-field-1-1" name="file" accept="application/pdf" class="form-control" required/>		
													</div>

													<div class="modal-footer no-margin-top">

														<button class="btn btn-sm btn-primary pull-right" type="submit">
															<i class="ace-icon fa fa-save"></i>
															Simpan
														</button>
													
													<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
														<i class="ace-icon fa fa-times"></i>
														Tutup
													</button>
												</div>
												</form>
											</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
									</div>

				
													<!-- div.dataTables_borderWrap -->
							<div class="table-responsive">
								<table id="dynamic-table-raport" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th class="center">No.</th>
											<th>Kelas</th>
											<th>Tahun Ajaran</th>
											<th>File dokumen raport</th>
											<th></th>
										</tr>
									</thead>

									<tbody>
										<?php 
										$count = 1;
										foreach ($raport_siswa as $dt_raport) { 
											$explod = explode('/',$dt_raport['file']);

											?>
											<tr>
												<td class="center"><?= $count++; ?></td>

												<td><?= $dt_raport['tingkatan']; ?> <?= $dt_raport['kelas']; ?></td>
												<td><?= $dt_raport['semester']; ?> <?= $dt_raport['tahun_ajaran']; ?></td>
												<td><a href="<?= base_url(); ?><?= $dt_raport['file']; ?>" target="_blank"><?= $explod[3]; ?></a></td>

												<td class="center">
													<div class="hidden-sm hidden-xs action-buttons">
														<?php 
														if ($this->session->userdata('role') == 'sekolah') {
															?>
															<a class="green" href="<?= base_url(); ?>siswa/do_delete_dokumen/<?= $dt_raport['id_dok_siswa']; ?>/<?= $profil_siswa['id_siswa']; ?>/<?= url_title($profil_siswa['nama']); ?>/raport" onClick="return doconfirm();">
																<i class="ace-icon fa fa-trash bigger-130" title="Hapus"></i>
															</a>
														<?php } ?>

													</div>

													<div class="hidden-md hidden-lg">
														<?php 
														if ($this->session->userdata('role') == 'sekolah') {
															?>
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-110"></i>
																</button>

																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																	
																<li>
																	<a href="<?= base_url(); ?>siswa/do_delete_dokumen/<?= $dt_raport['id_dok_siswa']; ?>/<?= $profil_siswa['id_siswa']; ?>/<?= url_title($profil_siswa['nama']); ?>/raport" class="tooltip-success" data-rel="tooltip" title="hapus" onClick="return doconfirm();">
																		<span class="green">
																			<i class="ace-icon fa fa-trash bigger-120"></i>
																		</span>
																	</a>
																</li>
																
																
															</ul>
														</div>
													<?php } ?>
												</div>

											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>

												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>

					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

<div class="modal fade" id="modalNilai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xs" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Detail Nilai Latihan Siswa</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Latihan</th>
									<th>Nilai</th>
								</tr>
							</thead>
							<tbody id="list_latihan"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php 
     if($this->session->userdata('status_tambah_dokumen') != NULL){ ?>
      <div class="alert alert-success" role="alert" style="position: fixed; top: 88px; right:4%; width: 45%; vertical-align: middle;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Sukses!</strong> Penambahan <b><?= $this->session->userdata('status_tambah_dokumen'); ?></b> siswa berhasil dilakukan.
      </div>
      <script type="text/javascript">
        $(document).ready (function(){
          window.setTimeout(function() {
              $(".alert").fadeTo(500, 0).slideUp(500, function(){
                  $(this).remove(); 
              });
          }, 4000);
        });
      </script>
      <?php 
        $this->session->set_userdata('status_tambah_dokumen', null);
} ?>

<?php 
     if($this->session->userdata('status_hapus_dokumen') != NULL){ ?>
      <div class="alert alert-success" role="alert" style="position: fixed; top: 88px; right:4%; width: 45%; vertical-align: middle;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Sukses!</strong> Data <b><?= $this->session->userdata('status_hapus_dokumen'); ?></b> siswa berhasil dihapus.
      </div>
      <script type="text/javascript">
        $(document).ready (function(){
          window.setTimeout(function() {
              $(".alert").fadeTo(500, 0).slideUp(500, function(){
                  $(this).remove(); 
              });
          }, 4000);
        });
      </script>
      <?php 
        $this->session->set_userdata('status_hapus_dokumen', null);
} ?>

<?php 
     if($this->session->userdata('status_aktif_siswa') != NULL){ ?>
      <div class="alert alert-success" role="alert" style="position: fixed; top: 88px; right:4%; width: 45%; vertical-align: middle;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Sukses!</strong> Perubahan status aktif Siswa berhasil dilakukan.
      </div>
      <script type="text/javascript">
        $(document).ready (function(){
          window.setTimeout(function() {
              $(".alert").fadeTo(500, 0).slideUp(500, function(){
                  $(this).remove(); 
              });
          }, 4000);
        });
      </script>
      <?php 
        $this->session->set_userdata('status_aktif_siswa', null);
} ?>

<?php 
     if($this->session->userdata('status_edit_siswa') != NULL){ ?>
      <div class="alert alert-success" role="alert" style="position: fixed; top: 88px; right:4%; width: 45%; vertical-align: middle;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Sukses!</strong> Perubahan data Siswa berhasil dilakukan.
      </div>
      <script type="text/javascript">
        $(document).ready (function(){
          window.setTimeout(function() {
              $(".alert").fadeTo(500, 0).slideUp(500, function(){
                  $(this).remove(); 
              });
          }, 4000);
        });
      </script>
      <?php 
        $this->session->set_userdata('status_edit_siswa', null);
} ?>

<?php 
     if($this->session->userdata('status_foto_siswa') != NULL){ ?>
      <div class="alert alert-success" role="alert" style="position: fixed; top: 88px; right:4%; width: 45%; vertical-align: middle;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Sukses!</strong> Perubahan foto diri Siswa berhasil dilakukan.
      </div>
      <script type="text/javascript">
        $(document).ready (function(){
          window.setTimeout(function() {
              $(".alert").fadeTo(500, 0).slideUp(500, function(){
                  $(this).remove(); 
              });
          }, 4000);
        });
      </script>
      <?php 
        $this->session->set_userdata('status_foto_siswa', null);
} ?>

<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>

<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='<?= base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>


<!-- page specific plugin scripts -->
<script src="<?= base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.flash.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.html5.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.print.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.colVis.min.js"></script>
<script src="<?= base_url(); ?>assets/js/dataTables.select.min.js"></script>

<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>




<!-- inline scripts related to this page -->
<script type="text/javascript">

	function detail_nilai_latihan(id_siswa, mapel, bab, kelas){
		$.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('siswa/get_nilai_latihan_siswa') ?>",
            "method": "POST",
			"data": {
				"id_siswa":id_siswa,
				"id_mapel":mapel,
				"id_bab":bab,
				"kelas":kelas
			}
        }).done(function (response) {
			var response = JSON.parse(response);
			var latihan = response.data;
			var html_latihan = "";
			if(latihan.length > 0){
				latihan.forEach(function(item, index){
					html_latihan += '<tr><td>Latihan ke-'+(index + 1)+'</td><td>'+item.nilai+'</td></tr>';
				});
			} else {
				html_latihan += '<tr><td colspan="2"2>Data Tidak Ada</td></tr>';
			}
			$("#list_latihan").html(html_latihan);

			$("#modalNilai").modal("show");
		});
	}
	jQuery(function($) {
				//initiate dataTables plugin
				var myTable = 
				$('#dynamic-table-presensi')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					bAutoWidth: false,
					
					
					select: {
						style: 'multi'
					}
				} );
				
				
				$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
				
				new $.fn.dataTable.Buttons( myTable, {
					buttons: [
					{
						"extend": "colvis",
						"text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
						"className": "btn btn-white btn-primary btn-bold",
						columns: ':not(:first):not(:last)'
					},
					{
						"extend": "copy",
						"text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "csv",
						"text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "excel",
						"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "pdf",
						"text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "print",
						"text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
						"className": "btn btn-white btn-primary btn-bold",
						autoPrint: false,
						message: 'Learning Management System - Lembaga Bakti Muslim Sragen'
					}		  
					]
				} );
				myTable.buttons().container().appendTo( $('.tableTools-container') );
				
				var defaultColvisAction = myTable.button(0).action();
				myTable.button(0).action(function (e, dt, button, config) {
					
					defaultColvisAction(e, dt, button, config);
					
					
					if($('.dt-button-collection > .dropdown-menu').length == 0) {
						$('.dt-button-collection')
						.wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
						.find('a').attr('href', '#').wrap("<li />")
					}
					$('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
				});

				var myTable1 = 
				$('#dynamic-table-nilai')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					bAutoWidth: false,
					select: {
						style: 'multi'
					},
					dom: 'Bfrtip',
					buttons: [
					{
						"extend": "csv",
						"text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "excel",
						"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "pdf",
						"text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "print",
						"text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
						"className": "btn btn-white btn-primary btn-bold",
						autoPrint: false,
						message: 'Learning Management System - Lembaga Bakti Muslim Sragen'
					}] 
				} );
				
				
				$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
				
				new $.fn.dataTable.Buttons( myTable1, {
					buttons: [
					{
						"extend": "colvis",
						"text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
						"className": "btn btn-white btn-primary btn-bold",
						columns: ':not(:first):not(:last)'
					},
					{
						"extend": "copy",
						"text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "csv",
						"text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "excel",
						"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "pdf",
						"text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "print",
						"text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
						"className": "btn btn-white btn-primary btn-bold",
						autoPrint: false,
						message: 'Learning Management System - Lembaga Bakti Muslim Sragen'
					}		  
					]
				} );
				myTable1.buttons().container().appendTo( $('.tableTools-container') );
				
				var defaultColvisAction = myTable1.button(0).action();
				myTable1.button(0).action(function (e, dt, button, config) {
					
					defaultColvisAction(e, dt, button, config);
					
					
					if($('.dt-button-collection > .dropdown-menu').length == 0) {
						$('.dt-button-collection')
						.wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
						.find('a').attr('href', '#').wrap("<li />")
					}
					$('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
				});

				var myTable2 = 
				$('#dynamic-table-raport')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					bAutoWidth: false,
					
					
					select: {
						style: 'multi'
					}
				} );
				
				
				$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
				
				new $.fn.dataTable.Buttons( myTable2, {
					buttons: [
					{
						"extend": "colvis",
						"text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
						"className": "btn btn-white btn-primary btn-bold",
						columns: ':not(:first):not(:last)'
					},
					{
						"extend": "copy",
						"text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "csv",
						"text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "excel",
						"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "pdf",
						"text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "print",
						"text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
						"className": "btn btn-white btn-primary btn-bold",
						autoPrint: false,
						message: 'Learning Management System - Lembaga Bakti Muslim Sragen'
					}		  
					]
				} );
				myTable2.buttons().container().appendTo( $('.tableTools-container') );
				
				var defaultColvisAction = myTable2.button(0).action();
				myTable2.button(0).action(function (e, dt, button, config) {
					
					defaultColvisAction(e, dt, button, config);
					
					
					if($('.dt-button-collection > .dropdown-menu').length == 0) {
						$('.dt-button-collection')
						.wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
						.find('a').attr('href', '#').wrap("<li />")
					}
					$('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
				});		
				
				
							
				
					
				
			});
		</script>

		<script>
			function doconfirm()
			{
				job=confirm("Anda yakin akan menghapus data?");
				if(job!=true)
				{
					return false;
				}
			}
		</script>