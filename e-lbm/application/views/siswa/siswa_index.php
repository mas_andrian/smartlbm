
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Siswa</a>
				</li>
				
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Siswa
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $profil['nama_sekolah'];?>
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
					

					
					<div class="row">
						<div class="col-xs-12">
							

							<div class="clearfix">
								<?php 
								if ($this->session->userdata('role') == 'sekolah') {
									?>
									<div class="pull-left">
										<a href="<?php echo site_url('siswa/tambah');?>"><button class="pull-right btn btn-sm btn-primary  btn-round" type="button">
											<i class="ace-icon fa fa-plus icon-on-right bigger-110"></i> <b>Tambah Siswa </b> 
										</button></a>
									</div>
								<?php } ?>

								<div class="pull-right tableTools-container"></div>
							</div>
							<div class="table-header">
								
							</div>

							<!-- div.table-responsive -->

							<!-- div.dataTables_borderWrap -->
							<div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th class="center">No.</th>
											<th>NISN</th>
											<th>Nama</th>
											<th class="hidden-480">Tempat/Tanggal Lahir</th>
											<th class="hidden-480">Tingkatan | Kelas</th>
											<th class="hidden-480">Status</th>
											<th class="hidden-480">Sekolah</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$count = 1;
										foreach ($siswa as $dt) { ?>
											
											<tr>
												<td class="center"><?= $count++; ?></td>
												<td><?= $dt['nis']; ?></td>
												<td><?= $dt['nama']; ?></td>
												<td class="hidden-480">
													<?php
													$date = $dt['tgl_lahir'];
													$dateObj = DateTime::createFromFormat('Y-m-d', $date);
													?>
													<?= $dt['tmp_lahir']; ?>, <?= $dateObj->format('d F Y'); ?>
												</td>
												<td class="hidden-480"><?= $dt['tingkatan']; ?> | <?= $dt['kelas']; ?></td>
												<td class="hidden-480">
													<?php 
													if ($dt['is_aktif'] == 'Y') {
														echo "Aktif";
													} else {
														echo "<span style='color:red;'>Non-aktif</span>";
													}
													?>
													
												</td>
												<td><?= $dt['nama_sekolah']; ?></td>
												<td class="center">
													<div class="hidden-sm hidden-xs action-buttons">
														<a class="blue" href="<?php echo site_url('siswa/profil');?>/<?= $dt['id_siswa']; ?>/<?= url_title($dt['nama'], 'dash', TRUE); ?>">
															<i class="ace-icon fa fa-search-plus bigger-130" title="Detail"></i>
														</a>
														<?php 
														if($this->session->userdata('role') == 'sekolah') {
															?>
															<a class="green" href="<?php echo site_url('siswa/edit');?>/<?= $dt['id_siswa']; ?>/<?= url_title($dt['nama'], 'dash', TRUE); ?>">
																<i class="ace-icon fa fa-pencil bigger-130" title="Edit"></i>
															</a>
														<?php } ?>

													</div>

													<div class="hidden-md hidden-lg">
														<div class="inline pos-rel">
															<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
																<i class="ace-icon fa fa-caret-down icon-only bigger-110"></i>
															</button>

															<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																<li>
																	<a href="<?php echo site_url('siswa/profil');?>/<?= $dt['id_siswa']; ?>/<?= url_title($dt['nama'], 'dash', TRUE); ?>" class="tooltip-info" data-rel="tooltip" title="View">
																		<span class="blue">
																			<i class="ace-icon fa fa-search-plus bigger-120"></i>
																		</span>
																	</a>
																</li>

																<?php 
																if($this->session->userdata('role') == 'sekolah') {
																	?>

																	<li>
																		<a href="<?php echo site_url('siswa/edit');?>/<?= $dt['id_siswa']; ?>/<?= url_title($dt['nama'], 'dash', TRUE); ?>" class="tooltip-success" data-rel="tooltip" title="Edit">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																			</span>
																		</a>
																	</li>
																<?php } ?>

															</ul>
														</div>
													</div>

												</td>
											</tr>

										<?php } ?>

									</tbody>
								</table>
							</div>
						</div>
					</div>

					
					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

<?php 
     if($this->session->userdata('status_tambah_siswa') != NULL){ ?>
      <div class="alert alert-success" role="alert" style="position: fixed; top: 88px; right:4%; width: 45%; vertical-align: middle;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Sukses!</strong> Penambahan siswa berhasil dilakukan.
      </div>
      <script type="text/javascript">
        $(document).ready (function(){
          window.setTimeout(function() {
              $(".alert").fadeTo(500, 0).slideUp(500, function(){
                  $(this).remove(); 
              });
          }, 4000);
        });
      </script>
      <?php 
        $this->session->set_userdata('status_tambah_siswa', null);
} ?>

<!-- basic scripts -->

<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>

<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='<?= base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<script src="<?= base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.flash.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.html5.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.print.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.colVis.min.js"></script>
<script src="<?= base_url(); ?>assets/js/dataTables.select.min.js"></script>

<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
	jQuery(function($) {
				//initiate dataTables plugin
				var myTable = 
				$('#dynamic-table')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					bAutoWidth: false,
					"aoColumns": [
					{ "bSortable": false },
					null, null,null, null, null, null,
					{ "bSortable": false }
					],
					"aaSorting": [],
					
					
					select: {
						style: 'multi'
					}
				} );
				
				
				$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
				
				new $.fn.dataTable.Buttons( myTable, {
					buttons: [
					{
						"extend": "colvis",
						"text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
						"className": "btn btn-white btn-primary btn-bold",
						columns: ':not(:first):not(:last)'
					},
					{
						"extend": "copy",
						"text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "csv",
						"text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "excel",
						"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "pdf",
						"text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "print",
						"text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
						"className": "btn btn-white btn-primary btn-bold",
						autoPrint: false,
						message: 'Learning Management System - Lembaga Bakti Muslim Sragen'
					}		  
					]
				} );
				myTable.buttons().container().appendTo( $('.tableTools-container') );
				
				var defaultColvisAction = myTable.button(0).action();
				myTable.button(0).action(function (e, dt, button, config) {
					
					defaultColvisAction(e, dt, button, config);
					
					
					if($('.dt-button-collection > .dropdown-menu').length == 0) {
						$('.dt-button-collection')
						.wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
						.find('a').attr('href', '#').wrap("<li />")
					}
					$('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
				});			
				
			})
		</script>