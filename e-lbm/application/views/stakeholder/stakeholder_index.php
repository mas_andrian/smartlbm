<!-- /.conten utama -->
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Stakeholder</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Stakeholder
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $profil['nama_sekolah'];?>
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
					<div class="row">
						<div class="col-xs-12">
							<table id="simple-table" class="table  table-bordered table-hover">
								<thead>
									<tr>
										<th style="text-align: center;">No.</th>
										<th>Nama</th>
										<th>Username</th>
										<th>Email</th>
										<th>Role</th>
										<th></th>
									</tr>
								</thead>
								<tbody>

									<?php 
									$count = 1;
									foreach ($stakeholder as $dt) { ?>
										<tr>
											<td style="text-align: center;"><?= $count++; ?></td>
											<td>
												<?= $dt['nama']; ?>
											</td>
											<td>
												<?= $dt['username']; ?>
											</td>
											<td>
												<?= $dt['email']; ?>
											</td>
											<td><?= $dt['role']; ?></td>
											
											<td class="center">
												<div class="hidden-sm hidden-xs btn-group pull-center">
													<a href="#modal-edit<?= $count; ?>" role="button" data-toggle="modal"><button class="btn btn-xs btn-info">
														<i class="ace-icon fa fa-pencil bigger-120" title="Edit"></i>
													</button></a>
													<a href="#modal-edit-pass<?= $count; ?>" role="button" data-toggle="modal"><button class="btn btn-xs btn-warning">
														<i class="ace-icon fa fa-key bigger-120" title="Atur Password"></i>
													</button></a>
													
												</div>
											</td>
										</tr>

										<div id="modal-edit<?= $count; ?>" class="modal fade" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header no-padding">
														<div class="table-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																<span class="white">&times;</span>
															</button>
															Edit Role: <span style="color: yellow;"><?= $dt['role']; ?></span>
														</div>
													</div>

													<div class="modal-body">
														<form role="form" action="<?= base_url(); ?>stakeholder/do_edit/<?= $dt['id_pengguna']; ?>" method="POST" enctype="multipart/form-data">	
															<label>Username</label>
															<input type="text" id="form-field-1-1" value="<?= $dt['username']; ?>" name="username" placeholder="Masukkan username ..." class="form-control" required/>
															<br>
															<label>Email</label>
															<input type="email" id="form-field-1-1" value="<?= $dt['email']; ?>" name="email" placeholder="Masukkan email ..." class="form-control" required/>			
															
														</div>

														<div class="modal-footer no-margin-top">

															<button class="btn btn-sm btn-primary pull-right" type="submit">
																<i class="ace-icon fa fa-save"></i>
																Simpan
															</button>
														</form>
														<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
															<i class="ace-icon fa fa-times"></i>
															Tutup
														</button>

														
													</div>
												</div><!-- /.modal-content -->
											</div><!-- /.modal-dialog -->
										</div>


										<div id="modal-edit-pass<?= $count; ?>" class="modal fade" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header no-padding">
														<div class="table-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																<span class="white">&times;</span>
															</button>
															Edit Password: <span style="color: yellow;"><?= $dt['role']; ?></span>
														</div>
													</div>

													<div class="modal-body">
														<form role="form" action="<?= base_url(); ?>stakeholder/do_edit_pass/<?= $dt['id_pengguna']; ?>" method="POST" enctype="multipart/form-data" onsubmit="return checkPass<?= $count; ?>(this);">	
															<label>Password Baru</label>
															<input type="password" name="password1" placeholder="Masukkan password baru ..." class="form-control" id="password1<?= $count; ?>" required/>
															<br>
															<label>Konfirmasi Password</label>
															<input type="password" name="Password2" placeholder="Masukkan konfirmasi password ..." class="form-control" id="password2<?= $count; ?>" required/>			
															<br>
															<center><input type="checkbox" onclick="myFunction<?= $count; ?>()"> <label> Lihat Password</label></center>
														</div>

														<script>
															function myFunction<?= $count; ?>() {
																var x = document.getElementById("password1<?= $count; ?>");
																if (x.type == "password") {
																	x.type = "text";
																} else {
																	x.type = "password";
																}
																var y = document.getElementById("password2<?= $count; ?>");
																if (y.type == "password") {
																	y.type = "text";
																} else {
																	y.type = "password";
																}
															}

															function checkPass<?= $count; ?>(theForm) {
																if (theForm.password1<?= $count; ?>.value != theForm.password2<?= $count; ?>.value)
																{
																	alert('Kata sandi tidak sesuai!');
																	return false;
																} else {
																	return true;
																}
															}
														</script>

														<div class="modal-footer no-margin-top">

															<button class="btn btn-sm btn-primary pull-right" type="submit">
																<i class="ace-icon fa fa-save"></i>
																Simpan
															</button>
														</form>
														<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
															<i class="ace-icon fa fa-times"></i>
															Tutup
														</button>
														
													</div>
												</div><!-- /.modal-content -->
											</div><!-- /.modal-dialog -->
										</div>

										
									<?php } ?>
									
								</tbody>
							</table>
						</div> <!-- /.span -->
					</div> <!-- /.row -->
					<div class="space-20"></div>
					<!-- PAGE CONTENT ENDS -->
				</div> <!-- /.col -->
			</div> <!-- /.page-content -->
		</div>
	</div> <!-- /.main-content -->
	
	
	<!-- basic scripts -->
	<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>

	<!-- ace scripts -->
	<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

	
	
	