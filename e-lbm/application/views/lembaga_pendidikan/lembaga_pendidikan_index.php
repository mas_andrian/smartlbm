
<div class="main-content">
	<div class="main-content-inner">

		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Lembaga Pendidikan</a>
				</li>
				
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">

			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->




			<div class="page-header">
				<h1>
					Lembaga Pendidikan
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $profil['nama_sekolah'];?>
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->

					<div class="row">
						<div class="col-xs-12">
							

							<div class="clearfix">
								<?php 
								if ($pengguna['role_id'] == 1) {
									?>
									<div class="pull-left">
										<a href="#modal-mp" role="button" data-toggle="modal"><button class="pull-right btn btn-sm btn-primary  btn-round" type="button">
											<i class="ace-icon fa fa-plus icon-on-right bigger-110"></i> <b>Tambah Lembaga Pendidikan </b> 
										</button></a>
									</div>

									<div id="modal-mp" class="modal fade" tabindex="-1">
										<div class="modal-dialog" style="width: 70%;">
											<div class="modal-content">


												<div class="modal-header no-padding">
													<div class="table-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
															<span class="white">&times;</span>
														</button>
														Tambah Lembaga Pendidikan
													</div>
												</div>

												<div class="modal-body">
													<form role="form" action="<?= base_url(); ?>lembaga_pendidikan/do_tambah_sekolah" method="POST" enctype="multipart/form-data">
													<div>
														<label for="form-field-select-3">Nama Lembaga Pendidikan /  Sekolah</label>
														<div>
															<div class="col-xs-9" style="padding: 0;">
																<input type="text" id="form-field-1-1" placeholder="Masukkan nama sekolah ..." class="form-control" name="nama_sekolah" value="" required="true"/>
															</div>
															<div class="col-xs-3" style="padding: 0 0 0 1em;">
																<select class="form-control" id="form-field-select-1" name="jenjang" required="true">
																	<option selected="true" value=" " disabled="true"> -- Pilih jenjang pendidikan -- </option>
																	<option value="Taman Kanak-kanak">Taman Kanak-kanak</option>
																	<option value="Sekolah Dasar">Sekolah Dasar</option>
																	<option value="Sekolah Menengah Pertama">Sekolah Menengah Pertama</option>
																	<option value="Sekolah Menengah Atas">Sekolah Menengah Atas</option>
																	<option value="Pondok Pesantren">Pondok Pesantren</option>
																</select>
															</div>
														</div>
														<br><br><br>
														<label for="form-field-select-3">Alamat</label><br>
														<div>
															<div class="col-xs-5" style="padding: 0;">
																<input  type="text" id="form-field-1-1" placeholder="Masukkan alamat ..." class="form-control" name="alamat" value="" required="true"/>
															</div>
															<div class="col-xs-3" style="padding: 0 1em 0 1em;">
																<input  type="text" id="form-field-1-1" placeholder="Kecamatan ..." class="form-control" name="kecamatan" value="" required="true"/>
															</div>
															<div class="col-xs-3" style="padding: 0 1em 0 0;">
																<input  type="text" id="form-field-1-1" placeholder="Kota atau kabupaten ..." class="form-control" name="kab_kota" value="" required="true"/>
															</div>
															<div class="col-xs-1"style="padding: 0;">
																<input  type="text" id="form-field-1-1" placeholder="Kode pos ..." class="form-control" name="kode_pos" value="" required="true"/>
															</div>
														</div>
														<br><br><br>
														<label for="form-field-select-3">Kontak</label><br>
														<div>
															<div class="col-xs-3" style="padding: 0;">
																<input  type="text" id="form-field-1-1" placeholder="Telepon ..." class="form-control" name="telp" value="" required="true"/>
															</div>
															<div class="col-xs-3" style="padding: 0 1em 0 1em;">
																<input  type="text" id="form-field-1-1" placeholder="Fax ..." class="form-control" name="fax" value="" required="true"/>
															</div>
															<div class="col-xs-3" style="padding: 0 1em 0 0;">
																<input  type="text" id="form-field-1-1" placeholder="Email ..." class="form-control" name="email" value="" required="true"/>
															</div>
															<div class="col-xs-3"style="padding: 0;">
																<input  type="text" id="form-field-1-1" placeholder="Website ..." class="form-control" name="website" value="" required="true" />
															</div>
														</div>
														<br><br><br>
														<label for="form-field-select-3">Nomor Sekolah</label><br>
														<div>
															<div class="col-xs-6" style="padding: 0;">
																<input  type="text" id="form-field-1-1" placeholder="Nomor Statistik Sekolah ..." class="form-control" name="nss" value="" required="true" />
															</div>
															<div class="col-xs-6" style="padding: 0 0 0 1em;">
																<input  type="text" id="form-field-1-1" placeholder="Nomor Pokok Sekolah Nasional  ..." class="form-control" name="npsn" value="" required="true" />
															</div>
														</div>
														<br><br><br>
														<label for="form-field-select-3">Kepala Sekolah</label><br>
														<div>
															<div class="col-xs-6" style="padding: 0;">
																<input  type="text" id="form-field-1-1" placeholder="No.peg kepala sekolah ..." class="form-control" name="nip_kepsek" value="" required="true" />
															</div>
															<div class="col-xs-6" style="padding: 0 0 0 1em;">
																<input  type="text" id="form-field-1-1" placeholder="Nama kepala sekolah ..." class="form-control" name="nama_kepsek" value="" required="true" />
															</div>
														</div>
														<br><br><br>
														<label for="form-field-select-3">Kode Akses <i>(Password)</i> Sekolah</label><br>
														<div>
															<div class="col-xs-12" style="padding: 0;">
																<input  type="text" id="form-field-1-1" placeholder="Kode akses sekolah ..." class="form-control" name="akses" value="" required="true" />
															</div>
														</div>
													</div>
													<label for="form-field-select-3" style="color: #fff;">.</label><br>
													<div class="space-3"></div>
												</div>

													<div class="modal-footer no-margin-top">

														<button class="btn btn-sm btn-primary pull-right" type="submit">
															<i class="ace-icon fa fa-save"></i>
															Simpan
														</button>
													
													<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
														<i class="ace-icon fa fa-times"></i>
														Tutup
													</button>
												</form>
													
												</div>
											</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
									</div>


								<?php } ?>

								<div class="pull-right tableTools-container"></div>
							</div>
							<div class="table-header">
								
							</div>

							<!-- div.table-responsive -->

							<!-- div.dataTables_borderWrap -->
							<div class="table-responsive">
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th class="center">No.</th>
											<th>Lembaga Pendidikan </th>
											<th>Jenjang</th>
											<th>Alamat</th>
											<th>Nomor Sekolah</th>
											<th>Kontak</th>
											<th>Akses Sekolah</th>
											<th></th>
										</tr>
									</thead>

									<tbody>
										
										<?php 
										$count = 1;
										foreach ($sekolah as $dt) { ?>
											
											<tr>
												<td class="center">
													<?= $count++; ?>
												</td>
												<td><?= $dt['nama_sekolah']; ?></td>
												<td><?= $dt['jenjang']; ?></td>
												<td><?= $dt['alamat']; ?>, <?= $dt['kecamatan']; ?>, <?= $dt['kab_kota']; ?> (<?= $dt['kode_pos']; ?>)</td>
												<td>NSS: <?= $dt['nss']; ?> <br>NPSN: <?= $dt['npsn']; ?></td>
												<td>Telp: <?= $dt['telp']; ?> <br>Email: <?= $dt['email']; ?></td>
												<td><?= $dt['flag']; ?>  [<?= $dt['akses']; ?>]</td>
												<td class="center">
													<div class="hidden-sm hidden-xs action-buttons">
														<?php 
														if ($pengguna['role_id'] == 1) {
															?>
															<a class="blue" href="#modal-edit-mp<?= $count; ?>" role="button" data-toggle="modal">
																<i class="ace-icon fa fa-pencil bigger-130" title="Edit"></i>
															</a>
															
															<a class="green" href="<?= base_url(); ?>lembaga_pendidikan/do_hapus_sekolah/<?= $dt['id']; ?>" onClick="return doconfirm();">
																<i class="ace-icon fa fa-trash bigger-130" title="Hapus"></i>
															</a>
														<?php } ?>

													</div>

													<div class="hidden-md hidden-lg">
														<?php 
														if ($pengguna['role_id'] == 1) {
															?>
															<div class="inline pos-rel">
																<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
																	<i class="ace-icon fa fa-caret-down icon-only bigger-110"></i>
																</button>

																<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																	<li>
																		<a href="#modal-edit-mp<?= $count; ?>" role="button" data-toggle="modal" class="tooltip-info" data-rel="tooltip" >
																		<span class="blue">
																			<i class="ace-icon fa fa-pencil bigger-120" title="Edit"></i>
																		</span>
																	</a>
																</li>

																<li>
																	<a href="<?= base_url(); ?>lembaga_pendidikan/do_hapus_sekolah/<?= $dt['id']; ?>" class="tooltip-success" data-rel="tooltip" title="hapus" onClick="return doconfirm();">
																		<span class="green">
																			<i class="ace-icon fa fa-trash bigger-120"></i>
																		</span>
																	</a>
																</li>
															</ul>
														</div>
													<?php } ?>
												</div>

											</td>
										</tr>


										<div id="modal-edit-mp<?= $count; ?>" class="modal fade" tabindex="-1">
											<div class="modal-dialog" style="width: 70%;">
												<div class="modal-content">
													<div class="modal-header no-padding">
														<div class="table-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																<span class="white">&times;</span>
															</button>
															Edit Lembaga Pendidikan: <span style="color: yellow;"><?= $dt['nama_sekolah']; ?></span>
														</div>
													</div>

													<div class="modal-body">
													<form role="form" action="<?= base_url(); ?>lembaga_pendidikan/do_edit_sekolah/<?= $dt['id']; ?>" method="POST" enctype="multipart/form-data">
													<div>
														<label for="form-field-select-3">Nama Lembaga Pendidikan /  Sekolah</label>
														<div>
															<div class="col-xs-8" style="padding: 0;">
																<input type="text" id="form-field-1-1" placeholder="Masukkan nama sekolah ..." class="form-control" name="nama_sekolah" value="<?= $dt['nama_sekolah'] ?>" required="true"/>
															</div>
															<div class="col-xs-4" style="padding: 0 0 0 1em;">
																<select class="form-control" id="form-field-select-1" name="jenjang" required="true">
																	<option value="<?= $dt['jenjang']; ?>"><?= $dt['jenjang']; ?></option>
																	<option disabled="true"> -- Pilih jenjang pendidikan -- </option>
																	<option value="Taman Kanak-kanak">Taman Kanak-kanak</option>
																	<option value="Sekolah Dasar">Sekolah Dasar</option>
																	<option value="Sekolah Menengah Pertama">Sekolah Menengah Pertama</option>
																	<option value="Sekolah Menengah Atas">Sekolah Menengah Atas</option>
																	<option value="Pondok Pesantren">Pondok Pesantren</option>
																</select>
															</div>
														</div>
														<br><br><br>
														<label for="form-field-select-3">Alamat</label><br>
														<div>
															<div class="col-xs-5" style="padding: 0;">
																<input  type="text" id="form-field-1-1" placeholder="Masukkan alamat ..." class="form-control" name="alamat" value="<?= $dt['alamat']; ?>" required="true"/>
															</div>
															<div class="col-xs-3" style="padding: 0 1em 0 1em;">
																<input  type="text" id="form-field-1-1" placeholder="Kecamatan ..." class="form-control" name="kecamatan" value="<?= $dt['kecamatan']; ?>" required="true"/>
															</div>
															<div class="col-xs-3" style="padding: 0 1em 0 0;">
																<input  type="text" id="form-field-1-1" placeholder="Kota atau kabupaten ..." class="form-control" name="kab_kota" value="<?= $dt['kab_kota']; ?>" required="true"/>
															</div>
															<div class="col-xs-1"style="padding: 0;">
																<input  type="text" id="form-field-1-1" placeholder="Kode pos ..." class="form-control" name="kode_pos" value="<?= $dt['kode_pos']; ?>" required="true"/>
															</div>
														</div>
														<br><br><br>
														<label for="form-field-select-3">Kontak</label><br>
														<div>
															<div class="col-xs-3" style="padding: 0;">
																<input  type="text" id="form-field-1-1" placeholder="Telepon ..." class="form-control" name="telp" value="<?= $dt['telp']; ?>" required="true"/>
															</div>
															<div class="col-xs-3" style="padding: 0 1em 0 1em;">
																<input  type="text" id="form-field-1-1" placeholder="Fax ..." class="form-control" name="fax" value="<?= $dt['fax']; ?>" required="true"/>
															</div>
															<div class="col-xs-3" style="padding: 0 1em 0 0;">
																<input  type="text" id="form-field-1-1" placeholder="Email ..." class="form-control" name="email" value="<?= $dt['email']; ?>" required="true"/>
															</div>
															<div class="col-xs-3"style="padding: 0;">
																<input  type="text" id="form-field-1-1" placeholder="Website ..." class="form-control" name="website" value="<?= $dt['website']; ?>" required="true"/>
															</div>
														</div>
														<br><br><br>
														<label for="form-field-select-3">Nomor Sekolah</label><br>
														<div>
															<div class="col-xs-6" style="padding: 0;">
																<input  type="text" id="form-field-1-1" placeholder="Nomor Statistik Sekolah ..." class="form-control" name="nss" value="<?= $dt['nss']; ?>" required="true"/>
															</div>
															<div class="col-xs-6" style="padding: 0 0 0 1em;">
																<input  type="text" id="form-field-1-1" placeholder="Nomor Pokok Sekolah Nasional  ..." class="form-control" name="npsn" value="<?= $dt['npsn']; ?>" required="true"/>
															</div>
														</div>
														<br><br><br>
														<label for="form-field-select-3">Kepala Sekolah</label><br>
														<div>
															<div class="col-xs-6" style="padding: 0;">
																<input  type="text" id="form-field-1-1" placeholder="No.peg kepala sekolah ..." class="form-control" name="nip_kepsek" value="<?= $dt['nip_kepsek']; ?>" required="true"/>
															</div>
															<div class="col-xs-6" style="padding: 0 0 0 1em;">
																<input  type="text" id="form-field-1-1" placeholder="Nama kepala sekolah ..." class="form-control" name="nama_kepsek" value="<?= $dt['nama_kepsek']; ?>" required="true"/>
															</div>
														</div>
														<br><br><br>
														<label for="form-field-select-3">Kode Akses Sekolah</label><br>
														<div>
															<div class="col-xs-12" style="padding: 0;">
																<input  type="text" id="form-field-1-1" placeholder="Kode akses sekolah ..." class="form-control" name="akses" value="<?= $dt['akses']; ?>" required="true"/>
															</div>
														</div>
													</div>
													<label for="form-field-select-3" style="color: #fff;">.</label><br>
													<div class="space-3"></div>
												</div>

													<div class="modal-footer no-margin-top">

														<button class="btn btn-sm btn-primary pull-right" type="submit">
															<i class="ace-icon fa fa-save"></i>
															Simpan
														</button>
													
													<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
														<i class="ace-icon fa fa-times"></i>
														Tutup
													</button>
												</form>

														
													</div>
												</div><!-- /.modal-content -->
											</div><!-- /.modal-dialog -->
										</div>

									<?php } ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>

				
				<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->

<?php 
     if($this->session->userdata('status_tambah_lembaga_pendidikan') != null){ ?>
      <div class="alert alert-success" role="alert" style="position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%);">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Sukses!</strong> Penambahan lembaga pendidikan berhasil dilakukan.
      </div>
      <script type="text/javascript">
        $(document).ready (function(){
          window.setTimeout(function() {
              $(".alert").fadeTo(500, 0).slideUp(500, function(){
                  $(this).remove(); 
              });
          }, 4000);
        });
      </script>
      <?php 
        $this->session->set_userdata('status_tambah_lembaga_pendidikan', null);
} ?>

<?php 
     if($this->session->userdata('status_edit_lembaga_pendidikan') != NULL){ ?>
      <div class="alert alert-success" role="alert" style="position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%);">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Sukses!</strong> Perubahan data lembaga pendidikan berhasil dilakukan.
      </div>
      <script type="text/javascript">
        $(document).ready (function(){
          window.setTimeout(function() {
              $(".alert").fadeTo(500, 0).slideUp(500, function(){
                  $(this).remove(); 
              });
          }, 4000);
        });
      </script>
      <?php 
        $this->session->set_userdata('status_edit_lembaga_pendidikan', null);
} ?>

<?php 
     if($this->session->userdata('status_hapus_lembaga_pendidikan') != NULL){ ?>
      <div class="alert alert-success" role="alert" style="position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%);">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Sukses!</strong> Penghapusan data lembaga pendidikan berhasil dilakukan.
      </div>
      <script type="text/javascript">
        $(document).ready (function(){
          window.setTimeout(function() {
              $(".alert").fadeTo(500, 0).slideUp(500, function(){
                  $(this).remove(); 
              });
          }, 4000);
        });
      </script>
      <?php 
        $this->session->set_userdata('status_hapus_lembaga_pendidikan', null);
} ?>

<!-- basic scripts -->

<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>

<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='<?= base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<script src="<?= base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.flash.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.html5.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.print.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.colVis.min.js"></script>
<script src="<?= base_url(); ?>assets/js/dataTables.select.min.js"></script>

<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
	jQuery(function($) {
				//initiate dataTables plugin
				var myTable = 
				$('#dynamic-table')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					bAutoWidth: false,
					"aoColumns": [
					{ "bSortable": false },
					null, null, null, null, null, null,
					{ "bSortable": false }
					],
					"aaSorting": [],
					
					select: {
						style: 'multi'
					}
				} );
				
				
				$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
				
				new $.fn.dataTable.Buttons( myTable, {
					buttons: [
					{
						"extend": "colvis",
						"text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
						"className": "btn btn-white btn-primary btn-bold",
						columns: ':not(:first):not(:last)'
					},
					{
						"extend": "copy",
						"text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "csv",
						"text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "excel",
						"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "pdf",
						"text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "print",
						"text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
						"className": "btn btn-white btn-primary btn-bold",
						autoPrint: false,
						message: 'Learning Management System - Lembaga Bakti Muslim Sragen'
					}		  
					]
				} );
				myTable.buttons().container().appendTo( $('.tableTools-container') );
				
				var defaultColvisAction = myTable.button(0).action();
				myTable.button(0).action(function (e, dt, button, config) {
					
					defaultColvisAction(e, dt, button, config);
					
					
					if($('.dt-button-collection > .dropdown-menu').length == 0) {
						$('.dt-button-collection')
						.wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
						.find('a').attr('href', '#').wrap("<li />")
					}
					$('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
				});			
				
			})
		</script>
		<script>
			function doconfirm()
			{
				job=confirm("Anda yakin akan menghapus data?");
				if(job!=true)
				{
					return false;
				}
			}
		</script>