
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Pemberitahuan</a>
				</li>
				
			</ul><!-- /.breadcrumb -->		
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Detail Pemberitahuan
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $pemberitahuan['update_time'];?>
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->

					<?php 
					if ($this->session->userdata('role') == 'superadmin') {
						?> 

						<div id="modal-mp" class="modal fade" tabindex="-1">
							<div class="modal-dialog " style="width: 75%;">
								<div class="modal-content">
									<div class="modal-header no-padding">
										<div class="table-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
												<span class="white">&times;</span>
											</button>
											Tambah Pemberitahuan
										</div>
									</div>
									<form role="form" action="<?= base_url(); ?>pemberitahuan/do_edit/<?= $pemberitahuan['slug'];?>" method="POST" enctype="multipart/form-data">	
										<div class="modal-body">
											
											<label>Perihal pemberitahuan</label>
											<input type="text" id="form-field-1-1" name="perihal" placeholder="Masukkan perihal pemberitahuan ..." class="form-control" value="<?= $pemberitahuan['perihal'];?>" required/>
											<br>
											<label>Isi pemberitahuan</label>
											<textarea id="editor1" name="isi"><?= $pemberitahuan['isi'];?></textarea>
											<script>
												CKEDITOR.replace('editor1',{
        filebrowserBrowseUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgbrowser.php'); ?>',
    filebrowserUploadUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgupload.php'); ?>'
    });
											</script>
											<br>
											<label>Batas pemberitahuan</label>
											<input type="date" id="form-field-1-1" value="<?= $pemberitahuan['batas'];?>" name="batas" class="form-control" required/>
											
										</div>

										<div class="modal-footer no-margin-top">

											<button class="btn btn-sm btn-primary pull-right" type="submit">
												<i class="ace-icon fa fa-save"></i>
												Simpan
											</button>
										</form>
										<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
											<i class="ace-icon fa fa-times"></i>
											Tutup
										</button>

										
									</div>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div>

					<?php } ?>

					<div class="widget-box" id="widget-box-1">
						<div class="widget-header">
							<h5 class="widget-title"><?= $pemberitahuan['perihal'];?></h5>

							<div class="widget-toolbar">
								<?php 
								if ($this->session->userdata('role') == 'superadmin') {
									?>
									<div class="widget-menu">
										<a href="#" data-action="settings" data-toggle="dropdown">
											<i class="ace-icon fa fa-bars"></i>
										</a>

										<ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">

											<li>
												<a href="#modal-mp" role="button" data-toggle="modal">Edit</a>
											</li>


											<li>
												<a href="<?= base_url(); ?>pemberitahuan/do_hapus/<?= $pemberitahuan['slug']; ?>" onClick="return doconfirm();">Hapus</a>
												<script>
													function doconfirm()
													{
														job=confirm("Anda yakin akan menghapus data?");
														if(job!=true)
														{
															return false;
														}
													}
												</script>
											</li>
										</ul>
									</div>
								<?php } ?>

								<a href="#" data-action="fullscreen" class="orange2">
									<i class="ace-icon fa fa-expand"></i>
								</a>
								
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main" style="padding: 2em;">
								<?= $pemberitahuan['isi'];?>
							</div>
						</div>
					</div>
					
					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

<!-- basic scripts -->

<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>


<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

