<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Cetak Laporan Hasil Penilaian Administrasi Perangkat Pembelajaran - Kode Supervisi: <?= $this->uri->segment(3); ?> | SIM APPS</title>

	<meta name="description" content="overview &amp; stats" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<link rel="icon" href="<?= base_url(); ?>assets/images/favicon/favicon.png" type="image/gif" >

	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/font-awesome/4.5.0/css/font-awesome.min.css" />

	<!-- page specific plugin styles -->
	<!-- form element -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.custom.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/chosen.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-datepicker3.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-timepicker.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/daterangepicker.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-datetimepicker.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-colorpicker.min.css" />

	<!-- form element 2 -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-duallistbox.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-multiselect.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/select2.min.css" />

	<!-- form wizard -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/select2.min.css" />

	<!-- jqgrid -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-datepicker3.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ui.jqgrid.min.css" />

	<!-- jquery-ui -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.min.css" />

	<!-- search result -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/select2.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.custom.min.css" />

	<!-- galeri -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/colorbox.min.css" />

	<!-- profil -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.custom.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery.gritter.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/select2.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-datepicker3.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-editable.min.css" />

	<!-- calender -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.custom.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/fullcalendar.min.css" />

	<!-- dropzone -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/dropzone.min.css" />

	<!-- elements -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.custom.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery.gritter.min.css" />

	<!-- typografi -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/prettify.min.css" />

	<!-- widget -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.custom.min.css" />

	<!-- wysiwyg -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.custom.min.css" />

	<!-- text fonts -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/fonts.googleapis.com.css" />

	<!-- ace styles -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace-rtl.min.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace-ie.min.css" />
		<![endif]-->
		<script src="<?php echo base_url('assets/ckeditor/ckeditor.js'); ?>"></script>

		<!-- inline styles related to this page -->
		<!-- elements -->
		<style>
		/* some elements used in demo only */
		.spinner-preview {
			width: 100px;
			height: 100px;
			text-align: center;
			margin-top: 60px;
		}

		.dropdown-preview {
			margin: 0 5px;
			display: inline-block;
		}
		.dropdown-preview  > .dropdown-menu {
			display: block;
			position: static;
			margin-bottom: 5px;
		}
	</style>

	<!-- ace settings handler -->
	<script src="<?= base_url(); ?>assets/js/ace-extra.min.js"></script>

	<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="<?= base_url(); ?>assets/js/html5shiv.min.js"></script>
		<script src="<?= base_url(); ?>assets/js/respond.min.js"></script>
	<![endif]-->

	<!-- Include external CSS. -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">

	<!-- Include Editor style. -->
	<link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/css/froala_style.min.css" rel="stylesheet" type="text/css" />

</head>
<body>