<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title><?= $title; ?> | SMART LBM</title>

	<meta name="description" content="overview &amp; stats" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />

	<link rel="icon" href="<?= base_url(); ?>assets/frontend/images/favicon-new.png" type="image/gif" > 

	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/font-awesome/4.5.0/css/font-awesome.min.css" />

	<!-- page specific plugin styles -->
	<!-- form element -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.custom.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/chosen.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-datepicker3.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-timepicker.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/daterangepicker.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-datetimepicker.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-colorpicker.min.css" />

	<!-- form element 2 -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-duallistbox.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-multiselect.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/select2.min.css" />

	<!-- form wizard -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/select2.min.css" />

	<!-- jqgrid -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-datepicker3.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ui.jqgrid.min.css" />

	<!-- jquery-ui -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.min.css" />

	<!-- search result -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/select2.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.custom.min.css" />

	<!-- galeri -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/colorbox.min.css" />

	<!-- profil -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.custom.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery.gritter.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/select2.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-datepicker3.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-editable.min.css" />

	<!-- calender -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.custom.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/fullcalendar.min.css" />

	<!-- dropzone -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/dropzone.min.css" />

	<!-- elements -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.custom.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery.gritter.min.css" />

	<!-- typografi -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/prettify.min.css" />

	<!-- widget -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.custom.min.css" />

	<!-- wysiwyg -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.custom.min.css" />

	<!-- text fonts -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/fonts.googleapis.com.css" />

	<!-- ace styles -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace-rtl.min.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace-ie.min.css" />
		<![endif]-->
		<script src="<?php echo base_url('assets/ckeditor/ckeditor.js'); ?>"></script> 


		<!-- inline styles related to this page -->
		<!-- elements -->
		<style>
		/* some elements used in demo only */
		.spinner-preview {
			width: 100px;
			height: 100px;
			text-align: center;
			margin-top: 60px;
		}

		.dropdown-preview {
			margin: 0 5px;
			display: inline-block;
		}
		.dropdown-preview  > .dropdown-menu {
			display: block;
			position: static;
			margin-bottom: 5px;
		}
		
	</style>

	<!-- ace settings handler -->
	<script src="<?= base_url(); ?>assets/js/ace-extra.min.js"></script>

	<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="<?= base_url(); ?>assets/js/html5shiv.min.js"></script>
		<script src="<?= base_url(); ?>assets/js/respond.min.js"></script>
	<![endif]-->

	<!-- Include external CSS. -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">

	<!-- Include Editor style. -->
	<link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/css/froala_style.min.css" rel="stylesheet" type="text/css" />

</head>

<body class="no-skin">
	<div id="navbar" class="navbar navbar-default          ace-save-state">
		<div class="navbar-container ace-save-state" id="navbar-container">
			<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
				<span class="sr-only">Toggle sidebar</span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>
			</button>

			<div class="navbar-header pull-left">
				<a href="<?php echo site_url('dashboard');?>" class="navbar-brand">
					<img class="img-fluid" height="25px" style="margin: 0; padding: 0;" src="<?= base_url(); ?>assets/frontend/images/lg-botton-white2.png" alt="smartlbm.png">
				</a>
			</div>

			<div class="navbar-buttons navbar-header pull-right" role="navigation">
				<ul class="nav ace-nav">

					<?php if ($pengguna['role_id'] == 2) { ?>
					<?php } ?>

					<?php if ($pengguna['role_id'] == 3) { ?>
					<?php } ?>


					<li class="green dropdown-modal">
						<a data-toggle="dropdown" class="dropdown-toggle" href="#">
							<i class="ace-icon fa fa-bell 

							<?php if (sizeof($info_aktif) != 0) { ?>
								icon-animated-bell
							<?php } ?>
							"></i>

							<?php if (sizeof($info_aktif) != 0) { ?>
								<span class="badge badge-important"><?= sizeof($info_aktif); ?></span>
							<?php } ?>	
						</a>

						<ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
							<li class="dropdown-header">
								<i class="ace-icon fa fa-bell"></i>
								<?= sizeof($info_aktif); ?> Pemberitahuan Aktif
							</li>

							<li class="dropdown-content">
								<ul class="dropdown-menu dropdown-navbar">
									<?php 
									foreach ($info_aktif as $dt_info) { ?>
										<li>
											<a href="<?= base_url(); ?>pemberitahuan/view/<?= $dt_info['slug']; ?>" class="clearfix">
												<img src="<?= base_url(); ?>assets/images/avatars/icon.png" class="msg-photo" alt="Wakakur" />
												<span class="msg-body">
													<span class="msg-title">
														<span class="blue"></span>
														<?php echo strip_tags(character_limiter($dt_info['perihal'], 40)); ?>
													</span>

													<span class="msg-time">
														<i class="ace-icon fa fa-calendar"></i>
														<?php
														$date = $dt_info['batas'];
														$dateObj = DateTime::createFromFormat('Y-m-d', $date);
														?>
														<span> <?= $dateObj->format('d F Y'); ?></span>
													</span>
												</span>
											</a>
										</li>
									<?php } ?>
								</ul>
							</li> 

							<li class="dropdown-footer">
								<a href="<?php echo site_url('pemberitahuan');?>">
									Lihat Semua Pemberitahuan
									<i class="ace-icon fa fa-arrow-right"></i>
								</a>
							</li>
						</ul>
					</li>



					<li class="light-blue dropdown-modal">
						<?php
						if($this->session->userdata('role') == 'sekolah'){ ?>
						<a data-toggle="dropdown" href="#" class="dropdown-toggle">
							<img class="nav-user-photo" src="<?= base_url(); ?><?= $sekolah['logo_sekolah'];?>" alt="Sekolah" />
							<span class="user-info">
								<small>Selamat datang,</small>
								<?= $sekolah['nama_sekolah'];?>
							</span>
							<i class="ace-icon fa fa-caret-down"></i>
						</a>
						<?php
						} else {
						?>
						<a data-toggle="dropdown" href="#" class="dropdown-toggle">
							<img class="nav-user-photo" src="<?= base_url(); ?><?= $identitas['foto_profil'];?>" alt="User" />
							<span class="user-info">
								<small>Selamat datang,</small>
								<?= $identitas['nama'];?>
							</span>
							<i class="ace-icon fa fa-caret-down"></i>
						</a>
						<?php } ?>

						<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
							<?php
							if ($pengguna['role_id'] == 2) {
								?>
								<li>
									<a href="<?php echo site_url('tenaga_pengajar/edit');?>/<?= $identitas['id']; ?>/<?= url_title($identitas['nama'], 'dash', TRUE); ?>">
										<i class="ace-icon fa fa-cog"></i>
										Pengaturan
									</a>
								</li>
								<li>
									<a href="<?php echo site_url('tenaga_pengajar/profil');?>/<?= $identitas['id']; ?>/<?= url_title($identitas['nama'], 'dash', TRUE); ?>">
										<i class="ace-icon fa fa-user"></i>
										Profil
									</a>
								</li>
								<li class="divider"></li>

							<?php } ?>

							<li>
								<a href="<?php echo site_url('login/do_logout');?>">
									<i class="ace-icon fa fa-power-off"></i>
									Keluar
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div><!-- /.navbar-container -->
	</div>

	<div class="main-container ace-save-state" id="main-container">
		<script type="text/javascript">
			try{ace.settings.loadState('main-container')}catch(e){}
		</script>

		<div id="sidebar" class="sidebar responsive ace-save-state">
			<script type="text/javascript">
				try{ace.settings.loadState('sidebar')}catch(e){}
			</script>
			
			<div class="sidebar-shortcuts" id="sidebar-shortcuts">
				<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">

				<?php if ($this->session->userdata('role') != 'guru') { ?>
					<button class="btn btn-danger">
						<a href="<?php echo site_url('profil_yayasan');?>"><i class="ace-icon fa fa-home white" title="Profil"></i></a>
					</button>
					<?php
					if ($this->session->userdata('role') == 'superadmin') {
						?>
					<button class="btn btn-success">
						<a href="<?php echo site_url('lembaga_pendidikan');?>"><i class="ace-icon fa fa-university white" title="Lembaga Pendidikan"></i></a>
					</button>
						
					<?php } ?>
					<button class="btn btn-info">
						<a href="<?php echo site_url('tenaga_pengajar');?>"><i class="ace-icon fa fa-graduation-cap white" title="Tenaga Pengajar"></i></a>
					</button>
					<button class="btn btn-default">
						<a href="<?php echo site_url('siswa');?>"><i class="ace-icon fa fa-users white" title="Siswa"></i></a>
					</button>
					<?php
					if ($this->session->userdata('role') != 'superadmin') {
						?>
					<button class="btn btn-warning">
						<a href="<?php echo site_url('mata_pelajaran');?>"><i class="ace-icon glyphicon glyphicon-book white" title="Mata Pelajaran"></i></a>
					</button>
					<?php } ?>
				<?php } else { ?>
					<button class="btn btn-info">
						<a href="<?php echo site_url('tenaga_pengajar/profil');?>/<?= $identitas['id']; ?>/<?= url_title($identitas['nama'], 'dash', TRUE); ?>"><i class="ace-icon fa fa-graduation-cap white" title="Profil Tenaga Pengajar"></i></a>
					</button>
					<button class="btn btn-default">
						<a href="<?php echo site_url('siswa');?>"><i class="ace-icon fa fa-users white" title="Siswa"></i></a>
					</button>
					<button class="btn btn-warning">
						<a href="<?php echo site_url('mata_pelajaran');?>"><i class="ace-icon glyphicon glyphicon-book white" title="Mata Pelajaran"></i></a>
					</button>
				<?php } ?>
				</div>

				<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
					<span class="btn btn-success"></span>

					<span class="btn btn-info"></span>

					<span class="btn btn-warning"></span>

					<span class="btn btn-danger"></span>
				</div>
			</div><!-- /.sidebar-shortcuts -->

			<ul class="nav nav-list">
				<li class="active">
					<a href="<?php echo site_url('dashboard');?>">
						<i class="menu-icon fa fa-tachometer"></i>
						<span class="menu-text"> Dashboard </span>
					</a>

					<b class="arrow"></b>
				</li>

			<?php if ($pengguna['role_id'] != 2) { ?>
				<li class="">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fa fa-folder"></i>
						<span class="menu-text"> Data Utama </span>
						<b class="arrow fa fa-angle-down"></b>
					</a>
					<b class="arrow"></b>

					<ul class="submenu">
						<?php
							if($this->session->userdata('role') != 'superadmin'){
								$c = "Sekolah";
							} else {
								$c = "Yayasan";
							}
						?>
						<li class="">
							<a href="<?php echo site_url('profil_yayasan');?>">
								<i class="menu-icon fa fa-caret-right"></i>
								Profil <?= $c; ?>
							</a>
							<b class="arrow"></b>
						</li>

						<?php
						if ($pengguna['role_id'] == 1) {
							?>
							<li class="">
								<a href="<?php echo site_url('lembaga_pendidikan');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Lembaga Pendidikan
								</a>
								<b class="arrow"></b>
							</li>
						<?php } ?>

						<li class="">
							<a href="<?php echo site_url('tenaga_pengajar');?>">
								<i class="menu-icon fa fa-caret-right"></i>
								Tenaga Kepegawaian
							</a>
							<b class="arrow"></b>
						</li>
						<li class="">
							<a href="<?php echo site_url('siswa');?>">
								<i class="menu-icon fa fa-caret-right"></i>
								Siswa
							</a>
							<b class="arrow"></b>
						</li>
						<li class="">
							<a href="<?php echo site_url('mata_pelajaran');?>">
								<i class="menu-icon fa fa-caret-right"></i>
								Mata Pelajaran
							</a>
							<b class="arrow"></b>
						</li>
						
						<?php if($pengguna['role_id'] == 1) { ?>
							<li class="">
								<a href="<?php echo site_url('stakeholder');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Stakeholder
								</a>
								<b class="arrow"></b>
							</li>	
						<?php } ?>
					</ul>
				</li>
			<?php } else { ?>
				<li class="">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fa fa-folder"></i>
						<span class="menu-text"> Data Utama </span>
						<b class="arrow fa fa-angle-down"></b>
					</a>
					<b class="arrow"></b>
					<ul class="submenu">
						<li class="">
							<a href="<?php echo site_url('tenaga_pengajar/profil');?>/<?= $identitas['id']; ?>/<?= url_title($identitas['nama'], 'dash', TRUE); ?>">
								<i class="menu-icon fa fa-caret-right"></i>
								Profil Tenaga Pengajar
							</a>
							<b class="arrow"></b>
						</li>
						<li class="">
							<a href="<?php echo site_url('siswa');?>">
								<i class="menu-icon fa fa-caret-right"></i>
								Siswa
							</a>
							<b class="arrow"></b>
						</li>
						<li class="">
							<a href="<?php echo site_url('mata_pelajaran');?>">
								<i class="menu-icon fa fa-caret-right"></i>
								Mata Pelajaran
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
				</li>
			<?php } ?>	
				
				<li class="">
					<a href="<?php echo site_url('materi');?>">
						<i class="menu-icon fa fa-book"></i>

						<span class="menu-text">
							Materi Kelas

						</span>
					</a>

					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="<?php echo site_url('kalender_pendidikan');?>">
						<i class="menu-icon fa fa-calendar"></i>

						<span class="menu-text">
							Kalender Pendidikan

						</span>
					</a>

					<b class="arrow"></b>
				</li>
				
				</li>


				<li class="">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fa fa-user"></i>
						<span class="menu-text"> Personal Menu </span>

						<b class="arrow fa fa-angle-down"></b>
					</a>
					<b class="arrow"></b>

					<ul class="submenu">
						<?php if($pengguna['role_id'] == 1) { ?>
							<li class="">
								<a href="<?php echo site_url('blogpost');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Posting blog
								</a>
								<b class="arrow"></b>
							</li>	
							<li class="">
								<a href="<?php echo site_url('kegiatan');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Kegiatan
								</a>
								<b class="arrow"></b>
							</li>	
							<li class="">
								<a href="<?php echo site_url('testimoni');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Testimoni
								</a>
								<b class="arrow"></b>
							</li>	
							<li class="">
								<a href="<?php echo site_url('faq');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									FAQ
								</a>
								<b class="arrow"></b>
							</li>	
						<?php } ?>
						<li class="">
							<a href="<?php echo site_url('pemberitahuan');?>">
								<i class="menu-icon fa fa-caret-right"></i>
								Pemberitahuan
							</a>

							<b class="arrow"></b>
						</li>
						<li class="">
							<a href="<?php echo site_url('login/do_logout');?>">
								<i class="menu-icon fa fa-caret-right"></i>
								Keluar / Logout
							</a>

							<b class="arrow"></b>
						</li>
					</ul>
				</li>
				

			</ul><!-- /.nav-list -->

			<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
				<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
			</div>
		</div>

