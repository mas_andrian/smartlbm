 
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Bab</a>
				</li>
				
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Detail Bab
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<b><?= $bab->nama_mapel;?> [<?= $bab->judul;?>]</b>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $profil['nama_sekolah'];?>
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
					
					<div>
						<div id="user-profile-2" class="user-profile">
							<div class="tabbable">
								<ul class="nav nav-tabs padding-18">
									<li class="active">
										<a data-toggle="tab" href="#home">
											<i class="green ace-icon fa fa-user bigger-120"></i>
											Detail BAB
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#subbab">
											<i class="blue ace-icon fa fa-list bigger-120"></i>
											Data Sub BAB
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#bank_soal_latihan">
											<i class="blue ace-icon fa fa-check-square-o bigger-120"></i>
											Bank Soal Latihan
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#bank_soal_ujian">
											<i class="blue ace-icon fa fa-check-square-o bigger-120"></i>
											Bank Soal Ujian
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#nilai">
											<i class="blue ace-icon fa fa-bookmark bigger-120"></i>
											Nilai
										</a>
									</li>
								</ul>

								<div class="tab-content no-border padding-24">
									<div id="home" class="tab-pane in active">
										<div class="row">

											<div class="col-xs-12 col-sm-12">
												
												<h4 class="blue">
													<span class="middle" style="font-weight: bold;">Detail Informasi Bab</span>
													<div class="btn-group pull-right">

														<?php if ($bab->is_aktif == 'N') { ?>
															<button class="btn btn-xs btn-info btn-inverse">
																<i class="ace-icon fa fa-toggle-off smaller-80 align-middle"></i> Non-aktif
															</button>
														<?php } else { ?>
															<button class="btn btn-xs btn-info btn-primary">
																<i class="ace-icon fa fa-toggle-on smaller-80 align-middle"></i> Aktif
															</button>
														<?php } ?>

														<?php if ($this->session->userdata('role') == 'guru') { ?>
															<button data-toggle="dropdown" class="btn btn-xs btn-info btn-white dropdown-toggle">
																<span class="ace-icon fa fa-caret-down icon-only"></span>
															</button>

															<ul class="dropdown-menu dropdown-info dropdown-menu-right ">
																<li>
																	<a href="<?= base_url(); ?>bab/do_edit_status/Y/<?= $bab->id;?>/<?= $this->uri->segment(4) ?>"><i class="ace-icon fa fa-toggle-on"></i> Aktifkan</a>
																</li>

																<li>
																	<a href="<?= base_url(); ?>bab/do_edit_status/N/<?= $bab->id;?>/<?= $this->uri->segment(4) ?>"><i class="ace-icon fa fa-toggle-off"></i> Nonaktifkan</a>
																</li>
															</ul>
														<?php } ?>

													</div><!-- /.btn-group -->
												</h4>

												<div class="hr hr-18 dotted hr-double"></div>
												<div class="profile-user-info">
													<div class="profile-info-row">
														<div class="profile-info-name"> Mata Pelajaran</div>

														<div class="profile-info-value">
															<span><?= $bab->nama_mapel;?></span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> Bab Ke -</div>

														<div class="profile-info-value">
															<span><?= $bab->urutan;?></span>
														</div>
													</div>
													
													<div class="profile-info-row">
														<div class="profile-info-name"> Judul</div>

														<div class="profile-info-value">
															<span><?= $bab->judul;?></span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> Deskripsi</div>

														<div class="profile-info-value">
															<span><?= $bab->deskripsi;?></span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> Thumbnail</div>

														<div class="profile-info-value">
															<span><img src="<?= base_url().$bab->img_thumbnail; ?>" class="img-thumbnail" width="100px" /></span>
														</div>
													</div>
													<br/>
													<a href="<?php echo site_url('materi/detail/').$this->uri->segment('5')."/".$this->uri->segment('6');?>" class="btn btn-xs btn-info">
														<i class="ace-icon fa fa-arrow-left bigger-110"></i>
														<span class="bigger-110">Kembali Ke Materi</span>
													</a>
													
												</div>															
											</div><!-- /.col -->
										</div><!-- /.row -->


									</div><!-- /#home -->

									<div id="subbab" class="tab-pane">
										<div class="profile-feed row">
											<div class="row">
												<div class="col-xs-12">
													<!-- div.table-responsive -->
													
													<h4 class="blue">
														<span class="middle" style="font-weight: bold;">Data Sub BAB Materi</span>
														<div class="pull-right">
														<?php 
															if ($this->session->userdata('role') == 'guru') {
																?>
															
																<a href="<?php echo site_url('sub_bab/tambah');?>/<?= $bab->id; ?>/<?= url_title($bab->slug, 'dash', TRUE); ?>" class="btn btn-sm btn-block btn-primary">
																	<i class="ace-icon fa fa-plus bigger-110"></i>
																	<span class="bigger-110">Tambah Sub BAB</span>
																</a>
															<?php } ?>
														</div>
													</h4>
													
													<div class="hr hr-18 dotted hr-double"></div>
														<div class="table-responsive">
															<table id="dynamic-table" class="table table-striped table-bordered table-hover">
																<thead>
																	<tr>
																		<th class="center" width="5%">No.</th>
																		<th>Urutan</th>
																		<th>Judul</th>
																		<th width="40%">Deskripsi</th>
																		<th>Video</th>
																		<th>File Pdf</th>
																		<th>Status</th>
																		<th></th>
																	</tr>
																</thead>
																<tbody>
																	<?php 
																	$count = 1;
																	if(! $sub_bab){
																		echo "<tr><td colspan='8' style='text-align:center;'>Data Sub Bab Tidak Ada</td></tr>";
																	}
																	foreach ($sub_bab as $dt) { ?>
																		
																		<tr>
																			<td class="center"><?= $count++; ?></td>
																			<td><?= "Materi Ke- ".$dt['urutan']; ?></td>
																			<td><?= $dt['judul']; ?></td>
																			<td><?= substr($dt['deskripsi'], 0, 250).'.......'; ?></td>
																			<td><a href="<?= $dt['file_video']; ?>" class="btn btn-xs btn-info" target="_blank">Lihat Video Materi</a></td>
																			<td><a href="<?= base_url().$dt['file_pdf'] ?>" class="btn btn-xs btn-info" target="_blank">Lihat PDF Materi</a></td>
																			<td><?= ($dt['is_aktif'] == "Y" ? "Aktif" : "Non Aktif") ?></td>
																			<td class="center">
																				<div class="hidden-sm hidden-xs action-buttons">
																					<a class="blue" href="<?php echo site_url('sub_bab/detail');?>/<?= $dt['id']; ?>/<?= url_title($dt['slug'], 'dash', TRUE); ?>/<?= $this->uri->segment(3) ?>/<?= url_title($dt['slug_bab'], 'dash', TRUE); ?>">
																						<i class="ace-icon fa fa-search-plus bigger-130" title="Detail"></i>
																					</a>
																					<?php 
																					if($this->session->userdata('role') == 'guru') {
																						?>
																						<a class="green" href="<?php echo site_url('sub_bab/edit');?>/<?= $dt['id']; ?>/<?= $dt['slug'] ?>/<?= $this->uri->segment(3) ?>/<?= url_title($dt['slug_bab'], 'dash', TRUE); ?>">
																							<i class="ace-icon fa fa-pencil bigger-130" title="Edit"></i>
																						</a>
																					<?php } ?>

																				</div>

																			</td>

																		</tr>

																	<?php } ?>

																</tbody>
															</table>
														</div>
												</div>
											</div>
										</div>
									</div>

									<div id="bank_soal_latihan" class="tab-pane">
										<div class="profile-feed row">
											<div class="row">
												<div class="col-xs-12">
													<!-- div.table-responsive -->
													
													<h4 class="blue">
														<span class="middle" style="font-weight: bold;">Data Bank Soal Latihan</span>
														<div class="pull-right">
														<?php 
															if ($this->session->userdata('role') == 'guru') {
																?>
															
																<a href="<?php echo site_url('bank_soal/tambah');?>/<?= $bab->id; ?>/<?= url_title($bab->slug, 'dash', TRUE); ?>/latihan" class="btn btn-sm btn-block btn-primary">
																	<i class="ace-icon fa fa-plus bigger-110"></i>
																	<span class="bigger-110">Tambah Soal Latihan</span>
																</a>
															<?php } ?>
														</div>
													</h4>
													
													<div class="hr hr-18 dotted hr-double"></div>
														<div class="table-responsive">
															
															<table id="dynamic-table-2" class="table table-striped table-bordered table-hover">
																<thead>
																	<tr>
																		<th class="center" width="5%">No.</th>
																		<th>Pertanyaan</th>
																		<th>Jawaban 1</th>
																		<th>Jawaban 2</th>
																		<th>Jawaban 3</th>
																		<th>Jawaban 4</th>
																		<th>Jawaban 5</th>
																		<th>Jawaban Benar</th>
																		<th></th>
																	</tr>
																</thead>
																<tbody>
																	<?php 
																	$count = 1;
																	if(! $latihan){
																		echo "<tr><td colspan='9' style='text-align:center;'>Data Bank Soal Latihan Tidak Ada</td></tr>";
																	}
																	foreach ($latihan as $dt) { ?>
																		
																		<tr>
																			<td class="center"><?= $count++; ?></td>
																			<td><?= $dt['pertanyaan']; ?></td>
																			<td><?= $dt['jawaban_1']; ?></td>
																			<td><?= $dt['jawaban_2']; ?></td>
																			<td><?= $dt['jawaban_3']; ?></td>
																			<td><?= $dt['jawaban_4']; ?></td>
																			<td><?= $dt['jawaban_5']; ?></td>
																			<td><?= 'Jawaban Ke '.$dt['jawaban_benar']; ?></td>
																			<td class="center">
																				<div class="hidden-sm hidden-xs action-buttons">
																					<?php 
																					if($this->session->userdata('role') == 'guru') {
																						?>
																						<a class="green" href="<?php echo site_url('bank_soal/edit');?>/<?= $dt['id']; ?>/<?= $this->uri->segment(3)?>/<?= $this->uri->segment(4)?>/<?= $dt['jenis_soal'] ?>">
																							<i class="ace-icon fa fa-pencil bigger-130" title="Edit"></i>
																						</a>
																					<?php } ?>

																				</div>

																			</td>

																		</tr>

																	<?php } ?>

																</tbody>
															</table>
														</div>
												</div>
											</div>
										</div>
									</div>

									<div id="bank_soal_ujian" class="tab-pane">
										<div class="profile-feed row">
											<div class="row">
												<div class="col-xs-12">
													<!-- div.table-responsive -->
													
													<h4 class="blue">
														<span class="middle" style="font-weight: bold;">Data Bank Soal ujian</span>
														<div class="pull-right">
														<?php 
															if ($this->session->userdata('role') == 'guru') {
																?>
															
																<a href="<?php echo site_url('bank_soal/tambah');?>/<?= $bab->id; ?>/<?= url_title($bab->slug, 'dash', TRUE); ?>/ujian" class="btn btn-sm btn-block btn-primary">
																	<i class="ace-icon fa fa-plus bigger-110"></i>
																	<span class="bigger-110">Tambah Soal ujian</span>
																</a>
															<?php } ?>
														</div>
													</h4>
													
													<div class="hr hr-18 dotted hr-double"></div>
														<div class="table-responsive">
															
															<table id="dynamic-table-3" class="table table-striped table-bordered table-hover">
																<thead>
																	<tr>
																		<th class="center" width="5%">No.</th>
																		<th>Pertanyaan</th>
																		<th>Jawaban 1</th>
																		<th>Jawaban 2</th>
																		<th>Jawaban 3</th>
																		<th>Jawaban 4</th>
																		<th>Jawaban 5</th>
																		<th>Jawaban Benar</th>
																		<th></th>
																	</tr>
																</thead>
																<tbody>
																	<?php 
																	$count = 1;
																	if(! $ujian){
																		echo "<tr><td colspan='9' style='text-align:center;'>Data Bank Soal ujian Tidak Ada</td></tr>";
																	}
																	foreach ($ujian as $dt) { ?>
																		
																		<tr>
																			<td class="center"><?= $count++; ?></td>
																			<td><?= $dt['pertanyaan']; ?></td>
																			<td><?= $dt['jawaban_1']; ?></td>
																			<td><?= $dt['jawaban_2']; ?></td>
																			<td><?= $dt['jawaban_3']; ?></td>
																			<td><?= $dt['jawaban_4']; ?></td>
																			<td><?= $dt['jawaban_5']; ?></td>
																			<td><?= 'Jawaban Ke '.$dt['jawaban_benar']; ?></td>
																			<td class="center">
																				<div class="hidden-sm hidden-xs action-buttons">
																					<?php 
																					if($this->session->userdata('role') == 'guru') {
																						?>
																						<a class="green" href="<?php echo site_url('bank_soal/edit');?>/<?= $dt['id']; ?>/<?= $this->uri->segment(3)?>/<?= $this->uri->segment(4)?>/<?= $dt['jenis_soal'] ?>">
																							<i class="ace-icon fa fa-pencil bigger-130" title="Edit"></i>
																						</a>
																					<?php } ?>

																				</div>

																			</td>

																		</tr>

																	<?php } ?>

																</tbody>
															</table>
														</div>
												</div>
											</div>
										</div>
									</div>

									<div id="nilai" class="tab-pane">
										<div class="profile-feed row">
											<div class="row">
												<div class="col-xs-12">
													<!-- div.table-responsive -->
													
													<h4 class="blue">
														<span class="middle" style="font-weight: bold;">Data Nilai Siswa</span>
													</h4>
													
													<div class="hr hr-18 dotted hr-double"></div>
														<div class="table-responsive">
															
															<table id="dynamic-table-4" class="table table-striped table-bordered table-hover">
																<thead>
																	<tr>
																		<th class="center" width="5%">No.</th>
																		<th>Nama Siswa</th>
																		<th>Kelas</th>
																		<th>Rata-Rata Latihan</th>
																		<th>Ujian</th>
																		<th>Waktu Mulai Ujian</th>
																		<th>Waktu Selesai Ujian</th>
																	</tr>
																</thead>
																<tbody>
																	<?php 
																	$count = 1;
																	if(! $ujian){
																		echo "<tr><td colspan='9' style='text-align:center;'>Data Bank Soal ujian Tidak Ada</td></tr>";
																	}
																	foreach ($nilai as $nl) { ?>
																		
																		<tr>
																			<td class="center"><?= $count++; ?></td>
																			<td><?= $nl['nama']; ?></td>
																			<td><?= $nl['tingkatan']." - ".$nl['kelas']; ?></td>
																			<td>
																				<span style="cursor:pointer" onClick="detail_nilai_latihan(
																					<?= $nl['id_siswa'] ?>, 
																					<?= $this->uri->segment('3')?>,
																				)"><?= round($nl['nilai_latihan']) ?></span>
																			
																			</td>
																			<td><?= $nl['nilai_ujian'] ?></td>
																			<td><?= ($nl['waktu_mulai'] != "" ? date("d/m/Y H:i:s", strtotime($nl['waktu_mulai'])) : "") ?></td>
																			<td><?= ($nl['waktu_selesai'] != "" ? date("d/m/Y H:i:s", strtotime($nl['waktu_selesai'])) : "") ?></td>

																		</tr>

																	<?php } ?>

																</tbody>
															</table>
														</div>
												</div>
											</div>
										</div>
									</div>


								</div>
							</div>
						</div>
					</div>

					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
<div class="modal fade" id="modalNilai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xs" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Detail Nilai Latihan Siswa</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Latihan</th>
									<th>Nilai</th>
								</tr>
							</thead>
							<tbody id="list_latihan"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- basic scripts -->
<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>

<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='<?= base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

<!-- page specific plugin scripts -->
<script src="<?= base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.flash.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.html5.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.print.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.colVis.min.js"></script>
<script src="<?= base_url(); ?>assets/js/dataTables.select.min.js"></script>

<script type="text/javascript">

	function detail_nilai_latihan(id_siswa, bab){
		$.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('siswa/get_nilai_latihan_siswa') ?>",
            "method": "POST",
			"data": {
				"id_siswa":id_siswa,
				"id_bab":bab,
			}
        }).done(function (response) {
			var response = JSON.parse(response);
			var latihan = response.data;
			var html_latihan = "";
			if(latihan.length > 0){
				latihan.forEach(function(item, index){
					html_latihan += '<tr><td>Latihan ke-'+(index + 1)+'</td><td>'+item.nilai+'</td></tr>';
				});
			} else {
				html_latihan += '<tr><td colspan="2"2>Data Tidak Ada</td></tr>';
			}
			$("#list_latihan").html(html_latihan);

			$("#modalNilai").modal("show");
		});
	}
	jQuery(function($) {
				//initiate dataTables plugin
				var myTable = 
				$('#dynamic-table')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					bAutoWidth: false,
					
					
					select: {
						style: 'multi'
					}
				} );

				var myTable2 = 
				$('#dynamic-table-2')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					bAutoWidth: false,
					
					
					select: {
						style: 'multi'
					}
				} );

				var myTable3 = 
				$('#dynamic-table-3')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					bAutoWidth: false,
					
					
					select: {
						style: 'multi'
					}
				} );

				var myTable4 = 
				$('#dynamic-table-4')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					bAutoWidth: false,
					
					
					select: {
						style: 'multi'
					},
					dom:'Bfrtip',
					buttons: [
					{
						"extend": "csv",
						"text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "excel",
						"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "pdf",
						"text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "print",
						"text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
						"className": "btn btn-white btn-primary btn-bold",
						autoPrint: false,
						message: 'Learning Management System - Lembaga Bakti Muslim Sragen'
					}		  
					]
				} );
					
				
			})
		</script>