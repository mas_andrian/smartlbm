
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Tenaga Kepegawaian</a>
				</li>
				
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Edit Tenaga Kepegawaian
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<a href="<?php echo site_url('tenaga_pengajar/profil');?>/<?= $profil_pengguna['id']; ?>/<?= url_title($profil_pengguna['nama'], 'dash', TRUE); ?>"><?= $profil_pengguna['nama'];?></a>
					</small>
				</h1>
			</div><!-- /.page-header -->

			<?php if ($this->session->userdata('cek_nip') == "ada") { ?>
			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert">
					<i class="ace-icon fa fa-times"></i>
				</button>
				<i class="ace-icon fa fa-university  "></i>
				<strong>Perhatian! </strong>
					NIPY sudah terdaftar atau NIPY masih sama dengan sebelumnya.
				<br />
			</div>
			<?php } 
				$this->session->set_userdata('cek_nip', NULL);
			?>

			<?php if ($this->session->userdata('cek_telp') == "ada") { ?>
			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert">
					<i class="ace-icon fa fa-times"></i>
				</button>
				<i class="ace-icon fa fa-phone"></i>
				<strong>Perhatian! </strong>
					Nomor telepon sudah digunakan atau nomor masih sama dengan sebelumnya.
				<br />
			</div>
			<?php } 
				$this->session->set_userdata('cek_telp', NULL);
			?>
			<?php if ($this->session->userdata('kolom_lain') == "sukses") { ?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">
					<i class="ace-icon fa fa-times"></i>
				</button>
				<i class="ace-icon fa fa-phone"></i>
				<strong>Sukses! </strong>
					Perubahan kolom yang lain berhasil.
				<br />
			</div>
			<?php } 
				$this->session->set_userdata('kolom_lain', NULL);
			?>

			<?php if ($this->session->userdata('cek_username') != null) { ?>
			<div class="alert <?= $this->session->userdata('class_username'); ?>">
				<button type="button" class="close" data-dismiss="alert">
					<i class="ace-icon fa fa-times"></i>
				</button>
				<i class="ace-icon fa fa-user"></i>
				<strong><?= $this->session->userdata('info_username'); ?> </strong>
					<?= $this->session->userdata('pesan_username'); ?>
				<br />
			</div>
			<?php } 
				$this->session->set_userdata('cek_username', NULL);
				$this->session->set_userdata('info_username', NULL);
				$this->session->set_userdata('pesan_username', NULL);
				$this->session->set_userdata('class_username', NULL);
			?>

			<?php if ($this->session->userdata('cek_email') != null) { ?>
			<div class="alert <?= $this->session->userdata('class_email'); ?>">
				<button type="button" class="close" data-dismiss="alert">
					<i class="ace-icon fa fa-times"></i>
				</button>
				<i class="ace-icon fa fa-envelope"></i>
				<strong><?= $this->session->userdata('info_email'); ?> </strong>
					<?= $this->session->userdata('pesan_email'); ?>
				<br />
			</div>
			<?php } 
				$this->session->set_userdata('cek_email', NULL);
				$this->session->set_userdata('info_email', NULL);
				$this->session->set_userdata('pesan_email', NULL);
				$this->session->set_userdata('class_email', NULL);
			?>

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->

					<div class="col-xs-12">
						<div class="tabbable">
							<ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
								<li class="active">
									<a data-toggle="tab" href="#profile"><i class="ace-icon fa fa-user home-icon"></i> Profil</a>
								</li>
								<li>
									<a data-toggle="tab" href="#pengaturan"><i class="ace-icon fa fa-cog home-icon"></i> Pengaturan</a>
								</li>

								<li>
									<a data-toggle="tab" href="#password"><i class="ace-icon fa fa-key home-icon"></i> Password</a>
								</li>
								
							</ul>

							<div class="tab-content">
								<div id="profile" class="tab-pane in active">
									
									<div class="widget-body">
										<div class="widget-main">
											<form class="form-horizontal " method="post" action="<?= base_url(); ?>tenaga_pengajar/do_edit_profil/<?= $profil_pengguna['id'];?>" enctype="multipart/form-data">
												<div id="fuelux-wizard-container">
													
													<div class="step-content pos-rel">
														<div class="step-pane active" data-step="1">
															<h3 class="lighter block green"><b>Informasi Profil:</b></h3>
															<div class="hr hr-18 dotted hr-double"></div>
															
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="password">NIPY:</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input type="text" name="nip_nik" class="col-xs-12 col-sm-4" value="<?= $profil_pengguna['nip_nik'];?>" required/>
																	</div>
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="password">NUPTK:</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input type="text" name="nuptk" class="col-xs-12 col-sm-4" value="<?= $profil_pengguna['nuptk'];?>" required/>
																	</div>
																</div>
															</div>

															<div class="space-2"></div>
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="email">Nama Lengkap (+gelar):</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input type="text" name="nama" value="<?= $profil_pengguna['nama'];?>" class="col-xs-12 col-sm-6" required/>
																	</div>
																</div>
															</div>
															<div class="space-2"></div>
													<div class="form-group">
														<label class="col-sm-3 col-xs-12 control-label no-padding-right"> Status Kepegawaian:</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<select class=" col-xs-12 col-sm-5" id="form-field-select-1" name="pangkat" required>
															<option value="<?= $profil_pengguna['pangkat']; ?>"><?= $profil_pengguna['pangkat']; ?></option>
															<option> -- pilih status kepegawaian -- </option>
															<option value="Belum Ada">Belum Ada ( - )</option>
															<option value="Honorer">Honorer</option>
															<option value="Pegawai Tidak Tetap">Pegawai Tidak Tetap</option>
															<option value="Pegawai Tetap">Pegawai Tetap</option>
															<option value="Pegawai Tidak Tetap">Guru Tidak Tetap</option>
															<option value="Pegawai Tetap">Guru Tetap</option>
														<!--
															<option value="Pengatur Muda, II/a">Pengatur Muda, II/a</option>
															<option value="Pengatur Muda Tk.I, II/b">Pengatur Muda Tk.I, II/b</option>
															<option value="Pengatur, II/c">Pengatur, II/c</option>
															<option value="Pengatur Tk.I, II/d">Pengatur Tk.I, II/d</option>
															<option value="Penata Muda, III/a">Penata Muda, III/a</option>
															<option value="Penata Muda Tk.I, III/b">Penata Muda Tk.I, III/b</option>
															<option value="Penata, III/c">Penata, III/c</option>
															<option value="Penata Tk.I, III/d">Penata Tk.I, III/d</option>
															<option value="Pembina, IV/a">Pembina, IV/a</option>
															<option value="Pembina Tk.I, IV/b">Pembina Tk.I, IV/b</option>
															<option value="Pembina Utama Muda, IV/c">Pembina Utama Muda, IV/c</option>
															<option value="Pembina Utama Madya, IV/d">Pembina Utama Madya, IV/d</option>
															<option value="Pembina Utama, IV/e">Pembina Utama, IV/e</option>
														-->

														</select>
															</div>
														</div>
													</div>

													<div class="space-2"></div>
													<div class="form-group">
														<label class="col-sm-3 col-xs-12 control-label no-padding-right">Pangkat / Jabatan:</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<select class=" col-xs-12 col-sm-5" id="form-field-select-1" name="pang_jab" required>
																	<option value="<?= $profil_pengguna['progli_ampu']; ?>"><?= $profil_pengguna['progli_ampu']; ?></option>
																<option> -- pilih pangkat / jabatan -- </option>
																<option value="Belum Ada">Belum Ada ( - )</option>
																<option value="Tenaga Pengajar / Guru">Tenaga Pengajar / Guru</option>
																<option value="Kepala Sekolah">Kepala Sekolah</option>
																<option value="Wakil Kepala Sekolah">Wakil Kepala Sekolah</option>
																<option value="Bagian Kurikulum">Bagian Kurikulum</option>
																<option value="Bagian Kesiswaan">Bagian Kesiswaan</option>
																<option value="Bagaian Sarana & Prasarana">Bagaian Sarana & Prasarana</option>
																<option value="Bagian Hubungan Masyarakat">Bagian Hubungan Masyarakat</option>
																<option value="Kepala Tata Usaha">Kepala Tata Usaha</option>
																<option value="Tenaga Administrasi">Tenaga Administrasi</option>
																<option value="Pustakawan">Pustakawan</option>
																<option value="Staf">Staf</option>
																<option value="Petugas Keamanan">Petugas Keamanan</option>
																<option value="Petugas Kebersihan">Petugas Kebersihan</option>
																</select>
															</div>
														</div>
													</div>

													<div class="space-2"></div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="url">TMT menjadi Guru:</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input type="date" name="tmt_guru" value="<?= $profil_pengguna['tmt_guru'];?>" class="col-xs-12 col-sm-5" required/>
																	</div>
																</div>
															</div>

													<div class="space-2"></div>
													
													<div class="space-2"></div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="url">Pendidikan terakhir / Spesialisasi:</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input type="text" name="spesialisasi" value="<?= $profil_pengguna['spesialisasi'];?>" class="col-xs-12 col-sm-7" required/>
																	</div>
																</div>
															</div>

															<div class="hr hr-dotted"></div>
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right">Jenis Kelamin:</label>

																<?php
																$l = ""; $p = "";
																if ($profil_pengguna['j_kelamin'] == "Laki-laki") {
																	$l = "checked";
																} else {
																	$p = "checked";
																}
																?>

																<div class="col-xs-12 col-sm-9">
																	<div>
																		<label class="line-height-1 blue">
																			<input name="j_kelamin" value="Laki-laki" type="radio" class="ace" required <?= $l ?> />
																			<span class="lbl"> Laki-laki</span>
																		</label>
																	</div>

																	<div>
																		<label class="line-height-1 blue">
																			<input name="j_kelamin" value="Perempuan" type="radio" class="ace" required <?= $p ?> />
																			<span class="lbl"> Perempuan</span>
																		</label>
																	</div>
																</div>
															</div>

															<div class="hr hr-dotted"></div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Tempat Lahir:</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input type="text" name="tmp_lahir" value="<?= $profil_pengguna['tmp_lahir'];?>" class="col-xs-12 col-sm-5" required/>
																	</div>
																</div>
															</div>

															<div class="space-2"></div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="url">Tanggal Lahir:</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input type="date" name="tgl_lahir" value="<?= $profil_pengguna['tgl_lahir'];?>" class="col-xs-12 col-sm-5" required/>
																	</div>
																</div>
															</div>

															<div class="hr hr-dotted"></div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="comment">Alamat</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<textarea class="input-xlarge" name="alamat" required><?= $profil_pengguna['alamat'];?></textarea>
																	</div>
																</div>
															</div>
															<div class="space-2"></div>
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Telepon:</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input type="text" value="<?= $profil_pengguna['telp_hp'];?>" name="telp_hp" class="col-xs-12 col-sm-5" required/>
																	</div>
																</div>
															</div>		
														</div>
														
													</div>
												</div>

												<hr />
												
												<div class="clearfix">
													<div class="pull-right">
														<button class="btn btn-info" type="submit">
															<i class="ace-icon fa fa-save bigger-110"></i>
															Simpan
														</button>

													</div>
												</div>

											</form>
										</div><!-- /.widget-main -->
									</div><!-- /.widget-body -->

								</div>
								<div id="pengaturan" class="tab-pane">
									
									<div class="widget-body">
										<div class="widget-main">
											<form class="form-horizontal " method="post" action="<?= base_url(); ?>tenaga_pengajar/do_edit_otorisasi/<?= $profil_pengguna['id_pengguna'];?>/<?= $profil_pengguna['nip_nik'];?>" enctype="multipart/form-data">
												<div id="fuelux-wizard-container">
													
													<div class="step-content pos-rel">
														<div class="step-pane active" data-step="1">
															
															<h3 class="lighter block green"><b>Pengaturan Akun:</b></h3>
															<div class="hr hr-18 dotted hr-double"></div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="email">Username:</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input type="text" name="username" value="<?= $profil_pengguna['username'];?>" class="col-xs-12 col-sm-6" required/>
																	</div>
																</div>
															</div>
															
															
															<div class="space-2"></div>
															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="email">Alamat Email:</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input type="email" value="<?= $profil_pengguna['email'];?>" name="email" class="col-xs-12 col-sm-6" required/>
																	</div>
																</div>
															</div>
															
														</div>
														
													</div>
												</div>

												<hr />
												
												<div class="clearfix">
													<div class="pull-right">
														<button class="btn btn-info" type="submit">
															<i class="ace-icon fa fa-save bigger-110"></i>
															Simpan
														</button>

													</div>
												</div>

											</form>
										</div><!-- /.widget-main -->
									</div><!-- /.widget-body -->

								</div>

								<div id="password" class="tab-pane">
									
									<div class="widget-body">
										<div class="widget-main">
											<form class="form-horizontal" method="post" id="validation-form" action="<?= base_url(); ?>tenaga_pengajar/do_edit_password/<?= $profil_pengguna['id_pengguna'];?>/<?= $profil_pengguna['nip_nik'];?>" enctype="multipart/form-data" onsubmit="return checkPass(this);">
												<div id="fuelux-wizard-container">
													
													<div class="step-content pos-rel">
														<div class="step-pane active" data-step="1">
															
															<h3 class="lighter block green"><b>Ubah Password:</b></h3>
															<div class="hr hr-18 dotted hr-double"></div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 " for="password">Password Baru:</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input type="password" name="password" id="password1" class="col-xs-12 col-sm-4" required/>
																	</div>
																</div>
															</div>

															<div class="space-2"></div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 " for="password2">Konfirmasi Password:</label>

																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input type="password" name="password2" id="password2" class="col-xs-12 col-sm-4" required />
																	</div>
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-xs-12 col-sm-3 " for="password2"></label>
																<div class="col-xs-12 col-sm-9">
																	<div class="clearfix">
																		<input type="checkbox" onclick="myFunction()"> <label> Lihat Password</label>
																	</div>
																</div>
															</div>
														</div>
														
													</div>
												</div>

												<hr />
												
												<div class="clearfix">
													<div class="pull-right">
														<button class="btn btn-info" type="submit">
															<i class="ace-icon fa fa-save bigger-110"></i>
															Simpan
														</button>

													</div>
												</div>

											</form>
										</div><!-- /.widget-main -->
									</div><!-- /.widget-body -->

								</div>
							</div>
						</div>
					</div><!-- /.col -->
					
					<div class="row">
						<div class="col-xs-12">
							

							
						</div>
						
					</div>
				</div>
				
				<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->



<!-- basic scripts -->
<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='<?= base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>


<!-- page specific plugin scripts -->
<script src="<?= base_url(); ?>assets/js/wizard.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-additional-methods.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootbox.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?= base_url(); ?>assets/js/select2.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.gritter.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
	function myFunction() {
		var x = document.getElementById("password1");
		if (x.type == "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
		var y = document.getElementById("password2");
		if (y.type == "password") {
			y.type = "text";
		} else {
			y.type = "password";
		}
	}

	function checkPass(theForm) {
		if (theForm.password.value != theForm.password2.value)
		{
			alert('Kata sandi tidak sesuai!');
			return false;
		} else {
			return true;
		}
	}

</script>