 
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Tenaga Kepegawaian</a>
				</li>
				
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Profil Tenaga Pengajar
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $profil['nama_sekolah'];?>
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
					
					<div>
						<div id="user-profile-2" class="user-profile">
							<div class="tabbable">
								<ul class="nav nav-tabs padding-18">
									<li class="active">
										<a data-toggle="tab" href="#home">
											<i class="green ace-icon fa fa-user bigger-120"></i>
											Profil Tenaga Kepegawaian
										</a>
									</li>
								</ul>

								<div class="tab-content no-border padding-24">
									<div id="home" class="tab-pane in active">
										<div class="row">
											<div class="col-xs-12 col-sm-3 center">
												<span class="profile-picture">
													<img class="editable img-responsive" alt="<?= $profil_pengguna['nama'];?>" id="avatar2" src="<?= base_url(); ?><?= $profil_pengguna['foto_profil'];?>" />
												</span>

												<div class="space space-4"></div>
												<?php 
												if ($this->session->userdata('role') == 'sekolah' or $identitas['id'] == $profil_pengguna['id']) {
													?>
													<a href="#modal-table" role="button" data-toggle="modal" class="btn btn-sm btn-block btn-success">
														<i class="ace-icon fa fa-camera-retro bigger-110"></i>
														<span class="bigger-110">Ubah Foto Profil</span>
													</a>
												
													<a href="<?php echo site_url('tenaga_pengajar/edit');?>/<?= $profil_pengguna['id']; ?>/<?= url_title($profil_pengguna['nama'], 'dash', TRUE); ?>" class="btn btn-sm btn-block btn-primary">
														<i class="ace-icon fa fa-edit bigger-110"></i>
														<span class="bigger-110">Edit Profil</span>
													</a>
												<?php } ?>
												

											</div><!-- /.col -->

											<?php 
											if ($this->session->userdata('role') == 'sekolah' or $identitas['id'] == $profil_pengguna['id']) {
												?>
												<div id="modal-table" class="modal fade" tabindex="-1">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header no-padding">
																<div class="table-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		<span class="white">&times;</span>
																	</button>
																	Ubah Foto Profil
																</div>
															</div>

															<div class="modal-body">
																<form role="form" action="<?= base_url(); ?>tenaga_pengajar/do_edit_foto_profil/<?= $profil_pengguna['id'];?>" method="POST" enctype="multipart/form-data">	
																	<input type="file" id="id-input-file-2"  accept="image/png, image/jpeg" class="form-control" name="foto" required/>
																	
																	
																</div>

																<div class="modal-footer no-margin-top">

																	<button class="btn btn-sm btn-primary pull-right" type="submit">
																		<i class="ace-icon fa fa-save"></i>
																		Simpan
																	</button>
																</form>
																<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
																	<i class="ace-icon fa fa-times"></i>
																	Tutup
																</button>

																
															</div>
														</div><!-- /.modal-content -->
													</div><!-- /.modal-dialog -->
												</div>
											<?php } ?>

											<div class="col-xs-12 col-sm-9">
												
												<h4 class="blue">
													<span class="middle" style="font-weight: bold;"> Tenaga Kepegawaian</span>


													<div class="btn-group pull-right">

														<?php if ($profil_pengguna['status'] == 0) { ?>
															<button class="btn btn-xs btn-info btn-inverse">
																<i class="ace-icon fa fa-toggle-off smaller-80 align-middle"></i> Non-aktif
															</button>
														<?php } else { ?>
															<button class="btn btn-xs btn-info btn-primary">
																<i class="ace-icon fa fa-toggle-on smaller-80 align-middle"></i> Aktif
															</button>
														<?php } ?>

														<?php if ($this->session->userdata('role') == 'sekolah') { ?>
															<button data-toggle="dropdown" class="btn btn-xs btn-info btn-white dropdown-toggle">
																<span class="ace-icon fa fa-caret-down icon-only"></span>
															</button>

															<ul class="dropdown-menu dropdown-info dropdown-menu-right ">
																<li>
																	<a href="<?= base_url(); ?>tenaga_pengajar/do_edit_status/1/<?= $profil_pengguna['id'];?>"><i class="ace-icon fa fa-toggle-on"></i> Aktifkan</a>
																</li>

																<li>
																	<a href="<?= base_url(); ?>tenaga_pengajar/do_edit_status/0/<?= $profil_pengguna['id'];?>"><i class="ace-icon fa fa-toggle-off"></i> Nonaktifkan</a>
																</li>
															</ul>
														<?php } ?>

													</div><!-- /.btn-group -->
												</h4>

												<div class="hr hr-18 dotted hr-double"></div>
												<div class="profile-user-info">
													<div class="profile-info-row">
														<div class="profile-info-name"> Unit Kerja</div>

														<div class="profile-info-value">
															<span><?= $profil_pengguna['nama_sekolah'];?></span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> NIPY</div>

														<div class="profile-info-value">
															<span><?= $profil_pengguna['nip_nik'];?></span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> NUPTK</div>

														<div class="profile-info-value">
															<span><?= $profil_pengguna['nuptk'];?></span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> Nama</div>

														<div class="profile-info-value">
															<span><?= $profil_pengguna['nama'];?></span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> Status Pegawai</div>

														<div class="profile-info-value">
															<span><?= $profil_pengguna['pangkat'];?></span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> Jabatan</div>

														<div class="profile-info-value">
															<span><?= $profil_pengguna['progli_ampu'];?></span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> TMT </div>

														<div class="profile-info-value">
															<span>
																<?php
																	$date = $profil_pengguna['tmt_guru'];
																	$dateObj = DateTime::createFromFormat('Y-m-d', $date);
																?>
																<?= $dateObj->format('d F Y'); ?>
															</span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> Masa Kerja </div>

														<div class="profile-info-value">
															<?php
															$awal  = date_create($profil_pengguna['tmt_guru']);
															$akhir = date_create(); // waktu sekarang
															$diff  = date_diff( $awal, $akhir );

															?>
															<span><?php echo '<b>'.$diff->y . '</b> Tahun <b>'; echo $diff->m . '</b> Bulan <b>'; echo $diff->d . '</b> Hari '; ?></span>
														</div>
													</div>
													
													<div class="profile-info-row">
														<div class="profile-info-name">Pend. Terakhir</div>

														<div class="profile-info-value">
															<span><?= $profil_pengguna['spesialisasi'];?></span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> Jenis Kelamin</div>

														<div class="profile-info-value">
															<span><?= $profil_pengguna['j_kelamin'];?></span>
														</div>
													</div>

													<div class="profile-info-row">
														<div class="profile-info-name"> Usia </div>

														<div class="profile-info-value">
															<?php
															$date = new DateTime($profil_pengguna['tgl_lahir']);
															$now = new DateTime();
															$usia = $now->diff($date)->y;
															?>
															<span><?= $usia; ?> tahun</span>
														</div>
													</div>


													<div class="profile-info-row">
														<div class="profile-info-name"> Tempat Lahir </div>

														<div class="profile-info-value">

															<span><?= $profil_pengguna['tmp_lahir']; ?></span>

														</div>
													</div>


													<div class="profile-info-row">
														<div class="profile-info-name"> Tanggal Lahir </div>

														<div class="profile-info-value">
															<span>
																<?php
																$date = $profil_pengguna['tgl_lahir'];
																$dateObj = DateTime::createFromFormat('Y-m-d', $date);
																?>
																<?= $dateObj->format('d F Y'); ?>
															</span>
														</div>
													</div>

													<div class="profile-info-row">
														<div class="profile-info-name"> Alamat </div>

														<div class="profile-info-value">
															<i class="fa fa-map-marker orange bigger-110"></i>
															<span><?= $profil_pengguna['alamat'];?></span>

														</div>
													</div>

													<div class="profile-info-row">
														<div class="profile-info-name"> Telepon </div>

														<div class="profile-info-value">
															<i class="fa fa-phone blue bigger-110"></i>
															<span><?= $profil_pengguna['telp_hp'];?></span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> Email </div>

														<div class="profile-info-value">
															<i class="fa fa-envelope light-orange bigger-110"></i>
															<span><?= $profil_pengguna['email'];?></span>
														</div>
													</div>

												</div>															
											</div><!-- /.col -->
										</div><!-- /.row -->


									</div><!-- /#home -->
								</div>
							</div>
						</div>
					</div>



					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

<?php 
     if($this->session->userdata('status_aktif_guru') != NULL){ ?>
      <div class="alert alert-success" role="alert" style="position: fixed; top: 88px; right:4%; width: 45%; vertical-align: middle;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Sukses!</strong> Perubahan status aktif tenaga pengajar berhasil dilakukan.
      </div>
      <script type="text/javascript">
        $(document).ready (function(){
          window.setTimeout(function() {
              $(".alert").fadeTo(500, 0).slideUp(500, function(){
                  $(this).remove(); 
              });
          }, 4000);
        });
      </script>
      <?php 
        $this->session->set_userdata('status_aktif_guru', null);
} ?>

<?php 
     if($this->session->userdata('status_edit_guru') != NULL){ ?>
      <div class="alert alert-success" role="alert" style="position: fixed; top: 88px; right:4%; width: 45%; vertical-align: middle;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Sukses!</strong> Perubahan data tenaga pengajar berhasil dilakukan.
      </div>
      <script type="text/javascript">
        $(document).ready (function(){
          window.setTimeout(function() {
              $(".alert").fadeTo(500, 0).slideUp(500, function(){
                  $(this).remove(); 
              });
          }, 4000);
        });
      </script>
      <?php 
        $this->session->set_userdata('status_edit_guru', null);
} ?>

<?php 
     if($this->session->userdata('status_foto_guru') != NULL){ ?>
      <div class="alert alert-success" role="alert" style="position: fixed; top: 88px; right:4%; width: 45%; vertical-align: middle;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Sukses!</strong> Perubahan foto diri tenaga pengajar berhasil dilakukan.
      </div>
      <script type="text/javascript">
        $(document).ready (function(){
          window.setTimeout(function() {
              $(".alert").fadeTo(500, 0).slideUp(500, function(){
                  $(this).remove(); 
              });
          }, 4000);
        });
      </script>
      <?php 
        $this->session->set_userdata('status_foto_guru', null);
} ?>

<!-- basic scripts -->
<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>

<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='<?= base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

<!-- page specific plugin scripts -->
<script src="<?= base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.flash.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.html5.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.print.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.colVis.min.js"></script>
<script src="<?= base_url(); ?>assets/js/dataTables.select.min.js"></script>

<!-- inline scripts related to this page -->

		<script>
			function doconfirm()
			{
				job=confirm("Anda yakin akan menghapus data?");
				if(job!=true)
				{
					return false;
				}
			}
		</script>
