
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Tenaga Kepegawaian</a>
				</li>
				
			</ul><!-- /.breadcrumb -->
			
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Tambah Tenaga Kepegawaian
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $profil['nama_sekolah'];?>
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
					
					<div class="row">
						<div class="col-xs-12">
							

							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal " method="post" action="<?= base_url(); ?>tenaga_pengajar/do_tambah" enctype="multipart/form-data">
										<div id="fuelux-wizard-container">
											<div>
												<ul class="steps">
													<li data-step="1" class="active">
														<span class="step"><i class="ace-icon fa fa-graduation-cap home-icon"></i></span>
														<span class="title">Informasi Profil Pegawai</span>
													</li>

													<li data-step="2">
														<span class="step"><i class="ace-icon fa fa-cog home-icon"></i></span>
														<span class="title">Pengaturan Akun</span>
													</li>
												</ul>
											</div>

											<hr />

											<?php if ($this->session->userdata('cek_nip') == "ada") { ?>
												<div class="alert alert-warning">
													<button type="button" class="close" data-dismiss="alert">
														<i class="ace-icon fa fa-times"></i>
													</button>
													<i class="ace-icon fa fa-university"></i>
													<strong>Perhatian! </strong>
													NIPY sudah terdaftar.
													<br />
												</div>
											<?php } 
											$this->session->set_userdata('cek_nip', NULL);
											?>
											<?php if ($this->session->userdata('cek_telp') == "ada") { ?>
												<div class="alert alert-warning">
													<button type="button" class="close" data-dismiss="alert">
														<i class="ace-icon fa fa-times"></i>
													</button>
													<i class="ace-icon fa fa-phone"></i>
													<strong>Perhatian! </strong>
													Nomor telepon sudah terdaftar.
													<br />
												</div>
											<?php } 
											$this->session->set_userdata('cek_telp', NULL);
											?>
											<?php if ($this->session->userdata('cek_username') == "ada") { ?>
												<div class="alert alert-warning">
													<button type="button" class="close" data-dismiss="alert">
														<i class="ace-icon fa fa-times"></i>
													</button>
													<i class="ace-icon fa fa-user"></i>
													<strong>Perhatian! </strong>
													Username telah digunakan.
													<br />
												</div>
											<?php } 
											$this->session->set_userdata('cek_username', NULL);
											?>
											<?php if ($this->session->userdata('cek_email') == "ada") { ?>
												<div class="alert alert-warning">
													<button type="button" class="close" data-dismiss="alert">
														<i class="ace-icon fa fa-times"></i>
													</button>
													<i class="ace-icon fa fa-envelope"></i>
													<strong>Perhatian! </strong>
													Email sudah terdaftar.
													<br />
												</div>
											<?php } 
											$this->session->set_userdata('cek_email', NULL);
											?>


											<div class="step-content pos-rel">
												<div class="step-pane active" data-step="1">
													<h3 class="lighter block green"><b>Informasi Profil Pegawai:</b></h3>
													<div class="hr hr-18 dotted hr-double"></div>
													
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="password">NIPY:</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<input type="text" name="nip_nik" class="col-xs-12 col-sm-4" placeholder="Nomor Induk Pegawai Yayasan" required/>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="password">NUPTK:</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<input type="text" name="nuptk" class="col-xs-12 col-sm-4" placeholder="jika tidak ada beri tanda (-)" required/>
															</div>
														</div>
													</div>

													<div class="space-2"></div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="email">Nama Lengkap (+gelar):</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<input type="text" name="nama" class="col-xs-12 col-sm-6" required/>
															</div>
														</div>
													</div>
													<div class="space-2"></div>
													<div class="form-group">
														<label class="col-sm-3 col-xs-12 control-label no-padding-right">Status Kepegawaian:</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<select class=" col-xs-12 col-sm-5" id="form-field-select-1" name="pangkat" required>
															<option> -- pilih status kepegawaian -- </option>
															<option value="Belum Ada">Belum Ada ( - )</option>
															<option value="Honorer">Honorer</option>
															<option value="Pegawai Tidak Tetap">Pegawai Tidak Tetap</option>
															<option value="Pegawai Tetap">Pegawai Tetap</option>
															<option value="Pegawai Tidak Tetap">Guru Tidak Tetap</option>
															<option value="Pegawai Tetap">Guru Tetap</option>
														<!--
															<option value="Pengatur Muda, II/a">Pengatur Muda, II/a</option>
															<option value="Pengatur Muda Tk.I, II/b">Pengatur Muda Tk.I, II/b</option>
															<option value="Pengatur, II/c">Pengatur, II/c</option>
															<option value="Pengatur Tk.I, II/d">Pengatur Tk.I, II/d</option>
															<option value="Penata Muda, III/a">Penata Muda, III/a</option>
															<option value="Penata Muda Tk.I, III/b">Penata Muda Tk.I, III/b</option>
															<option value="Penata, III/c">Penata, III/c</option>
															<option value="Penata Tk.I, III/d">Penata Tk.I, III/d</option>
															<option value="Pembina, IV/a">Pembina, IV/a</option>
															<option value="Pembina Tk.I, IV/b">Pembina Tk.I, IV/b</option>
															<option value="Pembina Utama Muda, IV/c">Pembina Utama Muda, IV/c</option>
															<option value="Pembina Utama Madya, IV/d">Pembina Utama Madya, IV/d</option>
															<option value="Pembina Utama, IV/e">Pembina Utama, IV/e</option>
														-->

														</select>
															</div>
														</div>
													</div>

													<div class="space-2"></div>
													<div class="form-group">
														<label class="col-sm-3 col-xs-12 control-label no-padding-right">Pangkat / Jabatan:</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<select class=" col-xs-12 col-sm-5" id="form-field-select-1" name="pang_jab" required>
																<option> -- pilih pangkat / jabatan -- </option>
																<option value="Belum Ada">Belum Ada ( - )</option>
																<option value="Tenaga Pengajar / Guru">Tenaga Pengajar / Guru</option>
																<option value="Kepala Sekolah">Kepala Sekolah</option>
																<option value="Wakil Kepala Sekolah">Wakil Kepala Sekolah</option>
																<option value="Bagian Kurikulum">Bagian Kurikulum</option>
																<option value="Bagian Kesiswaan">Bagian Kesiswaan</option>
																<option value="Bagaian Sarana & Prasarana">Bagaian Sarana & Prasarana</option>
																<option value="Bagian Hubungan Masyarakat">Bagian Hubungan Masyarakat</option>
																<option value="Kepala Tata Usaha">Kepala Tata Usaha</option>
																<option value="Tenaga Administrasi">Tenaga Administrasi</option>
																<option value="Pustakawan">Pustakawan</option>
																<option value="Staf">Staf</option>
																<option value="Petugas Keamanan">Petugas Keamanan</option>
																<option value="Petugas Kebersihan">Petugas Kebersihan</option>
																</select>
															</div>
														</div>
													</div>

													<div class="space-2"></div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="url">TMT (Tanggal Mulai Tugas):</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<input type="date" name="tmt_guru" class="col-xs-12 col-sm-5" required/>
															</div>
														</div>
													</div>
													<!--
													<div class="space-2"></div>
													<div class="form-group">
														<label class="col-sm-3 col-xs-12 control-label no-padding-right">Status Guru Mapel</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<select class=" col-xs-12 col-sm-4" id="form-field-select-1" name="guru_mapel" required>
															<option> -- pilih status guru mapel -- </option>
															<option value="Mata Pelajaran Umum">Mata Pelajaran Umum</option>
															<option value="Mata Pelajaran Produktif">Mata Pelajaran Produktif</option>
															
														</select>
															</div>
														</div>
													</div>
													!-->

													

													<div class="hr hr-dotted"></div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right">Jenis Kelamin:</label>

														<div class="col-xs-12 col-sm-9">
															<div>
																<label class="line-height-1 blue">
																	<input name="j_kelamin" value="Laki-laki" type="radio" class="ace" required/>
																	<span class="lbl"> Laki-laki</span>
																</label>
															</div>

															<div>
																<label class="line-height-1 blue">
																	<input name="j_kelamin" value="Perempuan" type="radio" class="ace" required/>
																	<span class="lbl"> Perempuan</span>
																</label>
															</div>
														</div>
													</div>
													<div class="space-2"></div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="email">Pendidikan terakhir / spesialisasi:</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<input type="text" name="spesialisasi" class="col-xs-12 col-sm-8" required/>
															</div>
														</div>
													</div>

													<div class="hr hr-dotted"></div>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Tempat Lahir:</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<input type="text" name="tmp_lahir" class="col-xs-12 col-sm-5" required/>
															</div>
														</div>
													</div>

													<div class="space-2"></div>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="url">Tanggal Lahir:</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<input type="date" name="tgl_lahir" class="col-xs-12 col-sm-5" required/>
															</div>
														</div>
													</div>

													<div class="hr hr-dotted"></div>

													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="comment">Alamat</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<textarea class="input-xlarge" name="alamat" required></textarea>
															</div>
														</div>
													</div>
													<div class="space-2"></div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Telepon:</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<input type="text"  name="telp_hp" class="col-xs-12 col-sm-5" required/>
															</div>
														</div>
													</div>

													<div class="hr hr-dotted"></div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Foto Profil:</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix ">
																<input type="file" id="id-input-file-2"  accept="image/png, image/jpeg" class="form-control " name="foto" required/>
															</div>
														</div>
													</div>

													<div class="space-2"></div>
													<h3 class="lighter block green"><b>Pengaturan Akun:</b></h3>
													<div class="hr hr-18 dotted hr-double"></div>

													
													<div class="space-2"></div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="email">Alamat Email:</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<input type="email" name="email" class="col-xs-12 col-sm-6" required/>
															</div>
														</div>
													</div>
													
												</div>

												
												
											</div>
										</div>

										<hr />
										
										<div class="clearfix">
											<div class="pull-right">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-save bigger-110"></i>
													Simpan
												</button>

											</div>
										</div>

									</form>
								</div><!-- /.widget-main -->
							</div><!-- /.widget-body -->
						</div>
						
					</div>
				</div>
				
				<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->

<!-- basic scripts -->
<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='<?= base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>


<!-- page specific plugin scripts -->
<script src="<?= base_url(); ?>assets/js/wizard.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-additional-methods.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootbox.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?= base_url(); ?>assets/js/select2.min.js"></script>


<!-- inline scripts related to this page -->
