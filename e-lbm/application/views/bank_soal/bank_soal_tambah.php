
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Bank Soal <?= ucfirst($this->uri->segment(5)) ?></a>
				</li>
				
			</ul><!-- /.breadcrumb -->
			
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Tambah Bank Soal <?= ucfirst($this->uri->segment(5)) ?>
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $profil['nama_sekolah'];?>
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
					
					<div class="row">
						<div class="col-xs-12">
							

							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal " method="post" action="<?= base_url(); ?>bank_soal/do_tambah/<?= $this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5) ?>" enctype="multipart/form-data">

											<?php if ($this->session->userdata('cek_nis') == "ada") { ?>
												<div class="alert alert-warning">
													<button type="button" class="close" data-dismiss="alert">
														<i class="ace-icon fa fa-times"></i>
													</button>
													<i class="ace-icon fa fa-university"></i>
													<strong>Perhatian! </strong>
													NIS sudah terdaftar.
													<br />
												</div>
											<?php } 
											$this->session->set_userdata('cek_nis', NULL);
											?>
											
											<?php if ($this->session->userdata('cek_email') == "ada") { ?>
												<div class="alert alert-warning">
													<button type="button" class="close" data-dismiss="alert">
														<i class="ace-icon fa fa-times"></i>
													</button>
													<i class="ace-icon fa fa-envelope"></i>
													<strong>Perhatian! </strong>
													Email sudah terdaftar.
													<br />
												</div>
											<?php } 
											$this->session->set_userdata('cek_email', NULL);
											?>



													<div class="form-group">
														<label class="col-xs-12 col-sm-1" for="email">Pertanyaan:</label>

														<div class="col-xs-12 col-sm-11">
															<div class="clearfix">
																<textarea name="pertanyaan" id="pertanyaan" required></textarea>
															</div>
														</div>
													</div>
													<div class="space-2"></div>

													<div class="form-group">
														<label class="col-xs-12 col-sm-1" for="email">Jawaban 1:</label>

														<div class="col-xs-12 col-sm-11">
															<div class="clearfix">
																<textarea name="jawaban_1" id="jawaban_1" required></textarea>
																<div class="radio radio-info radio-inline">
																	<input type="radio" id="inlineRadio" value="1" name="jawaban_benar" >
																	<label> Jawaban Benar</label>
																</div>
															</div>
														</div>
													</div>
													<div class="space-2"></div>

													<div class="form-group">
														<label class="col-xs-12 col-sm-1" for="email">Jawaban 2:</label>

														<div class="col-xs-12 col-sm-11">
															<div class="clearfix">
																<textarea name="jawaban_2" id="jawaban_2" required></textarea>
																<div class="radio radio-info radio-inline">
																	<input type="radio" id="inlineRadio" value="2" name="jawaban_benar" >
																	<label> Jawaban Benar</label>
																</div>
															</div>
														</div>
													</div>
													<div class="space-2"></div>

													<div class="form-group">
														<label class="col-xs-12 col-sm-1" for="email">Jawaban 3:</label>

														<div class="col-xs-12 col-sm-11">
															<div class="clearfix">
																<textarea name="jawaban_3" id="jawaban_3" required></textarea>
																<div class="radio radio-info radio-inline">
																	<input type="radio" id="inlineRadio" value="3" name="jawaban_benar" >
																	<label> Jawaban Benar</label>
																</div>
															</div>
														</div>
													</div>
													<div class="space-2"></div>

													<div class="form-group">
														<label class="col-xs-12 col-sm-1" for="email">Jawaban 4:</label>

														<div class="col-xs-12 col-sm-11">
															<div class="clearfix">
																<textarea name="jawaban_4" id="jawaban_4" required></textarea>
																<div class="radio radio-info radio-inline">
																	<input type="radio" id="inlineRadio" value="4" name="jawaban_benar" >
																	<label> Jawaban Benar</label>
																</div>
															</div>
														</div>
													</div>
													<div class="space-2"></div>

													<div class="form-group">
														<label class="col-xs-12 col-sm-1" for="email">Jawaban 5:</label>

														<div class="col-xs-12 col-sm-11">
															<div class="clearfix">
																<textarea name="jawaban_5" id="jawaban_5" required></textarea>
																<div class="radio radio-info radio-inline">
																	<input type="radio" id="inlineRadio" value="5" name="jawaban_benar" >
																	<label> Jawaban Benar</label>
																</div>
															</div>
														</div>
													</div>
													<div class="space-2"></div>
												

										<hr />
										
										<div class="clearfix">
											<div class="pull-right">
												<a href="<?php echo site_url('bab/detail/').$this->uri->segment('3').'/'.$this->uri->segment('4');?>" class="btn btn-success">
													<i class="ace-icon fa fa-arrow-left bigger-110"></i>
													<span class="bigger-110">Kembali</span>
												</a>
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-save bigger-110"></i>
													Simpan
												</button>

											</div>
										</div>

									</form>
								</div><!-- /.widget-main -->
							</div><!-- /.widget-body -->
						</div>
						
					</div>
				</div>
				
				<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->

<!-- basic scripts -->
<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='<?= base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>


<!-- page specific plugin scripts -->
<script src="<?= base_url(); ?>assets/js/wizard.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-additional-methods.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootbox.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?= base_url(); ?>assets/js/select2.min.js"></script>

<script type="text/javascript">
	CKEDITOR.replace('pertanyaan',{
		filebrowserBrowseUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgbrowser.php'); ?>',
		filebrowserUploadUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgupload.php'); ?>',
		filebrowserImageBrowseUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgbrowser.php'); ?>',
		filebrowserImageUploadUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgupload.php'); ?>'
	});

	CKEDITOR.replace('jawaban_1',{
		filebrowserBrowseUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgbrowser.php'); ?>',
		filebrowserUploadUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgupload.php'); ?>',
		filebrowserImageBrowseUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgbrowser.php'); ?>',
		filebrowserImageUploadUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgupload.php'); ?>'
	});

	CKEDITOR.replace('jawaban_2',{
		filebrowserBrowseUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgbrowser.php'); ?>',
		filebrowserUploadUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgupload.php'); ?>',
		filebrowserImageBrowseUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgbrowser.php'); ?>',
		filebrowserImageUploadUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgupload.php'); ?>'
	});

	CKEDITOR.replace('jawaban_3',{
		filebrowserBrowseUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgbrowser.php'); ?>',
		filebrowserUploadUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgupload.php'); ?>',
		filebrowserImageBrowseUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgbrowser.php'); ?>',
		filebrowserImageUploadUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgupload.php'); ?>'
	});

	CKEDITOR.replace('jawaban_4',{
		filebrowserBrowseUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgbrowser.php'); ?>',
		filebrowserUploadUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgupload.php'); ?>',
		filebrowserImageBrowseUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgbrowser.php'); ?>',
		filebrowserImageUploadUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgupload.php'); ?>'
	});

	CKEDITOR.replace('jawaban_5',{
		filebrowserBrowseUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgbrowser.php'); ?>',
		filebrowserUploadUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgupload.php'); ?>',
		filebrowserImageBrowseUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgbrowser.php'); ?>',
		filebrowserImageUploadUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgupload.php'); ?>'
	});
</script>