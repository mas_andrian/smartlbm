
<div class="main-content"> 
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">FAQ (Frequently Asked Questions)</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">

						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					FAQ (Frequently Asked Questions)
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $profil['nama_sekolah'];?>
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row"> 
				<div class="col-xs-12">
					
					
					<div class="pull-left" style="padding-bottom: 1em;">
						<?php 
						if ($this->session->userdata('role') == 'superadmin') {
							?> 
							<a href="#modal-mp" role="button" data-toggle="modal"><button class="pull-right btn btn-sm btn-primary  btn-round" type="button">
								<i class="ace-icon fa fa-plus icon-on-right bigger-110"></i> <b>Tambah FAQ </b> 
							</button></a>
						<?php } else {
							echo "<br><br>";
						} ?>
					</div>
					<?php 
					if ($this->session->userdata('role') == 'superadmin') {
						?>

						<div id="modal-mp" class="modal fade" tabindex="-1">
							<div class="modal-dialog " style="width: 75%;">
								<div class="modal-content">
									<div class="modal-header no-padding">
										<div class="table-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
												<span class="white">&times;</span>
											</button>
											Tambah FAQ (Frequently Asked Questions)
										</div>
									</div>
									<form role="form" action="<?= base_url(); ?>faq/do_tambah" method="POST" enctype="multipart/form-data">	
										<div class="modal-body">
											<label>Pertanyaan : </label>
											<textarea name="pertanyaan" rows="5" style="width: 100%"></textarea>
											<br>
											<label>Jawaban : </label>
											<textarea name="jawaban" rows="5" style="width: 100%"></textarea>
										</div>

										<div class="modal-footer no-margin-top">

											<button class="btn btn-sm btn-primary pull-right" type="submit">
												<i class="ace-icon fa fa-save"></i>
												Simpan
											</button>
										
										<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
											<i class="ace-icon fa fa-times"></i>
											Tutup
										</button>
										
									</div>
									</form>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div>

					<?php } ?>
					<div class="nav-search pull-right" id="nav-search">
						<form class="form-search" action="<?= base_url(); ?>faq/list" method="post" novalidate>
							<span class="input-icon">
								<input type="text" value="<?= $search ?>" name="search" placeholder="Telusuri ..." class="nav-search-input" />
								<i class="ace-icon fa fa-search nav-search-icon" ></i>
								<input type="submit" name="submit" value="Cari" style="padding-left: 7px;">
							</span>
						</form>
					</div><!-- /.nav-search -->

				</div>
			</div>

			<div class="row">
				<div id="accordion" class="accordion-style2 panel-group">
					<?php 
					foreach ($result as $dt) { ?>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
														<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $dt['id_faq'];?>">
															<i class="ace-icon fa fa-angle-right bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
															&nbsp;<?= $dt['pertanyaan']; ?>
														</a>
													</h4>
								
							</div>

							<div class="panel-collapse collapse" id="collapse<?= $dt['id_faq'];?>">
								<div class="panel-body">
									<?= $dt['jawaban']; ?>
									<hr style="margin: 0.5em 0 0.5em 0;">
									<div class="pull-right">
										<button href="#modal-mp<?= $dt['id_faq'];?>" data-toggle="modal" class="btn btn-white btn-info btn-bold btn-xs">
											<i class="ace-icon fa fa-edit bigger-120 blue"></i>
												Edit
										</button>
										<a href="<?= base_url(); ?>faq/do_hapus/<?= $dt['id_faq']; ?>" onClick="return doconfirm();"><button class="btn btn-white btn-warning btn-bold btn-xs">
											<i class="ace-icon fa fa-trash-o bigger-120 orange"></i>
												Delete
										</button></a>
										<script>
													function doconfirm()
													{
														job=confirm("Anda yakin akan menghapus data?");
														if(job!=true)
														{
															return false;
														}
													}
												</script>
									</div>
								</div>
							</div>
						</div>
						<br>

						<div id="modal-mp<?= $dt['id_faq'];?>" class="modal fade" tabindex="-1">
							<div class="modal-dialog " style="width: 75%;">
								<div class="modal-content">
									<div class="modal-header no-padding">
										<div class="table-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
												<span class="white">&times;</span>
											</button>
											Edit FAQ (Frequently Asked Questions)
										</div>
									</div>
									<?php $slug = $dt['id_faq']; ?>
									<form role="form" action="<?= base_url(); ?>faq/do_edit_faq/<?=$slug;?>" method="POST" enctype="multipart/form-data">	
										<div class="modal-body">
											<label>Pertanyaan : </label>
											<textarea name="pertanyaan" rows="5" style="width: 100%"><?= $dt['pertanyaan']; ?></textarea>
											<br>
											<label>Jawaban : </label>
											<textarea name="jawaban" rows="5" style="width: 100%"><?= $dt['jawaban']; ?></textarea>
										</div>

										<div class="modal-footer no-margin-top">

											<button class="btn btn-sm btn-primary pull-right" type="submit">
												<i class="ace-icon fa fa-save"></i>
												Simpan
											</button>
										
										<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
											<i class="ace-icon fa fa-times"></i>
											Tutup
										</button>
										
									</div>
									</form>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div>
						
					<?php } ?>
				</div>
			</div>

			<div class="row">
				<div class="col">
					<!--Tampilkan pagination-->
					<?php echo $pagination; ?>
				</div>
			</div>

		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

<!-- basic scripts -->

<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>


<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

<script src="<?= base_url(); ?>assets/js/holder.min.js"></script>


