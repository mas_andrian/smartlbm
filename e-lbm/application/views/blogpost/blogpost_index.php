

<div class="main-content"> 
	<div class="main-content-inner"> 
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">Pemberitahuan</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">

						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Blogpost
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $profil['nama_sekolah'];?>
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row"> 
				<div class="col-xs-12">
					
					
					<div class="pull-left" style="padding-bottom: 1em;">
						<?php 
						if ($this->session->userdata('role') == 'superadmin') {
							?> 
							<a href="#modal-mp" role="button" data-toggle="modal"><button class="pull-right btn btn-sm btn-primary  btn-round" type="button">
								<i class="ace-icon fa fa-plus icon-on-right bigger-110"></i> <b>Tambah Postingan </b> 
							</button></a>
						<?php } else {
							echo "<br><br>";
						} ?>
					</div>
					<?php 
					if ($this->session->userdata('role') == 'superadmin') {
						?>

						<div id="modal-mp" class="modal fade" tabindex="-1">
							<div class="modal-dialog " style="width: 75%;">
								<div class="modal-content">
									<div class="modal-header no-padding">
										<div class="table-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
												<span class="white">&times;</span>
											</button>
											Tambah Postingan
										</div>
									</div>
									<form role="form" action="<?= base_url(); ?>blogpost/do_tambah" method="POST" enctype="multipart/form-data">	
										<div class="modal-body">
											<label>Kategori</label>
											<input type="text" id="form-field-1-1" name="kategori" placeholder="Masukkan kategori (Ex: berita, pemberitahuan, artikel, dll.)..." class="form-control" required/>
											<br>
											<label>Judul</label>
											<input type="text" id="form-field-1-1" name="judul" placeholder="Masukkan judul ..." class="form-control" required/>
											<br>
											<label>Isi / Deskripsi</label>
											<textarea id="editor1" name="isi"></textarea>
											<script>
												CKEDITOR.replace( 'editor1', {
    filebrowserBrowseUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgbrowser.php'); ?>',
    filebrowserUploadUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgupload.php'); ?>',
    filebrowserImageBrowseUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgbrowser.php'); ?>',
    filebrowserImageUploadUrl: '<?php echo base_url('assets/ckeditor/plugins/imageuploader/imgupload.php'); ?>'
});
											</script>
											<br>
											<label>Banner Image</label>
											<input type="file" id="form-field-1-1" name="file" class="form-control" required/>
											
										</div> 

										<div class="modal-footer no-margin-top">

											<button class="btn btn-sm btn-primary pull-right" type="submit">
												<i class="ace-icon fa fa-save"></i>
												Simpan
											</button>
										
										<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
											<i class="ace-icon fa fa-times"></i>
											Tutup
										</button>
										
									</div>
									</form>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div>

					<?php } ?>
					<div class="nav-search pull-right" id="nav-search">
						<form class="form-search" action="<?= base_url(); ?>blogpost/list" method="post" novalidate>
							<span class="input-icon">
								<input type="text" value="<?= $search ?>" name="search" placeholder="Telusuri ..." class="nav-search-input" />
								<i class="ace-icon fa fa-search nav-search-icon" ></i>
								<input type="submit" name="submit" value="Cari" style="padding-left: 7px;">
							</span>
						</form>
					</div><!-- /.nav-search -->

				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<?php 
					foreach ($result as $dt) { ?>
						<div class="media search-media">
							<div class="media-left">
								<a href="<?= base_url(); ?>blogpost/view/<?= $dt['flag']; ?>">
									
								</a>
							</div>

							<div class="media-body">
								<div>
									<small><i><?= $dt['kategori']; ?></i></small>
									<h4 class="media-heading">
										<a href="<?= base_url(); ?>blogpost/view/<?= $dt['flag']; ?>" class="blue"><?= $dt['judul']; ?></a>
									</h4>
								</div>

								<p style="text-align: justify; padding-right: 1em;"><?php echo strip_tags(character_limiter($dt['deskripsi'], 300)); ?></p>

								<div class="search-actions text-center">
									<?php
									$date = $dt['time_at'];
									$dateObj = DateTime::createFromFormat('Y-m-d H:i:s', $date);
									?>
									
										<span class="text-info grey"><?= $dateObj->format('d F Y'); ?></span>
										<div class="action-buttons bigger-100">
											<i class="ace-icon fa fa-star blue"></i>
											<i class="ace-icon fa fa-star yellow"></i>
											<i class="ace-icon fa fa-star red"></i>
										</div>
										<span class="text-info grey"><?= $dateObj->format('H:i:s'); ?></span>
										<a href="<?= base_url(); ?>blogpost/view/<?= $dt['flag']; ?>" class="search-btn-action btn btn-sm btn-block btn-inverse">Read more</a>

									
								</div>
							</div>
						</div>
						<br>

					<?php } ?>

					
				</div>
			</div>

			<div class="row">
				<div class="col">
					<!--Tampilkan pagination-->
					<?php echo $pagination; ?>
				</div>
			</div>

		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

<!-- basic scripts -->

<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>


<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

<script src="<?= base_url(); ?>assets/js/holder.min.js"></script>


