<!-- Our Footer Middle Area -->
	<section class="footer_middle_area home3 p0">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-md-3 col-lg-3 col-xl-2 pb15 pt15">
					<div class="logo-widget home3">
						<img class="img-fluid" src="<?= base_url(); ?>assets/frontend/images/ylbm.png" alt="ylbm.png">
						<span>YLBM</span>
					</div>
				</div>
				<div class="col-sm-8 col-md-5 col-lg-6 col-xl-6 pb25 pt25 brdr_left_right home3">
					<div class="footer_menu_widget home3">
						<ul>
							<li class="list-inline-item"><a href="#">Beranda</a></li>
							<li class="list-inline-item"><a href="#">Privasi</a></li>
							<li class="list-inline-item"><a href="#">Sitemap</a></li>
							<li class="list-inline-item"><a href="#">FAQ</a></li>
							<li class="list-inline-item"><a href="#">Hubungi Kami</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-3 col-xl-4 pb15 pt15">
					<div class="footer_social_widget mt15">
						<ul>
							<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li class="list-inline-item"><a href="#"><i class="fa fa-google"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

<!-- Our Footer Bottom Area -->
	<section class="footer_bottom_area home3 pt30 pb30">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="copyright-widget text-center">
						<p>Copyright Yayasan Lembaga Bakti Muslim (YLBM) - Sragen © <?= date('Y') ?>. All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
<a class="scrollToHome home3" href="#"><i class="flaticon-up-arrow-1"></i></a>
</div>
<!-- Wrapper End -->
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/jquery-migrate-3.0.0.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/popper.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/jquery.mmenu.all.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/ace-responsive-menu.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/isotop.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/snackbar.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/simplebar.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/parallax.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/scrollto.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/jquery-scrolltofixed-min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/jquery.counterup.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/wow.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/progressbar.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/slider.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/timepicker.js"></script>
<!-- Custom script for all pages --> 
<script type="text/javascript" src="<?= base_url(); ?>assets/frontend/js/script.js"></script>
</body>
</html>