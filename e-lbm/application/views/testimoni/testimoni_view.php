
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Testimoni Pengguna</a>
				</li>
				
			</ul><!-- /.breadcrumb -->		
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Detail Testimoni Pengguna
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->

					<div class="widget-box" id="widget-box-1">
						<div class="widget-header">
							<h5 class="widget-title"><?= $testimoni['nama'];?></h5>

							<div class="widget-toolbar">
								<?php 
								if ($this->session->userdata('role') == 'superadmin') {
									?>
									<div class="widget-menu">
										<a href="#" data-action="settings" data-toggle="dropdown">
											<i class="ace-icon fa fa-bars"></i>
										</a>

										<ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
											<li>
												<a href="#modal-img" role="button" data-toggle="modal">Edit Foto Pengguna</a>
											</li>
											<li>
												<a href="#modal-mp" role="button" data-toggle="modal">Edit Testimoni</a>
											</li>
											<li>
												<a href="<?= base_url(); ?>testimoni/do_hapus/<?= $testimoni['id_testimoni']; ?>" onClick="return doconfirm();">Hapus</a>
												<script>
													function doconfirm()
													{
														job=confirm("Anda yakin akan menghapus data?");
														if(job!=true)
														{
															return false;
														}
													}
												</script>
											</li>
										</ul>
									</div>
								<?php } ?>

							</div>
						</div>

						
					</div>
					<div >
						<div style="padding: 2em; text-align: justify;">
							<div class="col-md-3">
								<img class="img-responsive" src="<?= base_url(); ?><?= $testimoni['gambar'];?>">
								<hr>
								
							</div>
							<div class="col-md-9">
								<?php
									$date = $testimoni['time_at'];
									$dateObj = DateTime::createFromFormat('Y-m-d H:i:s', $date);
								?>
								<table>
									<tr>
										<td width="80px;"><b>Nama</b></td><td width="20px">: </td><td> <?= $testimoni['nama'];?></td>
									</tr>
									<tr>
										<td><b>No. Telp</b></td><td>: </td><td> <?= $testimoni['phone'];?></td>
									</tr>
									<tr>
										<td><b>Kategori</b></td><td>: </td><td> <?= $testimoni['kategori'];?></td>
									</tr>
									<tr>
										<td><b>Tanggal</b></td><td>: </td><td> <?= $dateObj->format('d F Y'); ?></td>
									</tr>
									
								</table>
								<br>
								<p><b>Rating Penilaian : </b>
									<?php
											$star = $testimoni['rating'];
											for($i = 1; $i <= $star; $i++){
												echo '<i class="ace-icon fa fa-star orange2 icon-animated-bell"></i>';
											}
									?>
									<span style="font-size: 10px;">(<?= $star; ?>)</span>
								</p>

								<p><b>Kesan :</b></p>
								<?= $testimoni['kesan'];?>
								<br><br>
								<p><b>Saran :</b></p>
								<?= $testimoni['saran'];?>
							</div>
						</div>
					</div>
					
					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

						<div id="modal-mp" class="modal fade" tabindex="-1">
							<div class="modal-dialog " style="width: 75%;">
								<div class="modal-content">
									<div class="modal-header no-padding">
										<div class="table-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
												<span class="white">&times;</span>
											</button>
											Edit Tetimoni
										</div>
									</div>
									<?php $slug = $testimoni['id_testimoni']; ?>
									<form role="form" action="<?= base_url(); ?>testimoni/do_edit_testimoni/<?=$slug;?>" method="POST" enctype="multipart/form-data">	
										<div class="modal-body">
											
											<label>Nama </label>
											<input value="<?= $testimoni['nama']; ?>" type="text" id="form-field-1-1" name="nama" placeholder="Masukkan nama ..." class="form-control" required/>
											<br>
											<label>Telepon</label>
											<input value="<?= $testimoni['phone']; ?>" type="text" id="form-field-1-1" name="phone" placeholder="Masukkan Telepon ..." class="form-control" required/>
											<br>
											<label>Kategori Testimoni</label>
											<select class="form-control" id="form-field-select-1" name="kategori" required>
												<option selected="true" value="<?= $testimoni['kategori']; ?>"><?= $testimoni['kategori']; ?></option>
															<option  disabled="true"> -- Pilih kategori pengguna -- </option>
															<option value="Mitra Sekolah">Mitra Sekolah</option>
															<option value="Pengajar / Guru">Pengajar / Guru</option>
															<option value="siswa">Siswa</option>
															<option value="Orang tua / Wali siswa">Orang tua / Wali siswa</option>
															<option value="Umum">Umum</option>
											</select>
											<br>
											<label>Kesan : </label>
											<textarea name="kesan" rows="5" style="width: 100%"><?= $testimoni['kesan']; ?></textarea>
											<br>
											<label>Saran : </label>
											<textarea name="saran" rows="5" style="width: 100%"><?= $testimoni['saran']; ?></textarea>
											<br>
											<label>Rating Penilaian (1 - 5)</label>
											<input value="<?= $testimoni['rating']; ?>" type="number" min="1" max="5" id="form-field-1-1" name="rating" placeholder="Masukkan Rating ..." class="form-control" required/>
											
										</div>

										<div class="modal-footer no-margin-top">

											<button class="btn btn-sm btn-primary pull-right" type="submit">
												<i class="ace-icon fa fa-save"></i>
												Simpan
											</button>
										
										<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
											<i class="ace-icon fa fa-times"></i>
											Tutup
										</button>
										
									</div>
									</form>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div>

						<div id="modal-img" class="modal fade" tabindex="-1">
							<div class="modal-dialog " style="width: 75%;">
								<div class="modal-content">
									<div class="modal-header no-padding">
										<div class="table-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
												<span class="white">&times;</span>
											</button>
											Edit Foto Pengguna
										</div>
									</div>
									<?php $slug = $testimoni['id_testimoni']; ?>
									<form role="form" action="<?= base_url(); ?>testimoni/do_edit_img/<?=$slug;?>" method="POST" enctype="multipart/form-data">	
										<div class="modal-body">
											
											<input type="hidden"  name="path"  class="form-control" value="<?= $testimoni['gambar'];?>" required/>
											<label>Foto Pengguna</label>
											<input type="file" id="form-field-1-1" name="file" class="form-control" required/>
											
										</div>

										<div class="modal-footer no-margin-top">

											<button class="btn btn-sm btn-primary pull-right" type="submit">
												<i class="ace-icon fa fa-save"></i>
												Simpan
											</button>
										
										<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
											<i class="ace-icon fa fa-times"></i>
											Tutup
										</button>
										
									</div>
									</form>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div>


<!-- basic scripts -->

<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>


<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

