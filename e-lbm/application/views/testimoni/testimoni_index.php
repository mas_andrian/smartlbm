
<div class="main-content"> 
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">Testimoni Pengguna</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">

						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Testimoni Pengguna
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $profil['nama_sekolah'];?>
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row"> 
				<div class="col-xs-12">
					
					
					<div class="pull-left" style="padding-bottom: 1em;">
						<?php 
						if ($this->session->userdata('role') == 'superadmin') {
							?> 
							<a href="#modal-mp" role="button" data-toggle="modal"><button class="pull-right btn btn-sm btn-primary  btn-round" type="button">
								<i class="ace-icon fa fa-plus icon-on-right bigger-110"></i> <b>Tambah Testimoni </b> 
							</button></a>
						<?php } else {
							echo "<br><br>";
						} ?>
					</div>
					<?php 
					if ($this->session->userdata('role') == 'superadmin') {
						?>

						<div id="modal-mp" class="modal fade" tabindex="-1">
							<div class="modal-dialog " style="width: 75%;">
								<div class="modal-content">
									<div class="modal-header no-padding">
										<div class="table-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
												<span class="white">&times;</span>
											</button>
											Tambah Testimoni Pengguna
										</div>
									</div>
									<form role="form" action="<?= base_url(); ?>testimoni/do_tambah" method="POST" enctype="multipart/form-data">	
										<div class="modal-body">
											
											<label>Nama </label>
											<input type="text" id="form-field-1-1" name="nama" placeholder="Masukkan nama ..." class="form-control" required/>
											<br>
											<label>Telepon</label>
											<input type="text" id="form-field-1-1" name="phone" placeholder="Masukkan Telepon ..." class="form-control" required/>
											<br>
											<label>Kategori Testimoni</label>
											<select class="form-control" id="form-field-select-1" name="kategori" required>
															<option selected="true" disabled="true"> -- Pilih kategori pengguna -- </option>
															<option value="Mitra Sekolah">Mitra Sekolah</option>
															<option value="Pengajar / Guru">Pengajar / Guru</option>
															<option value="siswa">Siswa</option>
															<option value="Orang tua / Wali siswa">Orang tua / Wali siswa</option>
															<option value="Umum">Umum</option>
											</select>
											<br>
											<label>Kesan : </label>
											<textarea name="kesan" rows="5" style="width: 100%"></textarea>
											<br>
											<label>Saran : </label>
											<textarea name="saran" rows="5" style="width: 100%"></textarea>
											<br>
											<label>Rating Penilaian (1 - 5)</label>
											<input type="number" min="1" max="5" id="form-field-1-1" name="rating" placeholder="Masukkan Rating ..." class="form-control" required/>
											<br>
											<label>Foto Pengguna</label>
											<input type="file" id="form-field-1-1" name="file" class="form-control" required/>
											
										</div>

										<div class="modal-footer no-margin-top">

											<button class="btn btn-sm btn-primary pull-right" type="submit">
												<i class="ace-icon fa fa-save"></i>
												Simpan
											</button>
										
										<button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
											<i class="ace-icon fa fa-times"></i>
											Tutup
										</button>
										
									</div>
									</form>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div>

					<?php } ?>
					<div class="nav-search pull-right" id="nav-search">
						<form class="form-search" action="<?= base_url(); ?>testimoni/list" method="post" novalidate>
							<span class="input-icon">
								<input type="text" value="<?= $search ?>" name="search" placeholder="Telusuri ..." class="nav-search-input" />
								<i class="ace-icon fa fa-search nav-search-icon" ></i>
								<input type="submit" name="submit" value="Cari" style="padding-left: 7px;">
							</span>
						</form>
					</div><!-- /.nav-search -->

				</div>
			</div>

			<div class="row">
				
					<?php 
					foreach ($result as $dt) { ?>
						<div class="media search-media">
							<div class="media-left">
								
									<img style="height: 80px; width: 70px; border: 2px solid;" src="<?= base_url(); ?><?= $dt['gambar']; ?>">
								
							</div>

							<div class="media-body">
								<div>
									<h4 class="media-heading">
										<a href="<?= base_url(); ?>testimoni/view/<?= $dt['id_testimoni']; ?>/<?= url_title($dt['nama']); ?>" class="blue"><?= $dt['nama']; ?> [<?= $dt['kategori']; ?>]</a>
									</h4>
								</div>

								<p><b>Kesan:</b> <?php echo strip_tags(character_limiter($dt['kesan'], 130)); ?></p>
								<p><b>Saran:</b> <?php echo strip_tags(character_limiter($dt['saran'], 130)); ?></p>
								<div class="search-actions text-center">
									<?php
									$date = $dt['time_at'];
									$dateObj = DateTime::createFromFormat('Y-m-d H:i:s', $date);
									?>
									<span class="text-info"><?= $dateObj->format('d F Y'); ?></span>
									<div class="action-buttons bigger-125">
										<?php
											$star = $dt['rating'];
											for($i = 1; $i <= $star; $i++){
												echo '<i class="ace-icon fa fa-star orange2 icon-animated-bell"></i>';
											}
										?>
										<span style="font-size: 10px;">(<?= $star; ?>)</span>
										
											
									</div>
									<a href="<?= base_url(); ?>testimoni/view/<?= $dt['id_testimoni']; ?>/<?= url_title($dt['nama']); ?>" class="search-btn-action btn btn-sm btn-block btn-info">Detail</a>
								</div>
							</div>
						</div>
						<br>
					<?php } ?>

			</div>

			<div class="row">
				<div class="col">
					<!--Tampilkan pagination-->
					<?php echo $pagination; ?>
				</div>
			</div>

		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

<!-- basic scripts -->

<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>


<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

<script src="<?= base_url(); ?>assets/js/holder.min.js"></script>


