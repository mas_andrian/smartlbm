
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Materi</a>
				</li>
				
			</ul><!-- /.breadcrumb -->
			
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Tambah Materi
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $profil['nama_sekolah'];?>
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
					
					<div class="row">
						<div class="col-xs-12">
							

							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal " method="post" action="<?= base_url(); ?>Materi/do_tambah" enctype="multipart/form-data">
										<div id="fuelux-wizard-container">

											<?php if ($this->session->userdata('cek_nis') == "ada") { ?>
												<div class="alert alert-warning">
													<button type="button" class="close" data-dismiss="alert">
														<i class="ace-icon fa fa-times"></i>
													</button>
													<i class="ace-icon fa fa-university"></i>
													<strong>Perhatian! </strong>
													NIS sudah terdaftar.
													<br />
												</div>
											<?php } 
											$this->session->set_userdata('cek_nis', NULL);
											?>
											
											<?php if ($this->session->userdata('cek_email') == "ada") { ?>
												<div class="alert alert-warning">
													<button type="button" class="close" data-dismiss="alert">
														<i class="ace-icon fa fa-times"></i>
													</button>
													<i class="ace-icon fa fa-envelope"></i>
													<strong>Perhatian! </strong>
													Email sudah terdaftar.
													<br />
												</div>
											<?php } 
											$this->session->set_userdata('cek_email', NULL);
											?>


											<div class="step-content pos-rel">
												<div class="step-pane active" data-step="1">
													<h3 class="lighter block green"><b>Informasi Materi:</b></h3>
													<div class="hr hr-18 dotted hr-double"></div>
													
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="">Mata Pelajaran:</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<select name="id_mata_pelajaran" class="col-xs-12 col-sm-5" required>
																	<option disabled="true" selected="true">-- pilih mata pelajaran --</option>
																	<?php foreach ($mapel as $pel) { ?>
																	<option value="<?php echo $pel['id'] ?>"><?php echo $pel['nama_mapel']; ?></option>
																	<?php } ?>
																</select>
															</div>
														</div>
													</div>

													<div class="space-2"></div>
													<div class="form-group">
														<label class="col-sm-3 col-xs-12 control-label no-padding-right">Tingkatan:</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<select class=" col-xs-12 col-sm-5" id="form-field-select-1" name="tingkatan" required>
															<option disabled="true" selected="true">-- pilih tingkatan --</option>
															<?php
																if($sekolah['jenjang'] == 'Taman Kanak-kanak'){
										                            echo "
										                              <option value='TK-A (Kecil)'>TK-A (Kecil)</option>
										                              <option value='TK-B (Besar)'>TK-B (Besar)</option>
										                            ";
										                        } else if($sekolah['jenjang'] == 'Sekolah Dasar'){
										                             echo "
										                              <option value='I/1 (Satu)'>I/1 (Satu)</option>
										                              <option value='II/2 (Dua)'>II/2 (Dua)</option>
										                              <option value='III/3 (Tiga)'>III/3 (Tiga)</option>
										                              <option value='IV/4 (Empat)'>IV/4 (Empat)</option>
										                              <option value='V/5 (Lima)'>V/5 (Lima)</option>
										                              <option value='VI/6 (Enam)'>VI/6 (Enam)</option>
										                            ";
										                        } else if($sekolah['jenjang'] == 'Sekolah Menengah Pertama'){
										                            echo "
										                              <option value='VII/7 (Tujuh)'>VII/7 (Tujuh)</option>
										                              <option value='VIII/8 (Delapan)'>VIII/8 (Delapan)</option>
										                              <option value='IX/9 (Sembilan)'>IX/9 (Sembilan)</option>
										                            ";
										                        } else if($sekolah['jenjang'] == 'Sekolah Menengah Atas'){
										                             echo "
										                              <option value='X/10 (Sepuluh)'>X/10 (Sepuluh)</option>
										                              <option value='XI/11 (Sebelas)'>XI/11 (Sebelas)</option>
										                              <option value='XII/12 (Dua Belas)'>XII/12 (Dua Belas)</option>
										                            ";
										                        }
															?>
														</select>
															</div>
														</div>
													</div>
													<div class="space-2"></div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="email">Guru Penanggung Jawab:</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<select name="guru_penanggung_jawab[]" class="js-example-basic-multiple col-xs-12 col-sm-5" multiple="multiple" required>
																	<option disabled="true">-- pilih guru penanggung jawab --</option>
																	<?php foreach ($tenaga_pengajar as $tp) { ?>
																	<option value="<?php echo $tp['id'] ?>"><?php echo $tp['nama']; ?></option>
																	<?php } ?>
																</select>
															</div>
														</div>
													</div>
	
												</div>
												
											</div>
										</div>

										<hr />
										
										<div class="clearfix">
											<div class="pull-right">
												<a href="<?php echo site_url('materi');?>" class="btn btn-success">
													<i class="ace-icon fa fa-arrow-left bigger-110"></i>
													<span class="bigger-110">Kembali</span>
												</a>
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-save bigger-110"></i>
													Simpan
												</button>

											</div>
										</div>

									</form>
								</div><!-- /.widget-main -->
							</div><!-- /.widget-body -->
						</div>
						
					</div>
				</div>
				
				<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->

<!-- basic scripts -->
<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='<?= base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>


<!-- page specific plugin scripts -->
<script src="<?= base_url(); ?>assets/js/wizard.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-additional-methods.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootbox.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?= base_url(); ?>assets/js/select2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.js-example-basic-multiple').select2({
			placeholder: "-- pilih guru penanggung jawab --",
			closeOnSelect: false
		});
	});
</script>


<!-- inline scripts related to this page -->
