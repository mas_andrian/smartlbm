 
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Detail Materi</a>
				</li>
				
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Detail Materi
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<b><?= $materi->nama_mapel;?> [<?= "Kelas ".$materi->tingkatan;?>]</b>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $profil['nama_sekolah'];?>
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
					
					<div>
						<div id="user-profile-2" class="user-profile">
							<div class="tabbable">
								<ul class="nav nav-tabs padding-18">
									<li class="active">
										<a data-toggle="tab" href="#home">
											<i class="green ace-icon fa fa-book bigger-120"></i>
											Detail Materi
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#bab">
											<i class="blue ace-icon fa fa-list bigger-120"></i>
											Data BAB
										</a>
									</li>
									
								</ul>

								<div class="tab-content no-border padding-24">
									<div id="home" class="tab-pane in active">
										<div class="row">

											<div class="col-xs-12 col-sm-12">
												
												<h4 class="blue">
													<span class="middle" style="font-weight: bold;">Detail Informasi Materi</span>


													<div class="btn-group pull-right">

														<?php if ($materi->is_aktif == 'N') { ?>
															<button class="btn btn-xs btn-info btn-inverse">
																<i class="ace-icon fa fa-toggle-off smaller-80 align-middle"></i> Non-aktif
															</button>
														<?php } else { ?>
															<button class="btn btn-xs btn-info btn-primary">
																<i class="ace-icon fa fa-toggle-on smaller-80 align-middle"></i> Aktif
															</button>
														<?php } ?>

														<?php if ($this->session->userdata('role') == 'sekolah') { ?>
															<button data-toggle="dropdown" class="btn btn-xs btn-info btn-white dropdown-toggle">
																<span class="ace-icon fa fa-caret-down icon-only"></span>
															</button>

															<ul class="dropdown-menu dropdown-info dropdown-menu-right ">
																<li>
																	<a href="<?= base_url(); ?>materi/do_edit_status/Y/<?= $materi->id;?>/<?= $this->uri->segment(4) ?>"><i class="ace-icon fa fa-toggle-on"></i> Aktifkan</a>
																</li>

																<li>
																	<a href="<?= base_url(); ?>materi/do_edit_status/N/<?= $materi->id;?>/<?= $this->uri->segment(4) ?>"><i class="ace-icon fa fa-toggle-off"></i> Nonaktifkan</a>
																</li>
															</ul>
														<?php } ?>

													</div><!-- /.btn-group -->
												</h4>

												<div class="hr hr-18 dotted hr-double"></div>
												<div class="profile-user-info">
													<div class="profile-info-row">
														<div class="profile-info-name"> Mata Pelajaran</div>

														<div class="profile-info-value">
															<span><?= $materi->nama_mapel;?></span>
														</div>
													</div>
													<div class="profile-info-row">
														<div class="profile-info-name"> Tingkatan</div>

														<div class="profile-info-value">
															<span><?= $materi->tingkatan;?></span>
														</div>
													</div>
													
													

													<div class="profile-info-row">
														<div class="profile-info-name"> Guru Penanggung Jawab</div>

														<div class="profile-info-value">
															<span>
																<?php
																	function myfunction($products, $field, $value){
																		foreach($products as $key => $product){
																			if ( $product[$field] === $value )
																				return $key;
																		}
																		return false;
																	}
																	
																	$guru = explode(",", $materi->guru_penanggung_jawab);
																	unset($guru[count($guru) - 1]);
																	unset($guru[0]);
																	foreach ($guru as $g) {
																		echo "<li>".$tenaga_pengajar[myfunction($tenaga_pengajar, "id", $g)]["nama"]."</li>";
																	}
																	
																?>
															</span>
														</div>
													</div>
													<br/>
													<a href="<?php echo site_url('materi');?>" class="btn btn-xs btn-info">
														<i class="ace-icon fa fa-arrow-left bigger-110"></i>
														<span class="bigger-110">Kembali Ke List Materi Kelas</span>
													</a>
												</div>															
											</div><!-- /.col -->
										</div><!-- /.row -->


									</div><!-- /#home -->

									<div id="bab" class="tab-pane">
										<div class="profile-feed row">
											<div class="row">
												<div class="col-xs-12">
													<!-- div.table-responsive -->
													
													<h4 class="blue">
														<span class="middle" style="font-weight: bold;">Data BAB Materi</span>
														<div class="pull-right">
														<?php 
															if ($this->session->userdata('role') == 'guru') {
																?>
																<a href="<?php echo site_url('bab/tambah');?>/<?= $materi->id; ?>/<?= url_title($materi->nama_mapel, 'dash', TRUE); ?>" class="btn btn-sm btn-block btn-primary">
																	<i class="ace-icon fa fa-plus bigger-110"></i>
																	<span class="bigger-110">Tambah BAB</span>
																</a>
															<?php } ?>
														</div>
													</h4>
													
													<div class="hr hr-18 dotted hr-double"></div>
														<div class="table-responsive">
															<table id="dynamic-table" class="table table-striped table-bordered table-hover">
																<thead>
																	<tr>
																		<th class="center" width="5%">No.</th>
																		<th>Urutan</th>
																		<th>Judul</th>
																		<th>Thumbnail</th>
																		<th>Deskripsi</th>
																		<th>Status</th>
																		<th>Rating</th>
																		<th></th>
																	</tr>
																</thead>
																<tbody>
																	<?php 
																	$count = 1;
																	if(! $bab){
																		echo "<tr><td colspan='7' style='text-align:center;'>Data BAB Tidak Ada</td></tr>";
																	}
																	foreach ($bab as $dt) { ?>
																		
																		<tr>
																			<td class="center"><?= $count++; ?></td>
																			<td><?= "BAB ".$dt['urutan']; ?></td>
																			<td><?= $dt['judul']; ?></td>
																			<td><img src="<?= base_url().$dt['img_thumbnail'] ?>" class="img-thumbnail" width="100px" /></td>
																			<td><?= $dt['deskripsi']; ?></td>
																			
																			
																			<td><?= ($dt['is_aktif'] == "Y" ? "Aktif" : "Non Aktif") ?></td>
																			<td><?= ($dt['avg_rating'] == "" ? 0 : $dt['avg_rating']); ?></td>
																			<td class="center">
																				<div class="hidden-sm hidden-xs action-buttons">
																					<a class="blue" href="<?php echo site_url('bab/detail');?>/<?= $dt['id']; ?>/<?= url_title($dt['slug'], 'dash', TRUE); ?>/<?= $this->uri->segment(3) ?>/<?= url_title($dt['nama_mapel'], 'dash', TRUE); ?>">
																						<i class="ace-icon fa fa-search-plus bigger-130" title="Detail"></i>
																					</a>
																					<?php 
																					if($this->session->userdata('role') == 'guru') {
																						?>
																						<a class="green" href="<?php echo site_url('bab/edit');?>/<?= $dt['id']; ?>/<?= $dt['slug'] ?>/<?= $this->uri->segment(3) ?>/<?= url_title($dt['nama_mapel'], 'dash', TRUE); ?>">
																							<i class="ace-icon fa fa-pencil bigger-130" title="Edit"></i>
																						</a>
																					<?php } ?>

																				</div>

																			</td>

																		</tr>

																	<?php } ?>

																</tbody>
															</table>
														</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

<!-- basic scripts -->
<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>

<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='<?= base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

<!-- page specific plugin scripts -->
<script src="<?= base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.flash.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.html5.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.print.min.js"></script>
<script src="<?= base_url(); ?>assets/js/buttons.colVis.min.js"></script>
<script src="<?= base_url(); ?>assets/js/dataTables.select.min.js"></script>

<script type="text/javascript">
	jQuery(function($) {
				//initiate dataTables plugin
				var myTable = 
				$('#dynamic-table')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					bAutoWidth: false,
					
					
					select: {
						style: 'multi'
					}
				} );
				
				
				$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
				
				new $.fn.dataTable.Buttons( myTable, {
					buttons: [
					{
						"extend": "colvis",
						"text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
						"className": "btn btn-white btn-primary btn-bold",
						columns: ':not(:first):not(:last)'
					},
					{
						"extend": "copy",
						"text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "csv",
						"text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "excel",
						"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "pdf",
						"text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "print",
						"text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
						"className": "btn btn-white btn-primary btn-bold",
						autoPrint: false,
						message: 'Learning Management System - Lembaga Bakti Muslim Sragen'
					}		  
					]
				} );
				myTable.buttons().container().appendTo( $('.tableTools-container') );
				
				var defaultColvisAction = myTable.button(0).action();
				myTable.button(0).action(function (e, dt, button, config) {
					
					defaultColvisAction(e, dt, button, config);
					
					
					if($('.dt-button-collection > .dropdown-menu').length == 0) {
						$('.dt-button-collection')
						.wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
						.find('a').attr('href', '#').wrap("<li />")
					}
					$('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
				});			
				
			})
		</script>