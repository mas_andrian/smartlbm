<!-- /.conten utama -->
<?php
	if($this->session->userdata('role') != 'superadmin'){
		$c = "Sekolah";
	} else {
		$c = "Yayasan";
	}
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Profil <?= $c; ?></li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Profil <?= $c; ?>
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $profil['nama_sekolah'];?>

					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<?php
					if ($this->session->userdata('role') != 'superadmin' || $this->session->userdata('role') != 'sekolah') { ?>
						<div class="clearfix">
							<div class="pull-right">
								<a href="<?php echo site_url('profil_yayasan/edit');?>"><i class="fa fa-edit light-blue bigger-130"></i><span class="blue middle bolder"> Edit profil</span></a>
							</div>
						</div>
						<div class="hr dotted"></div>
					<?php } ?>

					
					<!-- PAGE CONTENT BEGINS -->
					<div>
						<div id="user-profile-1" class="user-profile row">
							<div class="col-xs-12 col-sm-3 center">
								<div>
									<span class="profile-picture">
										<img id="avatar" class=" img-responsive" alt="logo sekolah" src="<?= base_url(); ?><?= $profil['logo_sekolah']; ?>" />

									</span>

									<?php
									if ($this->session->userdata('role') != 'superadmin' || $this->session->userdata('role') != 'sekolah') { ?>

										<div class="widget-body" style="padding-bottom: 1em;">
											<div class="widget-main">
												<div class="form-group">
													<div class="col-xs-12">
														<div class="hr dotted"></div>
														<form role="form" action="<?= base_url(); ?>profil_yayasan/do_edit_logo" method="POST" enctype="multipart/form-data">
															<input type="hidden" name="id_sekolah" value="<?= $profil['id']; ?>">
															<label><b>Edit logo:</b></label>
															<input type="file" id="id-input-file-2"  accept="image/png, image/jpeg" class="form-control" name="foto" required/>
															<div style="margin: 0.5em;">
																<input type="submit" name="" value="Simpan">
															</div>
														</form>
														<div class="hr dotted"></div>
													</div>
												</div>
											</div>
										</div>

										<?php	
									}
									?>

									<div class="space-10"></div>

									<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
										<div class="inline position-relative">
											<a href="#" class="user-title-label dropdown-toggle" data-toggle="dropdown">
												<i class="ace-icon fa fa-university light-orange"></i>
												&nbsp;
												<span class="white"><?= $profil['nama_sekolah'];?></span>
											</a>

										</div>
									</div>
								</div>
								

								<div class="space-6"></div>

								<div class="profile-contact-info">
									<div class="profile-contact-links align-left">
										<a href="#" class="btn btn-link">
											<i class="ace-icon fa fa-phone bigger-120 green"></i>
											Telp. <?= $profil['telp'];?>
										</a>
										<br>
										<a href="#" class="btn btn-link">
											<i class="ace-icon glyphicon glyphicon-print bigger-120 green"></i>
											Fax. <?= $profil['fax'];?>
										</a>
										<br>
										<a href="#" class="btn btn-link">
											<i class="ace-icon fa fa-envelope bigger-120 pink"></i>
											<?= $profil['email'];?>
										</a>
										<br>
										<a href="#" class="btn btn-link">
											<i class="ace-icon fa fa-globe bigger-125 blue"></i>
											<?= $profil['website'];?>
										</a>
									</div>
									
								</div>

							</div>

							<div class="col-xs-12 col-sm-9">
								<div class="center">

									<button class="btn btn-app btn-primary btn-sm" title="Tenaga Pengajar">
										<i class="ace-icon fa fa-user bigger-200"></i>
										<?= $tot_pengajar;?>
									</button>

									<button class="btn btn-app btn-warning btn-sm" title="Siswa">
										<i class="ace-icon fa fa-users bigger-200"></i>
										<?= $tot_siswa;?>
									</button>

									<!--
									<button class="btn btn-app btn-pink btn-sm" title="Perangkat Ajar">
										<i class="ace-icon fa fa-book bigger-200"></i>
										<?= $tot_app;?>
									</button>

									<button class="btn btn-app btn-info btn-sm" title="Penilaian Kinerja Guru">
										<i class="ace-icon fa fa-pencil-square-o bigger-200"></i>
										<?= $tot_pkg;?>
									</button>
									!-->

								</div>

								<div class="space-12"></div>
								<?php
									if($this->session->userdata('role') != 'superadmin'){
										$a = "NSS";
										$b = "NPSN";
										$c = "Sekolah";
										$d = "(Kurikulum)";
									} else {
										$a = "No. Yayasan";
										$b = "Akta Notaris";
										$c = "Yayasan";
										$d = "";
									}
								?>

								<div class="profile-user-info profile-user-info-striped">
									<div class="profile-info-row">
										<div class="profile-info-name"> Nama <?= $c; ?> </div>

										<div class="profile-info-value">
											<span id="username"><?= $profil['nama_sekolah'];?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Alamat </div>

										<div class="profile-info-value">
											<i class="fa fa-map-marker light-orange bigger-110"></i>
											<span id="country"> <?= $profil['alamat'];?></span>
											<span id="city"><?= $profil['kecamatan'];?></span>
											<span id="city"><?= $profil['kab_kota'];?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Kode Pos </div>

										<div class="profile-info-value">
											<span  ><?= $profil['kode_pos'];?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> <?= $a; ?> </div>

										<div class="profile-info-value">
											<span  > <?= $profil['nss'];?> </span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> <?= $b; ?> </div>

										<div class="profile-info-value">
											<span > <?= $profil['npsn'];?> </span>
										</div>
									</div>
									<div class="profile-info-row">
										<div class="profile-info-name"> Telepon </div>

										<div class="profile-info-value">
											<span > <?= $profil['telp'];?> </span>
										</div>
									</div>
									<div class="profile-info-row">
										<div class="profile-info-name"> Email </div>

										<div class="profile-info-value">
											<span > <?= $profil['email'];?> </span>
										</div>
									</div>

								</div>

								<div class="space-20"></div>
								<div class="space-20"></div>

								<div class="clearfix">
									<center>
										<div class="grid2">
											<b>Kepala <?= $c;?></b> <br>
											<span class="bigger-175 blue"><?= $profil['nama_kepsek'];?></span>

											<br />
											NIPY. <?= $profil['nip_kepsek'];?>
										</div>

										<div class="grid2">
											<b>Wakil Kepala <?= $c;?> <?= $d;?></b> <br>
											<span class="bigger-175 blue"><?= $profil['nama_wakakur'];?></span>

											<br />
											NIPY. <?= $profil['nip_wakakur'];?>
										</div>
									</center>
								</div>

								
							</div>
						</div>
					</div>

					

					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

<?php 
     if($this->session->userdata('status_edit_sekolah') != NULL){ ?>
      <div class="alert alert-success" role="alert" style="position: fixed; top: 88px; right:4%; width: 45%; vertical-align: middle;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Sukses!</strong> Perubahan data profil <?= $c; ?> berhasil dilakukan.
      </div>
      <script type="text/javascript">
        $(document).ready (function(){
          window.setTimeout(function() {
              $(".alert").fadeTo(500, 0).slideUp(500, function(){
                  $(this).remove(); 
              });
          }, 4000);
        });
      </script>
      <?php 
        $this->session->set_userdata('status_edit_sekolah', null);
} ?>

<!-- basic scripts -->
<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>


<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

<!-- inline scripts related template footer wajib -->