<!-- /.conten utama -->
<?php
	if($this->session->userdata('role') != 'superadmin'){
											$a = "NSS";
											$b = "NPSN";
											$c = "Sekolah";
											$d = "(Kurikulum)";
	} else {
											$a = "No. Yayasan";
											$b = "Akta Notaris";
											$c = "Yayasan";
											$d = "";
	}
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Profil <?= $c;?></li>
			</ul><!-- /.breadcrumb -->

			<div class="nav-search" id="nav-search">
				<form class="form-search">
					<span class="input-icon">
						<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
						<i class="ace-icon fa fa-search nav-search-icon"></i>
					</span>
				</form>
			</div><!-- /.nav-search -->
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Edit Profil <?= $c;?>
					
				</h1>
			</div><!-- /.page-header -->


			<div class="row">
				<div class="col-xs-12">
					<div class="widget-body">
						<div class="widget-main">
									
							
							<form role="form" action="<?= base_url(); ?>profil_yayasan/do_edit_profil/<?= $profil['logo_sekolah']; ?>" method="POST" enctype="multipart/form-data">
								<div>
									<label for="form-field-select-3">Nama <?= $c;?></label>
									<input type="text" id="form-field-1-1" placeholder="Masukkan nama <?= $c; ?> ..." class="form-control" name="nama_sekolah" value="<?= $profil['nama_sekolah'];?>" required="true"/>
									<br>
									<label for="form-field-select-3">Alamat</label><br>
									<div>
										<div class="col-xs-5" style="padding: 0;">
											<input  type="text" id="form-field-1-1" placeholder="Masukkan alamat ..." class="form-control" name="alamat" value="<?= $profil['alamat'];?>" required="true"/>
										</div>
										<div class="col-xs-3" style="padding: 0 1em 0 1em;">
											<input  type="text" id="form-field-1-1" placeholder="Kecamatan ..." class="form-control" name="kecamatan" value="<?= $profil['kecamatan'];?>" required="true"/>
										</div>
										<div class="col-xs-3" style="padding: 0 1em 0 0;">
											<input  type="text" id="form-field-1-1" placeholder="Kota atau kabupaten ..." class="form-control" name="kab_kota" value="<?= $profil['kab_kota'];?>" required="true"/>
										</div>
										<div class="col-xs-1"style="padding: 0;">
											<input  type="text" id="form-field-1-1" placeholder="Kode pos ..." class="form-control" name="kode_pos" value="<?= $profil['kode_pos'];?>" required="true"/>
										</div>
									</div>
									<br><br><br>
									<label for="form-field-select-3">Kontak</label><br>
									<div>
										<div class="col-xs-3" style="padding: 0;">
											<input  type="text" id="form-field-1-1" placeholder="Telepon ..." class="form-control" name="telp" value="<?= $profil['telp'];?>" required="true"/>
										</div>
										<div class="col-xs-3" style="padding: 0 1em 0 1em;">
											<input  type="text" id="form-field-1-1" placeholder="Fax ..." class="form-control" name="fax" value="<?= $profil['fax'];?>" required="true"/>
										</div>
										<div class="col-xs-3" style="padding: 0 1em 0 0;">
											<input  type="text" id="form-field-1-1" placeholder="Email ..." class="form-control" name="email" value="<?= $profil['email'];?>" required="true"/>
										</div>
										<div class="col-xs-3"style="padding: 0;">
											<input  type="text" id="form-field-1-1" placeholder="Website ..." class="form-control" name="website" value="<?= $profil['website'];?>" required="true"/>
										</div>
									</div>
									
									<br><br><br>
									<label for="form-field-select-3">Nomor <?= $c; ?></label><br>
									<div>
										<div class="col-xs-6" style="padding: 0;">
											<input  type="text" id="form-field-1-1" placeholder="<?= $a;?> ..." class="form-control" name="nss" value="<?= $profil['nss'];?>" required="true"/>
										</div>
										<div class="col-xs-6" style="padding: 0 0 0 1em;">
											<input  type="text" id="form-field-1-1" placeholder="<?= $b;?> ..." class="form-control" name="npsn" value="<?= $profil['npsn'];?>" required="true"/>
										</div>
									</div>
									<br><br><br>
									<label for="form-field-select-3">Kepala <?= $c; ?></label><br>
									<div>
										<div class="col-xs-6" style="padding: 0;">
											<input  type="text" id="form-field-1-1" placeholder="NIPY kepala <?= $c; ?> ..." class="form-control" name="nip_kepsek" value="<?= $profil['nip_kepsek'];?>" required="true"/>
										</div>
										<div class="col-xs-6" style="padding: 0 0 0 1em;">
											<input  type="text" id="form-field-1-1" placeholder="Nama kepala <?= $c; ?> ..." class="form-control" name="nama_kepsek" value="<?= $profil['nama_kepsek'];?>" required="true"/>
										</div>
									</div>
									<br><br><br>
									<label for="form-field-select-3">Wakil Kepala <?= $c; ?> <?= $d;?></label><br>
									<div>
										<div class="col-xs-6" style="padding: 0;">
											<input  type="text" id="form-field-1-1" placeholder="NIPY wakil kepala <?= $c; ?> <?= $d;?> ..." class="form-control" name="nip_wakakur" value="<?= $profil['nip_wakakur'];?>" required="true"/>
										</div>
										<div class="col-xs-6" style="padding: 0 0 0 1em;">
											<input  type="text" id="form-field-1-1" placeholder="Nama wakil kepala <?= $c; ?> <?= $d;?> ..." class="form-control" name="nama_wakakur" value="<?= $profil['nama_wakakur'];?>" required="true"/>
										</div>
									</div>
									<input type="hidden" name="id_sekolah" value="<?= $profil['id']; ?>">
									
									
									
									<label for="form-field-select-3" style="color: #fff;">.</label><br>
									<div class="space-6"></div>

									<div class="clearfix">
										<div class="pull-right">
											<button class="btn btn-info" type="submit">
												<i class="ace-icon fa fa-save bigger-110"></i>
												Simpan
											</button>

										</div>
									</div>
									
								</div>
							</form>

							
						</div>
					</div>

					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

<!-- basic scripts -->
<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>

<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>

		<!-- inline scripts related template footer wajib -->