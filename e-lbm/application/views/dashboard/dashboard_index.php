<!-- /.conten utama -->
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Dashboard</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<div class="ace-settings-container" id="ace-settings-container">
				<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
					<i class="ace-icon fa fa-cog bigger-130"></i>
				</div>

				<div class="ace-settings-box clearfix" id="ace-settings-box">
					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="no-skin" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div><!-- /.pull-left -->

					<div class="pull-left width-50">
						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
							<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
							<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
						</div>

						<div class="ace-settings-item">
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
							<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
						</div>
					</div><!-- /.pull-left -->
				</div><!-- /.ace-settings-box -->
			</div><!-- /.ace-settings-container -->

			<div class="page-header">
				<h1>
					Dashboard
					<small>
						<i class="ace-icon fa fa-angle-double-right"></i>
						<?= $profil['nama_sekolah'];?>
						
					</small>
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">

					<!-- PAGE CONTENT BEGINS -->
					<div>
						<div id="user-profile-1" class="user-profile row">
							<div class="col-xs-12 col-sm-3 center">
								<div>
									<span class="profile-picture">
										<img id="avatar" class=" img-responsive" alt="logo sekolah" src="<?= base_url(); ?><?= $profil['logo_sekolah']; ?>" />

									</span>

									
								</div>
								

								<div class="space-6"></div>


							</div>

							<div class="col-xs-12 col-sm-9">
								

								<div class="space-12"></div>
								<?php
									if($this->session->userdata('role') != 'superadmin'){
										$a = "NSS";
										$b = "NPSN";
										$c = "Sekolah";
									} else {
										$a = "No. Yayasan";
										$b = "Akta Notaris";
										$c = "Yayasan";
									}
								?>

								<div class="profile-user-info profile-user-info-striped">
									<div class="profile-info-row">
										<div class="profile-info-name"> Nama <?= $c; ?> </div>

										<div class="profile-info-value">
											<span id="username"><?= $profil['nama_sekolah'];?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Alamat </div>

										<div class="profile-info-value">
											<i class="fa fa-map-marker light-orange bigger-110"></i>
											<span id="country"> <?= $profil['alamat'];?></span>
											<span id="city"><?= $profil['kecamatan'];?></span>
											<span id="city"><?= $profil['kab_kota'];?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Kode Pos </div>

										<div class="profile-info-value">
											<span  ><?= $profil['kode_pos'];?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> <?= $a; ?> </div>

										<div class="profile-info-value">
											<span  > <?= $profil['nss'];?> </span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> <?= $b; ?> </div>

										<div class="profile-info-value">
											<span > <?= $profil['npsn'];?> </span>
										</div>
									</div>
									<div class="profile-info-row">
										<div class="profile-info-name"> Telepon </div>

										<div class="profile-info-value">
											<span > <?= $profil['telp'];?> </span>
										</div>
									</div>
									<div class="profile-info-row">
										<div class="profile-info-name"> Email | Web </div>

										<div class="profile-info-value">
											<span > <?= $profil['email'];?> | <?= $profil['website'];?></span>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>

				<?php if ($this->session->userdata('role') == 'superadmin') { ?>
					<div class="space-6"></div>
					<div class="space-6"></div>
					<div class=" infobox-container" style="padding: 0; margin: 0;">
						<div class="infobox infobox-blue">
							<div class="infobox-icon">
								<i class="ace-icon fa fa-globe"></i>
							</div>
							<div class="infobox-data">
								<span class="infobox-data-number "><?= $tot_info;?></span>
								<div class="infobox-content">Pemberitahuan</div>
							</div>
						</div>
						<div class="infobox infobox-green">
							<div class="infobox-icon">
								<i class="ace-icon fa fa-home"></i>
							</div>
							<div class="infobox-data">
								<span class="infobox-data-number "><?= $tot_lp;?></span>
								<div class="infobox-content">Lembaga Pendidikan</div>
							</div>
						</div>

						<div class="infobox infobox-red">
							<div class="infobox-icon">
								<i class="ace-icon fa fa-user"></i>
							</div>
							<div class="infobox-data">
								<span class="infobox-data-number"><?= $tot_pengajar;?></span>
								<div class="infobox-content">Tenaga Pengajar</div>
							</div>
						</div>

						<div class="infobox infobox-purple">
							<div class="infobox-icon">
								<i class="ace-icon fa fa-users"></i>
							</div>
							<div class="infobox-data">
								<span class="infobox-data-number"><?= $tot_siswa;?></span>
								<div class="infobox-content">Siswa</div>
							</div>
						</div>
						<div class="space-6"></div>
					</div>
				<?php } ?>
				<?php if ($this->session->userdata('role') == 'sekolah') { ?>
					<div class="space-6"></div>
					<div class="space-6"></div>
					<div class=" infobox-container" style="padding: 0; margin: 0;">
						<div class="infobox infobox-green">
							<div class="infobox-icon">
								<i class="ace-icon fa fa-globe"></i>
							</div>
							<div class="infobox-data">
								<span class="infobox-data-number "><?= $tot_info;?></span>
								<div class="infobox-content">Pemberitahuan</div>
							</div>
						</div>
						<div class="infobox infobox-red">
							<div class="infobox-icon">
								<i class="ace-icon fa fa-user"></i>
							</div>
							<div class="infobox-data">
								<span class="infobox-data-number"><?= $tot_pengajar;?></span>
								<div class="infobox-content">Tenaga Pengajar</div>
							</div>
						</div>
						<div class="infobox infobox-purple">
							<div class="infobox-icon">
								<i class="ace-icon fa fa-users"></i>
							</div>
							<div class="infobox-data">
								<span class="infobox-data-number"><?= $tot_siswa;?></span>
								<div class="infobox-content">Siswa</div>
							</div>
						</div>
						
						<div class="space-6"></div>
					</div>
				<?php } ?>

				</div>
			</div>

			
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->


<!-- basic scripts -->
<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- ace scripts -->
<script src="<?= base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ace.min.js"></script>
<!-- inline scripts related template footer wajib -->

