<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Login - SMART LBM</title>

	<meta name="description" content="User login page" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<link rel="icon" href="<?= base_url(); ?>assets/frontend/images/favicon-new.png" type="image/gif" >

	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/font-awesome/4.5.0/css/font-awesome.min.css" />

	<!-- text fonts -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/fonts.googleapis.com.css" />

	<!-- ace styles -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace-rtl.min.css" />

</head>

<body class="login-layout">
	<div class="main-container">
		<div class="main-content">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<div class="login-container" style="padding-top: 6em;">
						<div class="center">
							<h1>
								<center><img class="img-fluid" src="<?= base_url(); ?>assets/frontend/images/lg-botton-white.png" alt="smartlbm.png"></center>
								
							</h1>
							<h5 class="white" id="id-company-text">Learning Management System<br>Lembaga Bakti Muslim</h5>
						</div>

						<div class="space-6"></div>

						<div class="position-relative">
							<div id="login-box" class="login-box visible widget-box no-border">
								<div class="widget-body">
									<div class="widget-main">
										<h4 class="header blue lighter bigger">
											<i class="ace-icon fa fa-edit green"></i>
											Silahkan masukkan informasi
										</h4>

										<div class="space-6"></div>

										<form action="<?= base_url(); ?>login/do_login" method="POST" enctype="multipart/form-data">
											<fieldset>
												<label class="block clearfix">
													<span class="block input-icon input-icon-right">
														<input type="text" class="form-control" placeholder="Username" name="username" required="true" />
														<i class="ace-icon fa fa-user"></i>
													</span>
												</label>

												<label class="block clearfix">
													<span class="block input-icon input-icon-right">
														<input type="password" class="form-control" id="password" placeholder="Password" name="password" required="true" />
														<i class="ace-icon fa fa-lock"></i>
													</span>
												</label>
												
													
												

												<center>
													<span style="color: red;">

														<?php
														if($this->session->userdata('error') != NULL) {?>
															<br>
															<?php echo $this->session->userdata('error');
														}
														?>

													</span></center>
													<div class="space"></div>

													<div class="clearfix">
														<input class="pull-left" type="checkbox" onclick="myFunction()"> <label style="font-size: 12px;"> &nbsp; Lihat Password</label>

														<button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
															<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">Masuk</span>
														</button>
													</div>

													<script>
														function myFunction() {
															var x = document.getElementById("password");
															if (x.type == "password") {
																x.type = "text";
															} else {
																x.type = "password";
															}
														}
													</script>

													<div class="space-4"></div>
												</fieldset>
											</form>
										</div><!-- /.widget-main -->

										<div class="toolbar clearfix">
											<div>
												<a href="#" data-target="#forgot-box" class="forgot-password-link">
													<i class="ace-icon fa fa-arrow-left"></i>
													Lupa kata sandi
												</a>
											</div>

										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->

								<div id="forgot-box" class="forgot-box widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header red lighter bigger">
												<i class="ace-icon fa fa-key"></i>
												Dapatkan password Anda
											</h4>

											<div class="space-6"></div>
											<p>
												Masukkan email untuk menerima instruksi
											</p>

											<form action="<?= base_url(); ?>recover" method="POST" enctype="multipart/form-data">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="email" name="email" class="form-control" placeholder="Masukkan e-mail" required="true" />
															<i class="ace-icon fa fa-envelope"></i>
														</span>
													</label>

													<div class="clearfix">
														<button type="submit" class="width-35 pull-right btn btn-sm btn-danger">
															<i class="ace-icon fa fa-lightbulb-o"></i>
															<span class="bigger-110">Kirimkan!</span>
														</button>
													</div>
												</fieldset>
											</form>
										</div><!-- /.widget-main -->

										<div class="toolbar center">
											<a href="#" data-target="#login-box" class="back-to-login-link">
												Kembali ke halaman Login
												<i class="ace-icon fa fa-arrow-right"></i>
											</a>
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.forgot-box -->

								
							</div><!-- /.position-relative -->

							<div style="text-align: center; padding: 1em; color: #fff;">
								<?php
									if($this->session->userdata('lupa_pass') != NULL) {
										echo $this->session->userdata('lupa_pass');
									}
									$this->session->set_userdata('lupa_pass', '');
								?>
							</div>

							<?php 
								if($this->session->userdata('pemulihan') != NULL) {
							?>

								<script type="text/javascript">
									alert("<?php echo $this->session->userdata('pemulihan'); ?>");
								</script>

							<?php 
								
								} 
								$this->session->set_userdata('pemulihan', '');
							?>

							<div class="navbar-fixed-top align-right" >
								<br />
								&nbsp;
								<span class="blue">Display: </span><span>&nbsp;</span>
								<a id="btn-login-dark" href="#" style="color: #fff;">Dark</a>
								&nbsp;
								<span class="blue">/</span>
								&nbsp;
								<a id="btn-login-blur" href="#" style="color: #fff;">Blur</a>
								&nbsp;
								<span class="blue">/</span>
								&nbsp;
								<a id="btn-login-light" href="#" style="color: #fff;">Light</a>
								&nbsp; &nbsp; &nbsp;
							</div>
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>

		
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?= base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				$(document).on('click', '.toolbar a[data-target]', function(e) {
					e.preventDefault();
					var target = $(this).data('target');
				$('.widget-box.visible').removeClass('visible');//hide others
				$(target).addClass('visible');//show target
			});
			});
			
			
			
			//you don't need this, just used for changing background
			jQuery(function($) {
				$('#btn-login-dark').on('click', function(e) {
					$('body').attr('class', 'login-layout');
					$('#id-text2').attr('class', 'white');
					$('#id-company-text').attr('class', 'white');
					
					e.preventDefault();
				});
				$('#btn-login-light').on('click', function(e) {
					$('body').attr('class', 'login-layout light-login');
					$('#id-text2').attr('class', 'grey');
					$('#id-company-text').attr('class', 'blue');
					
					e.preventDefault();
				});
				$('#btn-login-blur').on('click', function(e) {
					$('body').attr('class', 'login-layout blur-login');
					$('#id-text2').attr('class', 'white');
					$('#id-company-text').attr('class', 'light-blue');
					
					e.preventDefault();
				});
				
			});
		</script>
	</body>
	</html>
