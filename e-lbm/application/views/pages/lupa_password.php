<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Recover Password - SMART LBM</title>

	<meta name="description" content="User login page" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<link rel="icon" href="<?= base_url(); ?>assets/images/favicon/favicon.png" type="image/gif" >

	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/font-awesome/4.5.0/css/font-awesome.min.css" />

	<!-- text fonts -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/fonts.googleapis.com.css" />

	<!-- ace styles -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace-rtl.min.css" />

</head>

	<body class="login-layout">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1" >
						<div class="login-container" style="padding-top: 6em;">
							<div class="center">
								<h1>
									<i class="ace-icon fa fa-book blue"></i>
									<span class="red">SMART</span>
									<span class="white" id="id-text2">LBM</span>
								</h1>
								<h5 class="white" id="id-company-text">Learning Management System<br>Lembaga Bakti Muslim</h5>
							</div>

							<div class="space-6"></div>

							<div class="position-relative" >
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue  bigger">
												<i class="ace-icon fa fa-key green"></i>
												Masukkan Kode Pemulihan
											</h4>

											<div class="space-6"></div>
											<?php
									if($this->session->userdata('lupa_pass_to') != NULL) {
										echo $this->session->userdata('lupa_pass_to');
										echo '<br>';
									}
								?>

											<form action="<?= base_url(); ?>recover/do_konfirmasi" method="POST" enctype="multipart/form-data">
												<fieldset>
													<input type="hidden" name="email" value="<?php echo $this->session->userdata('email'); ?>" />
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="kode" class="form-control" placeholder="Masukkan kode" required />
															<i class="ace-icon fa fa-key"></i>
														</span>
													</label>

													<center>
													<span style="color: red;">

														<?php
														if($this->session->userdata('cek_kode') != NULL) {?>
															<br>
															<?php echo $this->session->userdata('cek_kode');
														}
														$this->session->set_userdata('cek_kode', '');
														?>

													</span></center>

													<div class="space"></div>
													<center>
													<b>Kami mengirimkan kode Anda ke:</b>
													<?php echo $this->session->userdata('email'); ?>
													</center>
													<div class="space"></div>
													<div class="clearfix">
														

														<button type="submit" class="width-35 pull-left btn btn-sm btn-primary">
															<span class="bigger-110">Lanjutkan</span>
														</button>

														<a href="<?php echo site_url('recover/batal');?>" style="color: #fff;"><button type="button" class="width-35 pull-right btn btn-sm btn-danger">
															<span class="bigger-110">Batal</span></a>
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>
										</div><!-- /.widget-main -->

										<div class="toolbar clearfix" style="background-color: #718093; ">
											<div>
												
												<a href="<?= base_url(); ?>recover/kirim_ulang/?email=<?php echo $this->session->userdata('email'); ?>" data-target="#forgot-box" class="forgot-password-link">
													Belum menerima kode ?
												</a>
											</div>

										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->

								<?php 
									if($this->session->userdata('kirim_ulang') != NULL) {

								?>

								<script type="text/javascript">
									alert("<?php echo $this->session->userdata('kirim_ulang'); ?>");
								</script>

								<?php 
									$this->session->set_userdata('kirim_ulang', '');
									} 
								?>


								
							</div><!-- /.position-relative -->

							<div class="navbar-fixed-top align-right" >
								<br />
								&nbsp;
								<span class="blue">Display: </span><span>&nbsp;</span>
								<a id="btn-login-dark" href="#" style="color: #fff;">Dark</a>
								&nbsp;
								<span class="blue">/</span>
								&nbsp;
								<a id="btn-login-blur" href="#" style="color: #fff;">Blur</a>
								&nbsp;
								<span class="blue">/</span>
								&nbsp;
								<a id="btn-login-light" href="#" style="color: #fff;">Light</a>
								&nbsp; &nbsp; &nbsp;
							</div>
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>

		
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?= base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			
			
			
			
			//you don't need this, just used for changing background
			jQuery(function($) {
			 $('#btn-login-dark').on('click', function(e) {
				$('body').attr('class', 'login-layout');
				$('#id-text2').attr('class', 'white');
				$('#id-company-text').attr('class', 'white');
				
				e.preventDefault();
			 });
			 $('#btn-login-light').on('click', function(e) {
				$('body').attr('class', 'login-layout light-login');
				$('#id-text2').attr('class', 'grey');
				$('#id-company-text').attr('class', 'blue');
				
				e.preventDefault();
			 });
			 $('#btn-login-blur').on('click', function(e) {
				$('body').attr('class', 'login-layout blur-login');
				$('#id-text2').attr('class', 'white');
				$('#id-company-text').attr('class', 'light-blue');
				
				e.preventDefault();
			 });
			 
			});
		</script>
	</body>
</html>
