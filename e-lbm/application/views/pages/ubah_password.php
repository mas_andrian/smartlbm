<!DOCTYPE html>
<html lang="en">
	<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Ubah Password - SMART LBM</title>

	<meta name="description" content="User login page" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<link rel="icon" href="<?= base_url(); ?>assets/images/favicon/favicon.png" type="image/gif" >

	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/font-awesome/4.5.0/css/font-awesome.min.css" />

	<!-- text fonts -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/fonts.googleapis.com.css" />

	<!-- ace styles -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace.min.css" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/ace-rtl.min.css" />

</head>

<body class="login-layout">
	<div class="main-container">
		<div class="main-content">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<div class="login-container" style="padding-top: 6em;">
						<div class="center">
							<h1>
								<i class="ace-icon fa fa-book blue"></i>
								<span class="red">SMART</span>
								<span class="white" id="id-text2">LBM</span>
							</h1>
							<h5 class="white" id="id-company-text">Learning Management System<br>Lembaga Bakti Muslim</h5>
						</div>

							<div class="space-6"></div>

							<div class="position-relative">
								

								<div id="forgot-box" class="forgot-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header red lighter bigger">
												<i class="ace-icon fa fa-lock"></i>
												Ubah Password
											</h4>

											<div class="space-6"></div>
											<p style="text-align: center;">
												Username Anda: <b><?php echo $this->session->userdata('user_name'); ?></b> 
											</p>
											<p>
												Masukkan password baru Anda
											</p>

											<form action="<?= base_url(); ?>recover/do_ubah_password" method="POST" enctype="multipart/form-data">
												<input type="hidden" name="id_pengguna" value="<?php echo $this->session->userdata('id_pengguna'); ?>" />
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" name="password" class="form-control" placeholder="Password baru" id="password" required />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>


													<div class="clearfix">
														<input class="pull-left" type="checkbox" onclick="myFunction()"> <label style="font-size: 12px;"> &nbsp; Lihat Password</label>
														<button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
															<i class="ace-icon fa fa-save"></i>
															<span class="bigger-110">Simpan</span>
														</button>
													</div>
												</fieldset>
											</form>
										</div><!-- /.widget-main -->

										<div class="toolbar center" style="background-color: #2980b9;">
											
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.forgot-box -->

								
							</div><!-- /.position-relative -->


													<script>
														function myFunction() {
															var x = document.getElementById("password");
															if (x.type == "password") {
																x.type = "text";
															} else {
																x.type = "password";
															}
														}
													</script>

							<div class="navbar-fixed-top align-right" >
								<br />
								&nbsp;
								<span class="blue">Display: </span><span>&nbsp;</span>
								<a id="btn-login-dark" href="#" style="color: #fff;">Dark</a>
								&nbsp;
								<span class="blue">/</span>
								&nbsp;
								<a id="btn-login-blur" href="#" style="color: #fff;">Blur</a>
								&nbsp;
								<span class="blue">/</span>
								&nbsp;
								<a id="btn-login-light" href="#" style="color: #fff;">Light</a>
								&nbsp; &nbsp; &nbsp;
							</div>
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<script src="<?= base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>

		
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?= base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 $(document).on('click', '.toolbar a[data-target]', function(e) {
				e.preventDefault();
				var target = $(this).data('target');
				$('.widget-box.visible').removeClass('visible');//hide others
				$(target).addClass('visible');//show target
			 });
			});
			
			
			
			//you don't need this, just used for changing background
			jQuery(function($) {
			 $('#btn-login-dark').on('click', function(e) {
				$('body').attr('class', 'login-layout');
				$('#id-text2').attr('class', 'white');
				$('#id-company-text').attr('class', 'white');
				
				e.preventDefault();
			 });
			 $('#btn-login-light').on('click', function(e) {
				$('body').attr('class', 'login-layout light-login');
				$('#id-text2').attr('class', 'grey');
				$('#id-company-text').attr('class', 'blue');
				
				e.preventDefault();
			 });
			 $('#btn-login-blur').on('click', function(e) {
				$('body').attr('class', 'login-layout blur-login');
				$('#id-text2').attr('class', 'white');
				$('#id-company-text').attr('class', 'light-blue');
				
				e.preventDefault();
			 });
			 
			});
		</script>
	</body>
</html>
