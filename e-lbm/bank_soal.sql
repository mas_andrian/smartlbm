/*
 Navicat Premium Data Transfer

 Source Server         : LOCALHOST
 Source Server Type    : MySQL
 Source Server Version : 100130
 Source Host           : localhost:3306
 Source Schema         : smartlbm

 Target Server Type    : MySQL
 Target Server Version : 100130
 File Encoding         : 65001

 Date: 03/11/2020 19:02:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for materi
-- ----------------------------
DROP TABLE IF EXISTS `materi`;
CREATE TABLE `materi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sekolah` int(11) NULL DEFAULT NULL,
  `id_mata_pelajaran` int(11) NULL DEFAULT NULL,
  `tingkatan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kelas` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `guru_penanggung_jawab` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_aktif` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of materi
-- ----------------------------
INSERT INTO `materi` VALUES (1, 4, 10, 'IV/4 (Empat)', '1', '3', 'Y', NULL);
INSERT INTO `materi` VALUES (2, 4, 12, 'I/1 (Satu)', 'A', '6', 'N', NULL);
INSERT INTO `materi` VALUES (4, 4, 14, 'III/3 (Tiga)', 'A', '3,6', 'Y', NULL);
INSERT INTO `materi` VALUES (5, 4, NULL, NULL, '', NULL, 'Y', NULL);

-- ----------------------------
-- Table structure for materi_bab
-- ----------------------------
DROP TABLE IF EXISTS `materi_bab`;
CREATE TABLE `materi_bab`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_materi` int(11) NULL DEFAULT NULL,
  `urutan` int(11) NULL DEFAULT NULL,
  `judul` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `slug` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_aktif` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of materi_bab
-- ----------------------------
INSERT INTO `materi_bab` VALUES (1, 4, 1, 'Pengenalan IPA Dasar', '<p>halo semuanya gensz</p>\r\n', 'bab_pengenalan-ipa-dasar', 'Y');
INSERT INTO `materi_bab` VALUES (2, 4, 2, 'Sendi', '<p>pergerakan sendi</p>\r\n', 'bab_Sendi', NULL);
INSERT INTO `materi_bab` VALUES (3, 4, 3, 'Darah', '<p><u><strong>Darah Besar</strong></u></p>\r\n\r\n<p>ketika ada mengalir</p>\r\n', 'bab_darah', NULL);
INSERT INTO `materi_bab` VALUES (4, 4, 4, 'e', '<p>e</p>\r\n', 'bab_e', 'Y');

-- ----------------------------
-- Table structure for materi_sub_bab
-- ----------------------------
DROP TABLE IF EXISTS `materi_sub_bab`;
CREATE TABLE `materi_sub_bab`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_bab` int(11) NULL DEFAULT NULL,
  `urutan` int(11) NULL DEFAULT NULL,
  `judul` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `file_pdf` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_video` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_aktif` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of materi_sub_bab
-- ----------------------------
INSERT INTO `materi_sub_bab` VALUES (1, 1, 1, 'Ilmu Pengetahuan Alam Dasar', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', './assets/file_materi/Important1.pdf', 'https://www.youtube.com/watch?v=dBFp0Ext0y8&list=RDdBFp0Ext0y8&start_radio=1', 'sub_bab_ilmu-pengetahuan-alam-dasar', 'N');
INSERT INTO `materi_sub_bab` VALUES (2, 1, 2, 'Materi Dasar', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', './assets/file_materi/Layanan_Pengaduan.pdf', 'https://www.youtube.com/watch?v=E5nKgJgq_GE', 'sub_bab_materi-dasar', NULL);

SET FOREIGN_KEY_CHECKS = 1;
